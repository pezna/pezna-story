#include "instruction.hpp"

#include <algorithm> // std::random_shuffle
#include <numeric> // std::iota
#include <utility>

#include "typedefs.hpp"
#include "utils.hpp"

namespace PSVM
{

void equalsOp(Frame& frame, ErrorHandler& errorHandler)
{
	SharedPtr<StoryObject> right = frame.popStack();
	SharedPtr<StoryObject> left = frame.popStack();
	SharedPtr<StoryObject> result = left->equals(*right, errorHandler);
	frame.pushStack(result);
}

void notEqualsOp(Frame& frame, ErrorHandler& errorHandler)
{
	SharedPtr<StoryObject> right = frame.popStack();
	SharedPtr<StoryObject> left = frame.popStack();
	SharedPtr<StoryObject> result = left->notEquals(*right, errorHandler);
	frame.pushStack(result);
}

void greaterThanEqualsOp(Frame& frame, ErrorHandler& errorHandler)
{
	SharedPtr<StoryObject> right = frame.popStack();
	SharedPtr<StoryObject> left = frame.popStack();
	SharedPtr<StoryObject> result = left->greaterThanOrEquals(*right, errorHandler);
	frame.pushStack(result);
}

void lessThanEqualsOp(Frame& frame, ErrorHandler& errorHandler)
{
	SharedPtr<StoryObject> right = frame.popStack();
	SharedPtr<StoryObject> left = frame.popStack();
	SharedPtr<StoryObject> result = left->lessThanOrEquals(*right, errorHandler);
	frame.pushStack(result);
}

void greaterThanOp(Frame& frame, ErrorHandler& errorHandler)
{
	SharedPtr<StoryObject> right = frame.popStack();
	SharedPtr<StoryObject> left = frame.popStack();
	SharedPtr<StoryObject> result = left->greaterThan(*right, errorHandler);
	frame.pushStack(result);
}

void lessThanOp(Frame& frame, ErrorHandler& errorHandler)
{
	SharedPtr<StoryObject> right = frame.popStack();
	SharedPtr<StoryObject> left = frame.popStack();
	SharedPtr<StoryObject> result = left->lessThan(*right, errorHandler);
	frame.pushStack(result);
}

void notOp(Frame& frame, ErrorHandler& errorHandler)
{
	SharedPtr<StoryObject> val = frame.popStack();
	SharedPtr<StoryObject> result = val->boolNot(errorHandler);
	frame.pushStack(result);
}

bool performIn(Frame& frame, ErrorHandler& errorHandler)
{
	SharedPtr<StoryObject> right = frame.popStack();
	SharedPtr<StoryObject> left = frame.popStack();
	bool in = false;
	
	if (right->type == ObjectType::List)
	{
		auto list = static_cast<ListObject*>(right.get());
		for (auto& item : list->value)
		{
			if (item == left)
			{
				in = true;
				break;
			}
		}
	}
	else if (right->type == ObjectType::Dict)
	{
		OutStringStream oss;
		left->toCodeString(oss);
		auto leftString = oss.str();
		auto dict = static_cast<DictObject*>(right.get());
		for (auto& pair : dict->value)
		{
			if (pair.first == leftString)
			{
				in = true;
				break;
			}
		}
	}
	else if (right->type == ObjectType::Character)
	{
		OutStringStream oss;
		left->toCodeString(oss);
		auto leftString = oss.str();
		auto character = static_cast<CharacterObject*>(right.get());
		for (auto& pair : character->value)
		{
			if (pair.first == leftString)
			{
				in = true;
				break;
			}
		}
	}
	else
	{
		String msg = "The right-hand side of In requires a list, dict, or character, not " + StoryObject::getObjectTypeName(right->type);
		errorHandler.handleError(msg);
	}
	
	return in;
}

void inOp(Frame& frame, ErrorHandler& errorHandler)
{
	frame.pushStack(make_shared<BooleanObject>(performIn(frame, errorHandler)));
}

void notInOp(Frame& frame, ErrorHandler& errorHandler)
{
	frame.pushStack(make_shared<BooleanObject>(!performIn(frame, errorHandler)));
}

bool performIs(Frame& frame, ErrorHandler& errorHandler, BytecodeSource& source)
{
	SharedPtr<StoryObject> val = frame.popStack();
	int64_t typeNum = Utils::readVarInt(source);
	ObjectType type = static_cast<ObjectType>(typeNum);
	
	return val->type == type;
}

void isOp(Frame& frame, ErrorHandler& errorHandler, BytecodeSource& source)
{
	frame.pushStack(make_shared<BooleanObject>(performIs(frame, errorHandler, source)));
}

void isNotOp(Frame& frame, ErrorHandler& errorHandler, BytecodeSource& source)
{
	frame.pushStack(make_shared<BooleanObject>(!performIs(frame, errorHandler, source)));
}

void orOp(Frame& frame, ErrorHandler& errorHandler)
{
	SharedPtr<StoryObject> right = frame.popStack();
	SharedPtr<StoryObject> left = frame.popStack();
	SharedPtr<StoryObject> result = left->boolOr(*right, errorHandler);
	frame.pushStack(result);
}

void powerOp(Frame& frame, ErrorHandler& errorHandler)
{
	SharedPtr<StoryObject> right = frame.popStack();
	SharedPtr<StoryObject> left = frame.popStack();
	SharedPtr<StoryObject> result = left->power(*right, errorHandler);
	frame.pushStack(result);
}

void additionOp(Frame& frame, ErrorHandler& errorHandler)
{
	SharedPtr<StoryObject> right = frame.popStack();
	SharedPtr<StoryObject> left = frame.popStack();
	SharedPtr<StoryObject> result = left->add(*right, errorHandler);
	frame.pushStack(result);
}

void subtractOp(Frame& frame, ErrorHandler& errorHandler)
{
	SharedPtr<StoryObject> right = frame.popStack();
	SharedPtr<StoryObject> left = frame.popStack();
	SharedPtr<StoryObject> result = left->subtract(*right, errorHandler);
	frame.pushStack(result);
}

void negateOp(Frame& frame, ErrorHandler& errorHandler)
{
	SharedPtr<StoryObject> val = frame.popStack();
	SharedPtr<StoryObject> result = val->negate(errorHandler);
	frame.pushStack(result);
}

void multiplyOp(Frame& frame, ErrorHandler& errorHandler)
{
	SharedPtr<StoryObject> right = frame.popStack();
	SharedPtr<StoryObject> left = frame.popStack();
	SharedPtr<StoryObject> result = left->multiply(*right, errorHandler);
	frame.pushStack(result);
}

void divideOp(Frame& frame, ErrorHandler& errorHandler)
{
	SharedPtr<StoryObject> right = frame.popStack();
	SharedPtr<StoryObject> left = frame.popStack();
	SharedPtr<StoryObject> result = left->divide(*right, errorHandler);
	frame.pushStack(result);
}

void modulusOp(Frame& frame, ErrorHandler& errorHandler)
{
	SharedPtr<StoryObject> right = frame.popStack();
	SharedPtr<StoryObject> left = frame.popStack();
	SharedPtr<StoryObject> result = left->modulus(*right, errorHandler);
	frame.pushStack(result);
}

void bitAndOp(Frame& frame, ErrorHandler& errorHandler)
{
	SharedPtr<StoryObject> right = frame.popStack();
	SharedPtr<StoryObject> left = frame.popStack();
	SharedPtr<StoryObject> result = left->bitAnd(*right, errorHandler);
	frame.pushStack(result);
}

void bitXorOp(Frame& frame, ErrorHandler& errorHandler)
{
	SharedPtr<StoryObject> right = frame.popStack();
	SharedPtr<StoryObject> left = frame.popStack();
	SharedPtr<StoryObject> result = left->bitXor(*right, errorHandler);
	frame.pushStack(result);
}

void bitOrOp(Frame& frame, ErrorHandler& errorHandler)
{
	SharedPtr<StoryObject> right = frame.popStack();
	SharedPtr<StoryObject> left = frame.popStack();
	SharedPtr<StoryObject> result = left->bitOr(*right, errorHandler);
	frame.pushStack(result);
}

void bitNotOp(Frame& frame, ErrorHandler& errorHandler)
{
	SharedPtr<StoryObject> val = frame.popStack();
	SharedPtr<StoryObject> result = val->bitNot(errorHandler);
	frame.pushStack(result);
}

void bitLeftShiftOp(Frame& frame, ErrorHandler& errorHandler)
{
	SharedPtr<StoryObject> right = frame.popStack();
	SharedPtr<StoryObject> left = frame.popStack();
	SharedPtr<StoryObject> result = left->leftShift(*right, errorHandler);
	frame.pushStack(result);
}

void bitRightShiftOp(Frame& frame, ErrorHandler& errorHandler)
{
	SharedPtr<StoryObject> right = frame.popStack();
	SharedPtr<StoryObject> left = frame.popStack();
	SharedPtr<StoryObject> result = left->rightShift(*right, errorHandler);
	frame.pushStack(result);
}

void intConstOp(Frame& frame, BytecodeSource& source)
{
	int64_t num = Utils::readVarInt(source);
	frame.pushStack(make_shared<IntegerObject>(num));
}

void floatConstOp(Frame& frame, BytecodeSource& source)
{
	int64_t wholeNum = Utils::readVarInt(source);
	int64_t decimalNum = Utils::readVarInt(source);
	double d = wholeNum + Utils::numberToDecimal(decimalNum);
	frame.pushStack(make_shared<FloatObject>(d));
}

void boolConstOp(Frame& frame, BytecodeSource& source)
{
	bool b = source.nextByte();
	frame.pushStack(make_shared<BooleanObject>(b));
}

void stringConstOp(Frame& frame, BytecodeSource& source, StoryMetadata& metadata)
{
	int64_t stringId = Utils::readVarInt(source);
	String& s = metadata.strings.at(stringId);
	frame.pushStack(make_shared<StringObject>(s));
}

void buildListOp(Frame& frame, BytecodeSource& source)
{
	int64_t numItems = Utils::readVarInt(source);
	Vector<SharedPtr<StoryObject>> value(numItems);
	
	for (auto i = numItems - 1; i >= 0; --i)
	{
		value[i] = frame.popStack();
	}
	
	frame.pushStack(make_shared<ListObject>(value));
}

void buildListWithDefaultOp(Frame& frame, BytecodeSource& source)
{
	int64_t numItems = Utils::readVarInt(source);
	Vector<SharedPtr<StoryObject>> value(numItems);
	SharedPtr<StoryObject> defaultValue = frame.popStack();
	
	for (auto i = numItems - 1; i >= 0; --i)
	{
		value[i] = frame.popStack();
	}
	
	frame.pushStack(make_shared<ListObject>(value, defaultValue));
}

void buildDictOp(Frame& frame, BytecodeSource& source)
{
	int64_t numPairs = Utils::readVarInt(source);
	Map<String, SharedPtr<StoryObject>> map;
	
	OutStringStream oss;
	for (auto i = numPairs - 1; i >= 0; --i)
	{
		auto value = frame.popStack();
		auto key = frame.popStack();
		
		key->toRepresentationString(oss);
		map[oss.str()] = value;
		
		oss.clear();
		oss.str("");
	}
	
	frame.pushStack(make_shared<DictObject>(map));
}

void buildDictWithDefaultOp(Frame& frame, BytecodeSource& source)
{
	int64_t numPairs = Utils::readVarInt(source);
	Map<String, SharedPtr<StoryObject>> map;
	SharedPtr<StoryObject> defaultValue = frame.popStack();
	
	OutStringStream oss;
	for (auto i = numPairs - 1; i >= 0; --i)
	{
		auto value = frame.popStack();
		auto key = frame.popStack();
		
		key->toRepresentationString(oss);
		map[oss.str()] = value;
		
		oss.clear();
		oss.str("");
	}
	
	frame.pushStack(make_shared<DictObject>(map, defaultValue));
}

void buildCharacterOp(Frame& frame, BytecodeSource& source, StoryMetadata& metadata)
{
	int64_t numPairs = Utils::readVarInt(source);
	Vector<int64_t> propertyNameIds(numPairs);
	for (int i = 0; i < numPairs; ++i)
	{
		propertyNameIds[i] = Utils::readVarInt(source);
	}
	
	Map<String, SharedPtr<StoryObject>> map;
	
	for (auto i = numPairs - 1; i >= 0; --i)
	{
		auto value = frame.popStack();
		auto nameId = propertyNameIds.at(i);
		auto& nameText = metadata.names.at(nameId);
		
		map[nameText] = value;
	}
	
	frame.pushStack(make_shared<CharacterObject>(map));
}

void getSequenceIndexOp(StoryContext& context, Frame& frame, BytecodeSource& source)
{
	bool loop = source.nextByte();
	bool random = source.nextByte();
	bool order = source.nextByte();
	auto numItems = Utils::readVarInt(source);
	
	SequenceContext& sequenceContext = context.sequenceContexts[frame.container->id];
	auto& indexOrder = sequenceContext.indexOrder;
	
	if (sequenceContext.numVisits == 0)
	{
		// initial the sequence context
		indexOrder.resize(numItems);
		std::iota(indexOrder.begin(), indexOrder.end(), 0);
		
		if (random)
		{
			std::random_shuffle(indexOrder.begin(), indexOrder.end());
		}
	}
	
	int64_t indexIntoOrder = sequenceContext.numVisits++;
	if (random && !order)
	{
		indexIntoOrder += Utils::randomInt(numItems);
	}
	
	if (!random && !loop)
	{
		indexIntoOrder = __min(indexIntoOrder, numItems - 1);
	}
	
	auto nextIndex = indexOrder.at(indexIntoOrder % numItems);
	
	frame.pushStack(make_shared<IntegerObject>(nextIndex));
}

}