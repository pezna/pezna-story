#ifndef PSVM_BYTECODE_PRINTER_H
#define PSVM_BYTECODE_PRINTER_H

#include "bytecode_source.hpp"
#include "typedefs.hpp"

namespace PSVM::BytecodePrinter
{

void print(BytecodeSource& source, OutStream& oss);
void printOffset(BytecodeSource& source, OutStream& oss);
void noop(OutStream& oss);
void equals(OutStream& oss);
void notEquals(OutStream& oss);
void greaterThanEquals(OutStream& oss);
void greaterThan(OutStream& oss);
void lessThanEquals(OutStream& oss);
void lessThan(OutStream& oss);
void nott(OutStream& oss);
void in(OutStream& oss);
void notIn(OutStream& oss);
void is(BytecodeSource& source, OutStream& oss);
void isNot(BytecodeSource& source, OutStream& oss);
void orr(OutStream& oss);
void power(OutStream& oss);
void addition(OutStream& oss);
void subtract(OutStream& oss);
void negate(OutStream& oss);
void multiply(OutStream& oss);
void divide(OutStream& oss);
void modulus(OutStream& oss);
void bitAnd(OutStream& oss);
void bitXor(OutStream& oss);
void bitOr(OutStream& oss);
void bitNot(OutStream& oss);
void bitLeftShift(OutStream& oss);
void bitRightShift(OutStream& oss);
void intConst(BytecodeSource& source, OutStream& oss);
void floatConst(BytecodeSource& source, OutStream& oss);
void boolConst(BytecodeSource& source, OutStream& oss);
void stringConst(BytecodeSource& source, OutStream& oss);
void buildList(BytecodeSource& source, OutStream& oss);
void buildListWithDefault(BytecodeSource& source, OutStream& oss);
void buildDictionary(BytecodeSource& source, OutStream& oss);
void buildDictionaryWithDefault(BytecodeSource& source, OutStream& oss);
void buildCharacter(BytecodeSource& source, OutStream& oss);
void getSequenceIndex(BytecodeSource& source, OutStream& oss);
void defaultConst(OutStream& oss);
void line(OutStream& oss);
void choices(BytecodeSource& source, OutStream& oss);
void gotoo(BytecodeSource& source, OutStream& oss);
void jumpForward(BytecodeSource& source, OutStream& oss);
void popJumpIfTrue(BytecodeSource& source, OutStream& oss);
void popJumpIfFalse(BytecodeSource& source, OutStream& oss);
void jumpIfTrueOrPop(BytecodeSource& source, OutStream& oss);
void jumpIfFalseOrPop(BytecodeSource& source, OutStream& oss);
void pop(OutStream& oss);
void duplicate(OutStream& oss);
void createVariable(BytecodeSource& source, OutStream& oss);
void loadVariable(BytecodeSource& source, OutStream& oss);
void store(OutStream& oss);
void getAttribute(BytecodeSource& source, OutStream& oss);
void call(BytecodeSource& source, OutStream& oss);
void slice(BytecodeSource& source, OutStream& oss);
void subscript(OutStream& oss);
void forIterable(BytecodeSource& source, OutStream& oss);
void event(OutStream& oss);
void returnn(OutStream& oss);
void returnValue(OutStream& oss);
void terminate(OutStream& oss);
void beginMetadata(OutStream& oss);
void endMetadata(OutStream& oss);
void beginNames(BytecodeSource& source, OutStream& oss);
void endNames(OutStream& oss);
void beginStrings(BytecodeSource& source, OutStream& oss);
void endStrings(OutStream& oss);
void beginContainers(BytecodeSource& source, OutStream& oss);
void endContainers(OutStream& oss);
void beginBlock(BytecodeSource& source, OutStream& oss);
void endBlock(OutStream& oss);

}

#endif // PSVM_BYTECODE_PRINTER_H