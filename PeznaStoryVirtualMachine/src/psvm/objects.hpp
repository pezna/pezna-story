#ifndef PSVM_RUNTIME_DATA_H
#define PSVM_RUNTIME_DATA_H

#include <utility>
#include <sstream>

#include "metadata.hpp"
#include "error_handler.hpp"
#include "typedefs.hpp"
#include "utils.hpp"

namespace PSVM
{

enum class ObjectType
{
	Unknown,
	Boolean = 1 << 0,
	Integer = 1 << 1,
	Float = 1 << 2,
	String = 1 << 3,
	List = 1 << 4,
	Dict = 1 << 5,
	Character = 1 << 6,
};

struct StoryObject
{
	ObjectType type;
	bool isFinal;
	
	StoryObject()
	:
		type(ObjectType::Unknown),
		isFinal(false)
	{}
	
	StoryObject(ObjectType type)
	:
		type(type),
		isFinal(false)
	{}
	
	StoryObject(const StoryObject& other) = default;
	StoryObject(StoryObject&& other) = default;
	StoryObject& operator=(const StoryObject& other) = default;
	
	~StoryObject() {}
	
	static String getObjectTypeName(ObjectType type);
	
	virtual bool operator==(const StoryObject& other) const;
	virtual bool operator!=(const StoryObject& other) const;
	
	virtual SharedPtr<StoryObject> equals(const StoryObject& other, ErrorHandler& errorHandler);
	virtual SharedPtr<StoryObject> notEquals(const StoryObject& other, ErrorHandler& errorHandler);
	virtual SharedPtr<StoryObject> greaterThanOrEquals(const StoryObject& other, ErrorHandler& errorHandler);
	virtual SharedPtr<StoryObject> greaterThan(const StoryObject& other, ErrorHandler& errorHandler);
	virtual SharedPtr<StoryObject> lessThanOrEquals(const StoryObject& other, ErrorHandler& errorHandler);
	virtual SharedPtr<StoryObject> lessThan(const StoryObject& other, ErrorHandler& errorHandler);
	virtual SharedPtr<StoryObject> boolNot(ErrorHandler& errorHandler);
	virtual SharedPtr<StoryObject> boolOr(const StoryObject& other, ErrorHandler& errorHandler);
	virtual SharedPtr<StoryObject> add(const StoryObject& other, ErrorHandler& errorHandler);
	virtual SharedPtr<StoryObject> subtract(const StoryObject& other, ErrorHandler& errorHandler);
	virtual SharedPtr<StoryObject> negate(ErrorHandler& errorHandler);
	virtual SharedPtr<StoryObject> multiply(const StoryObject& other, ErrorHandler& errorHandler);
	virtual SharedPtr<StoryObject> divide(const StoryObject& other, ErrorHandler& errorHandler);
	virtual SharedPtr<StoryObject> modulus(const StoryObject& other, ErrorHandler& errorHandler);
	virtual SharedPtr<StoryObject> power(const StoryObject& other, ErrorHandler& errorHandler);
	virtual SharedPtr<StoryObject> bitAnd(const StoryObject& other, ErrorHandler& errorHandler);
	virtual SharedPtr<StoryObject> bitOr(const StoryObject& other, ErrorHandler& errorHandler);
	virtual SharedPtr<StoryObject> bitXor(const StoryObject& other, ErrorHandler& errorHandler);
	virtual SharedPtr<StoryObject> bitNot(ErrorHandler& errorHandler);
	virtual SharedPtr<StoryObject> leftShift(const StoryObject& other, ErrorHandler& errorHandler);
	virtual SharedPtr<StoryObject> rightShift(const StoryObject& other, ErrorHandler& errorHandler);
	
	virtual void toRepresentationString(OutStringStream& oss) const;
	virtual void toCodeString(OutStringStream& oss) const;
	virtual bool toBoolean() const;
};

struct BooleanObject : StoryObject
{
	bool value;
	
	BooleanObject(bool value)
	:
		StoryObject(ObjectType::Boolean),
		value(value)
	{}
	
	BooleanObject(const BooleanObject& other) = default;
	BooleanObject(BooleanObject&& other) = default;
	BooleanObject& operator=(const BooleanObject& other) = default;
	
	bool operator==(const StoryObject& other) const override;
	
	SharedPtr<StoryObject> equals(const StoryObject& other, ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> notEquals(const StoryObject& other, ErrorHandler& errorHandler) override;
	
	void toRepresentationString(OutStringStream& oss) const override
	{
		oss << "Boolean(" << value << ")";
	}
	
	void toCodeString(OutStringStream& oss) const override
	{
		oss << value;
	}
	
	bool toBoolean() const override
	{
		return value;
	}
};

struct IntegerObject : StoryObject
{
	int64_t value;
	
	IntegerObject(int64_t value)
	:
		StoryObject(ObjectType::Integer),
		value(value)
	{}
	
	IntegerObject(const IntegerObject& other) = default;
	IntegerObject(IntegerObject&& other) = default;
	IntegerObject& operator=(const IntegerObject& other) = default;
	
	bool operator==(const StoryObject& other) const override;
	
	SharedPtr<StoryObject> equals(const StoryObject& other, ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> notEquals(const StoryObject& other, ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> greaterThanOrEquals(const StoryObject& other, ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> greaterThan(const StoryObject& other, ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> lessThanOrEquals(const StoryObject& other, ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> lessThan(const StoryObject& other, ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> add(const StoryObject& other, ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> subtract(const StoryObject& other, ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> negate(ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> multiply(const StoryObject& other, ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> divide(const StoryObject& other, ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> modulus(const StoryObject& other, ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> power(const StoryObject& other, ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> bitAnd(const StoryObject& other, ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> bitOr(const StoryObject& other, ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> bitXor(const StoryObject& other, ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> bitNot(ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> leftShift(const StoryObject& other, ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> rightShift(const StoryObject& other, ErrorHandler& errorHandler) override;
	
	void toRepresentationString(OutStringStream& oss) const override
	{
		oss << "Integer(" << value << ")";
	}
	
	void toCodeString(OutStringStream& oss) const override
	{
		oss << value;
	}
	
	bool toBoolean() const override
	{
		return value != 0;
	}
};

struct FloatObject : StoryObject
{
	double value;
	
	FloatObject(double value)
	:
		StoryObject(ObjectType::Float),
		value(value)
	{}
	
	FloatObject(const FloatObject& other) = default;
	FloatObject(FloatObject&& other) = default;
	FloatObject& operator=(const FloatObject& other) = default;
	
	bool operator==(const StoryObject& other) const override;
	
	SharedPtr<StoryObject> equals(const StoryObject& other, ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> notEquals(const StoryObject& other, ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> greaterThanOrEquals(const StoryObject& other, ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> greaterThan(const StoryObject& other, ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> lessThanOrEquals(const StoryObject& other, ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> lessThan(const StoryObject& other, ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> add(const StoryObject& other, ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> subtract(const StoryObject& other, ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> negate(ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> multiply(const StoryObject& other, ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> divide(const StoryObject& other, ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> modulus(const StoryObject& other, ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> power(const StoryObject& other, ErrorHandler& errorHandler) override;
	
	void toRepresentationString(OutStringStream& oss) const override
	{
		oss << "Float(" << value << ")";
	}
	
	void toCodeString(OutStringStream& oss) const override
	{
		oss << value;
	}
	
	bool toBoolean() const override
	{
		return value != 0;
	}
};

struct StringObject : StoryObject
{
	String value;
	
	StringObject(String& value)
	:
		StoryObject(ObjectType::String),
		value(value)
	{}
	
	StringObject(String&& value)
	:
		StoryObject(ObjectType::String),
		value(std::move(value))
	{}
	
	StringObject(const StringObject& other) = default;
	StringObject(StringObject&& other) = default;
	StringObject& operator=(const StringObject& other) = default;
	
	bool operator==(const StoryObject& other) const override;
	
	SharedPtr<StoryObject> equals(const StoryObject& other, ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> notEquals(const StoryObject& other, ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> add(const StoryObject& other, ErrorHandler& errorHandler) override;
	
	void toRepresentationString(OutStringStream& oss) const override
	{
		oss << "String(";
		Utils::writeCodeString(value, oss);
		oss << ")";
	}
	
	void toCodeString(OutStringStream& oss) const override
	{
		Utils::writeCodeString(value, oss);
	}
	
	bool toBoolean() const override
	{
		return value.length() != 0;
	}
};

struct ListObject : StoryObject
{
	Vector<SharedPtr<StoryObject>> value;
	SharedPtr<StoryObject> defaultValue;
	
	ListObject()
	:
		StoryObject(ObjectType::List),
		defaultValue(nullptr)
	{}
	
	ListObject(Vector<SharedPtr<StoryObject>>& value)
	:
		value(value),
		defaultValue(nullptr)
	{}
	
	ListObject(Vector<SharedPtr<StoryObject>>& value, SharedPtr<StoryObject> defaultValue)
	:
		value(value),
		defaultValue(defaultValue)
	{}
	
	ListObject(const ListObject& other) = default;
	ListObject(ListObject&& other) = default;
	ListObject& operator=(const ListObject& other) = default;
	
	bool operator==(const StoryObject& other) const override;
	
	SharedPtr<StoryObject> equals(const StoryObject& other, ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> notEquals(const StoryObject& other, ErrorHandler& errorHandler) override;
	
	void toRepresentationString(OutStringStream& oss) const override
	{
		oss << "List(";
		toCodeString(oss);
		oss << ")";
	}
	
	void toCodeString(OutStringStream& oss) const override
	{
		std::size_t i = 0;
		for (auto& item : value)
		{
			item->toCodeString(oss);
			if (i++ < value.size() - 1)
			{
				oss << ", ";
			}
		}
	}
	
	bool toBoolean() const override
	{
		return value.size() != 0;
	}
};

struct DictObject : StoryObject
{
	Map<String, SharedPtr<StoryObject>> value;
	SharedPtr<StoryObject> defaultValue;
	
	DictObject()
	:
		StoryObject(ObjectType::Dict),
		defaultValue(nullptr)
	{}
	
	DictObject(Map<String, SharedPtr<StoryObject>>& value)
	:
		value(value),
		defaultValue(nullptr)
	{}
	
	DictObject(Map<String, SharedPtr<StoryObject>>& value, SharedPtr<StoryObject> defaultValue)
	:
		value(value),
		defaultValue(defaultValue)
	{}
	
	DictObject(const DictObject& other) = default;
	DictObject(DictObject&& other) = default;
	DictObject& operator=(const DictObject& other) = default;
	
	bool operator==(const StoryObject& other) const override;
	
	SharedPtr<StoryObject> equals(const StoryObject& other, ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> notEquals(const StoryObject& other, ErrorHandler& errorHandler) override;
	
	void toRepresentationString(OutStringStream& oss) const override
	{
		oss << "Dict(";
		toCodeString(oss);
		oss << ")";
	}
	
	void toCodeString(OutStringStream& oss) const override
	{
		std::size_t i = 0;
		for (auto& entry : value)
		{
			oss << entry.first << ": ";
			entry.second->toCodeString(oss);
			if (i++ < value.size() - 1)
			{
				oss << ", ";
			}
		}
	}
	
	bool toBoolean() const override
	{
		return value.size() != 0;
	}
};

struct CharacterObject : StoryObject
{
	Map<String, SharedPtr<StoryObject>> value;
	
	CharacterObject()
	:
		StoryObject(ObjectType::Character)
	{}
	
	CharacterObject(Map<String, SharedPtr<StoryObject>>& value)
	:
		value(value)
	{}
	
	CharacterObject(const CharacterObject& other) = default;
	CharacterObject(CharacterObject&& other) = default;
	CharacterObject& operator=(const CharacterObject& other) = default;
	
	bool operator==(const StoryObject& other) const override;
	
	SharedPtr<StoryObject> equals(const StoryObject& other, ErrorHandler& errorHandler) override;
	SharedPtr<StoryObject> notEquals(const StoryObject& other, ErrorHandler& errorHandler) override;
	
	void toRepresentationString(OutStringStream& oss) const override
	{
		oss << "Character(";
		toCodeString(oss);
		oss << ")";
	}
	
	void toCodeString(OutStringStream& oss) const override
	{
		std::size_t i = 0;
		for (auto& entry : value)
		{
			oss << entry.first << " = ";
			entry.second->toCodeString(oss);
			if (i++ < value.size() - 1)
			{
				oss << ", ";
			}
		}
	}
	
	bool toBoolean() const override
	{
		return value.size() != 0;
	}
};

struct Variable
{
	VariableMetadata* metadata;
	SharedPtr<StoryObject> value;
	
	Variable(VariableMetadata* metadata, SharedPtr<StoryObject> value)
	:
		metadata(metadata),
		value(value)
	{}
	
	Variable(const Variable& other) = default;
	Variable(Variable&& other) = default;
	Variable& operator=(const Variable& other) = default;
};

}

#endif // PSVM_RUNTIME_DATA_H