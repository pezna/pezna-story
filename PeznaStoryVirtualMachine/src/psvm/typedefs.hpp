#ifndef PSVM_TYPEDEFS_H
#define PSVM_TYPEDEFS_H

#include <cassert>
#include <string>
#include <sstream>
#include <vector>
#include <unordered_map>
#include <memory>

namespace PSVM
{

#define ASSERT(x) assert(x)
#define UNEXPECTED_ERROR(msg) ASSERT((msg, false))

using String = std::string;

template<typename T>
using Vector = std::vector<T>;

template<typename K, typename V>
using Map = std::unordered_map<K, V>;

using OutStringStream = std::ostringstream;
using OutStream = std::ostream;

template<typename T>
using SharedPtr = std::shared_ptr<T>;

using std::make_shared;
}

#endif // PSVM_TYPEDEFS_H