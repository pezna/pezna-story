#ifndef PSVM_METADATA_H
#define PSVM_METADATA_H

#include <cstdint>
#include <vector>

#include "typedefs.hpp"

namespace PSVM
{

struct VariableMetadata
{
	int64_t containerId;
	int64_t nameId;
	bool isFinal;
	bool isParameter;
	
	VariableMetadata()
	:
		containerId(0),
		nameId(0),
		isFinal(false),
		isParameter(false)
	{}
	
	VariableMetadata(const VariableMetadata& other) = default;
	VariableMetadata(VariableMetadata&& other) = default;
	VariableMetadata& operator=(const VariableMetadata& other) = default;
	VariableMetadata& operator=(VariableMetadata&& other) = default;
};

struct LabelMetadata
{
	int64_t containerId;
	int64_t nameId;
	int64_t bytecodeOffset;
	
	LabelMetadata()
	:
		containerId(0),
		nameId(0),
		bytecodeOffset(0)
	{}
	
	LabelMetadata(const LabelMetadata& other) = default;
	LabelMetadata(LabelMetadata&& other) = default;
	LabelMetadata& operator=(const LabelMetadata& other) = default;
	LabelMetadata& operator=(LabelMetadata&& other) = default;
};

struct ContainerMetadata
{
	int64_t id;
	int64_t parentId;
	int64_t nameId;
	bool isNamespace;
	int64_t numParameters;
	int64_t bytecodeOffset;
	int64_t bytecodeLength;
	int64_t numVariables;
	Map<int64_t, VariableMetadata> variables;
	int64_t numLabels;
	Map<int64_t, LabelMetadata> labels;
	
	ContainerMetadata()
	:
		id(0),
		parentId(0),
		nameId(0),
		isNamespace(false),
		numParameters(0),
		bytecodeOffset(0),
		bytecodeLength(0),
		numVariables(0),
		numLabels(0)
	{}
	
	ContainerMetadata(const ContainerMetadata& other) = default;
	ContainerMetadata(ContainerMetadata&& other) = default;
	ContainerMetadata& operator=(const ContainerMetadata& other) = default;
	ContainerMetadata& operator=(ContainerMetadata&& other) = default;
};

struct StoryMetadata
{
	int64_t metadataByteLength; //!< The amount of space taken up by the metadata information
	int64_t numNames; //!< Number of names in the bytecode
	Map<int64_t, String> names; //!< The actual names in the bytecode
	int64_t numStrings; //!< Number of strings in the bytecode
	Map<int64_t, String> strings; //!< The actual strings in the bytecode
	int64_t numContainers;  //!< Number of containers in the bytecode
	Map<int64_t, ContainerMetadata> containers; //!< The namespaces and sections (which contain code) in the bytecode
	Vector<int64_t> namespaceOrder; // The order that namespaces appear in the bytecode; also the order they should be executed in.
	
	StoryMetadata()
	:
		metadataByteLength(0),
		numNames(0),
		numStrings(0),
		numContainers(0)
	{}
	
	StoryMetadata(const StoryMetadata& other) = default;
	StoryMetadata(StoryMetadata&& other) = default;
	StoryMetadata& operator=(const StoryMetadata& other) = default;
	StoryMetadata& operator=(StoryMetadata&& other) = default;
};

}

#endif // PSVM_METADATA_H