#ifndef PSVM_INSTRUCTION_H
#define PSVM_INSTRUCTION_H

#include "bytecode_source.hpp"
#include "context.hpp"
#include "error_handler.hpp"
#include "metadata.hpp"

namespace PSVM
{

enum StoryOpcode
{
	NOOP_OP,
	EQUALS_OP,
	NOT_EQUALS_OP,
	GREATER_THAN_EQUALS_OP,
	LESS_THAN_EQUALS_OP,
	GREATER_THAN_OP,
	LESS_THAN_OP,
	NOT_OP,
	IN_OP,
	NOT_IN_OP,
	IS_OP,
	IS_NOT_OP,
	OR_OP,
	POWER_OP,
	ADDITION_OP,
	SUBTRACT_OP,
	NEGATE_OP,
	MULTIPLY_OP,
	DIVIDE_OP,
	MODULUS_OP,
	BIT_AND_OP,
	BIT_XOR_OP,
	BIT_OR_OP,
	BIT_NOT_OP,
	BIT_LEFT_SHIFT_OP,
	BIT_RIGHT_SHIFT_OP,
	INT_CONST_OP,
	FLOAT_CONST_OP,
	BOOL_CONST_OP,
	STRING_CONST_OP,
	BUILD_LIST_OP,
	BUILD_LIST_WITH_DEFAULT_OP,
	BUILD_DICTIONARY_OP,
	BUILD_DICTIONARY_WITH_DEFAULT_OP,
	BUILD_CHARACTER_OP,
	GET_SEQUENCE_INDEX_OP,
	DEFAULT_CONST_OP,
	LINE_OP,
	CHOICES_OP,
	GOTO_OP,
	JUMP_FORWARD_OP,
	POP_JUMP_IF_TRUE_OP,
	POP_JUMP_IF_FALSE_OP,
	JUMP_IF_TRUE_OR_POP_OP,
	JUMP_IF_FALSE_OR_POP_OP,
	POP_OP,
	DUPLICATE_OP,
	CREATE_VARIABLE_OP,
	LOAD_VARIABLE_OP,
	STORE_OP,
	GET_ATTRIBUTE_OP,
	CALL_OP,
	SLICE_OP,
	SUBSCRIPT_OP,
	FOR_ITERABLE_OP,
	EVENT_OP,
	RETURN_OP,
	RETURN_VALUE_OP,
	TERMINATE_OP,
	BEGIN_METADATA_OP = (signed char)-64, // or C0 (11000000)
	END_METADATA_OP, //-63 or C1
	BEGIN_NAMES_OP, //-62 or C2
	END_NAMES_OP, //-61 or C3
	BEGIN_STRINGS_OP, //-60 or C4
	END_STRINGS_OP, //-59 or C5
	BEGIN_CONTAINERS_OP, //-58 or C6
	END_CONTAINERS_OP, //-57 or C7
	BEGIN_VARIABLES_OP, //-56 or C8
	END_VARIABLES_OP, //-55 or C9
	BEGIN_LABELS_OP, //54 or CA
	END_LABELS_OP, //-53 or CB
	BEGIN_BLOCK_OP, //-52 or CC
	END_BLOCK_OP, //-51 or CD
};

void equalsOp(Frame& frame, ErrorHandler& errorHandler);
void notEqualsOp(Frame& frame, ErrorHandler& errorHandler);
void greaterThanEqualsOp(Frame& frame, ErrorHandler& errorHandler);
void lessThanEqualsOp(Frame& frame, ErrorHandler& errorHandler);
void greaterThanOp(Frame& frame, ErrorHandler& errorHandler);
void lessThanOp(Frame& frame, ErrorHandler& errorHandler);
void notOp(Frame& frame, ErrorHandler& errorHandler);
void inOp(Frame& frame, ErrorHandler& errorHandler);
void notInOp(Frame& frame, ErrorHandler& errorHandler);
void isOp(Frame& frame, ErrorHandler& errorHandler, BytecodeSource& source);
void isNotOp(Frame& frame, ErrorHandler& errorHandler, BytecodeSource& source);
void orOp(Frame& frame, ErrorHandler& errorHandler);
void powerOp(Frame& frame, ErrorHandler& errorHandler);
void additionOp(Frame& frame, ErrorHandler& errorHandler);
void subtractOp(Frame& frame, ErrorHandler& errorHandler);
void negateOp(Frame& frame, ErrorHandler& errorHandler);
void multiplyOp(Frame& frame, ErrorHandler& errorHandler);
void divideOp(Frame& frame, ErrorHandler& errorHandler);
void modulusOp(Frame& frame, ErrorHandler& errorHandler);
void bitAndOp(Frame& frame, ErrorHandler& errorHandler);
void bitXorOp(Frame& frame, ErrorHandler& errorHandler);
void bitOrOp(Frame& frame, ErrorHandler& errorHandler);
void bitNotOp(Frame& frame, ErrorHandler& errorHandler);
void bitLeftShiftOp(Frame& frame, ErrorHandler& errorHandler);
void bitRightShiftOp(Frame& frame, ErrorHandler& errorHandler);
void intConstOp(Frame& frame, BytecodeSource& source);
void floatConstOp(Frame& frame, BytecodeSource& source);
void boolConstOp(Frame& frame, BytecodeSource& source);
void stringConstOp(Frame& frame, BytecodeSource& source, StoryMetadata& metadata);
void buildListOp(Frame& frame, BytecodeSource& source);
void buildListWithDefaultOp(Frame& frame, BytecodeSource& source);
void buildDictOp(Frame& frame, BytecodeSource& source);
void buildDictWithDefaultOp(Frame& frame, BytecodeSource& source);
void buildCharacterOp(Frame& frame, BytecodeSource& source, StoryMetadata& metadata);
void getSequenceIndexOp(StoryContext& context, Frame& frame, BytecodeSource& source);

}

#endif // PSVM_INSTRUCTION_H