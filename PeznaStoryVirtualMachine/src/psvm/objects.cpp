#include "objects.hpp"

#include <algorithm>
#include <cmath>

namespace PSVM
{

/***************
 * StoryObject *
 ***************/

String StoryObject::getObjectTypeName(ObjectType type)
{
	switch (type)
	{
		case ObjectType::Unknown:
			return "unknown";
		case ObjectType::Boolean:
			return "bool";
		case ObjectType::Integer:
			return "int";
		case ObjectType::Float:
			return "float";
		case ObjectType::String:
			return "string";
		case ObjectType::List:
			return "list";
		case ObjectType::Dict:
			return "dict";
		case ObjectType::Character:
			return "character";
	}
	
	ASSERT(("No object type name defined for ", type, false));
	return "";
}

bool StoryObject::operator==(const StoryObject& other) const
{
	UNEXPECTED_ERROR("StoryObject operator == method not overriden");
	return false;
}

bool StoryObject::operator!=(const StoryObject& other) const
{
	return !(*this == other);
}

SharedPtr<StoryObject> StoryObject::equals(const StoryObject& other, ErrorHandler& errorHandler)
{
	UNEXPECTED_ERROR("StoryObject virtual method not overriden - equals");
	return nullptr;
}

SharedPtr<StoryObject> StoryObject::notEquals(const StoryObject& other, ErrorHandler& errorHandler)
{
	UNEXPECTED_ERROR("StoryObject virtual method not overriden - notEquals");
	return nullptr;
}

SharedPtr<StoryObject> StoryObject::greaterThanOrEquals(const StoryObject& other, ErrorHandler& errorHandler)
{
	UNEXPECTED_ERROR("StoryObject virtual method not overriden - greaterThanOrEquals");
	return nullptr;
}

SharedPtr<StoryObject> StoryObject::greaterThan(const StoryObject& other, ErrorHandler& errorHandler)
{
	UNEXPECTED_ERROR("StoryObject virtual method not overriden - greaterThan");
	return nullptr;
}

SharedPtr<StoryObject> StoryObject::lessThanOrEquals(const StoryObject& other, ErrorHandler& errorHandler)
{
	UNEXPECTED_ERROR("StoryObject virtual method not overriden - lessThanOrEquals");
	return nullptr;
}

SharedPtr<StoryObject> StoryObject::lessThan(const StoryObject& other, ErrorHandler& errorHandler)
{
	UNEXPECTED_ERROR("StoryObject virtual method not overriden - lessThan");
	return nullptr;
}

SharedPtr<StoryObject> StoryObject::boolNot(ErrorHandler& errorHandler)
{
	UNEXPECTED_ERROR("StoryObject virtual method not overriden - boolNot");
	return nullptr;
}

SharedPtr<StoryObject> StoryObject::boolOr(const StoryObject& other, ErrorHandler& errorHandler)
{
	UNEXPECTED_ERROR("StoryObject virtual method not overriden - boolOr");
	return nullptr;
}

SharedPtr<StoryObject> StoryObject::add(const StoryObject& other, ErrorHandler& errorHandler)
{
	UNEXPECTED_ERROR("StoryObject virtual method not overriden - add");
	return nullptr;
}

SharedPtr<StoryObject> StoryObject::subtract(const StoryObject& other, ErrorHandler& errorHandler)
{
	UNEXPECTED_ERROR("StoryObject virtual method not overriden - subtract");
	return nullptr;
}

SharedPtr<StoryObject> StoryObject::negate(ErrorHandler& errorHandler)
{
	UNEXPECTED_ERROR("StoryObject virtual method not overriden - negate");
	return nullptr;
}

SharedPtr<StoryObject> StoryObject::multiply(const StoryObject& other, ErrorHandler& errorHandler)
{
	UNEXPECTED_ERROR("StoryObject virtual method not overriden - multiply");
	return nullptr;
}

SharedPtr<StoryObject> StoryObject::divide(const StoryObject& other, ErrorHandler& errorHandler)
{
	UNEXPECTED_ERROR("StoryObject virtual method not overriden - divide");
	return nullptr;
}

SharedPtr<StoryObject> StoryObject::modulus(const StoryObject& other, ErrorHandler& errorHandler)
{
	UNEXPECTED_ERROR("StoryObject virtual method not overriden - modulus");
	return nullptr;
}

SharedPtr<StoryObject> StoryObject::power(const StoryObject& other, ErrorHandler& errorHandler)
{
	UNEXPECTED_ERROR("StoryObject virtual method not overriden - power");
	return nullptr;
}

SharedPtr<StoryObject> StoryObject::bitAnd(const StoryObject& other, ErrorHandler& errorHandler)
{
	UNEXPECTED_ERROR("StoryObject virtual method not overriden - bitAnd");
	return nullptr;
}

SharedPtr<StoryObject> StoryObject::bitOr(const StoryObject& other, ErrorHandler& errorHandler)
{
	UNEXPECTED_ERROR("StoryObject virtual method not overriden - bitOr");
	return nullptr;
}

SharedPtr<StoryObject> StoryObject::bitXor(const StoryObject& other, ErrorHandler& errorHandler)
{
	UNEXPECTED_ERROR("StoryObject virtual method not overriden - bitXor");
	return nullptr;
}

SharedPtr<StoryObject> StoryObject::bitNot(ErrorHandler& errorHandler)
{
	UNEXPECTED_ERROR("StoryObject virtual method not overriden - bitNot");
	return nullptr;
}

SharedPtr<StoryObject> StoryObject::leftShift(const StoryObject& other, ErrorHandler& errorHandler)
{
	UNEXPECTED_ERROR("StoryObject virtual method not overriden - leftShift");
	return nullptr;
}

SharedPtr<StoryObject> StoryObject::rightShift(const StoryObject& other, ErrorHandler& errorHandler)
{
	UNEXPECTED_ERROR("StoryObject virtual method not overriden - rightShift");
	return nullptr;
}

void StoryObject::toRepresentationString(OutStringStream& oss) const
{
	UNEXPECTED_ERROR("StoryObject virtual method not overriden - toRepresentationString");
}

void StoryObject::toCodeString(OutStringStream& oss) const
{
	UNEXPECTED_ERROR("StoryObject virtual method not overriden - toCodeString");
}

bool StoryObject::toBoolean() const
{
	UNEXPECTED_ERROR("StoryObject virtual method not overriden - toBoolean");
	return false;
}

/*****************
 * BooleanObject *
 *****************/

bool BooleanObject::operator==(const StoryObject& other) const
{
	if (type != other.type)
	{
		return false;
	}
	
	return value == static_cast<const BooleanObject*>(&other)->value;
}

SharedPtr<StoryObject> BooleanObject::equals(const StoryObject& other, ErrorHandler& errorHandler)
{
	if (type != other.type)
	{
		return make_shared<BooleanObject>(false);
	}
	
	return make_shared<BooleanObject>(value == static_cast<const BooleanObject*>(&other)->value);
}

SharedPtr<StoryObject> BooleanObject::notEquals(const StoryObject& other, ErrorHandler& errorHandler)
{
	if (type != other.type)
	{
		return make_shared<BooleanObject>(true);
	}
	
	return make_shared<BooleanObject>(value != static_cast<const BooleanObject*>(&other)->value);
}

/*****************
 * IntegerObject *
 *****************/

bool IntegerObject::operator==(const StoryObject& other) const
{
	if (type != other.type)
	{
		return false;
	}
	
	return value == static_cast<const IntegerObject*>(&other)->value;
}

SharedPtr<StoryObject> IntegerObject::equals(const StoryObject& other, ErrorHandler& errorHandler)
{
	switch (other.type)
	{
		case ObjectType::Float:
			return make_shared<BooleanObject>(value == static_cast<const FloatObject*>(&other)->value);
		case ObjectType::Integer:
			return make_shared<BooleanObject>(value == static_cast<const IntegerObject*>(&other)->value);
		default:
			return make_shared<BooleanObject>(false);
	}
}

SharedPtr<StoryObject> IntegerObject::notEquals(const StoryObject& other, ErrorHandler& errorHandler)
{
	switch (other.type)
	{
		case ObjectType::Float:
			return make_shared<BooleanObject>(value != static_cast<const FloatObject*>(&other)->value);
		case ObjectType::Integer:
			return make_shared<BooleanObject>(value != static_cast<const IntegerObject*>(&other)->value);
		default:
			return make_shared<BooleanObject>(true);
	}
}

SharedPtr<StoryObject> IntegerObject::greaterThanOrEquals(const StoryObject& other, ErrorHandler& errorHandler)
{
	switch (other.type)
	{
		case ObjectType::Float:
			return make_shared<BooleanObject>(value >= static_cast<const FloatObject*>(&other)->value);
		case ObjectType::Integer:
			return make_shared<BooleanObject>(value >= static_cast<const IntegerObject*>(&other)->value);
		default:
			String msg = "Cannot GreaterThanOrEquals( >= ) " + StoryObject::getObjectTypeName(other.type) + " and " + StoryObject::getObjectTypeName(type);
			errorHandler.handleError(msg);
			return nullptr;
	}
}

SharedPtr<StoryObject> IntegerObject::greaterThan(const StoryObject& other, ErrorHandler& errorHandler)
{
	switch (other.type)
	{
		case ObjectType::Float:
			return make_shared<BooleanObject>(value > static_cast<const FloatObject*>(&other)->value);
		case ObjectType::Integer:
			return make_shared<BooleanObject>(value > static_cast<const IntegerObject*>(&other)->value);
		default:
			String msg = "Cannot GreaterThan( > ) " + StoryObject::getObjectTypeName(other.type) + " and " + StoryObject::getObjectTypeName(type);
			errorHandler.handleError(msg);
			return nullptr;
	}
}

SharedPtr<StoryObject> IntegerObject::lessThanOrEquals(const StoryObject& other, ErrorHandler& errorHandler)
{
	switch (other.type)
	{
		case ObjectType::Float:
			return make_shared<BooleanObject>(value <= static_cast<const FloatObject*>(&other)->value);
		case ObjectType::Integer:
			return make_shared<BooleanObject>(value <= static_cast<const IntegerObject*>(&other)->value);
		default:
			String msg = "Cannot LessThanOrEquals( <= ) " + StoryObject::getObjectTypeName(other.type) + " and " + StoryObject::getObjectTypeName(type);
			errorHandler.handleError(msg);
			return nullptr;
	}
}

SharedPtr<StoryObject> IntegerObject::lessThan(const StoryObject& other, ErrorHandler& errorHandler)
{
	switch (other.type)
	{
		case ObjectType::Float:
			return make_shared<BooleanObject>(value < static_cast<const FloatObject*>(&other)->value);
		case ObjectType::Integer:
			return make_shared<BooleanObject>(value < static_cast<const IntegerObject*>(&other)->value);
		default:
			String msg = "Cannot LessThan( < ) " + StoryObject::getObjectTypeName(other.type) + " and " + StoryObject::getObjectTypeName(type);
			errorHandler.handleError(msg);
			return nullptr;
	}
}

SharedPtr<StoryObject> IntegerObject::add(const StoryObject& other, ErrorHandler& errorHandler)
{
	switch (other.type)
	{
		case ObjectType::Float:
			return make_shared<FloatObject>(value + static_cast<const FloatObject*>(&other)->value);
			break;
		case ObjectType::Integer:
			return make_shared<IntegerObject>(value + static_cast<const IntegerObject*>(&other)->value);
			break;
		case ObjectType::String:
			return make_shared<StringObject>(std::to_string(value) + static_cast<const StringObject*>(&other)->value);
			break;
		default:
			String msg = "Cannot Add( + ) " + StoryObject::getObjectTypeName(other.type) + " and " + StoryObject::getObjectTypeName(type);
			errorHandler.handleError(msg);
			return nullptr;
	}
}

SharedPtr<StoryObject> IntegerObject::subtract(const StoryObject& other, ErrorHandler& errorHandler)
{
	switch (other.type)
	{
		case ObjectType::Float:
			return make_shared<FloatObject>(value - static_cast<const FloatObject*>(&other)->value);
			break;
		case ObjectType::Integer:
			return make_shared<IntegerObject>(value - static_cast<const IntegerObject*>(&other)->value);
			break;
		default:
			String msg = "Cannot Subtract( - ) " + StoryObject::getObjectTypeName(other.type) + " and " + StoryObject::getObjectTypeName(type);
			errorHandler.handleError(msg);
			return nullptr;
	}
}

SharedPtr<StoryObject> IntegerObject::negate(ErrorHandler& errorHandler)
{
	return make_shared<IntegerObject>(-value);
}

SharedPtr<StoryObject> IntegerObject::multiply(const StoryObject& other, ErrorHandler& errorHandler)
{
	switch (other.type)
	{
		case ObjectType::Float:
			return make_shared<FloatObject>(value * static_cast<const FloatObject*>(&other)->value);
			break;
		case ObjectType::Integer:
			return make_shared<IntegerObject>(value * static_cast<const IntegerObject*>(&other)->value);
			break;
		default:
			String msg = "Cannot Multiply( * ) " + StoryObject::getObjectTypeName(other.type) + " and " + StoryObject::getObjectTypeName(type);
			errorHandler.handleError(msg);
			return nullptr;
	}
}

SharedPtr<StoryObject> IntegerObject::divide(const StoryObject& other, ErrorHandler& errorHandler)
{
	switch (other.type)
	{
		case ObjectType::Float:
			return make_shared<FloatObject>(value / static_cast<const FloatObject*>(&other)->value);
			break;
		case ObjectType::Integer:
			// All division must result in float. In the future, add operator for integer division
			return make_shared<FloatObject>((double)value / static_cast<const IntegerObject*>(&other)->value);
			break;
		default:
			String msg = "Cannot Divide( / ) " + StoryObject::getObjectTypeName(other.type) + " and " + StoryObject::getObjectTypeName(type);
			errorHandler.handleError(msg);
			return nullptr;
	}
}

SharedPtr<StoryObject> IntegerObject::modulus(const StoryObject& other, ErrorHandler& errorHandler)
{
	switch (other.type)
	{
		case ObjectType::Float:
			return make_shared<FloatObject>(std::fmod(value, static_cast<const FloatObject*>(&other)->value));
			break;
		case ObjectType::Integer:
			return make_shared<IntegerObject>(value % static_cast<const IntegerObject*>(&other)->value);
			break;
		default:
			String msg = "Cannot Modulus( % ) " + StoryObject::getObjectTypeName(other.type) + " and " + StoryObject::getObjectTypeName(type);
			errorHandler.handleError(msg);
			return nullptr;
	}
}

SharedPtr<StoryObject> IntegerObject::power(const StoryObject& other, ErrorHandler& errorHandler)
{
	switch (other.type)
	{
		case ObjectType::Float:
			return make_shared<FloatObject>(std::pow(value, static_cast<const FloatObject*>(&other)->value));
			break;
		case ObjectType::Integer:
			return make_shared<IntegerObject>(std::pow(value, static_cast<const IntegerObject*>(&other)->value));
			break;
		default:
			String msg = "Cannot Power( ** ) " + StoryObject::getObjectTypeName(other.type) + " and " + StoryObject::getObjectTypeName(type);
			errorHandler.handleError(msg);
			return nullptr;
	}
}

SharedPtr<StoryObject> IntegerObject::bitAnd(const StoryObject& other, ErrorHandler& errorHandler)
{
	if (type != other.type)
	{
		String msg = "Cannot BitAnd( & ) " + StoryObject::getObjectTypeName(other.type) + " and " + StoryObject::getObjectTypeName(type);
		errorHandler.handleError(msg);
		return nullptr;
	}
	
	auto& otherValue = static_cast<const IntegerObject*>(&other)->value;
	return make_shared<IntegerObject>(value & otherValue);
}

SharedPtr<StoryObject> IntegerObject::bitOr(const StoryObject& other, ErrorHandler& errorHandler)
{
	if (type != other.type)
	{
		String msg = "Cannot BitOr( | ) " + StoryObject::getObjectTypeName(other.type) + " and " + StoryObject::getObjectTypeName(type);
		errorHandler.handleError(msg);
		return nullptr;
	}
	
	auto& otherValue = static_cast<const IntegerObject*>(&other)->value;
	return make_shared<IntegerObject>(value | otherValue);
}

SharedPtr<StoryObject> IntegerObject::bitXor(const StoryObject& other, ErrorHandler& errorHandler)
{
	if (type != other.type)
	{
		String msg = "Cannot BitXor( ^ ) " + StoryObject::getObjectTypeName(other.type) + " and " + StoryObject::getObjectTypeName(type);
		errorHandler.handleError(msg);
		return nullptr;
	}
	
	auto& otherValue = static_cast<const IntegerObject*>(&other)->value;
	return make_shared<IntegerObject>(value ^ otherValue);
}

SharedPtr<StoryObject> IntegerObject::bitNot(ErrorHandler& errorHandler)
{
	return make_shared<IntegerObject>(~value);
}

SharedPtr<StoryObject> IntegerObject::leftShift(const StoryObject& other, ErrorHandler& errorHandler)
{
	if (type != other.type)
	{
		String msg = "Cannot LeftShift( << ) " + StoryObject::getObjectTypeName(other.type) + " and " + StoryObject::getObjectTypeName(type);
		errorHandler.handleError(msg);
		return nullptr;
	}
	
	auto& otherValue = static_cast<const IntegerObject*>(&other)->value;
	return make_shared<IntegerObject>(value << otherValue);
}

SharedPtr<StoryObject> IntegerObject::rightShift(const StoryObject& other, ErrorHandler& errorHandler)
{
	if (type != other.type)
	{
		String msg = "Cannot RightShift( >> ) " + StoryObject::getObjectTypeName(other.type) + " and " + StoryObject::getObjectTypeName(type);
		errorHandler.handleError(msg);
		return nullptr;
	}
	
	auto& otherValue = static_cast<const IntegerObject*>(&other)->value;
	return make_shared<IntegerObject>(value >> otherValue);
}


/***************
 * FloatObject *
 ***************/

bool FloatObject::operator==(const StoryObject& other) const
{
	if (type != other.type)
	{
		return false;
	}
	
	return value == static_cast<const FloatObject*>(&other)->value;
}

SharedPtr<StoryObject> FloatObject::equals(const StoryObject& other, ErrorHandler& errorHandler)
{
	switch (other.type)
	{
		case ObjectType::Float:
			return make_shared<BooleanObject>(value == static_cast<const FloatObject*>(&other)->value);
		case ObjectType::Integer:
			return make_shared<BooleanObject>(value == static_cast<const IntegerObject*>(&other)->value);
		default:
			return make_shared<BooleanObject>(false);
	}
}

SharedPtr<StoryObject> FloatObject::notEquals(const StoryObject& other, ErrorHandler& errorHandler)
{
	switch (other.type)
	{
		case ObjectType::Float:
			return make_shared<BooleanObject>(value != static_cast<const FloatObject*>(&other)->value);
		case ObjectType::Integer:
			return make_shared<BooleanObject>(value != static_cast<const IntegerObject*>(&other)->value);
		default:
			return make_shared<BooleanObject>(true);
	}
}

SharedPtr<StoryObject> FloatObject::greaterThanOrEquals(const StoryObject& other, ErrorHandler& errorHandler)
{
	switch (other.type)
	{
		case ObjectType::Float:
			return make_shared<BooleanObject>(value >= static_cast<const FloatObject*>(&other)->value);
		case ObjectType::Integer:
			return make_shared<BooleanObject>(value >= static_cast<const IntegerObject*>(&other)->value);
		default:
			String msg = "Cannot GreaterThanOrEquals( >= ) " + StoryObject::getObjectTypeName(other.type) + " and " + StoryObject::getObjectTypeName(type);
			errorHandler.handleError(msg);
			return nullptr;
	}
}

SharedPtr<StoryObject> FloatObject::greaterThan(const StoryObject& other, ErrorHandler& errorHandler)
{
	switch (other.type)
	{
		case ObjectType::Float:
			return make_shared<BooleanObject>(value > static_cast<const FloatObject*>(&other)->value);
		case ObjectType::Integer:
			return make_shared<BooleanObject>(value > static_cast<const IntegerObject*>(&other)->value);
		default:
			String msg = "Cannot GreaterThan( > ) " + StoryObject::getObjectTypeName(other.type) + " and " + StoryObject::getObjectTypeName(type);
			errorHandler.handleError(msg);
			return nullptr;
	}
}

SharedPtr<StoryObject> FloatObject::lessThanOrEquals(const StoryObject& other, ErrorHandler& errorHandler)
{
	switch (other.type)
	{
		case ObjectType::Float:
			return make_shared<BooleanObject>(value <= static_cast<const FloatObject*>(&other)->value);
		case ObjectType::Integer:
			return make_shared<BooleanObject>(value <= static_cast<const IntegerObject*>(&other)->value);
		default:
			String msg = "Cannot LessThanOrEquals( <= ) " + StoryObject::getObjectTypeName(other.type) + " and " + StoryObject::getObjectTypeName(type);
			errorHandler.handleError(msg);
			return nullptr;
	}
}

SharedPtr<StoryObject> FloatObject::lessThan(const StoryObject& other, ErrorHandler& errorHandler)
{
	switch (other.type)
	{
		case ObjectType::Float:
			return make_shared<BooleanObject>(value < static_cast<const FloatObject*>(&other)->value);
		case ObjectType::Integer:
			return make_shared<BooleanObject>(value < static_cast<const IntegerObject*>(&other)->value);
		default:
			String msg = "Cannot LessThan( < ) " + StoryObject::getObjectTypeName(other.type) + " and " + StoryObject::getObjectTypeName(type);
			errorHandler.handleError(msg);
			return nullptr;
	}
}

SharedPtr<StoryObject> FloatObject::add(const StoryObject& other, ErrorHandler& errorHandler)
{
	switch (other.type)
	{
		case ObjectType::Float:
			return make_shared<FloatObject>(value + static_cast<const FloatObject*>(&other)->value);
			break;
		case ObjectType::Integer:
			return make_shared<FloatObject>(value + static_cast<const IntegerObject*>(&other)->value);
			break;
		case ObjectType::String:
			return make_shared<StringObject>(std::to_string(value) + static_cast<const StringObject*>(&other)->value);
			break;
		default:
			String msg = "Cannot Add( + ) " + StoryObject::getObjectTypeName(other.type) + " and " + StoryObject::getObjectTypeName(type);
			errorHandler.handleError(msg);
			return nullptr;
	}
}

SharedPtr<StoryObject> FloatObject::subtract(const StoryObject& other, ErrorHandler& errorHandler)
{
	switch (other.type)
	{
		case ObjectType::Float:
			return make_shared<FloatObject>(value - static_cast<const FloatObject*>(&other)->value);
			break;
		case ObjectType::Integer:
			return make_shared<FloatObject>(value - static_cast<const IntegerObject*>(&other)->value);
			break;
		default:
			String msg = "Cannot Subtract( - ) " + StoryObject::getObjectTypeName(other.type) + " and " + StoryObject::getObjectTypeName(type);
			errorHandler.handleError(msg);
			return nullptr;
	}
}

SharedPtr<StoryObject> FloatObject::negate(ErrorHandler& errorHandler)
{
	return make_shared<FloatObject>(-value);
}

SharedPtr<StoryObject> FloatObject::multiply(const StoryObject& other, ErrorHandler& errorHandler)
{
	switch (other.type)
	{
		case ObjectType::Float:
			return make_shared<FloatObject>(value * static_cast<const FloatObject*>(&other)->value);
			break;
		case ObjectType::Integer:
			return make_shared<FloatObject>(value * static_cast<const IntegerObject*>(&other)->value);
			break;
		default:
			String msg = "Cannot Multiply( * ) " + StoryObject::getObjectTypeName(other.type) + " and " + StoryObject::getObjectTypeName(type);
			errorHandler.handleError(msg);
			return nullptr;
	}
}

SharedPtr<StoryObject> FloatObject::divide(const StoryObject& other, ErrorHandler& errorHandler)
{
	switch (other.type)
	{
		case ObjectType::Float:
			return make_shared<FloatObject>(value / static_cast<const FloatObject*>(&other)->value);
			break;
		case ObjectType::Integer:
			return make_shared<FloatObject>(value / static_cast<const IntegerObject*>(&other)->value);
			break;
		default:
			String msg = "Cannot Divide( / ) " + StoryObject::getObjectTypeName(other.type) + " and " + StoryObject::getObjectTypeName(type);
			errorHandler.handleError(msg);
			return nullptr;
	}
}

SharedPtr<StoryObject> FloatObject::modulus(const StoryObject& other, ErrorHandler& errorHandler)
{
	switch (other.type)
	{
		case ObjectType::Float:
			return make_shared<FloatObject>(std::fmod(value, static_cast<const FloatObject*>(&other)->value));
			break;
		case ObjectType::Integer:
			return make_shared<FloatObject>(std::fmod(value, static_cast<const IntegerObject*>(&other)->value));
			break;
		default:
			String msg = "Cannot Modulus( % ) " + StoryObject::getObjectTypeName(other.type) + " and " + StoryObject::getObjectTypeName(type);
			errorHandler.handleError(msg);
			return nullptr;
	}
}

SharedPtr<StoryObject> FloatObject::power(const StoryObject& other, ErrorHandler& errorHandler)
{
	switch (other.type)
	{
		case ObjectType::Float:
			return make_shared<FloatObject>(std::pow(value, static_cast<const FloatObject*>(&other)->value));
			break;
		case ObjectType::Integer:
			return make_shared<FloatObject>(std::pow(value, static_cast<const IntegerObject*>(&other)->value));
			break;
		default:
			String msg = "Cannot Power( ** ) " + StoryObject::getObjectTypeName(other.type) + " and " + StoryObject::getObjectTypeName(type);
			errorHandler.handleError(msg);
			return nullptr;
	}
}

/****************
 * StringObject *
 ****************/

bool StringObject::operator==(const StoryObject& other) const
{
	if (type != other.type)
	{
		return false;
	}
	
	return value == static_cast<const StringObject*>(&other)->value;
}

SharedPtr<StoryObject> StringObject::equals(const StoryObject& other, ErrorHandler& errorHandler)
{
	if (type != other.type)
	{
		return make_shared<BooleanObject>(false);
	}
	
	return make_shared<BooleanObject>(value == static_cast<const StringObject*>(&other)->value);
}

SharedPtr<StoryObject> StringObject::notEquals(const StoryObject& other, ErrorHandler& errorHandler)
{
	if (type != other.type)
	{
		return make_shared<BooleanObject>(true);
	}
	
	return make_shared<BooleanObject>(value != static_cast<const StringObject*>(&other)->value);
}

SharedPtr<StoryObject> StringObject::add(const StoryObject& other, ErrorHandler& errorHandler)
{
	OutStringStream oss;
	oss << value;
	switch (other.type)
	{
		case ObjectType::Boolean:
			oss << static_cast<const BooleanObject*>(&other)->value;
			break;
		case ObjectType::Float:
			oss << static_cast<const FloatObject*>(&other)->value;
			break;
		case ObjectType::Integer:
			oss << static_cast<const IntegerObject*>(&other)->value;
			break;
		case ObjectType::String:
			oss << static_cast<const StringObject*>(&other)->value;
			break;
		case ObjectType::List:
			static_cast<const ListObject*>(&other)->toCodeString(oss);
			break;
		case ObjectType::Dict:
			static_cast<const DictObject*>(&other)->toCodeString(oss);
			break;
		case ObjectType::Character:
			static_cast<const CharacterObject*>(&other)->toCodeString(oss);
			break;
		default:
			String msg = "Cannot Add( + ) " + StoryObject::getObjectTypeName(other.type) + " and " + StoryObject::getObjectTypeName(type);
			errorHandler.handleError(msg);
			return nullptr;
	}
	
	return make_shared<StringObject>(oss.str());
}

/**************
 * ListObject *
 **************/

bool ListObject::operator==(const StoryObject& other) const
{
	if (type != other.type)
	{
		return false;
	}
	
	return value == static_cast<const ListObject*>(&other)->value;
}

SharedPtr<StoryObject> ListObject::equals(const StoryObject& other, ErrorHandler& errorHandler)
{
	if (type != other.type)
	{
		return make_shared<BooleanObject>(false);
	}
	
	return make_shared<BooleanObject>(value == static_cast<const ListObject*>(&other)->value);
}

SharedPtr<StoryObject> ListObject::notEquals(const StoryObject& other, ErrorHandler& errorHandler)
{
	if (type != other.type)
	{
		return make_shared<BooleanObject>(true);
	}
	
	return make_shared<BooleanObject>(value != static_cast<const ListObject*>(&other)->value);
}

/**************
 * DictObject *
 **************/

bool DictObject::operator==(const StoryObject& other) const
{
	if (type != other.type)
	{
		return false;
	}
	
	return value == static_cast<const DictObject*>(&other)->value;
}

SharedPtr<StoryObject> DictObject::equals(const StoryObject& other, ErrorHandler& errorHandler)
{
	if (type != other.type)
	{
		return make_shared<BooleanObject>(false);
	}
	
	return make_shared<BooleanObject>(value == static_cast<const DictObject*>(&other)->value);
}

SharedPtr<StoryObject> DictObject::notEquals(const StoryObject& other, ErrorHandler& errorHandler)
{
	if (type != other.type)
	{
		return make_shared<BooleanObject>(true);
	}
	
	return make_shared<BooleanObject>(value != static_cast<const DictObject*>(&other)->value);
}

/*******************
 * CharacterObject *
 *******************/

bool CharacterObject::operator==(const StoryObject& other) const
{
	if (type != other.type)
	{
		return false;
	}
	
	return value == static_cast<const CharacterObject*>(&other)->value;
}

SharedPtr<StoryObject> CharacterObject::equals(const StoryObject& other, ErrorHandler& errorHandler)
{
	if (type != other.type)
	{
		return make_shared<BooleanObject>(false);
	}
	
	return make_shared<BooleanObject>(value == static_cast<const CharacterObject*>(&other)->value);
}

SharedPtr<StoryObject> CharacterObject::notEquals(const StoryObject& other, ErrorHandler& errorHandler)
{
	if (type != other.type)
	{
		return make_shared<BooleanObject>(true);
	}
	
	return make_shared<BooleanObject>(value != static_cast<const CharacterObject*>(&other)->value);
}

}