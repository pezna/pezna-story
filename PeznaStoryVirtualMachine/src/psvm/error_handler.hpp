#ifndef PSVM_ERROR_HANDLER_H
#define PSVM_ERROR_HANDLER_H

#include "typedefs.hpp"

namespace PSVM
{

class ErrorHandler
{
public:
	/**
	 * Receive an error that occurred while running the story's bytecode
	 */
	virtual void handleError(String message);
	virtual bool hasError();
	virtual void clearErrors();
};

}

#endif // PSVM_ERROR_HANDLER_H