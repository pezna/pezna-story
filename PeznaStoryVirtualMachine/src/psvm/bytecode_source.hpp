#ifndef PSVM_BYTECODE_SOURCE_H
#define PSVM_BYTECODE_SOURCE_H

namespace PSVM
{

/**
 * An abstract class meant to be used to fetch the bytecode from whatever location the user keeps it in.
 * BytecodeSource has no implementation in this library because it is meant to be implemented by the user of this library.
 * Different game developers and engines may have different ways of loading a file into memory,
 * so this allwos everyone to adapt it for their own engine/convention.
 */
class BytecodeSource
{
public:
	virtual ~BytecodeSource() {}
	virtual char nextByte() = 0;
	virtual char peekByte(int delta) = 0;
	virtual void skipBytes(int delta) = 0;
	virtual void setOffset(int offset) = 0;
	virtual int getOffset() = 0;
	virtual bool hasNextByte() = 0;
};

}

#endif // PSVM_BYTECODE_SOURCE_H