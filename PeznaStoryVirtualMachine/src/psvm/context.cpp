#include "context.hpp"

#include <cassert>
#include <utility>

namespace PSVM
{

SharedPtr<StoryObject> Frame::popStack()
{
	auto last = stack.back();
	stack.pop_back();
	return last;
}

SharedPtr<StoryObject>& Frame::peekStack()
{
	return stack.back();
}

void Frame::pushStack(SharedPtr<StoryObject> object)
{
	stack.push_back(object);
}

void StoryContext::createAndPushFrame(ContainerMetadata& container)
{
	auto inserted = activeFrames.emplace(container.id, Frame(&container));
	assert(("Could not insert frame", inserted.second));
	callStack.push_back(container.id);
}

Frame& StoryContext::getCurrentFrame()
{
	assert(("No frame in call stack to get", callStack.size()));
	auto id = callStack.back();
	return activeFrames.at(id);
}

void StoryContext::popCurrentFrame()
{
	assert(("No frame in call stack to pop", callStack.size()));
	auto id = callStack.back();
	callStack.pop_back();
	activeFrames.erase(id);
}

void StoryContext::popCurrentFrameWithReturnValue()
{
	assert(("No frame in call stack to pop", callStack.size()));
	auto id = callStack.back();
	callStack.pop_back();
	auto it = activeFrames.find(id);
	assert(("No frame found for ID", id, it != activeFrames.end()));
	auto& frame = it->second;
	
	auto id2 = callStack[callStack.size() - 2];
	auto it2 = activeFrames.find(id2);
	assert(("No frame found for second ID", id2, it2 != activeFrames.end()));
	auto& frame2 = it2->second;
	// put top object from top frame's stack into second frame's stack
	frame2.pushStack(frame.popStack());
	
	// delete top frame
	activeFrames.erase(it);
}

}