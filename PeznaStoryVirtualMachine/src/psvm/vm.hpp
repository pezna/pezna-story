#ifndef PSVM_VM_H
#define PSVM_VM_H

#include "bytecode_source.hpp"
#include "context.hpp"
#include "error_handler.hpp"
#include "metadata.hpp"

namespace PSVM
{

class StoryVirtualMachine
{
	StoryMetadata _metadata;
	BytecodeSource& _source;
	
	/**
	 * @brief Load all the metadata for the given story context.
	 * Story context must have been created externally by the caller.
	 * @return true if the initialization was successful, false otherwise
	 */
	bool loadNames();
	/**
	 * @brief Load all the metadata for the given story context.
	 * Story context must have been created externally by the caller.
	 * @return true if the initialization was successful, false otherwise
	 */
	bool loadStrings();
	/**
	 * @brief Load all the metadata for the given story context.
	 * Story context must have been created externally by the caller.
	 * @return true if the initialization was successful, false otherwise
	 */
	bool loadContainers();
	
public:
	StoryVirtualMachine(BytecodeSource& source);
	
	/**
	 * @brief Load all the metadata for the given story context.
	 * Story context must have been created externally by the caller.
	 * @return true if the initialization was successful, false otherwise
	 */
    bool load(ErrorHandler* errorHandler);
	
	/**
	 * @brief Initial the given context by running all the namespace code in it.
	 */
	void initContext(StoryContext& context);
	
	/**
	 * Run the frame in the given context
	 */
	void run(StoryContext& context, Frame& frame);
	
	StoryMetadata& metadata() { return _metadata; }
	BytecodeSource& source() { return _source; }
};

}

#endif // PSVM_VM_H