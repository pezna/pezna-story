#include "vm.hpp"

#include "instruction.hpp"
#include "typedefs.hpp"
#include "utils.hpp"

namespace PSVM
{

StoryVirtualMachine::StoryVirtualMachine(BytecodeSource& source)
:
	_source(source)
{}

bool StoryVirtualMachine::load(ErrorHandler* errorHandler)
{
	char byte = _source.nextByte();
	if (byte != BEGIN_METADATA_OP)
	{
		UNEXPECTED_ERROR("The first byte must be BEGIN_METADATA_OP. "
			"Is the source starting from the very first byte in the file? "
			"Has this function been called before?");
		return false;
	}
	
	bool loadedNames = false;
	bool loadedStrings = false;
	bool loadedContainers = false;
	
	while ((byte = _source.peekByte(1)) != END_METADATA_OP)
	{
		_source.skipBytes(1);
		
		switch (byte)
		{
			case BEGIN_NAMES_OP:
				if (!loadNames())
				{
					return false;
				}
				loadedNames = true;
				break;
			case BEGIN_STRINGS_OP:
				if (!loadStrings())
				{
					return false;
				}
				loadedStrings = true;
				break;
			case BEGIN_CONTAINERS_OP:
				if (!loadContainers())
				{
					return false;
				}
				loadedContainers = true;
				break;
			default:
				UNEXPECTED_ERROR("No opcode that can start a metadata group was encountered. "
					"Is the source starting from the very first byte in the file? "
					"Has this function been called before?");
				return false;
		}
	}
	
	char end = _source.nextByte();
	ASSERT(("Expected END_METADATA_OP", end == END_METADATA_OP));
	_metadata.metadataByteLength = _source.getOffset();
	
	return loadedNames && loadedStrings && loadedContainers;
}

bool StoryVirtualMachine::loadNames()
{
	_metadata.numNames = Utils::readVarInt(_source);
	
	for (int n = 0; n < _metadata.numNames; ++n)
	{
		// 1. name id
		// 2. name text length
		// 3. name text character sequence
		int64_t id = Utils::readVarInt(_source);
		int textLength = Utils::readVarInt(_source);
		String text(textLength, '*');
		for (int i = 0; i < textLength; ++i)
		{
			text[i] = _source.nextByte();
		}
		_metadata.names[id] = std::move(text);
	}
	
	char end = _source.nextByte();
	ASSERT(("Expected END_NAMES_OP", end == END_NAMES_OP));
	
	return true;
}

bool StoryVirtualMachine::loadStrings()
{
	_metadata.numStrings = Utils::readVarInt(_source);
	
	for (int n = 0; n < _metadata.numStrings; ++n)
	{
		// 1. srings id
		// 2. string text length
		// 3. string text character sequence
		int64_t id = Utils::readVarInt(_source);
		int textLength = Utils::readVarInt(_source);
		String text(textLength, '*');
		for (int i = 0; i < textLength; ++i)
		{
			text[i] = _source.nextByte();
		}
		_metadata.strings[id] = std::move(text);
	}
	
	char end = _source.nextByte();
	ASSERT(("Expected END_STRINGS_OP", end == END_STRINGS_OP));
	
	return true;
}

bool StoryVirtualMachine::loadContainers()
{
	_metadata.numContainers = Utils::readVarInt(_source);
	
	for (int n = 0; n < _metadata.numContainers; ++n)
	{
		ContainerMetadata container;
		container.id = Utils::readVarInt(_source);
		container.nameId = Utils::readVarInt(_source);
		container.parentId = Utils::readVarInt(_source);
		container.isNamespace = _source.nextByte() == 0 ? false : true;
		container.numParameters = Utils::readVarInt(_source);
		container.bytecodeOffset = Utils::readVarInt(_source);
		container.bytecodeLength = Utils::readVarInt(_source);
		
		char byte = _source.nextByte();
		ASSERT(("Expected BEGIN_VARIABLES_OP", byte == BEGIN_VARIABLES_OP));
		
		container.numVariables = Utils::readVarInt(_source);
		for (int nv = 0; nv < container.numVariables; ++nv)
		{
			VariableMetadata variable;
			variable.containerId = container.id;
			variable.nameId = Utils::readVarInt(_source);
			int modifiers = _source.nextByte();
			variable.isFinal = modifiers;
			variable.isParameter = _source.nextByte();
			
			container.variables[variable.nameId] = std::move(variable);
		}
		
		byte = _source.nextByte();
		ASSERT(("Expected END_VARIABLES_OP", byte == END_VARIABLES_OP));
		byte = _source.nextByte();
		ASSERT(("Expected BEGIN_LABELS_OP", byte == BEGIN_LABELS_OP));
		
		container.numLabels = Utils::readVarInt(_source);
		for (int nl = 0; nl < container.numLabels; ++nl)
		{
			LabelMetadata label;
			label.containerId = container.id;
			label.nameId = Utils::readVarInt(_source);
			label.bytecodeOffset = Utils::readVarInt(_source);
			
			container.labels[label.nameId] = std::move(label);
		}
		
		byte = _source.nextByte();
		ASSERT(("Expected END_LABELS_OP", byte == END_LABELS_OP));
		
		_metadata.containers[container.id] = std::move(container);
		if (container.isNamespace)
		{
			_metadata.namespaceOrder.push_back(container.id);
		}
	}
	
	char byte = _source.nextByte();
	ASSERT(("Expected END_CONTAINERS_OP", byte == END_CONTAINERS_OP));
	
	return true;
}

void StoryVirtualMachine::initContext(StoryContext& context)
{
	for (int64_t namespaceId : _metadata.namespaceOrder)
	{
		ContainerMetadata& container = _metadata.containers.at(namespaceId);
		context.createAndPushFrame(container);
		run(context, context.getCurrentFrame());
	}
}

void StoryVirtualMachine::run(StoryContext& context, Frame& frame)
{
	ErrorHandler& errorHandler = *context.errorHandler;
	ContainerMetadata* container = frame.container;
	_source.setOffset(container->bytecodeOffset);
	
	while (_source.hasNextByte() && _source.getOffset() - container->bytecodeOffset < container->bytecodeLength)
	{
		if (context.errorHandler->hasError())
		{
			break;
		}
		
		char byte = _source.nextByte();
		switch (byte)
		{
			case StoryOpcode::NOOP_OP:
				break;
			case StoryOpcode::EQUALS_OP:
				equalsOp(frame, errorHandler);
				break;
			case StoryOpcode::NOT_EQUALS_OP:
				notEqualsOp(frame, errorHandler);
				break;
			case StoryOpcode::GREATER_THAN_EQUALS_OP:
				greaterThanEqualsOp(frame, errorHandler);
				break;
			case StoryOpcode::LESS_THAN_EQUALS_OP:
				lessThanEqualsOp(frame, errorHandler);
				break;
			case StoryOpcode::GREATER_THAN_OP:
				greaterThanOp(frame, errorHandler);
				break;
			case StoryOpcode::LESS_THAN_OP:
				lessThanOp(frame, errorHandler);
				break;
			case StoryOpcode::NOT_OP:
				notOp(frame, errorHandler);
				break;
			case StoryOpcode::IN_OP:
				inOp(frame, errorHandler);
				break;
			case StoryOpcode::NOT_IN_OP:
				notInOp(frame, errorHandler);
				break;
			case StoryOpcode::IS_OP:
				isOp(frame, errorHandler, _source);
				break;
			case StoryOpcode::IS_NOT_OP:
				isNotOp(frame, errorHandler, _source);
				break;
			case StoryOpcode::OR_OP:
				orOp(frame, errorHandler);
				break;
			case StoryOpcode::POWER_OP:
				powerOp(frame, errorHandler);
				break;
			case StoryOpcode::ADDITION_OP:
				additionOp(frame, errorHandler);
				break;
			case StoryOpcode::SUBTRACT_OP:
				subtractOp(frame, errorHandler);
				break;
			case StoryOpcode::NEGATE_OP:
				negateOp(frame, errorHandler);
				break;
			case StoryOpcode::MULTIPLY_OP:
				multiplyOp(frame, errorHandler);
				break;
			case StoryOpcode::DIVIDE_OP:
				divideOp(frame, errorHandler);
				break;
			case StoryOpcode::MODULUS_OP:
				modulusOp(frame, errorHandler);
				break;
			case StoryOpcode::BIT_AND_OP:
				bitAndOp(frame, errorHandler);
				break;
			case StoryOpcode::BIT_XOR_OP:
				bitXorOp(frame, errorHandler);
				break;
			case StoryOpcode::BIT_OR_OP:
				bitOrOp(frame, errorHandler);
				break;
			case StoryOpcode::BIT_NOT_OP:
				bitNotOp(frame, errorHandler);
				break;
			case StoryOpcode::BIT_LEFT_SHIFT_OP:
				bitLeftShiftOp(frame, errorHandler);
				break;
			case StoryOpcode::BIT_RIGHT_SHIFT_OP:
				bitRightShiftOp(frame, errorHandler);
				break;
			case StoryOpcode::INT_CONST_OP:
				intConstOp(frame, _source);
				break;
			case StoryOpcode::FLOAT_CONST_OP:
				floatConstOp(frame, _source);
				break;
			case StoryOpcode::BOOL_CONST_OP:
				boolConstOp(frame, _source);
				break;
			case StoryOpcode::STRING_CONST_OP:
				stringConstOp(frame, _source, _metadata);
				break;
			case StoryOpcode::BUILD_LIST_OP:
				buildListOp(frame, _source);
				break;
			case StoryOpcode::BUILD_LIST_WITH_DEFAULT_OP:
				buildListWithDefaultOp(frame, _source);
				break;
			case StoryOpcode::BUILD_DICTIONARY_OP:
				buildDictOp(frame, _source);
				break;
			case StoryOpcode::BUILD_DICTIONARY_WITH_DEFAULT_OP:
				buildDictWithDefaultOp(frame, _source);
				break;
			case StoryOpcode::BUILD_CHARACTER_OP:
				buildCharacterOp(frame, _source, _metadata);
				break;
			case StoryOpcode::GET_SEQUENCE_INDEX_OP:
				getSequenceIndexOp(context, frame, _source);
				break;
			case StoryOpcode::DEFAULT_CONST_OP:
				break;
			case StoryOpcode::LINE_OP:
				break;
			case StoryOpcode::CHOICES_OP:
				break;
			case StoryOpcode::GOTO_OP:
				break;
			case StoryOpcode::JUMP_FORWARD_OP:
				break;
			case StoryOpcode::POP_JUMP_IF_TRUE_OP:
				break;
			case StoryOpcode::POP_JUMP_IF_FALSE_OP:
				break;
			case StoryOpcode::JUMP_IF_TRUE_OR_POP_OP:
				break;
			case StoryOpcode::JUMP_IF_FALSE_OR_POP_OP:
				break;
			case StoryOpcode::POP_OP:
				break;
			case StoryOpcode::DUPLICATE_OP:
				break;
			case StoryOpcode::CREATE_VARIABLE_OP:
				break;
			case StoryOpcode::LOAD_VARIABLE_OP:
				break;
			case StoryOpcode::STORE_OP:
				break;
			case StoryOpcode::GET_ATTRIBUTE_OP:
				break;
			case StoryOpcode::CALL_OP:
				break;
			case StoryOpcode::SLICE_OP:
				break;
			case StoryOpcode::SUBSCRIPT_OP:
				break;
			case StoryOpcode::FOR_ITERABLE_OP:
				break;
			case StoryOpcode::EVENT_OP:
				break;
			case StoryOpcode::RETURN_OP:
				break;
			case StoryOpcode::RETURN_VALUE_OP:
				break;
			case StoryOpcode::TERMINATE_OP:
				break;
		}
	}
}

}