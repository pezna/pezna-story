#include "utils.hpp"

#include <random>

#define CONVERT_NUM(num, magnitude) \
if ((num) < (magnitude)) { return ((double)num)/(magnitude); }

namespace PSVM::Utils
{

int64_t readVarInt(char* input, int* numBytesRead)
{
	int position = 0;
	char b = *input;
	int64_t result = b & 0x7f;
	
	while ((b & 0x80) != 0)
	{
		++position;
		b = *(input + position);
		result = (result << 7) | (b & 0x7f);
	}
	
	if (numBytesRead)
	{
		*numBytesRead = position + 1;
	}
	
	return result;
}

int64_t readVarInt(BytecodeSource& source, int* numBytesRead)
{
	int position = 0;
	char b = source.nextByte();
	int64_t result = b & 0x7f;
	
	while ((b & 0x80) != 0)
	{
		++position;
		b = source.nextByte();
		result = (result << 7) | (b & 0x7f);
	}
	
	if (numBytesRead)
	{
		*numBytesRead = position + 1;
	}
	
	return result;
}

double numberToDecimal(int64_t num)
{
	CONVERT_NUM(num, 10LL);
	CONVERT_NUM(num, 100LL);
	CONVERT_NUM(num, 1'000LL);
	CONVERT_NUM(num, 10'000LL);
	CONVERT_NUM(num, 100'000LL);
	CONVERT_NUM(num, 1'000'000LL);
	CONVERT_NUM(num, 10'000'000LL);
	CONVERT_NUM(num, 100'000'000LL);
	CONVERT_NUM(num, 1'000'000'000LL);
	CONVERT_NUM(num, 10'000'000'000LL);
	CONVERT_NUM(num, 100'000'000'000LL);
	CONVERT_NUM(num, 1'000'000'000'000LL);
	CONVERT_NUM(num, 10'000'000'000'000LL);
	CONVERT_NUM(num, 100'000'000'000'000LL);
	CONVERT_NUM(num, 1'000'000'000'000'000LL);
	CONVERT_NUM(num, 10'000'000'000'000'000LL);
	CONVERT_NUM(num, 100'000'000'000'000'000LL);
	CONVERT_NUM(num, 1'000'000'000'000'000'000LL);
	CONVERT_NUM(num, 10'000'000'000'000'000'000ULL);
	return std::numeric_limits<double>::infinity();
}

void escapeString(const String& str, OutStringStream& out)
{
	for (auto i = str.begin(), end = str.end(); i != end; ++i)
	{
		auto c = *i;
		switch(c)
		{
			case '"':
				out << "\\\"";
				break;
			case '\\':
				out << "\\\\";
				break;
			case '\t':
				out << "\\t";
				break;
			case '\r':
				out << "\\r";
				break;
			case '\n':
				out << "\\n";
				break;
			case '\v':
				out << "\\v";
				break;
			default:
				out << c;
		}
	}
}

void writeCodeString(const String& str, OutStringStream& out)
{
	out << "\"";
	escapeString(str, out);
	out << "\"";
}

std::random_device rd;
std::default_random_engine re(rd());
std::uniform_int_distribution<int64_t> ud(0, std::numeric_limits<int64_t>::max());

int64_t randomInt(int64_t max)
{
	auto rand = ud(re);
	return rand % max;
}

}