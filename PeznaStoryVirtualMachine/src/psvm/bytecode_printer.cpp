#include "bytecode_printer.hpp"

#include "instruction.hpp"

namespace PSVM::BytecodePrinter
{

void print(BytecodeSource& source, OutStream& oss)
{
	auto originalOffset = source.getOffset();
	source.setOffset(0);
	
	while (source.hasNextByte())
	{
		printOffset(source, oss);
		char byte = source.nextByte();
		switch (byte)
		{
			case StoryOpcode::NOOP_OP: noop(oss); break;
			case StoryOpcode::EQUALS_OP: equals(oss); break;
			case StoryOpcode::NOT_EQUALS_OP: notEquals(oss); break;
			case StoryOpcode::GREATER_THAN_EQUALS_OP: greaterThanEquals(oss); break;
			case StoryOpcode::LESS_THAN_EQUALS_OP: lessThanEquals(oss); break;
			case StoryOpcode::GREATER_THAN_OP: greaterThan(oss); break;
			case StoryOpcode::LESS_THAN_OP: lessThan(oss); break;
			case StoryOpcode::NOT_OP: nott(oss); break;
			case StoryOpcode::IN_OP: in(oss); break;
			case StoryOpcode::NOT_IN_OP: notIn(oss); break;
			case StoryOpcode::IS_OP: is(source, oss); break;
			case StoryOpcode::IS_NOT_OP: isNot(source, oss); break;
			case StoryOpcode::OR_OP: orr(oss); break;
			case StoryOpcode::POWER_OP: power(oss); break;
			case StoryOpcode::ADDITION_OP: addition(oss); break;
			case StoryOpcode::SUBTRACT_OP: subtract(oss); break;
			case StoryOpcode::NEGATE_OP: negate(oss); break;
			case StoryOpcode::MULTIPLY_OP: multiply(oss); break;
			case StoryOpcode::DIVIDE_OP: divide(oss); break;
			case StoryOpcode::MODULUS_OP: modulus(oss); break;
			case StoryOpcode::BIT_AND_OP: bitAnd(oss); break;
			case StoryOpcode::BIT_XOR_OP: bitXor(oss); break;
			case StoryOpcode::BIT_OR_OP: bitOr(oss); break;
			case StoryOpcode::BIT_NOT_OP: bitNot(oss); break;
			case StoryOpcode::BIT_LEFT_SHIFT_OP: bitLeftShift(oss); break;
			case StoryOpcode::BIT_RIGHT_SHIFT_OP: bitRightShift(oss); break;
			case StoryOpcode::INT_CONST_OP: intConst(source, oss); break;
			case StoryOpcode::FLOAT_CONST_OP: floatConst(source, oss); break;
			case StoryOpcode::BOOL_CONST_OP: boolConst(source, oss); break;
			case StoryOpcode::STRING_CONST_OP: stringConst(source, oss); break;
			case StoryOpcode::BUILD_LIST_OP: buildList(source, oss); break;
			case StoryOpcode::BUILD_LIST_WITH_DEFAULT_OP: buildListWithDefault(source, oss); break;
			case StoryOpcode::BUILD_DICTIONARY_OP: buildDictionary(source, oss); break;
			case StoryOpcode::BUILD_DICTIONARY_WITH_DEFAULT_OP: buildDictionaryWithDefault(source, oss); break;
			case StoryOpcode::BUILD_CHARACTER_OP: buildCharacter(source, oss); break;
			case StoryOpcode::GET_SEQUENCE_INDEX_OP: getSequenceIndex(source, oss); break;
			case StoryOpcode::DEFAULT_CONST_OP: defaultConst(oss); break;
			case StoryOpcode::LINE_OP: line(oss); break;
			case StoryOpcode::CHOICES_OP: choices(source, oss); break;
			case StoryOpcode::GOTO_OP: gotoo(source, oss); break;
			case StoryOpcode::JUMP_FORWARD_OP: jumpForward(source, oss); break;
			case StoryOpcode::POP_JUMP_IF_TRUE_OP: popJumpIfTrue(source, oss); break;
			case StoryOpcode::POP_JUMP_IF_FALSE_OP: popJumpIfFalse(source, oss); break;
			case StoryOpcode::JUMP_IF_TRUE_OR_POP_OP: jumpIfTrueOrPop(source, oss); break;
			case StoryOpcode::JUMP_IF_FALSE_OR_POP_OP: jumpIfFalseOrPop(source, oss); break;
			case StoryOpcode::POP_OP: pop(oss); break;
			case StoryOpcode::DUPLICATE_OP: duplicate(oss); break;
			case StoryOpcode::CREATE_VARIABLE_OP: createVariable(source, oss); break;
			case StoryOpcode::LOAD_VARIABLE_OP: loadVariable(source, oss); break;
			case StoryOpcode::STORE_OP: store(oss); break;
			case StoryOpcode::GET_ATTRIBUTE_OP: getAttribute(source, oss); break;
			case StoryOpcode::CALL_OP: call(source, oss); break;
			case StoryOpcode::SLICE_OP: slice(source, oss); break;
			case StoryOpcode::SUBSCRIPT_OP: subscript(oss); break;
			case StoryOpcode::FOR_ITERABLE_OP: forIterable(source, oss); break;
			case StoryOpcode::EVENT_OP: event(oss); break;
			case StoryOpcode::RETURN_OP: returnn(oss); break;
			case StoryOpcode::RETURN_VALUE_OP: returnValue(oss); break;
			case StoryOpcode::TERMINATE_OP: terminate(oss); break;
			case StoryOpcode::BEGIN_METADATA_OP: beginMetadata(oss); break;
			case StoryOpcode::END_METADATA_OP: endMetadata(oss); break;
			case StoryOpcode::BEGIN_NAMES_OP: beginNames(source, oss); break;
			case StoryOpcode::END_NAMES_OP: endNames(oss); break;
			case StoryOpcode::BEGIN_STRINGS_OP: beginStrings(source, oss); break;
			case StoryOpcode::END_STRINGS_OP: endStrings(oss); break;
			case StoryOpcode::BEGIN_CONTAINERS_OP: beginContainers(source, oss); break;
			case StoryOpcode::END_CONTAINERS_OP: endContainers(oss); break;
			case StoryOpcode::BEGIN_VARIABLES_OP:
			case StoryOpcode::END_VARIABLES_OP:
			case StoryOpcode::BEGIN_LABELS_OP:
			case StoryOpcode::END_LABELS_OP:
				// all handled in beginContainers()
				break;
			case StoryOpcode::BEGIN_BLOCK_OP: beginBlock(source, oss); break;
			case StoryOpcode::END_BLOCK_OP: endBlock(oss); break;
			default:
				throw std::runtime_error(String("Not a bytecode operation: ") + std::to_string((int)byte) + " at offset " + std::to_string(source.getOffset() - 1));
		}
		
		oss << "\n";
	}
	
	source.setOffset(originalOffset);
}

void printOffset(BytecodeSource& source, OutStream& oss)
{
	oss << "[" << source.getOffset() << "] ";
}

void noop(OutStream& oss)
{
	oss << "NOOP";
}

void equals(OutStream& oss)
{
	oss << "EQUALS";
}

void notEquals(OutStream& oss)
{
	oss << "NOT_EQUALS";
}

void greaterThanEquals(OutStream& oss)
{
	oss << "GREATER_THAN_EQUALS";
}

void lessThanEquals(OutStream& oss)
{
	oss << "LESS_THAN_EQUALS";
}

void greaterThan(OutStream& oss)
{
	oss << "GREATER_THAN";
}

void lessThan(OutStream& oss)
{
	oss << "LESS_THAN";
}

void nott(OutStream& oss)
{
	oss << "NOT";
}

void in(OutStream& oss)
{
	oss << "IN";
}

void notIn(OutStream& oss)
{
	oss << "NOT IN";
}

void is(BytecodeSource& source, OutStream& oss)
{
	oss << "IS ";
	auto type = Utils::readVarInt(source);
	switch ((ObjectType)type)
	{
		case ObjectType::Unknown:
			oss << "[Unknown] " << type;
			break;
		case ObjectType::Boolean:
			oss << "[Boolean] " << type;
			break;
		case ObjectType::Integer:
			oss << "[Integer] " << type;
			break;
		case ObjectType::Float:
			oss << "[Float] " << type;
			break;
		case ObjectType::String:
			oss << "[String] " << type;
			break;
		case ObjectType::List:
			oss << "[List] " << type;
			break;
		case ObjectType::Dict:
			oss << "[Dict] " << type;
			break;
		case ObjectType::Character:
			oss << "[Character] " << type;
			break;
		default:
			oss << "[Any] " << type;
			break;
	}
}

void isNot(BytecodeSource& source, OutStream& oss)
{
	oss << "IS NOT ";
	auto type = Utils::readVarInt(source);
	switch ((ObjectType)type)
	{
		case ObjectType::Unknown:
			oss << "[Unknown] " << type;
			break;
		case ObjectType::Boolean:
			oss << "[Boolean] " << type;
			break;
		case ObjectType::Integer:
			oss << "[Integer] " << type;
			break;
		case ObjectType::Float:
			oss << "[Float] " << type;
			break;
		case ObjectType::String:
			oss << "[String] " << type;
			break;
		case ObjectType::List:
			oss << "[List] " << type;
			break;
		case ObjectType::Dict:
			oss << "[Dict] " << type;
			break;
		case ObjectType::Character:
			oss << "[Character] " << type;
			break;
		default:
			oss << "[Any] " << type;
			break;
	}
}

void orr(OutStream& oss)
{
	oss << "OR";
}

void power(OutStream& oss)
{
	oss << "POWER";
}

void addition(OutStream& oss)
{
	oss << "ADDITION";
}

void subtract(OutStream& oss)
{
	oss << "SUBTRACT";
}

void negate(OutStream& oss)
{
	oss << "NEGATE";
}

void multiply(OutStream& oss)
{
	oss << "MULTIPLY";
}

void divide(OutStream& oss)
{
	oss << "DIVIDE";
}

void modulus(OutStream& oss)
{
	oss << "MODULUS";
}

void bitAnd(OutStream& oss)
{
	oss << "BIT_AND";
}

void bitXor(OutStream& oss)
{
	oss << "BIT_XOR";
}

void bitOr(OutStream& oss)
{
	oss << "BIT_OR";
}

void bitNot(OutStream& oss)
{
	oss << "BIT_NOT";
}

void bitLeftShift(OutStream& oss)
{
	oss << "BIT_LEFT_SHIFT";
}

void bitRightShift(OutStream& oss)
{
	oss << "BIT_RIGHT_SHIFT";
}

void intConst(BytecodeSource& source, OutStream& oss)
{
	oss << "INT_CONST " << Utils::readVarInt(source);
}

void floatConst(BytecodeSource& source, OutStream& oss)
{
	oss << "FLOAT_CONST ";
	auto wholeNum = Utils::readVarInt(source);
	auto decimalNum = Utils::readVarInt(source);
	oss << wholeNum << " " << decimalNum;
}

void boolConst(BytecodeSource& source, OutStream& oss)
{
	oss << "BOOL_CONST " << Utils::readVarInt(source);
}

void stringConst(BytecodeSource& source, OutStream& oss)
{
	oss << "STRING_CONST " << Utils::readVarInt(source);
}

void buildList(BytecodeSource& source, OutStream& oss)
{
	oss << "BUILD_LIST " << Utils::readVarInt(source);
}

void buildListWithDefault(BytecodeSource& source, OutStream& oss)
{
	oss << "BUILD_LIST_WITH_DEFAULT " << Utils::readVarInt(source);
}

void buildDictionary(BytecodeSource& source, OutStream& oss)
{
	oss << "BUILD_DICTIONARY " << Utils::readVarInt(source);
}

void buildDictionaryWithDefault(BytecodeSource& source, OutStream& oss)
{
	oss << "BUILD_DICTIONARY_WITH_DEFAULT " << Utils::readVarInt(source);
}

void buildCharacter(BytecodeSource& source, OutStream& oss)
{
	oss << "BUILD_CHARACTER ";
	auto numPairs = Utils::readVarInt(source);
	oss << numPairs << " ";
	
	for (int i = 0; i < numPairs; ++i)
	{
		oss << Utils::readVarInt(source);
		if (i < numPairs - 1)
		{
			oss << " ";
		}
	}
}

void getSequenceIndex(BytecodeSource& source, OutStream& oss)
{
	oss << "GET_SEQUENCE_INDEX [loop] " << (int)source.nextByte() << " [random] " << (int)source.nextByte()
		<< " [order] " << (int)source.nextByte() << " [num items] " << Utils::readVarInt(source);
}

void defaultConst(OutStream& oss)
{
	oss << "DEFAULT_CONST";
}

void line(OutStream& oss)
{
	oss << "LINE";
}

void choices(BytecodeSource& source, OutStream& oss)
{
	oss << "CHOICES " << Utils::readVarInt(source);
}

void gotoo(BytecodeSource& source, OutStream& oss)
{
	oss << "GOTO [Label] " << Utils::readVarInt(source);
}

void jumpForward(BytecodeSource& source, OutStream& oss)
{
	oss << "JUMP_FORWARD [bytes] " << Utils::readVarInt(source);
}

void popJumpIfTrue(BytecodeSource& source, OutStream& oss)
{
	oss << "POP_JUMP_IF_TRUE [bytes] " << Utils::readVarInt(source);
}

void popJumpIfFalse(BytecodeSource& source, OutStream& oss)
{
	oss << "POP_JUMP_IF_FALSE [bytes] " << Utils::readVarInt(source);
}

void jumpIfTrueOrPop(BytecodeSource& source, OutStream& oss)
{
	oss << "JUMP_IF_TRUE_OR_POP [bytes] " << Utils::readVarInt(source);
}

void jumpIfFalseOrPop(BytecodeSource& source, OutStream& oss)
{
	oss << "JUMP_IF_FALSE_OR_POP [bytes] " << Utils::readVarInt(source);
}

void pop(OutStream& oss)
{
	oss << "POP";
}

void duplicate(OutStream& oss)
{
	oss << "DUPLICATE";
}

void createVariable(BytecodeSource& source, OutStream& oss)
{
	oss << "CREATE_VARIABLE [Name] " << Utils::readVarInt(source);
}

void loadVariable(BytecodeSource& source, OutStream& oss)
{
	oss << "LOAD_VARIABLE [Container] " << Utils::readVarInt(source) << " [Name] " << Utils::readVarInt(source);
}

void store(OutStream& oss)
{
	oss << "STORE";
}

void getAttribute(BytecodeSource& source, OutStream& oss)
{
	oss << "GET_ATTRIBUTE [Name] " << Utils::readVarInt(source);
}

void call(BytecodeSource& source, OutStream& oss)
{
	oss << "CALL [Container] " << Utils::readVarInt(source) << " [num arguments] " << Utils::readVarInt(source);
}

void slice(BytecodeSource& source, OutStream& oss)
{
	oss << "SLICE [modifier] " << Utils::readVarInt(source);
}

void subscript(OutStream& oss)
{
	oss << "SUBSCRIPT";
}

void forIterable(BytecodeSource& source, OutStream& oss)
{
	oss << "FOR_ITERABLE [Variable] " << Utils::readVarInt(source) << " [Label after] " << Utils::readVarInt(source);
}

void event(OutStream& oss)
{
	oss << "EVENT";
}

void returnn(OutStream& oss)
{
	oss << "RETURN";
}

void returnValue(OutStream& oss)
{
	oss << "RETURN_VALUE";
}

void terminate(OutStream& oss)
{
	oss << "TERMINATE";
}

void beginMetadata(OutStream& oss)
{
	oss << "BEGIN_METADATA";
}

void endMetadata(OutStream& oss)
{
	oss << "END_METADATA\n";
}

void beginNames(BytecodeSource& source, OutStream& oss)
{
	oss << "BEGIN_NAMES ";
	auto numNames = Utils::readVarInt(source);
	oss << numNames << "\n";
	for (int n = 0; n < numNames; ++n)
	{
		printOffset(source, oss);
		
		int64_t id = Utils::readVarInt(source);
		int textLength = Utils::readVarInt(source);
		String text(textLength, '*');
		for (int i = 0; i < textLength; ++i)
		{
			text[i] = source.nextByte();
		}
		
		oss << id << " " << textLength << " " << text;
		if (n < numNames - 1)
		{
			oss << "\n";
		}
	}
}

void endNames(OutStream& oss)
{
	oss << "END_NAMES";
}

void beginStrings(BytecodeSource& source, OutStream& oss)
{
	oss << "BEGIN_STRINGS ";
	auto numStrings = Utils::readVarInt(source);
	oss << numStrings;
	if (numStrings > 0)
	{
		oss << "\n";
	}
	
	for (int n = 0; n < numStrings; ++n)
	{
		printOffset(source, oss);
		
		int64_t id = Utils::readVarInt(source);
		int textLength = Utils::readVarInt(source);
		String text(textLength, '*');
		for (int i = 0; i < textLength; ++i)
		{
			text[i] = source.nextByte();
		}
		
		oss << id << " " << textLength << " " << text;
		if (n < numStrings - 1)
		{
			oss << "\n";
		}
	}
}

void endStrings(OutStream& oss)
{
	oss << "END_STRINGS";
}

void beginContainers(BytecodeSource& source, OutStream& oss)
{
	oss << "BEGIN_CONTAINERS ";
	auto numContainers = Utils::readVarInt(source);
	oss << numContainers;
	if (numContainers > 0)
	{
		oss << "\n";
	}
	
	for (int n = 0; n < numContainers; ++n)
	{
		printOffset(source, oss);
		auto id = Utils::readVarInt(source);
		auto nameId = Utils::readVarInt(source);
		auto parentId = Utils::readVarInt(source);
		auto isNamespace = source.nextByte() == 0 ? false : true;
		auto numParameters = Utils::readVarInt(source);
		auto bytecodeOffset = Utils::readVarInt(source);
		auto bytecodeLength = Utils::readVarInt(source);
		
		oss << "[ID] " << id << " [Name] " << nameId << " [Parent] " << parentId << " [is namespace] " << isNamespace
			<< " [num parameters] " << numParameters << " [bytecode offset] " << bytecodeOffset << " [bytecode length] " << bytecodeLength << "\n";
		
		printOffset(source, oss);
		char byte = source.nextByte();
		ASSERT(("Expected BEGIN_VARIABLES_OP", byte == BEGIN_VARIABLES_OP));
		
		auto numVariables = Utils::readVarInt(source);
		oss << "BEGIN_VARIABLES " << numVariables << "\n";
		for (int nv = 0; nv < numVariables; ++nv)
		{
			printOffset(source, oss);
			auto nameId = Utils::readVarInt(source);
			int modifiers = source.nextByte();
			auto isFinal = modifiers;
			int isParameter = source.nextByte();
			
			oss << "[Name] " << nameId << " [is final] " << isFinal << " [is parameter] " << isParameter << "\n";
		}
		
		printOffset(source, oss);
		byte = source.nextByte();
		ASSERT(("Expected END_VARIABLES_OP", byte == END_VARIABLES_OP));
		oss << "END_VARIABLES\n";
		
		printOffset(source, oss);
		byte = source.nextByte();
		ASSERT(("Expected BEGIN_LABELS_OP", byte == BEGIN_LABELS_OP));
		
		auto numLabels = Utils::readVarInt(source);
		oss << "BEGIN_LABELS " << numLabels << "\n";
		for (int nl = 0; nl < numLabels; ++nl)
		{
			printOffset(source, oss);
			auto nameId = Utils::readVarInt(source);
			auto bytecodeOffset = Utils::readVarInt(source);
			
			oss << "[Name] " << nameId << " [bytecode offset] " << bytecodeOffset << "\n";
		}
		
		printOffset(source, oss);
		byte = source.nextByte();
		ASSERT(("Expected END_LABELS_OP", byte == END_LABELS_OP));
		oss << "END_LABELS";
		
		if (n < numContainers - 1)
		{
			oss << "\n";
		}
	}
}

void endContainers(OutStream& oss)
{
	oss << "END_CONTAINERS";
}

void beginBlock(BytecodeSource& source, OutStream& oss)
{
	oss << "BEGIN_BLOCK " << Utils::readVarInt(source);
}

void endBlock(OutStream& oss)
{
	oss << "END_BLOCK\n";
}

}