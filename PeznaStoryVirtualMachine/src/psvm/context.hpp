#ifndef PSVM_CONTEXT_H
#define PSVM_CONTEXT_H

#include "error_handler.hpp"
#include "metadata.hpp"
#include "objects.hpp"
#include "typedefs.hpp"

namespace PSVM
{

struct SequenceContext
{
	int64_t numVisits;
	Vector<int64_t> indexOrder;
	
	SequenceContext()
	:
		numVisits{0}
	{}
};

struct Frame
{
	ContainerMetadata* container;
	int64_t instructionPointer;
	Vector<SharedPtr<StoryObject>> stack;
	Map<int64_t, SharedPtr<Variable>> variables;
	
	Frame()
	:
		container{nullptr},
		instructionPointer{0}
	{}
	
	Frame(ContainerMetadata* container)
	:
		container(container),
		instructionPointer(container->bytecodeOffset)
	{}
	
	Frame(const Frame& other) = default;
	Frame(Frame&& other) = default;
	
	SharedPtr<StoryObject> popStack();
	SharedPtr<StoryObject>& peekStack();
	void pushStack(SharedPtr<StoryObject> object);
};

struct StoryContext
{
	Map<int64_t, Frame> activeFrames; // namespaces and currently running sections
	Vector<int64_t> callStack; //container IDs as keys into `activeFrames`
	Map<int64_t, SequenceContext> sequenceContexts; // Container ID to sequence context
	ErrorHandler* errorHandler;
	
	StoryContext(ErrorHandler* errorHandler)
	:
		errorHandler(errorHandler)
	{}
	StoryContext(const StoryContext& other) = default;
	StoryContext(StoryContext&& other) = default;
	
	void createAndPushFrame(ContainerMetadata& containerMetadata);
	Frame& getCurrentFrame();
	void popCurrentFrame();
	void popCurrentFrameWithReturnValue();
};

}

#endif // PSVM_CONTEXT_H