#ifndef PSVM_UTILS_H
#define PSVM_UTILS_H

#include <cstdint>

#include "bytecode_source.hpp"
#include "typedefs.hpp"

namespace PSVM::Utils
{

int64_t readVarInt(char* input, int* numBytesRead = nullptr);
int64_t readVarInt(BytecodeSource& source, int* numBytesRead = nullptr);
double numberToDecimal(int64_t num);
void escapeString(const String& str, OutStringStream& out);
void writeCodeString(const String& str, OutStringStream& out);
int64_t randomInt(int64_t max); // generate a random number between [0, max) exclusive

}

#endif // PSVM_UTILS_H