#ifndef PST_UTIL_DEFS_H
#define PST_UTIL_DEFS_H

#include <doctest.h>

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <memory>
#include <vector>

#include <language/nodes/nodes.hpp>
#include <language/compiler/value.hpp>

#ifndef PST_COMPILE_OUTPUT_FOLDER
	#define PST_COMPILE_OUTPUT_FOLDER ""
#endif
#ifndef PST_INPUT_FOLDER
	#define PST_INPUT_FOLDER "stories/"
#endif

namespace PST
{
using std::string;
using std::cout;
using std::ifstream;
using std::ostringstream;
using std::istringstream;
using std::shared_ptr;
using std::vector;

inline doctest::String toString(const PSL::TokenType& type)
{
	return PSL::Token::getTypeName(type).data();
}

inline doctest::String toString(const PSL::ExpressionType& type)
{
	return PSL::ExpressionNode::getTypeName(type).data();
}

inline doctest::String toString(const PSL::StatementType& type)
{
	return PSL::StatementNode::getTypeName(type).data();
}

inline doctest::String toString(const PSL::NodeType& type)
{
	return PSL::Node::getTypeName(type).data();
}

inline doctest::String toString(const PSL::ValueType& value)
{
	return value.toString().data();
}

}

#endif // PST_UTIL_DEFS_H