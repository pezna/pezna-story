#include <doctest.h>

#include <set>

#include "vm_manager.hpp"

#include <language/utils/byte_writer_utils.hpp>

#include <psvm/metadata.hpp>
#include <psvm/vm.hpp>
#include <psvm/utils.hpp>

namespace PST
{

TEST_CASE("Load Metadata w/ Labels but no Variables")
{
	TestErrorManager errorManager;
	TestBytecodeSource source(PST_COMPILE_OUTPUT_FOLDER "15_labels_and_gotos.bin");
	PSVM::StoryVirtualMachine vm(source);
	vm.load(&errorManager);
	
	CHECK_FALSE(errorManager.hasError());
	if (errorManager.hasError())
	{
		errorManager.printErrors();
	}
	
	std::vector<string> names = { "first", "main", "second", "~namespace~" };
	
	REQUIRE(vm.metadata().numNames == 4);
	auto& metadataNames = vm.metadata().names;
	auto found1 = std::find_if(metadataNames.begin(), metadataNames.end(), [&names](const auto& item) {return item.second == names[0];});
	auto found2 = std::find_if(metadataNames.begin(), metadataNames.end(), [&names](const auto& item) {return item.second == names[1];});
	auto found3 = std::find_if(metadataNames.begin(), metadataNames.end(), [&names](const auto& item) {return item.second == names[2];});
	auto found4 = std::find_if(metadataNames.begin(), metadataNames.end(), [&names](const auto& item) {return item.second == names[3];});
	
	CHECK(found1 != metadataNames.end());
	CHECK(found2 != metadataNames.end());
	CHECK(found3 != metadataNames.end());
	CHECK(found4 != metadataNames.end());
	
	REQUIRE(vm.metadata().numContainers == 2);
	PSVM::ContainerMetadata& firstContainer = vm.metadata().containers[1651];
	PSVM::ContainerMetadata& secondContainer = vm.metadata().containers[62629];
	
	CHECK(firstContainer.id > 0);
	CHECK(secondContainer.id > 0);
	CHECK(secondContainer.id != firstContainer.id);
	CHECK(firstContainer.nameId != secondContainer.nameId);
	CHECK(firstContainer.parentId == 0);
	CHECK(secondContainer.parentId == firstContainer.id);
	CHECK(firstContainer.isNamespace == true);
	CHECK(secondContainer.isNamespace == false);
	CHECK(firstContainer.numParameters == 0);
	CHECK(secondContainer.numParameters == 0);
	CHECK(firstContainer.bytecodeOffset == 0);
	CHECK(secondContainer.bytecodeOffset > firstContainer.bytecodeOffset);
	CHECK(secondContainer.bytecodeLength > firstContainer.bytecodeLength);
	CHECK(firstContainer.numVariables == 0);
	CHECK(firstContainer.numLabels == 0);
	CHECK(secondContainer.numVariables == 0);
	REQUIRE(secondContainer.numLabels == 2);
	
	PSVM::LabelMetadata& firstLabel = secondContainer.labels[61015];
	PSVM::LabelMetadata& secondLabel = secondContainer.labels[4457];
	CHECK(firstLabel.containerId == secondContainer.id);
	CHECK(secondLabel.containerId == secondContainer.id);
	CHECK(firstLabel.nameId != secondLabel.nameId);
	CHECK(firstLabel.bytecodeOffset != secondLabel.bytecodeOffset);
}

TEST_CASE("Load Metadata w/ Variables but no Labels")
{
	TestErrorManager errorManager;
	TestBytecodeSource source(PST_COMPILE_OUTPUT_FOLDER "11_sections.bin");
	PSVM::StoryVirtualMachine vm(source);
	vm.load(&errorManager);
	
	CHECK_FALSE(errorManager.hasError());
	if (errorManager.hasError())
	{
		errorManager.printErrors();
	}
	
	REQUIRE(vm.metadata().numContainers == 6);
	PSVM::ContainerMetadata& firstContainer = vm.metadata().containers[1651];
	PSVM::ContainerMetadata& secondContainer = vm.metadata().containers[62629];
	PSVM::ContainerMetadata& thirdContainer = vm.metadata().containers[12314];
	PSVM::ContainerMetadata& fourthContainer = vm.metadata().containers[30125];
	PSVM::ContainerMetadata& fifthContainer = vm.metadata().containers[56756];
	PSVM::ContainerMetadata& sixthContainer = vm.metadata().containers[40309];
	
	// first container checks
	CHECK(firstContainer.id > 0);
	CHECK(firstContainer.numLabels == 0);
	REQUIRE(firstContainer.numVariables == 5);
	
	PSVM::VariableMetadata& variable_0_0 = firstContainer.variables[41938];
	PSVM::VariableMetadata& variable_0_1 = firstContainer.variables[26571];
	PSVM::VariableMetadata& variable_0_2 = firstContainer.variables[2497];
	PSVM::VariableMetadata& variable_0_3 = firstContainer.variables[61628];
	PSVM::VariableMetadata& variable_0_4 = firstContainer.variables[59798];
	
	CHECK(variable_0_0.containerId == firstContainer.id);
	CHECK(variable_0_0.isFinal == false);
	CHECK(variable_0_0.isParameter == false);
	CHECK(variable_0_1.containerId == firstContainer.id);
	CHECK(variable_0_1.isFinal == false);
	CHECK(variable_0_1.isParameter == false);
	CHECK(variable_0_2.containerId == firstContainer.id);
	CHECK(variable_0_2.isFinal == false);
	CHECK(variable_0_2.isParameter == false);
	CHECK(variable_0_3.containerId == firstContainer.id);
	CHECK(variable_0_3.isFinal == true);
	CHECK(variable_0_3.isParameter == false);
	
	// second container checks
	CHECK(secondContainer.id > 0);
	CHECK(secondContainer.numLabels == 0);
	CHECK(secondContainer.numVariables == 0);
	
	// third container checks
	CHECK(thirdContainer.id > 0);
	// don't check for labels since choices create labels
	REQUIRE(thirdContainer.numVariables == 2);
	
	PSVM::VariableMetadata& variable_2_0 = thirdContainer.variables[59004];
	PSVM::VariableMetadata& variable_2_1 = thirdContainer.variables[59798];
	
	CHECK(variable_2_0.containerId == thirdContainer.id);
	CHECK(variable_2_0.isFinal == false);
	CHECK(variable_2_0.isParameter == true);
	CHECK(variable_2_1.containerId == thirdContainer.id);
	CHECK(variable_2_1.isFinal == false);
	CHECK(variable_2_1.isParameter == false);
	
	// fourth container checks
	CHECK(fourthContainer.id > 0);
	CHECK(fourthContainer.numLabels == 0);
	REQUIRE(fourthContainer.numVariables == 3);
	
	PSVM::VariableMetadata& variable_3_0 = fourthContainer.variables[54121];
	PSVM::VariableMetadata& variable_3_1 = fourthContainer.variables[1021];
	PSVM::VariableMetadata& variable_3_2 = fourthContainer.variables[1767];
	
	CHECK(variable_3_0.containerId == fourthContainer.id);
	CHECK(variable_3_0.isFinal == false);
	CHECK(variable_3_0.isParameter == false);
	CHECK(variable_3_1.containerId == fourthContainer.id);
	CHECK(variable_3_1.isFinal == true);
	CHECK(variable_3_1.isParameter == false);
	CHECK(variable_3_2.containerId == fourthContainer.id);
	CHECK(variable_3_2.isFinal == true);
	CHECK(variable_3_2.isParameter == false);
	
	// fifth container checks
	CHECK(fifthContainer.id > 0);
	CHECK(fifthContainer.numLabels == 0);
	REQUIRE(fifthContainer.numVariables == 4);
	
	PSVM::VariableMetadata& variable_4_0 = fifthContainer.variables[48707];
	PSVM::VariableMetadata& variable_4_1 = fifthContainer.variables[61433];
	PSVM::VariableMetadata& variable_4_2 = fifthContainer.variables[57199];
	PSVM::VariableMetadata& variable_4_3 = fifthContainer.variables[50572];
	
	CHECK(variable_4_0.containerId == fifthContainer.id);
	CHECK(variable_4_0.isFinal == false);
	CHECK(variable_4_0.isParameter == true);
	CHECK(variable_4_1.containerId == fifthContainer.id);
	CHECK(variable_4_1.isFinal == true);
	CHECK(variable_4_1.isParameter == true);
	CHECK(variable_4_2.containerId == fifthContainer.id);
	CHECK(variable_4_2.isFinal == false);
	CHECK(variable_4_2.isParameter == true);
	CHECK(variable_4_3.containerId == fifthContainer.id);
	CHECK(variable_4_3.isFinal == true);
	CHECK(variable_4_3.isParameter == false);
	
	// sixth container checks
	CHECK(sixthContainer.id > 0);
	CHECK(sixthContainer.numLabels == 0);
	CHECK(sixthContainer.numVariables == 0);
}

}