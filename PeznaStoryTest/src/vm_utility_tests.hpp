#include <doctest.h>

#include <vector>
#include <streambuf>
#include <istream>
#include <tuple>
#include <cstdint>

#include "vm_manager.hpp"
#include <language/utils/byte_writer_utils.hpp>
#include <psvm/utils.hpp>

namespace PST
{

TEST_CASE("Read Variable Integer")
{
	int64_t n1 = 284548;
	int64_t n2 = 1;
	int64_t n3 = 0;
	int64_t n4 = 4294967295;
	int64_t n5 = 7295;
	int64_t n6 = 651295;
	int64_t n7 = -651295;
	int64_t n8 = -1;
	
	vector<int8_t> out;
	
	PSL::Utils::writeVarInt(n1, out);
	CHECK(PSVM::Utils::readVarInt((char*)out.data()) == n1);
	CHECK(out.size() == 3);
	out.clear();
	
	PSL::Utils::writeVarInt(n2, out);
	CHECK(PSVM::Utils::readVarInt((char*)out.data()) == n2);
	CHECK(out.size() == 1);
	out.clear();
	
	PSL::Utils::writeVarInt(n3, out);
	CHECK(PSVM::Utils::readVarInt((char*)out.data()) == n3);
	CHECK(out.size() == 1);
	out.clear();
	
	PSL::Utils::writeVarInt(n4, out);
	CHECK(PSVM::Utils::readVarInt((char*)out.data()) == n4);
	CHECK(out.size() == 5);
	out.clear();
	
	PSL::Utils::writeVarInt(n5, out);
	CHECK(PSVM::Utils::readVarInt((char*)out.data()) == n5);
	CHECK(out.size() == 2);
	out.clear();
	
	int numBytesRead = 0;
	PSL::Utils::writeVarInt(n6, out);
	CHECK(PSVM::Utils::readVarInt((char*)out.data(), &numBytesRead) == n6);
	CHECK(out.size() == 3);
	CHECK(numBytesRead == 3);
	out.clear();
	
	PSL::Utils::writeVarInt(n7, out);
	CHECK(PSVM::Utils::readVarInt((char*)out.data()) == n7);
	CHECK(out.size() == 10);
	out.clear();
	
	PSL::Utils::writeVarInt(n8, out);
	CHECK(PSVM::Utils::readVarInt((char*)out.data()) == n8);
	CHECK(out.size() == 10);
	out.clear();
}

TEST_CASE("Number To Decimal")
{
	int64_t decimal = 20;
	double d = PSVM::Utils::numberToDecimal(decimal);
	CHECK(d == 0.20);
	
	decimal = 8125;
	d = PSVM::Utils::numberToDecimal(decimal);
	CHECK(d == 0.8125);
	
	decimal = 9223372037;
	d = PSVM::Utils::numberToDecimal(decimal);
	CHECK(d == 0.9223372037);
}

TEST_CASE("Escape String")
{
	const string s1 = "Yo \r\n\v\t\" is anyone there?";
	std::ostringstream oss;
	PSVM::Utils::escapeString(s1, oss);
	CHECK(oss.str() == "Yo \\r\\n\\v\\t\\\" is anyone there?");
	
	oss.clear();
	oss.str("");
	PSVM::Utils::writeCodeString(s1, oss);
	CHECK(oss.str() == "\"Yo \\r\\n\\v\\t\\\" is anyone there?\"");
}

}