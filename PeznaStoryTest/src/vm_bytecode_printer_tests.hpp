#include <doctest.h>

#include <fstream>
#include <sstream>

#include <psvm/bytecode_printer.hpp>

#include "vm_manager.hpp"

namespace PST
{

TEST_CASE("Print Events")
{
	TestBytecodeSource source(PST_COMPILE_OUTPUT_FOLDER "09_data_structures.bin");
	std::ofstream out(PST_COMPILE_OUTPUT_FOLDER "printed_09_data_structures.txt", std::ofstream::out);
	
	PSVM::BytecodePrinter::print(source, out);
	
	out.close();
}

TEST_CASE("Print Events")
{
	TestBytecodeSource source(PST_COMPILE_OUTPUT_FOLDER "10_events.bin");
	std::ofstream out(PST_COMPILE_OUTPUT_FOLDER "printed_10_events.txt", std::ofstream::out);
	
	PSVM::BytecodePrinter::print(source, out);
	
	out.close();
}

TEST_CASE("Print Sections")
{
	TestBytecodeSource source(PST_COMPILE_OUTPUT_FOLDER "11_sections.bin");
	std::ofstream out(PST_COMPILE_OUTPUT_FOLDER "printed_11_sections.txt", std::ofstream::out);
	
	PSVM::BytecodePrinter::print(source, out);
	
	out.close();
}

TEST_CASE("Print Namespaces")
{
	TestBytecodeSource source(PST_COMPILE_OUTPUT_FOLDER "12_namespaces.bin");
	std::ofstream out(PST_COMPILE_OUTPUT_FOLDER "printed_12_namespaces.txt", std::ofstream::out);
	
	PSVM::BytecodePrinter::print(source, out);
	
	out.close();
}

TEST_CASE("Print Labels and Goto")
{
	TestBytecodeSource source(PST_COMPILE_OUTPUT_FOLDER "15_labels_and_gotos.bin");
	std::ofstream out(PST_COMPILE_OUTPUT_FOLDER "printed_15_labels_and_gotos.txt", std::ofstream::out);
	
	PSVM::BytecodePrinter::print(source, out);
	
	out.close();
}

TEST_CASE("Print Default Expressions")
{
	TestBytecodeSource source(PST_COMPILE_OUTPUT_FOLDER "17_default_expressions.bin");
	std::ofstream out(PST_COMPILE_OUTPUT_FOLDER "printed_17_default_expressions.txt", std::ofstream::out);
	
	PSVM::BytecodePrinter::print(source, out);
	
	out.close();
}

}