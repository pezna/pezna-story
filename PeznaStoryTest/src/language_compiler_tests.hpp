#include <doctest.h>

#include <fstream>
#include <iostream>
#include <chrono>

#include <ghc/filesystem.hpp>

#include <language/exceptions.hpp>
#include <language/lexer.hpp>
#include <language/parser.hpp>
#include <language/compiler/compiler.hpp>
#include <language/compiler/value.hpp>
#include <language/compiler/story_manager.hpp>
#include <language/visitors/node_register.hpp>

namespace PST
{

TEST_CASE("Compile Sections")
{
	string fileLocation = PST_INPUT_FOLDER "11_sections.story";
	ifstream filestream(fileLocation);
	PSL::Lexer lexer(fileLocation, filestream);
	PSL::Parser parser(fileLocation, lexer.lex());

	shared_ptr<PSL::StoryNode> story = parser.parse();
	ghc::filesystem::path storyPath = ghc::filesystem::path(fileLocation);
	story->filepath = ghc::filesystem::absolute(storyPath.lexically_normal()).string();

	PSL::Compiler compiler(story);
	compiler.compile();

	std::ofstream out(PST_COMPILE_OUTPUT_FOLDER "11_sections.bin", std::ofstream::out | std::ofstream::binary);
	compiler.output(out);
	out.close();
}

TEST_CASE("Compile Labels and Goto")
{
	string fileLocation = PST_INPUT_FOLDER "15_labels_and_gotos.story";
	ifstream filestream(fileLocation);
	PSL::Lexer lexer(fileLocation, filestream);
	PSL::Parser parser(fileLocation, lexer.lex());

	shared_ptr<PSL::StoryNode> story = parser.parse();
	ghc::filesystem::path storyPath = ghc::filesystem::path(fileLocation);
	story->filepath = ghc::filesystem::absolute(storyPath.lexically_normal()).string();

	PSL::Compiler compiler(story);
	compiler.compile();

	std::ofstream out(PST_COMPILE_OUTPUT_FOLDER "15_labels_and_gotos.bin", std::ofstream::out | std::ofstream::binary);
	compiler.output(out);
	out.close();
}

// TEST_CASE("Big File Compile")
// {
// 	auto start = std::chrono::system_clock::now();
// 	string fileLocation = PST_INPUT_FOLDER "16_big_file.story";
// 	ifstream filestream(fileLocation);
// 	PSL::Lexer lexer(fileLocation, filestream);
// 	PSL::Parser parser(fileLocation, lexer.lex());
// 	auto end = std::chrono::system_clock::now();
// 	auto diffMillis = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
// 	std::cout << "finished lexing - elapsed " << diffMillis << "ms" << std::endl;
// 	start = std::chrono::system_clock::now();
// 	shared_ptr<PSL::StoryNode> story = parser.parse();
// 	end = std::chrono::system_clock::now();
// 	diffMillis = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
// 	std::cout << "finished parsing - elapsed " << diffMillis << "ms" << std::endl;
// 	ghc::filesystem::path storyPath = ghc::filesystem::path(fileLocation);
// 	story->filepath = ghc::filesystem::absolute(storyPath.lexically_normal()).string();
	
// 	start = std::chrono::system_clock::now();
// 	PSL::Compiler compiler(story);
// 	compiler.compile();
// 	end = std::chrono::system_clock::now();
// 	diffMillis = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
// 	std::cout << "finished compiling - elapsed " << diffMillis << "ms" << std::endl;
	
// 	start = std::chrono::system_clock::now();
// 	std::ofstream out(PST_COMPILE_OUTPUT_FOLDER "16_big_file.bin", std::ofstream::out | std::ofstream::binary);
// 	compiler.output(out);
// 	out.close();
// 	end = std::chrono::system_clock::now();
// 	diffMillis = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
// 	std::cout << "finished saving bytecode - elapsed " << diffMillis << "ms" << std::endl;
// }

TEST_CASE("Compile Namespaces")
{
	string fileLocation = PST_INPUT_FOLDER "12_namespaces.story";
	ifstream filestream(fileLocation);
	PSL::Lexer lexer(fileLocation, filestream);
	PSL::Parser parser(fileLocation, lexer.lex());

	shared_ptr<PSL::StoryNode> story = parser.parse();
	ghc::filesystem::path storyPath = ghc::filesystem::path(fileLocation);
	story->filepath = ghc::filesystem::absolute(storyPath.lexically_normal()).string();

	PSL::Compiler compiler(story);
	compiler.compile();

	std::ofstream out(PST_COMPILE_OUTPUT_FOLDER "12_namespaces.bin", std::ofstream::out | std::ofstream::binary);
	compiler.output(out);
	out.close();
}

TEST_CASE("Compile Default Expressions")
{
	string fileLocation = PST_INPUT_FOLDER "17_default_expressions.story";
	ifstream filestream(fileLocation);
	PSL::Lexer lexer(fileLocation, filestream);
	PSL::Parser parser(fileLocation, lexer.lex());
	
	shared_ptr<PSL::StoryNode> story = parser.parse();
	ghc::filesystem::path storyPath = ghc::filesystem::path(fileLocation);
	story->filepath = ghc::filesystem::absolute(storyPath.lexically_normal()).string();
	
	PSL::Compiler compiler(story);
	compiler.compile();
	
	std::ofstream out(PST_COMPILE_OUTPUT_FOLDER "17_default_expressions.bin", std::ofstream::out | std::ofstream::binary);
	compiler.output(out);
	out.close();
}

TEST_CASE("Compile Events")
{
	string fileLocation = PST_INPUT_FOLDER "10_events.story";
	ifstream filestream(fileLocation);
	PSL::Lexer lexer(fileLocation, filestream);
	PSL::Parser parser(fileLocation, lexer.lex());
	
	shared_ptr<PSL::StoryNode> story = parser.parse();
	ghc::filesystem::path storyPath = ghc::filesystem::path(fileLocation);
	story->filepath = ghc::filesystem::absolute(storyPath.lexically_normal()).string();
	
	PSL::Compiler compiler(story);
	compiler.compile();
	
	std::ofstream out(PST_COMPILE_OUTPUT_FOLDER "10_events.bin", std::ofstream::out | std::ofstream::binary);
	compiler.output(out);
	out.close();
}

TEST_CASE("Compile Data Structures")
{
	string fileLocation = PST_INPUT_FOLDER "09_data_structures.story";
	ifstream filestream(fileLocation);
	PSL::Lexer lexer(fileLocation, filestream);
	PSL::Parser parser(fileLocation, lexer.lex());
	
	shared_ptr<PSL::StoryNode> story = parser.parse();
	ghc::filesystem::path storyPath = ghc::filesystem::path(fileLocation);
	story->filepath = ghc::filesystem::absolute(storyPath.lexically_normal()).string();
	
	PSL::Compiler compiler(story);
	compiler.compile();
	
	std::ofstream out(PST_COMPILE_OUTPUT_FOLDER "09_data_structures.bin", std::ofstream::out | std::ofstream::binary);
	compiler.output(out);
	out.close();
}

}
