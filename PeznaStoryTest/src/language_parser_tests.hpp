#include <doctest.h>
#include <utf8.h>

#include <language/lexer.hpp>
#include <language/nodes/nodes.hpp>
#include <language/parser.hpp>
#include <language/visitors/node_printer.hpp>
#include <language/utils/parser_utils.hpp>
#include <language/utils/string_utils.hpp>

namespace PST
{

TEST_CASE("Unicode IDs")
{
	REQUIRE(PSL::Utils::isIdentifierStart(0x47) == true);
	REQUIRE(PSL::Utils::isIdentifierStart(0xF900) == true);
	REQUIRE(PSL::Utils::isIdentifierStart(0x32) == false);
	REQUIRE(PSL::Utils::isIdentifierPart(0x32) == true);
	REQUIRE(PSL::Utils::isIdentifierPart(0x10) == false);
}

TEST_CASE("String Utils")
{
	vector<string> vecStr = {"Hello", "Matey", "!"};
	ostringstream oss;
	oss << vecStr;
	REQUIRE(oss.str() == "[Hello, Matey, !]");
}

TEST_CASE("Lexer Simple Expressions")
{
	string filepath = PST_INPUT_FOLDER "01_simple_expressions.story";
	ifstream filestream(filepath);
	PSL::Lexer lexer(filepath, filestream);
	vector<shared_ptr<PSL::Token>> tokens = lexer.lex();

	//x
	REQUIRE(tokens[0]->line == 1);
	REQUIRE(tokens[0]->column == 1);
	//0x81A24F
	REQUIRE(tokens[14]->line == 4);
	REQUIRE(tokens[14]->column == 5);
	//0b10101
	REQUIRE(tokens[18]->line == 5);
	REQUIRE(tokens[18]->column == 8);

	REQUIRE(tokens.size() == 19);
}

TEST_CASE("Lexer Big String Expressions")
{
	string filepath = PST_INPUT_FOLDER "02_big_string_expressions.story";
	ifstream filestream(filepath);
	PSL::Lexer lexer(filepath, filestream);
	vector<shared_ptr<PSL::Token>> tokens = lexer.lex();

	REQUIRE(tokens.size() == 37);
	REQUIRE(tokens[16]->text == "\"Nunc est turpis, efficitur pharetra hendrerit at, vestibulum non enim. Praesent faucibus maximus justo ullamcorper commodo. Cras sed vehicula leo, quis consectetur sem. Etiam ut consequat ex, tempor consectetur lorem. Curabitur varius nisi in elit vehicula sollicitudin. Nam pharetra tincidunt urna sed vulputate. Aenean pretium vel ante sed sollicitudin. Suspendisse ornare lacus euismod, tincidunt augue a, suscipit ipsum. Integer suscipit iaculis velit, ut placerat est bibendum eu.\"");
	REQUIRE(tokens[30]->text == "\"Phasellus in auctor elit. Ut porta justo vitae magna tristique, sit amet molestie nisi feugiat. Nunc pellentesque nibh et lectus tempor varius. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean ultrices metus nec mauris lacinia auctor. Curabitur et augue sem. Maecenas vitae justo viverra, faucibus dolor et, maximus dolor. Morbi semper eros vitae semper vestibulum. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed eu egestas sapien, nec eleifend erat. Nam pretium, augue sit amet facilisis imperdiet, magna ipsum interdum orci, sit amet tempus velit dolor maximus nunc. Curabitur commodo nisi in sapien malesuada faucibus. Donec laoreet lacus id arcu semper iaculis. Suspendisse fermentum placerat sodales.\"");
	REQUIRE(tokens[16]->column == 1);
	REQUIRE(tokens[30]->column == 1);
}

TEST_CASE("Lexer String Multi-line")
{
	string filepath = PST_INPUT_FOLDER "03_string_multiline.story";
	ifstream filestream(filepath);
	PSL::Lexer lexer(filepath, filestream);
	vector<shared_ptr<PSL::Token>> tokens = lexer.lex();

	//cout << tokens << std::endl;

	REQUIRE(tokens.size() == 15);
	REQUIRE(tokens[0]->type == PSL::TokenType::StringLiteral);
	REQUIRE(tokens[0]->text == "\"Single line string\"");
	REQUIRE(tokens[0]->line == 1);
	REQUIRE(tokens[0]->column == 1);
	REQUIRE(tokens[1]->type == PSL::TokenType::Newline);
	REQUIRE(tokens[2]->type == PSL::TokenType::StringLiteral);
	REQUIRE(tokens[2]->text == "\"\"\"\nThis is multi-line\n\"\"\"");
	REQUIRE(tokens[2]->line == 3);
	REQUIRE(tokens[2]->column == 1);
	REQUIRE(tokens[3]->type == PSL::TokenType::Newline);
	REQUIRE(tokens[4]->type == PSL::TokenType::StringLiteral);
	REQUIRE(tokens[4]->text == "\"\"\"Start right after\nend right before\"\"\"");
	REQUIRE(tokens[4]->line == 7);
	REQUIRE(tokens[4]->column == 1);
	REQUIRE(tokens[5]->type == PSL::TokenType::Newline);
	REQUIRE(tokens[6]->type == PSL::TokenType::StringLiteral);
	REQUIRE(tokens[6]->text == "'''\nHere\nwe\n\ngo\nnow!\n'''");
	REQUIRE(tokens[6]->line == 10);
	REQUIRE(tokens[6]->column == 1);
	REQUIRE(tokens[7]->type == PSL::TokenType::Newline);
	REQUIRE(tokens[8]->type == PSL::TokenType::StringLiteral);
	REQUIRE(tokens[8]->text == "\"\"\"\n\tyolo\t\n\"\"\"");
	REQUIRE(tokens[8]->line == 18);
	REQUIRE(tokens[8]->column == 1);
	REQUIRE(tokens[9]->type == PSL::TokenType::Newline);
	REQUIRE(tokens[10]->type == PSL::TokenType::StringLiteral);
	REQUIRE(tokens[10]->text == "'Another single line'");
	REQUIRE(tokens[10]->line == 22);
	REQUIRE(tokens[10]->column == 1);
	REQUIRE(tokens[11]->type == PSL::TokenType::Newline);
	REQUIRE(tokens[12]->type == PSL::TokenType::StringLiteral);
	REQUIRE(tokens[12]->text == "\"\"\"\nThere are 'some' \"quote\" marks here.\nA straggling \"\n\"\"\"");
	REQUIRE(tokens[12]->line == 24);
	REQUIRE(tokens[12]->column == 1);
	REQUIRE(tokens[13]->type == PSL::TokenType::Newline);
	REQUIRE(tokens[14]->type == PSL::TokenType::StringLiteral);
	REQUIRE(tokens[14]->text == "'''\nMore 'quote' \"marks\" '\n'''");
	REQUIRE(tokens[14]->line == 29);
	REQUIRE(tokens[14]->column == 1);
}

TEST_CASE("Lexer Complex Expressions")
{
	string filepath = PST_INPUT_FOLDER "04_complex_expressions.story";
	ifstream filestream(filepath);
	PSL::Lexer lexer(filepath, filestream);
	vector<shared_ptr<PSL::Token>> tokens = lexer.lex();

	REQUIRE(tokens.size() == 117);

	REQUIRE(tokens[11]->type == PSL::TokenType::Name);
	REQUIRE(tokens[11]->text == "have");
	REQUIRE(tokens[11]->line == 3);
	REQUIRE(tokens[11]->column == 13);
	REQUIRE(tokens[51]->type == PSL::TokenType::Pow);
	REQUIRE(tokens[51]->text == "**");
	REQUIRE(tokens[51]->line == 9);
	REQUIRE(tokens[51]->column == 27);
	REQUIRE(tokens[106]->type == PSL::TokenType::IntLiteral);
	REQUIRE(tokens[106]->text == "54898982");
	REQUIRE(tokens[106]->line == 17);
	REQUIRE(tokens[106]->column == 54);
	REQUIRE(tokens[110]->type == PSL::TokenType::BoolLiteral);
	REQUIRE(tokens[110]->text == "true");
	REQUIRE(tokens[110]->line == 19);
	REQUIRE(tokens[110]->column == 8);
}

TEST_CASE("Lexer All PSL::Tokens")
{
	string filepath = PST_INPUT_FOLDER "06_all_tokens.story";
	ifstream filestream(filepath);
	PSL::Lexer lexer(filepath, filestream);
	vector<shared_ptr<PSL::Token>> tokens = lexer.lex();

	PSL::TokenType tokenTypes[] = {
			PSL::TokenType::Story, PSL::TokenType::Section, PSL::TokenType::Include, PSL::TokenType::Continue, PSL::TokenType::Return, PSL::TokenType::End, PSL::TokenType::Choice, PSL::TokenType::If, PSL::TokenType::Else, PSL::TokenType::ElseIf,
			PSL::TokenType::While, PSL::TokenType::For, PSL::TokenType::Do, PSL::TokenType::Goto, PSL::TokenType::Exit, PSL::TokenType::Label, PSL::TokenType::Event, PSL::TokenType::Character, PSL::TokenType::Sequence, PSL::TokenType::Newline, PSL::TokenType::Arrow, PSL::TokenType::Equals,
			PSL::TokenType::NotEquals, PSL::TokenType::GreaterThanEquals, PSL::TokenType::LessThanEquals, PSL::TokenType::GreaterThan, PSL::TokenType::LessThan, PSL::TokenType::Not, PSL::TokenType::Not, PSL::TokenType::And, PSL::TokenType::And, PSL::TokenType::Or, PSL::TokenType::Or, PSL::TokenType::In,
			PSL::TokenType::NotIn, PSL::TokenType::Is, PSL::TokenType::IsNot, PSL::TokenType::Pow, PSL::TokenType::Add, PSL::TokenType::Sub, PSL::TokenType::Mul, PSL::TokenType::Div, PSL::TokenType::Mod, PSL::TokenType::BitAnd, PSL::TokenType::BitXor, PSL::TokenType::BitOr, PSL::TokenType::BitNot,
			PSL::TokenType::BitLeftShift, PSL::TokenType::BitRightShift, PSL::TokenType::Assign, PSL::TokenType::PowAssign, PSL::TokenType::AddAssign, PSL::TokenType::SubAssign, PSL::TokenType::MulAssign, PSL::TokenType::DivAssign, PSL::TokenType::ModAssign, PSL::TokenType::BitAndAssign,
			PSL::TokenType::BitXorAssign, PSL::TokenType::BitOrAssign, PSL::TokenType::BitLeftShiftAssign, PSL::TokenType::BitRightShiftAssign, PSL::TokenType::OpenBrack, PSL::TokenType::CloseBrack, PSL::TokenType::OpenParen, PSL::TokenType::CloseParen, PSL::TokenType::OpenBrace,
			PSL::TokenType::CloseBrace, PSL::TokenType::Comma, PSL::TokenType::Colon, PSL::TokenType::DoubleColon, PSL::TokenType::SemiColon, PSL::TokenType::Dot, PSL::TokenType::Ternary, PSL::TokenType::BoolLiteral, PSL::TokenType::BoolLiteral,
			PSL::TokenType::IntLiteral, PSL::TokenType::IntLiteral, PSL::TokenType::IntLiteral, PSL::TokenType::IntLiteral, PSL::TokenType::FloatLiteral, PSL::TokenType::StringLiteral, PSL::TokenType::StringLiteral, PSL::TokenType::StringLiteral, PSL::TokenType::StringLiteral, PSL::TokenType::Name, PSL::TokenType::Newline, PSL::TokenType::IgnoreNewline, PSL::TokenType::Var
	};

	REQUIRE(tokens.size() == 88);

	for (int64_t i = 0; i < tokens.size(); ++i)
	{
		CHECK(tokens[i]->type == tokenTypes[i]);
	}
}

TEST_CASE("Parser Simple Expressions")
{
	string filepath = PST_INPUT_FOLDER "01_simple_expressions.story";
	ifstream filestream(filepath);
	PSL::Lexer lexer(filepath, filestream);
	PSL::Parser parser(filepath, lexer.lex());

	shared_ptr<PSL::StoryNode> story = parser.parse();

	REQUIRE(story->mainNamespace->sections.size() == 0);
	REQUIRE(story->mainNamespace->statements.size() == 4);
	REQUIRE(story->mainNamespace->statements[0]->statementType() == PSL::StatementType::AssignmentStatement);
	REQUIRE(story->mainNamespace->statements[1]->statementType() == PSL::StatementType::AssignmentStatement);
	REQUIRE(story->mainNamespace->statements[2]->statementType() == PSL::StatementType::AssignmentStatement);
	REQUIRE(story->mainNamespace->statements[3]->statementType() == PSL::StatementType::AssignmentStatement);

	string data[] = {"x", u8"s你itioni们好n", u8"כֹּ֚האָמַ֣ריְהוָ֔ה", "n"};

	for (size_t i = 0; i < story->mainNamespace->statements.size(); ++i)
	{
		shared_ptr<PSL::StatementNode>& stmt = story->mainNamespace->statements[i];
		PSL::AssignmentStatementNode* assign = static_cast<PSL::AssignmentStatementNode*>(stmt.get());

		REQUIRE(assign->left->expressionType() == PSL::ExpressionType::AtomPrimary);
		PSL::AtomPrimaryNode* left = static_cast<PSL::AtomPrimaryNode*>(assign->left.get());
		REQUIRE(left->token->text == data[i]);
	}
}

TEST_CASE("Parser Big String Expression")
{
	string filepath = PST_INPUT_FOLDER "02_big_string_expressions.story";
	ifstream filestream(filepath);
	PSL::Lexer lexer(filepath, filestream);
	PSL::Parser parser(filepath, lexer.lex());

	shared_ptr<PSL::StoryNode> story = parser.parse();
	REQUIRE(story->mainNamespace->sections.size() == 0);
	REQUIRE(story->mainNamespace->statements.size() == 1);
	REQUIRE(story->mainNamespace->statements[0]->statementType() == PSL::StatementType::AssignmentStatement);

	PSL::AssignmentStatementNode* assign = static_cast<PSL::AssignmentStatementNode*>(story->mainNamespace->statements[0].get());

	REQUIRE(assign->right->expressionType() == PSL::ExpressionType::AdditiveExpression);
	PSL::BinaryExpressionNode* right = static_cast<PSL::BinaryExpressionNode*>(assign->right.get());
	REQUIRE(right->left->expressionType() == PSL::ExpressionType::AdditiveExpression);
	REQUIRE(right->op->type == PSL::TokenType::Add);
	REQUIRE(right->right->expressionType() == PSL::ExpressionType::AtomPrimary);
	PSL::AtomPrimaryNode* lastString = static_cast<PSL::AtomPrimaryNode*>(right->right.get());
	REQUIRE(lastString->token->text == "\"Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus feugiat hendrerit venenatis. Curabitur id sapien vitae libero semper ultricies ut et justo. Ut pharetra massa sit amet sollicitudin sodales. Etiam lobortis leo scelerisque enim sagittis hendrerit. Quisque quis maximus mi. Mauris eu nullam.\"");
}

TEST_CASE("Parser Complex Expressions")
{
	string filepath = PST_INPUT_FOLDER "04_complex_expressions.story";
	ifstream filestream(filepath);
	PSL::Lexer lexer(filepath, filestream);
	PSL::Parser parser(filepath, lexer.lex());

	shared_ptr<PSL::StoryNode> story = parser.parse();
	REQUIRE(story->mainNamespace->sections.size() == 0);
	REQUIRE(story->mainNamespace->statements.size() == 6);
	REQUIRE(story->mainNamespace->statements[0]->statementType() == PSL::StatementType::AssignmentStatement);
	REQUIRE(story->mainNamespace->statements[1]->statementType() == PSL::StatementType::AssignmentStatement);
	REQUIRE(story->mainNamespace->statements[2]->statementType() == PSL::StatementType::AssignmentStatement);
	REQUIRE(story->mainNamespace->statements[3]->statementType() == PSL::StatementType::AssignmentStatement);
	REQUIRE(story->mainNamespace->statements[4]->statementType() == PSL::StatementType::AssignmentStatement);
	REQUIRE(story->mainNamespace->statements[5]->statementType() == PSL::StatementType::AssignmentStatement);

	PSL::ExpressionType types[] = {PSL::ExpressionType::DictionaryPrimary, PSL::ExpressionType::MultiplicativeExpression, PSL::ExpressionType::BitOrExpression, PSL::ExpressionType::CharacterPrimary, PSL::ExpressionType::BitOrExpression, PSL::ExpressionType::OrExpression};

	for (size_t i = 0; i < story->mainNamespace->statements.size(); ++i)
	{
		PSL::AssignmentStatementNode* assign = static_cast<PSL::AssignmentStatementNode*>(story->mainNamespace->statements[i].get());
		CHECK(assign->right->expressionType() == types[i]);
	}
}

TEST_CASE("Parser Simple Structures")
{
	string filepath = PST_INPUT_FOLDER "05_simple_structures.story";
	ifstream filestream(filepath);
	PSL::Lexer lexer(filepath, filestream);
	PSL::Parser parser(filepath, lexer.lex());

	shared_ptr<PSL::StoryNode> story = parser.parse();
	REQUIRE(story->mainNamespace->sections.size() == 1);
	REQUIRE(story->mainNamespace->statements.size() == 0);

	shared_ptr<PSL::SectionNode>& section = story->mainNamespace->sections[0];
	REQUIRE(section->statements.size() == 5);
	REQUIRE(section->statements[0]->statementType() == PSL::StatementType::WhileStatement);
	REQUIRE(section->statements[1]->statementType() == PSL::StatementType::ForStatement);
	REQUIRE(section->statements[2]->statementType() == PSL::StatementType::IfStatement);
	REQUIRE(section->statements[3]->statementType() == PSL::StatementType::DoStatement);
	REQUIRE(section->statements[4]->statementType() == PSL::StatementType::IfStatement);

	PSL::WhileStatementNode* whileStmt = static_cast<PSL::WhileStatementNode*>(section->statements[0].get());
	PSL::ForStatementNode* forStmt = static_cast<PSL::ForStatementNode*>(section->statements[1].get());
	PSL::IfStatementNode* ifStmt = static_cast<PSL::IfStatementNode*>(section->statements[2].get());
	PSL::DoStatementNode* loopStmt = static_cast<PSL::DoStatementNode*>(section->statements[3].get());
	PSL::IfStatementNode* ifStmt2 = static_cast<PSL::IfStatementNode*>(section->statements[4].get());

	REQUIRE(whileStmt->statements.size() == 0);
	REQUIRE(forStmt->statements.size() == 0);
	REQUIRE(forStmt->expression->expressionType() == PSL::ExpressionType::ListPrimary);
	REQUIRE(ifStmt->ifPart->condition->expressionType() == PSL::ExpressionType::AndExpression);
	REQUIRE(ifStmt->elseIfParts.size() == 1);
	REQUIRE(ifStmt->elsePart == nullptr);
	REQUIRE(loopStmt->endCondition->expressionType() == PSL::ExpressionType::ComparisonExpression);
	REQUIRE(ifStmt2->ifPart->condition->expressionType() == PSL::ExpressionType::ComparisonExpression);
	REQUIRE(ifStmt2->elseIfParts.size() == 0);
}

TEST_CASE("Parser Assignments")
{
	string filepath = PST_INPUT_FOLDER "07_assignments.story";
	ifstream filestream(filepath);
	PSL::Lexer lexer(filepath, filestream);
	PSL::Parser parser(filepath, lexer.lex());

	shared_ptr<PSL::StoryNode> story = parser.parse();
	REQUIRE(story->mainNamespace->sections.size() == 1);
	REQUIRE(story->mainNamespace->statements.size() == 0);

	shared_ptr<PSL::SectionNode>& section = story->mainNamespace->sections[0];
	REQUIRE(section->statements.size() == 5);
	REQUIRE(section->statements[0]->statementType() == PSL::StatementType::AssignmentStatement);
	REQUIRE(section->statements[1]->statementType() == PSL::StatementType::AssignmentStatement);
	REQUIRE(section->statements[2]->statementType() == PSL::StatementType::AssignmentStatement);
	REQUIRE(section->statements[3]->statementType() == PSL::StatementType::IfStatement);
	REQUIRE(section->statements[4]->statementType() == PSL::StatementType::AssignmentStatement);

	PSL::AssignmentStatementNode* assignStmt1 = static_cast<PSL::AssignmentStatementNode*>(section->statements[0].get());
	CHECK(assignStmt1->create == true);
	PSL::AssignmentStatementNode* assignStmt2 = static_cast<PSL::AssignmentStatementNode*>(section->statements[1].get());
	CHECK(assignStmt2->create == false);
	CHECK(assignStmt2->left->expressionType() == PSL::ExpressionType::DotExpression);
	CHECK(assignStmt2->op->type == PSL::TokenType::AddAssign);
	PSL::AssignmentStatementNode* assignStmt3 = static_cast<PSL::AssignmentStatementNode*>(section->statements[2].get());
	CHECK(assignStmt3->create == true);

	PSL::IfStatementNode* ifStmt = static_cast<PSL::IfStatementNode*>(section->statements[3].get());
	PSL::AssignmentStatementNode* assignStmt4 = static_cast<PSL::AssignmentStatementNode*>(ifStmt->ifPart->statements[0].get());
	CHECK(assignStmt4->create == true);
	PSL::AssignmentStatementNode* assignStmt5 = static_cast<PSL::AssignmentStatementNode*>(ifStmt->ifPart->statements[1].get());
	CHECK(assignStmt5->create == false);
	PSL::AssignmentStatementNode* assignStmt6 = static_cast<PSL::AssignmentStatementNode*>(ifStmt->elsePart->statements[0].get());
	CHECK(assignStmt6->create == false);
	PSL::AssignmentStatementNode* assignStmt7 = static_cast<PSL::AssignmentStatementNode*>(ifStmt->elsePart->statements[1].get());
	CHECK(assignStmt7->create == true);

	PSL::AssignmentStatementNode* assignStmt8 = static_cast<PSL::AssignmentStatementNode*>(section->statements[4].get());
	CHECK(assignStmt8->create == false);
	CHECK(assignStmt8->op->type == PSL::TokenType::MulAssign);
}

TEST_CASE("Parser Nested Structures")
{
	string filepath = PST_INPUT_FOLDER "08_nested_structures.story";
	ifstream filestream(filepath);
	PSL::Lexer lexer(filepath, filestream);
	PSL::Parser parser(filepath, lexer.lex());

	shared_ptr<PSL::StoryNode> story = parser.parse();
	REQUIRE(story->mainNamespace->sections.size() == 1);
	shared_ptr<PSL::SectionNode>& section = story->mainNamespace->sections[0];
	REQUIRE(section->statements.size() == 2);
	REQUIRE(section->statements[0]->statementType() == PSL::StatementType::WhileStatement);

	PSL::WhileStatementNode* whileStmt1 = static_cast<PSL::WhileStatementNode*>(section->statements[0].get());
	REQUIRE(whileStmt1->statements.size() == 1);
	REQUIRE(whileStmt1->statements[0]->statementType() == PSL::StatementType::WhileStatement);

	PSL::WhileStatementNode* whileStmt2 = static_cast<PSL::WhileStatementNode*>(whileStmt1->statements[0].get());
	REQUIRE(whileStmt2->statements.size() == 2);
	REQUIRE(whileStmt2->statements[0]->statementType() == PSL::StatementType::ForStatement);
	REQUIRE(whileStmt2->statements[1]->statementType() == PSL::StatementType::ExitStatement);

	PSL::ForStatementNode* forStmt1 = static_cast<PSL::ForStatementNode*>(whileStmt2->statements[0].get());
	REQUIRE(forStmt1->statements.size() == 1);
	REQUIRE(forStmt1->statements[0]->statementType() == PSL::StatementType::DoStatement);

	PSL::DoStatementNode* doStmt1 = static_cast<PSL::DoStatementNode*>(forStmt1->statements[0].get());
	REQUIRE(doStmt1->statements.size() == 2);
	REQUIRE(doStmt1->endCondition != nullptr);
	REQUIRE(doStmt1->statements[0]->statementType() == PSL::StatementType::DoStatement);
	REQUIRE(doStmt1->statements[1]->statementType() == PSL::StatementType::GotoStatement);

	PSL::DoStatementNode* doStmt2 = static_cast<PSL::DoStatementNode*>(doStmt1->statements[0].get());
	REQUIRE(doStmt2->statements.size() == 3);
	REQUIRE(doStmt2->endCondition != nullptr);
	REQUIRE(doStmt2->statements[2]->statementType() == PSL::StatementType::IfStatement);

	PSL::IfStatementNode* ifStmt1 = static_cast<PSL::IfStatementNode*>(doStmt2->statements[2].get());
	REQUIRE(ifStmt1->elseIfParts.size() == 2);
	REQUIRE(ifStmt1->elseIfParts[1]->statements.size() == 1);
	REQUIRE(ifStmt1->elseIfParts[1]->statements[0]->statementType() == PSL::StatementType::ForStatement);
	REQUIRE(ifStmt1->elsePart != nullptr);

	REQUIRE(section->statements[1]->statementType() == PSL::StatementType::LabelStatement);
}

TEST_CASE("Parser Data Structures")
{
	string filepath = PST_INPUT_FOLDER "09_data_structures.story";
	ifstream filestream(filepath);
	PSL::Lexer lexer(filepath, filestream);
	PSL::Parser parser(filepath, lexer.lex());

	shared_ptr<PSL::StoryNode> story = parser.parse();
	REQUIRE(story->mainNamespace->sections.size() == 0);
	REQUIRE(story->mainNamespace->statements.size() == 7);
	REQUIRE(story->mainNamespace->statements[0]->statementType() == PSL::StatementType::AssignmentStatement);
	REQUIRE(story->mainNamespace->statements[1]->statementType() == PSL::StatementType::AssignmentStatement);
	REQUIRE(story->mainNamespace->statements[2]->statementType() == PSL::StatementType::AssignmentStatement);
	REQUIRE(story->mainNamespace->statements[3]->statementType() == PSL::StatementType::AssignmentStatement);
	REQUIRE(story->mainNamespace->statements[4]->statementType() == PSL::StatementType::AssignmentStatement);
	REQUIRE(story->mainNamespace->statements[5]->statementType() == PSL::StatementType::AssignmentStatement);
	REQUIRE(story->mainNamespace->statements[6]->statementType() == PSL::StatementType::AssignmentStatement);

	PSL::ExpressionType rightTypes[] = {
		PSL::ExpressionType::CharacterPrimary,
		PSL::ExpressionType::SequencePrimary,
		PSL::ExpressionType::ListPrimary,
		PSL::ExpressionType::DictionaryPrimary,
		PSL::ExpressionType::CharacterPrimary,
		PSL::ExpressionType::SequencePrimary,
		PSL::ExpressionType::CallExpression,
	};
	PSL::SequencePrimaryNode* sequences[2];
	size_t seqIndex = 0;

	for (size_t i = 0; i < 7; ++i)
	{
		PSL::AssignmentStatementNode* assign = static_cast<PSL::AssignmentStatementNode*>(story->mainNamespace->statements[i].get());
		REQUIRE(assign->right->expressionType() == rightTypes[i]);
		if (rightTypes[i] == PSL::ExpressionType::SequencePrimary) {
			sequences[seqIndex++] = static_cast<PSL::SequencePrimaryNode*>(assign->right.get());
		}
	}

	CHECK(sequences[0]->random == true);
	CHECK(sequences[0]->order == false);
	CHECK(sequences[0]->loop == false);

	CHECK(sequences[1]->random == false);
	CHECK(sequences[1]->order == true);
	CHECK(sequences[1]->loop == true);
}

TEST_CASE("Parser Events")
{
	string filepath = PST_INPUT_FOLDER "10_events.story";
	ifstream filestream(filepath);
	PSL::Lexer lexer(filepath, filestream);
	PSL::Parser parser(filepath, lexer.lex());

	shared_ptr<PSL::StoryNode> story = parser.parse();
	REQUIRE(story->mainNamespace->sections.size() == 2);
	shared_ptr<PSL::SectionNode>& section = story->mainNamespace->sections[0];
	REQUIRE(section->statements.size() == 7);
	REQUIRE(section->statements[0]->statementType() == PSL::StatementType::AssignmentStatement);
	REQUIRE(section->statements[1]->statementType() == PSL::StatementType::EventStatement);
	REQUIRE(section->statements[2]->statementType() == PSL::StatementType::AssignmentStatement);
	REQUIRE(section->statements[3]->statementType() == PSL::StatementType::EventStatement);
	REQUIRE(section->statements[4]->statementType() == PSL::StatementType::EventStatement);
	REQUIRE(section->statements[5]->statementType() == PSL::StatementType::EventStatement);
	REQUIRE(section->statements[6]->statementType() == PSL::StatementType::EventStatement);

	SUBCASE("Assign Event w/ Parameters")
	{
		PSL::EventStatementNode* event = static_cast<PSL::EventStatementNode*>(static_cast<PSL::EventStatementNode*>(section->statements[1].get()));
		REQUIRE(event->expression != nullptr);
		REQUIRE(event->expression->expressionType() == PSL::ExpressionType::CallExpression);
	}
}

TEST_CASE("Parser Sections")
{
	string filepath = PST_INPUT_FOLDER "11_sections.story";
	ifstream filestream(filepath);
	PSL::Lexer lexer(filepath, filestream);
	PSL::Parser parser(filepath, lexer.lex());

	shared_ptr<PSL::StoryNode> story = parser.parse();
	REQUIRE(story->mainNamespace->sections.size() == 5);

	SUBCASE("Namespace Statements")
	{
		REQUIRE(story->mainNamespace->statements.size() == 5);
		REQUIRE(story->mainNamespace->statements[0]->statementType() == PSL::StatementType::AssignmentStatement);
		REQUIRE(story->mainNamespace->statements[1]->statementType() == PSL::StatementType::AssignmentStatement);
		REQUIRE(story->mainNamespace->statements[2]->statementType() == PSL::StatementType::AssignmentStatement);
		REQUIRE(story->mainNamespace->statements[3]->statementType() == PSL::StatementType::AssignmentStatement);
		REQUIRE(story->mainNamespace->statements[4]->statementType() == PSL::StatementType::AssignmentStatement);

		PSL::AssignmentStatementNode* assign1 = static_cast<PSL::AssignmentStatementNode*>(story->mainNamespace->statements[0].get());
		CHECK(assign1->create == true);
		CHECK(assign1->final == false);

		PSL::AssignmentStatementNode* assign2 = static_cast<PSL::AssignmentStatementNode*>(story->mainNamespace->statements[2].get());
		CHECK(assign2->create == true);
		CHECK(assign2->final == false);
		CHECK(assign2->variableType == PSL::TokenType::Bool);

		PSL::AssignmentStatementNode* assign3 = static_cast<PSL::AssignmentStatementNode*>(story->mainNamespace->statements[3].get());
		CHECK(assign3->create == true);
		CHECK(assign3->final == true);
		CHECK(assign3->variableType == PSL::TokenType::Var);
	}

	SUBCASE("Main Section")
	{
		shared_ptr<PSL::SectionNode>& section = story->mainNamespace->sections[0];
		REQUIRE(*section->returnType == PSL::TokenType::String);
		REQUIRE(section->statements.size() == 2);
		REQUIRE(section->statements[0]->statementType() == PSL::StatementType::ExpressionStatement);
		REQUIRE(section->statements[1]->statementType() == PSL::StatementType::ExpressionStatement);

		PSL::ExpressionNode* expr = static_cast<PSL::ExpressionNode*>(section->statements[0].get());
		REQUIRE(expr->expressionType() == PSL::ExpressionType::CallExpression);

		expr = static_cast<PSL::ExpressionNode*>(section->statements[1].get());
		REQUIRE(expr->expressionType() == PSL::ExpressionType::CallExpression);
	}

	SUBCASE("Section Vars w/ Param and Choices")
	{
		shared_ptr<PSL::SectionNode>& section = story->mainNamespace->sections[1];
		REQUIRE(section->returnType == nullptr);
		REQUIRE(section->statements.size() == 2);
		CHECK(section->parameters.size() == 1);
		CHECK(section->statements[0]->statementType() == PSL::StatementType::AssignmentStatement);
		CHECK(section->statements[1]->statementType() == PSL::StatementType::LineStatement);

		PSL::LineStatementNode* line = static_cast<PSL::LineStatementNode*>(section->statements[1].get());
		CHECK(line->character->expressionType() == PSL::ExpressionType::AtomPrimary);
		CHECK(line->text->expressionType() == PSL::ExpressionType::AdditiveExpression);
		REQUIRE(line->choices.size() == 2);

		PSL::ChoiceStatementNode* choice1 = static_cast<PSL::ChoiceStatementNode*>(line->choices[0].get());
		CHECK(choice1->expressions.size() == 1);
		CHECK(choice1->statements.size() == 1);

		PSL::ChoiceStatementNode* choice2 = static_cast<PSL::ChoiceStatementNode*>(line->choices[1].get());
		CHECK(choice2->expressions.size() == 2);
		CHECK(choice2->statements.size() == 1);
	}

	SUBCASE("Section Another One")
	{
		shared_ptr<PSL::SectionNode>& section = story->mainNamespace->sections[2];
		REQUIRE(section->statements.size() == 4);
		CHECK(section->parameters.size() == 0);
		REQUIRE(section->statements[0]->statementType() == PSL::StatementType::AssignmentStatement);
		REQUIRE(section->statements[1]->statementType() == PSL::StatementType::AssignmentStatement);
		REQUIRE(section->statements[2]->statementType() == PSL::StatementType::AssignmentStatement);
		REQUIRE(section->statements[3]->statementType() == PSL::StatementType::AssignmentStatement);

		PSL::AssignmentStatementNode* assign1 = static_cast<PSL::AssignmentStatementNode*>(section->statements[0].get());
		CHECK(assign1->variableType == PSL::TokenType::Int);
		CHECK(assign1->create == true);
		CHECK(assign1->final == false);
		CHECK(assign1->right->expressionType() == PSL::ExpressionType::AtomPrimary);

		PSL::AssignmentStatementNode* assign3 = static_cast<PSL::AssignmentStatementNode*>(section->statements[2].get());
		CHECK(assign3->create == true);
		CHECK(assign3->final == true);
		CHECK(assign3->right->expressionType() == PSL::ExpressionType::AtomPrimary);

		PSL::AssignmentStatementNode* assign4 = static_cast<PSL::AssignmentStatementNode*>(section->statements[3].get());
		CHECK(assign4->create == false);
		CHECK(assign4->final == false);
		CHECK(assign4->right->expressionType() == PSL::ExpressionType::SelectorExpression);
	}

	SUBCASE("Section Yolo")
	{
		shared_ptr<PSL::SectionNode>& section = story->mainNamespace->sections[3];
		REQUIRE(section->statements.size() == 1);
		CHECK(section->parameters.size() == 3);
		REQUIRE(section->statements[0]->statementType() == PSL::StatementType::AssignmentStatement);

		PSL::AssignmentStatementNode* assign1 = static_cast<PSL::AssignmentStatementNode*>(section->statements[0].get());
		CHECK(assign1->create == true);
		CHECK(assign1->final == true);
		CHECK(assign1->right->expressionType() == PSL::ExpressionType::DictionaryPrimary);
	}
}

TEST_CASE("Parser Namespaces")
{
	string filepath = PST_INPUT_FOLDER "12_namespaces.story";
	ifstream filestream(filepath);
	PSL::Lexer lexer(filepath, filestream);
	PSL::Parser parser(filepath, lexer.lex());

	shared_ptr<PSL::StoryNode> story = parser.parse();
	REQUIRE(story->mainNamespace->sections.size() == 0);
	REQUIRE(story->mainNamespace->subNamespaces.size() == 2);

	SUBCASE("Main Namespace")
	{
		REQUIRE(story->mainNamespace->statements.size() == 2);
		REQUIRE(story->mainNamespace->statements[0]->statementType() == PSL::StatementType::AssignmentStatement);
		REQUIRE(story->mainNamespace->statements[1]->statementType() == PSL::StatementType::IncludeStatement);

		PSL::AssignmentStatementNode* assign1 = static_cast<PSL::AssignmentStatementNode*>(story->mainNamespace->statements[0].get());
		CHECK(assign1->create == true);
		CHECK(assign1->final == false);
		CHECK(assign1->right->expressionType() == PSL::ExpressionType::CharacterPrimary);

		PSL::IncludeStatementNode* include = static_cast<PSL::IncludeStatementNode*>(story->mainNamespace->statements[1].get());
		CHECK(include->filename->text == "\"14_includes.story\"");
	}

	SUBCASE("House Namespace")
	{
		shared_ptr<PSL::NamespaceNode>& house = story->mainNamespace->subNamespaces[0];
		CHECK(house->name->text == "House");
		REQUIRE(house->sections.size() == 1);
		REQUIRE(house->subNamespaces.size() == 0);

		shared_ptr<PSL::SectionNode>& manager = house->sections[0];
		CHECK(manager->name->text == "Manager");
		CHECK(manager->statements.size() == 1);
	}

	SUBCASE("Road Namespace")
	{
		shared_ptr<PSL::NamespaceNode>& road = story->mainNamespace->subNamespaces[1];
		CHECK(road->name->text == "Road");
		REQUIRE(road->sections.size() == 0);
		REQUIRE(road->subNamespaces.size() == 0);
	}
}

}
