#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include <doctest.h>

#include "util_defs.hpp"
#include "language_parser_tests.hpp"
#include "language_utility_tests.hpp"
#include "language_compiler_tests.hpp"
#include "vm_utility_tests.hpp"
#include "vm_metadata_tests.hpp"
#include "vm_bytecode_printer_tests.hpp"