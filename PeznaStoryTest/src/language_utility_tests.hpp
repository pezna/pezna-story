#include <doctest.h>

#include <fstream>

#include <ghc/filesystem.hpp>

#include <language/compiler/compiler.hpp>
#include <language/compiler/value.hpp>
#include <language/compiler/story_manager.hpp>
#include <language/visitors/node_register.hpp>
#include <language/utils/byte_writer_utils.hpp>

namespace PST
{

TEST_CASE("Value Type Single")
{
	PSL::ValueType bb = PSL::ValueType::BOOLEAN;
	PSL::ValueType ii = PSL::ValueType::INTEGER;
	PSL::ValueType ff = PSL::ValueType::FLOAT;
	PSL::ValueType ss = PSL::ValueType::STRING;
	PSL::ValueType ll = PSL::ValueType::LIST;
	PSL::ValueType dd = PSL::ValueType::DICT;
	PSL::ValueType cc = PSL::ValueType::CHARACTER;

	CHECK(bb.isBoolean());
	CHECK(!bb.isInteger());
	CHECK(!bb.isFloat());
	CHECK(!bb.isString());
	CHECK(!bb.isList());
	CHECK(!bb.isDict());
	CHECK(!bb.isCharacter());
	CHECK(!bb.isNumber());
	CHECK(bb.isHashable());

	CHECK(!ii.isBoolean());
	CHECK(ii.isInteger());
	CHECK(!ii.isFloat());
	CHECK(!ii.isString());
	CHECK(!ii.isList());
	CHECK(!ii.isDict());
	CHECK(!ii.isCharacter());
	CHECK(ii.isNumber());
	CHECK(ii.isHashable());

	CHECK(!ff.isBoolean());
	CHECK(!ff.isInteger());
	CHECK(ff.isFloat());
	CHECK(!ff.isString());
	CHECK(!ff.isList());
	CHECK(!ff.isDict());
	CHECK(!ff.isCharacter());
	CHECK(ff.isNumber());
	CHECK(ff.isHashable());

	CHECK(!ss.isBoolean());
	CHECK(!ss.isInteger());
	CHECK(!ss.isFloat());
	CHECK(ss.isString());
	CHECK(!ss.isList());
	CHECK(!ss.isDict());
	CHECK(!ss.isCharacter());
	CHECK(!ss.isNumber());
	CHECK(ss.isHashable());

	CHECK(!ll.isBoolean());
	CHECK(!ll.isInteger());
	CHECK(!ll.isFloat());
	CHECK(!ll.isString());
	CHECK(ll.isList());
	CHECK(!ll.isDict());
	CHECK(!ll.isCharacter());
	CHECK(!ll.isNumber());
	CHECK(!ll.isHashable());

	CHECK(!dd.isBoolean());
	CHECK(!dd.isInteger());
	CHECK(!dd.isFloat());
	CHECK(!dd.isString());
	CHECK(!dd.isList());
	CHECK(dd.isDict());
	CHECK(!dd.isCharacter());
	CHECK(!dd.isNumber());
	CHECK(!dd.isHashable());

	CHECK(!cc.isBoolean());
	CHECK(!cc.isInteger());
	CHECK(!cc.isFloat());
	CHECK(!cc.isString());
	CHECK(!cc.isList());
	CHECK(!cc.isDict());
	CHECK(cc.isCharacter());
	CHECK(!cc.isNumber());
	CHECK(!cc.isHashable());
}

TEST_CASE("Value Type Single Conversion")
{
	PSL::ValueType bb(PSL::ValueType::BOOLEAN);
	PSL::ValueType ii(PSL::ValueType::INTEGER);
	PSL::ValueType ff(PSL::ValueType::FLOAT);
	PSL::ValueType ss(PSL::ValueType::STRING);
	PSL::ValueType ll(PSL::ValueType::LIST);
	PSL::ValueType dd(PSL::ValueType::DICT);
	PSL::ValueType cc(PSL::ValueType::CHARACTER);

	CHECK(bb.canConvertTo(PSL::ValueType::BOOLEAN));
	CHECK(bb.canConvertTo(PSL::ValueType::INTEGER));
	CHECK(bb.canConvertTo(PSL::ValueType::FLOAT));
	CHECK(bb.canConvertTo(PSL::ValueType::STRING));
	CHECK(!bb.canConvertTo(PSL::ValueType::LIST));
	CHECK(!bb.canConvertTo(PSL::ValueType::DICT));
	CHECK(!bb.canConvertTo(PSL::ValueType::CHARACTER));

	CHECK(ii.canConvertTo(PSL::ValueType::BOOLEAN));
	CHECK(ii.canConvertTo(PSL::ValueType::INTEGER));
	CHECK(ii.canConvertTo(PSL::ValueType::FLOAT));
	CHECK(ii.canConvertTo(PSL::ValueType::STRING));
	CHECK(!ii.canConvertTo(PSL::ValueType::LIST));
	CHECK(!ii.canConvertTo(PSL::ValueType::DICT));
	CHECK(!ii.canConvertTo(PSL::ValueType::CHARACTER));

	CHECK(ff.canConvertTo(PSL::ValueType::BOOLEAN));
	CHECK(ff.canConvertTo(PSL::ValueType::INTEGER));
	CHECK(ff.canConvertTo(PSL::ValueType::FLOAT));
	CHECK(ff.canConvertTo(PSL::ValueType::STRING));
	CHECK(!ff.canConvertTo(PSL::ValueType::LIST));
	CHECK(!ff.canConvertTo(PSL::ValueType::DICT));
	CHECK(!ff.canConvertTo(PSL::ValueType::CHARACTER));

	CHECK(ss.canConvertTo(PSL::ValueType::BOOLEAN));
	CHECK(ss.canConvertTo(PSL::ValueType::INTEGER));
	CHECK(ss.canConvertTo(PSL::ValueType::FLOAT));
	CHECK(ss.canConvertTo(PSL::ValueType::STRING));
	CHECK(!ss.canConvertTo(PSL::ValueType::LIST));
	CHECK(!ss.canConvertTo(PSL::ValueType::DICT));
	CHECK(!ss.canConvertTo(PSL::ValueType::CHARACTER));

	CHECK(ll.canConvertTo(PSL::ValueType::BOOLEAN));
	CHECK(!ll.canConvertTo(PSL::ValueType::INTEGER));
	CHECK(!ll.canConvertTo(PSL::ValueType::FLOAT));
	CHECK(ll.canConvertTo(PSL::ValueType::STRING));
	CHECK(ll.canConvertTo(PSL::ValueType::LIST));
	CHECK(ll.canConvertTo(PSL::ValueType::DICT));
	CHECK(!ll.canConvertTo(PSL::ValueType::CHARACTER));

	CHECK(dd.canConvertTo(PSL::ValueType::BOOLEAN));
	CHECK(!dd.canConvertTo(PSL::ValueType::INTEGER));
	CHECK(!dd.canConvertTo(PSL::ValueType::FLOAT));
	CHECK(dd.canConvertTo(PSL::ValueType::STRING));
	CHECK(dd.canConvertTo(PSL::ValueType::LIST));
	CHECK(dd.canConvertTo(PSL::ValueType::DICT));
	CHECK(!dd.canConvertTo(PSL::ValueType::CHARACTER));

	CHECK(cc.canConvertTo(PSL::ValueType::BOOLEAN));
	CHECK(!cc.canConvertTo(PSL::ValueType::INTEGER));
	CHECK(!cc.canConvertTo(PSL::ValueType::FLOAT));
	CHECK(cc.canConvertTo(PSL::ValueType::STRING));
	CHECK(!cc.canConvertTo(PSL::ValueType::LIST));
	CHECK(cc.canConvertTo(PSL::ValueType::DICT));
	CHECK(cc.canConvertTo(PSL::ValueType::CHARACTER));
}

TEST_CASE("Node Register")
{
	string filepath = PST_INPUT_FOLDER "08_nested_structures.story";
	ifstream filestream(filepath);
	PSL::Lexer lexer(filepath, filestream);
	PSL::Parser parser(filepath, lexer.lex());

	shared_ptr<PSL::StoryNode> story = parser.parse();

	PSL::NodeRegister reg;
	reg.addStory(story);
	vector<PSL::Node*> children = reg.getChildren(story.get());
	PSL::Node* parent = reg.getParent(story.get());

	REQUIRE(children.size() == 1);
	REQUIRE(children[0]->nodeType() == PSL::NodeType::Namespace);
	REQUIRE(parent == nullptr);

	vector<PSL::Node*> descendants = reg.getDescendantsWithType(story.get(), PSL::ChildType::ElseIfPart);
	REQUIRE(descendants.size() == 2);
	REQUIRE(static_cast<PSL::ElseIfPartNode*>(descendants[0])->statements.size() == 0);
	REQUIRE(static_cast<PSL::ElseIfPartNode*>(descendants[1])->statements.size() == 1);

	parent = reg.getParent(descendants[0]);
	REQUIRE(parent->nodeType() == PSL::NodeType::Statement);
	REQUIRE(static_cast<PSL::StatementNode*>(parent)->statementType() == PSL::StatementType::IfStatement);

	vector<PSL::Node*> ancestors = reg.getAncestorsWithType(descendants[0], PSL::StatementType::DoStatement);
	REQUIRE(ancestors.size() == 2);
	REQUIRE(static_cast<PSL::DoStatementNode*>(ancestors[0])->statements.size() == 3);
	REQUIRE(static_cast<PSL::DoStatementNode*>(ancestors[1])->statements.size() == 2);

	children = reg.getChildren(ancestors[1]);
	REQUIRE(children.size() == 3);
	REQUIRE(static_cast<PSL::StatementNode*>(children[0])->statementType() == PSL::StatementType::DoStatement);
	REQUIRE(static_cast<PSL::StatementNode*>(children[1])->statementType() == PSL::StatementType::GotoStatement);
	REQUIRE(static_cast<PSL::StatementNode*>(children[2])->statementType() == PSL::StatementType::ExpressionStatement);

	ancestors = reg.getAncestorsWithType(children[1], PSL::StatementType::WhileStatement);
	REQUIRE(ancestors.size() == 2);
	REQUIRE(static_cast<PSL::WhileStatementNode*>(ancestors[0])->statements.size() == 2);
	REQUIRE(static_cast<PSL::WhileStatementNode*>(ancestors[1])->statements.size() == 1);

	descendants = reg.getDescendantsWithType(ancestors[0], PSL::StatementType::ExitStatement);
	REQUIRE(descendants.size() == 1);
	REQUIRE(static_cast<PSL::ExitStatementNode*>(descendants[0])->level->text == "while");
}

TEST_CASE("Story Manager Named Containers")
{
	string filepath = PST_INPUT_FOLDER "14_includes.story";
	ifstream filestream(filepath);
	PSL::Lexer lexer(filepath, filestream);
	PSL::Parser parser(filepath, lexer.lex());

	shared_ptr<PSL::StoryNode> story = parser.parse();
	ghc::filesystem::path storyPath = ghc::filesystem::path(PST_INPUT_FOLDER "14_includes.story");
	story->filepath = ghc::filesystem::absolute(storyPath.lexically_normal()).string();

	PSL::StoryManager storyManager(story);
	shared_ptr<PSL::NamedContainer> mainContainer = storyManager.getNamedContainer(story->mainNamespace.get());

	REQUIRE(mainContainer->isNamespace == true);
	REQUIRE(mainContainer->isSection == false);
	REQUIRE(mainContainer->parent == nullptr);
	CHECK(mainContainer->nodes.size() == 3);
	CHECK(mainContainer->children.size() == 4);

	auto it = std::find_if(
			mainContainer->children.begin(),
			mainContainer->children.end(),
			[](shared_ptr<PSL::NamedContainer>& c)
			{
				return c->name == "House" && c->isNamespace;
			}
		);
	REQUIRE(it != mainContainer->children.end());

	shared_ptr<PSL::NamedContainer> houseContainer = *it;

	REQUIRE(houseContainer->nodes.size() == 2);
	REQUIRE(houseContainer->children.size() == 1);
}

TEST_CASE("Story Manager String Indices")
{
	string filepath = PST_INPUT_FOLDER "01_simple_expressions.story";
	ifstream filestream(filepath);
	PSL::Lexer lexer(filepath, filestream);
	PSL::Parser parser(filepath, lexer.lex());

	shared_ptr<PSL::StoryNode> story = parser.parse();
	ghc::filesystem::path storyPath = ghc::filesystem::path(PST_INPUT_FOLDER "01_simple_expressions.story");
	story->filepath = ghc::filesystem::absolute(storyPath.lexically_normal()).string();

	PSL::StoryManager storyManager(story);

	string s = "s你itioni们好n = \"s你itioni们好n\"";
	auto stringId = storyManager.addStringGetId(s);
	CHECK(stringId == 0);
	CHECK(storyManager.strings().size() == 1);
}

TEST_CASE("Instruction Container Indices and Length")
{
	string filepath = PST_INPUT_FOLDER "11_sections.story";
	ifstream filestream(filepath);
	PSL::Lexer lexer(filepath, filestream);
	PSL::Parser parser(filepath, lexer.lex());

	shared_ptr<PSL::StoryNode> story = parser.parse();
	ghc::filesystem::path storyPath = ghc::filesystem::path(PST_INPUT_FOLDER "11_sections.story");
	story->filepath = ghc::filesystem::absolute(storyPath.lexically_normal()).string();

	PSL::Compiler compiler(story);
	compiler.compile();
	PSL::InstructionContainer& firstMetadata = compiler.firstMetadata();
	PSL::InstructionContainer* first = firstMetadata.firstSibling();
	PSL::InstructionContainer* last = firstMetadata.lastSibling();

	REQUIRE(first != nullptr);
	REQUIRE(last != nullptr);

	CHECK(firstMetadata.listSize() == first->listSize());
	CHECK(first->listSize() == last->listSize());
	CHECK(firstMetadata.listIndex() == 0);
	CHECK(first->listIndex() == 0);
	CHECK(last->listIndex() > first->listIndex());
}

TEST_CASE("Byte Writer and Reader")
{
	int64_t n1 = 284548;
	int64_t n2 = 1;
	int64_t n3 = 0;
	int64_t n4 = 4294967295;
	int64_t n5 = 7295;
	int64_t n6 = -651295;

	vector<int8_t> out;

	PSL::Utils::writeVarInt(n1, out);
	CHECK(PSL::Utils::readVarInt(out) == n1);
	CHECK(out.size() == 3);
	out.clear();

	PSL::Utils::writeVarInt(n2, out);
	CHECK(PSL::Utils::readVarInt(out) == n2);
	CHECK(out.size() == 1);
	out.clear();

	PSL::Utils::writeVarInt(n3, out);
	CHECK(PSL::Utils::readVarInt(out) == n3);
	CHECK(out.size() == 1);
	out.clear();

	PSL::Utils::writeVarInt(n4, out);
	CHECK(PSL::Utils::readVarInt(out) == n4);
	CHECK(out.size() == 5);
	out.clear();

	PSL::Utils::writeVarInt(n5, out);
	CHECK(PSL::Utils::readVarInt(out) == n5);
	CHECK(out.size() == 2);
	out.clear();
	
	PSL::Utils::writeVarInt(n6, out);
	CHECK(PSL::Utils::readVarInt(out) == n6);
	CHECK(out.size() == 10);
	out.clear();
}

}
