#ifndef PST_VM_MANAGER_H
#define PST_VM_MANAGER_H

#include <vector>
#include <string>
#include <unordered_map>

#include <psvm/bytecode_source.hpp>
#include <psvm/error_handler.hpp>
#include <psvm/vm.hpp>

namespace PST
{

struct TestErrorMessage
{
	std::string message;
	
	TestErrorMessage(const std::string& message)
	:
		message(message)
	{}
};

struct TestErrorManager : public PSVM::ErrorHandler
{
	std::vector<TestErrorMessage> errorMessages;
	bool throwExceptions;
	
	void handleError(std::string message) override;
	bool hasError() override;
	void clearErrors() override;
	void printErrors();
	void setThrowExceptions(bool te);
};

struct TestBytecodeSource : public PSVM::BytecodeSource
{
	std::string fileLocation;
	char* bytecode;
	int bytecodeLength;
	int bytecodeOffset;
	
	TestBytecodeSource(const std::string& fileLocation);
	~TestBytecodeSource();
	
	char nextByte() override;
	char peekByte(int delta) override;
	void skipBytes(int delta) override;
	void setOffset(int offset) override;
	int getOffset() override;
	bool hasNextByte() override;
};

}

#endif // PST_VM_MANAGER_H