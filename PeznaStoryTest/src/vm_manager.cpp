#include "vm_manager.hpp"

#include <iostream>
#include <string>
#include <fstream>

namespace PST
{

void TestErrorManager::handleError(std::string message)
{
	errorMessages.push_back(TestErrorMessage(message));
	
	if (throwExceptions)
	{
		throw std::runtime_error(std::string("VM code error: ") + message);
	}
	else
	{
		std::cerr << "VM code error: " << message << "\n";
	}
}

bool TestErrorManager::hasError()
{
	return errorMessages.size() > 0;
}

void TestErrorManager::clearErrors()
{
	errorMessages.clear();
}

void TestErrorManager::setThrowExceptions(bool te)
{
	throwExceptions = te;
}

void TestErrorManager::printErrors()
{
	for (TestErrorMessage& error : errorMessages)
	{
		std::cout << "VM Error; message: " << error.message << std::endl;
	}
}

/**********************
 * TestBytecodeSource *
 **********************/

TestBytecodeSource::TestBytecodeSource(const std::string& fileLocation)
{
	this->fileLocation = fileLocation;
	std::ifstream file(fileLocation, std::ios::in | std::ios::binary | std::ios::ate);
	if (!file.is_open())
	{
		throw std::runtime_error("Could not open bytecode file: " + fileLocation);
	}
	
	bytecodeOffset = 0;
	bytecodeLength = file.tellg();
	bytecode = new char[bytecodeLength];
	file.seekg(0, std::ios::beg);
	file.read(bytecode, bytecodeLength);
	file.close();
}

TestBytecodeSource::~TestBytecodeSource()
{
	delete[] bytecode;
}

char TestBytecodeSource::nextByte()
{
	if (bytecodeOffset >= bytecodeLength)
	{
		throw std::runtime_error("nextByte - EOF for the story: " + fileLocation);
	}
	
	char byte = *(bytecode + bytecodeOffset);
	//std::cout << "byte: " << (int)byte << "; offset: " << bytecodeSource.offset << "; length: " << bytecodeSource.length << "; all: " << bytecodeSource.data << std::endl;
	
	++bytecodeOffset;
	
	return byte;
}

char TestBytecodeSource::peekByte(int delta)
{
	int offset = bytecodeOffset + (delta - 1);
	
	if (offset < 0 || offset >= bytecodeLength)
	{
		throw std::runtime_error("peekByte (total offset " + std::to_string(offset) + ") - out of bounds for the story: " + fileLocation);
	}
	
	char byte = *(bytecode + offset);
	
	return byte;
}

void TestBytecodeSource::skipBytes(int delta)
{
	// do not subtract 1 from delta, because the offset is always set to 1 past the current
	// see that nextByte() returns the data pointed to by the current offset before incrementing it.
	int offset = bytecodeOffset + delta;
	
	if (offset < 0 || offset >= bytecodeLength)
	{
		throw std::runtime_error("skipBytes (total offset " + std::to_string(offset) + ") - out of bounds for the story: " + fileLocation);
	}
	
	bytecodeOffset = offset;
}

void TestBytecodeSource::setOffset(int offset)
{
	if (offset >= bytecodeLength)
	{
		throw std::runtime_error("setOffset - Cannot set offset " + std::to_string(offset) + " for the story: " + fileLocation);
	}
	
	bytecodeOffset = offset;
}

int TestBytecodeSource::getOffset()
{
	return bytecodeOffset;
}

bool TestBytecodeSource::hasNextByte()
{
	return bytecodeOffset < bytecodeLength;
}

}