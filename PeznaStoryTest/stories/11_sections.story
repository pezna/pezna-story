var n = 100
var j = ""
var bool YOLO = false
final sad = 5

section main -> string:
	vars_with_param_and_choices(john)
	yolo("2", 2, john)
end section

section vars_with_param_and_choices(character owner):
	var john = character {}
	john: "This house belongs to " + owner.name
	choice "First Choice":
		owner: "first"
	end choice
	choice "Second Choice", "Another Second Choice Text":
		owner: "second"
	end choice
end section

var john = character {name = "John"}

section another_one():
	int ACCESSED = 0
	var final CODES = []
	
	var final h = "blah blah"
	j = h[,,-1]
end section

section yolo(string a, final int b, character c):
	final var VAR = {"""a""": """b"""}
end section

section no_static_vars_no_params:

end section