section main:
	var john = character {name = "John"}
	
	event death(john.name, 20)
	
	var open = "open"
	event open
	
	event ["fire", 20, ["no", "way"]]
	
	event []
	
	event john
end section

section death(string name, int age)
	return ["death", name, age, "yolo"]
end section