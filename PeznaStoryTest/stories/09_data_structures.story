var anna = character {}

var seqVal = sequence <random> ["ate your sandwich", "shot a butterfly", "ran over a cat"]

var l = ["one", [2, true], {"h": 8}, false, seqVal]

var d = {
	"ready": [seqVal],
	"set": anna,
	"go": {
		4: "fire"
	}
}

var john = character {
	name = "John Doe",
	money = 100.44
}

var seq2Val = sequence boo <loop order> [
	"here": l[1][0] = 8, "we", "go": john.money += 10, "again"
]

var t = boo()