#include "token.hpp"

#include <sstream>

#include "localization.hpp"

namespace PSL
{

//static
string Token::getTypeName(TokenType type)
{
	switch (type)
	{
		case TokenType::Story: return "Story";
		case TokenType::Namespace: return "Namespace";
		case TokenType::Section: return "Section";
		case TokenType::Include: return "Include";
		case TokenType::Continue: return "Continue";
		case TokenType::Return: return "Return";
		case TokenType::End: return "End";
		case TokenType::Choice: return "Choice";
		case TokenType::If: return "If";
		case TokenType::Else: return "Else";
		case TokenType::ElseIf: return "ElseIf";
		case TokenType::While: return "While";
		case TokenType::For: return "For";
		case TokenType::Do: return "Do";
		case TokenType::Goto: return "Goto";
		case TokenType::Exit: return "Exit";
		case TokenType::Label: return "Label";
		case TokenType::Event: return "Event";
		case TokenType::Default: return "Default";
		case TokenType::Var: return "Var";
		case TokenType::Final: return "Final";
		case TokenType::Character: return "Character";
		case TokenType::Sequence: return "Sequence";
		case TokenType::Bool: return "Bool";
		case TokenType::Int: return "Int";
		case TokenType::Float: return "Float";
		case TokenType::String: return "String";
		case TokenType::List: return "List";
		case TokenType::Dict: return "Dict";
		case TokenType::Equals: return "Equals";
		case TokenType::NotEquals: return "NotEquals";
		case TokenType::GreaterThanEquals: return "GreaterThanEquals";
		case TokenType::LessThanEquals: return "LessThanEquals";
		case TokenType::GreaterThan: return "GreaterThan";
		case TokenType::LessThan: return "LessThan";
		case TokenType::Not: return "Not";
		case TokenType::And: return "And";
		case TokenType::Or: return "Or";
		case TokenType::In: return "In";
		case TokenType::NotIn: return "NotIn";
		case TokenType::Is: return "Is";
		case TokenType::IsNot: return "IsNot";
		case TokenType::Pow: return "Pow";
		case TokenType::Add: return "Add";
		case TokenType::Sub: return "Sub";
		case TokenType::Mul: return "Mul";
		case TokenType::Div: return "Div";
		case TokenType::Mod: return "Mod";
		case TokenType::BitAnd: return "BitAnd";
		case TokenType::BitXor: return "BitXor";
		case TokenType::BitOr: return "BitOr";
		case TokenType::BitNot: return "BitNot";
		case TokenType::BitLeftShift: return "BitLeftShift";
		case TokenType::BitRightShift: return "BitRightShift";
		case TokenType::Assign: return "Assign";
		case TokenType::PowAssign: return "PowAssign";
		case TokenType::AddAssign: return "AddAssign";
		case TokenType::SubAssign: return "SubAssign";
		case TokenType::MulAssign: return "MulAssign";
		case TokenType::DivAssign: return "DivAssign";
		case TokenType::ModAssign: return "ModAssign";
		case TokenType::BitAndAssign: return "BitAndAssign";
		case TokenType::BitXorAssign: return "BitXorAssign";
		case TokenType::BitOrAssign: return "BitOrAssign";
		case TokenType::BitLeftShiftAssign: return "BitLeftShiftAssign";
		case TokenType::BitRightShiftAssign: return "BitRightShiftAssign";
		case TokenType::OpenBrack: return "OpenBrack";
		case TokenType::CloseBrack: return "CloseBrack";
		case TokenType::OpenParen: return "OpenParen";
		case TokenType::CloseParen: return "CloseParen";
		case TokenType::OpenBrace: return "OpenBrace";
		case TokenType::CloseBrace: return "CloseBrace";
		case TokenType::Comma: return "Comma";
		case TokenType::Colon: return "Colon";
		case TokenType::DoubleColon: return "DoubleColon";
		case TokenType::SemiColon: return "SemiColon";
		case TokenType::Dot: return "Dot";
		case TokenType::Ternary: return "Ternary";
		case TokenType::Arrow: return "Arrow";
		case TokenType::BoolLiteral: return "BoolLiteral";
		case TokenType::IntLiteral: return "IntLiteral";
		case TokenType::FloatLiteral: return "FloatLiteral";
		case TokenType::StringLiteral: return "StringLiteral";
		case TokenType::Name: return "Name";
		case TokenType::Comment: return "Comment";
		case TokenType::Whitespace: return "Whitespace";
		case TokenType::IgnoreNewline: return "IgnoreNewline";
		case TokenType::Newline: return "Newline";
		case TokenType::Eof: return "EOF";
	}
	
	std::ostringstream oss;
	oss << "Requesting name for non-existent token type: "
		<< static_cast<std::underlying_type<TokenType>::type>(type);
	throw std::runtime_error(oss.str());
}

//static
string Token::getSymbolText(TokenType type)
{
	switch (type)
	{
		case TokenType::Arrow: return "->";
		case TokenType::Equals: return "==";
		case TokenType::NotEquals: return "!=";
		case TokenType::GreaterThanEquals: return ">=";
		case TokenType::LessThanEquals: return "<=";
		case TokenType::GreaterThan: return ">";
		case TokenType::LessThan: return "<";
		case TokenType::Not: return "!";
		case TokenType::And: return "&&";
		case TokenType::Or: return "||";
		case TokenType::Pow: return "**";
		case TokenType::Add: return "+";
		case TokenType::Sub: return "-";
		case TokenType::Mul: return "*";
		case TokenType::Div: return "/";
		case TokenType::Mod: return "%";
		case TokenType::BitAnd: return "&";
		case TokenType::BitXor: return "^";
		case TokenType::BitOr: return "|";
		case TokenType::BitNot: return "~";
		case TokenType::BitLeftShift: return "<<";
		case TokenType::BitRightShift: return ">>";
		case TokenType::Assign: return "=";
		case TokenType::PowAssign: return "**=";
		case TokenType::AddAssign: return "+=";
		case TokenType::SubAssign: return "-=";
		case TokenType::MulAssign: return "*=";
		case TokenType::DivAssign: return "/=";
		case TokenType::ModAssign: return "%=";
		case TokenType::BitAndAssign: return "&=";
		case TokenType::BitXorAssign: return "^=";
		case TokenType::BitOrAssign: return "|=";
		case TokenType::BitLeftShiftAssign: return "<<=";
		case TokenType::BitRightShiftAssign: return ">>=";
		case TokenType::OpenBrack: return "[";
		case TokenType::CloseBrack: return "]";
		case TokenType::OpenParen: return "(";
		case TokenType::CloseParen: return ")";
		case TokenType::OpenBrace: return "{";
		case TokenType::CloseBrace: return "}";
		case TokenType::Comma: return ",";
		case TokenType::Colon: return ":";
		case TokenType::DoubleColon: return "::";
		case TokenType::SemiColon: return ";";
		case TokenType::Dot: return ".";
		case TokenType::Ternary: return "?";
		case TokenType::Newline: return "\n";
		case TokenType::IgnoreNewline: return "\\";
	}
	
	std::ostringstream oss;
	oss << "Requesting symbol text for non-existent symbol: "
		<< static_cast<std::underlying_type<TokenType>::type>(type);
	throw std::runtime_error(oss.str());
}

//static
TokenType Token::getKeywordType(const string& text)
{
	return Localization::getKeywordType(text, TokenType::Name);
}

bool Token::isAssignment()
{
	switch (type)
	{
		case TokenType::Assign:
		case TokenType::PowAssign:
		case TokenType::AddAssign:
		case TokenType::SubAssign:
		case TokenType::MulAssign:
		case TokenType::DivAssign:
		case TokenType::ModAssign:
		case TokenType::BitAndAssign:
		case TokenType::BitXorAssign:
		case TokenType::BitOrAssign:
		case TokenType::BitLeftShiftAssign:
		case TokenType::BitRightShiftAssign:
			return true;
		default:
			return false;
	}
}

bool Token::isBinaryOperator()
{
	switch (type)
	{
		case TokenType::Equals:
		case TokenType::NotEquals:
		case TokenType::GreaterThanEquals:
		case TokenType::LessThanEquals:
		case TokenType::GreaterThan:
		case TokenType::LessThan:
		case TokenType::And:
		case TokenType::Or:
		case TokenType::In:
		case TokenType::NotIn:
		case TokenType::Is:
		case TokenType::IsNot:
		case TokenType::Pow:
		case TokenType::Add:
		case TokenType::Sub:
		case TokenType::Mul:
		case TokenType::Div:
		case TokenType::Mod:
		case TokenType::BitAnd:
		case TokenType::BitXor:
		case TokenType::BitOr:
		case TokenType::BitLeftShift:
		case TokenType::BitRightShift:
		case TokenType::OpenParen:
		case TokenType::OpenBrack:
		case TokenType::Dot:
		case TokenType::DoubleColon:
			return true;
		default:
			return false;
	}
}

bool Token::canStartExpression()
{
	switch (type)
	{
		case TokenType::OpenBrack:
		case TokenType::OpenParen:
		case TokenType::OpenBrace:
		case TokenType::BoolLiteral:
		case TokenType::IntLiteral:
		case TokenType::FloatLiteral:
		case TokenType::StringLiteral:
		case TokenType::Not:
		case TokenType::BitNot:
		case TokenType::Sub:
		case TokenType::Name:
		case TokenType::Character:
		case TokenType::Sequence:
		case TokenType::Default:
			return true;
		default:
			return false;
	}
}

bool Token::canStartStatement()
{
	switch (type)
	{
		case TokenType::If:
		case TokenType::While:
		case TokenType::For:
		case TokenType::Do:
		case TokenType::Goto:
		case TokenType::Label:
		case TokenType::Choice:
		case TokenType::Exit:
		case TokenType::Continue:
		case TokenType::Return:
		case TokenType::Include:
		case TokenType::Name:
		case TokenType::Var:
		case TokenType::Final:
		case TokenType::Character:
		case TokenType::Sequence:
		case TokenType::Event:
		case TokenType::Bool:
		case TokenType::Int:
		case TokenType::Float:
		case TokenType::String:
		case TokenType::List:
		case TokenType::Dict:
		case TokenType::OpenParen:
		case TokenType::OpenBrace:
		case TokenType::OpenBrack:
			return true;
		default:
			return false;
	}
}

bool Token::canStartVariableCreation()
{
	switch (type)
	{
		case TokenType::Var:
		case TokenType::Final:
		case TokenType::Character:
		case TokenType::Bool:
		case TokenType::Int:
		case TokenType::Float:
		case TokenType::String:
		case TokenType::List:
		case TokenType::Dict:
			return true;
		default:
			return false;
	}
}

bool Token::isVariableType()
{
	switch (type)
	{
		case TokenType::Character:
		case TokenType::Bool:
		case TokenType::Int:
		case TokenType::Float:
		case TokenType::String:
		case TokenType::List:
		case TokenType::Dict:
			return true;
		default:
			return false;
	}
}

ostream& operator<<(ostream& os, const Token& token)
{
	return os << Token::getTypeName(token.type) << "(\"" << token.text << "\", line "
			<< token.line << ":" << token.column << ")";
}

}
