#ifndef PSL_STRING_UTILS_H
#define PSL_STRING_UTILS_H

#include <iostream>
#include <vector>
#include <iterator>
#include <memory>

template<typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& vec)
{
	if (vec.empty())
	{
		return os << "[]";
	}
	
	os << "[";
	bool first = true;
	for (const T& item : vec)
	{
		if (!first)
		{
			os << ", ";
		}
		
		os << item;
		first = false;
	}
	return os << "]";
}

template<typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<std::shared_ptr<T>>& vec)
{
	if (vec.empty())
	{
		return os << "[]";
	}
	
	os << "[";
	bool first = true;
	for (const std::shared_ptr<T>& item : vec)
	{
		if (!first)
		{
			os << ", ";
		}
		
		os << *item;
		first = false;
	}
	return os << "]";
}

#endif //PSL_STRING_UTILS_H
