#ifndef PSL_PARSER_UTILS_H
#define PSL_PARSER_UTILS_H

#include <cstdint>

namespace PSL
{

namespace Utils
{

/**
 * @param codePoint - the integer code of a unicode character
 * @return {@code true} if the specified unicode character can start an identifier, {@code false} otherwise
 */
bool isIdentifierStart(uint32_t codePoint);

/**
 * @param codePoint - the integer code of a unicode character
 * @return {@code true} if the specified unicode character can occur in an identifier, {@code false} otherwise
 */
bool isIdentifierPart(uint32_t codePoint);

}

}

#endif //PSL_PARSER_UTILS_H
