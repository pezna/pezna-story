#ifndef BYTE_WRITER_UTILS_H
#define BYTE_WRITER_UTILS_H

#include <cstdint>
#include <vector>

namespace PSL::Utils
{

/**
 * Write an integer to the output in variable-length form
 * @param value - the integer to be written
 * @param output - the output it will be written to
 * @return the number of bytes written
 */
int writeVarInt(int64_t value, std::vector<int8_t>& output);

/**
 * Read an integer from variable-length form
 * @param input - the vector it will be read from
 * @return the integer that was read
 */
int64_t readVarInt(std::vector<int8_t>& input);

}

#endif // BYTE_WRITER_UTILS_H
