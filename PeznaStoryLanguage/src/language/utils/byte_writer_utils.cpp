#include "byte_writer_utils.hpp"

namespace PSL::Utils
{

int writeVarInt(int64_t value, std::vector<int8_t>& output)
{
	if (value >> 7 == 0)
	{
		output.push_back(static_cast<int8_t>(value));
		return 1;
	}
	
	if (value >> 14 == 0)
	{
		output.push_back(static_cast<int8_t>((value >> 7) | 0x80));
		output.push_back(static_cast<int8_t>(value & 0x7f));
		return 2;
	}
	
	if (value >> 21 == 0)
	{
		output.push_back(static_cast<int8_t>((value >> 14) | 0x80));
		output.push_back(static_cast<int8_t>((value >> 7) | 0x80));
		output.push_back(static_cast<int8_t>(value & 0x7f));
		return 3;
	}
	
	if (value >> 28 == 0)
	{
		output.push_back(static_cast<int8_t>((value >> 21) | 0x80));
		output.push_back(static_cast<int8_t>((value >> 14) | 0x80));
		output.push_back(static_cast<int8_t>((value >> 7) | 0x80));
		output.push_back(static_cast<int8_t>(value & 0x7f));
		return 4;
	}
	
	if (value >> 35 == 0)
	{
		output.push_back(static_cast<int8_t>((value >> 28) | 0x80));
		output.push_back(static_cast<int8_t>((value >> 21) | 0x80));
		output.push_back(static_cast<int8_t>((value >> 14) | 0x80));
		output.push_back(static_cast<int8_t>((value >> 7) | 0x80));
		output.push_back(static_cast<int8_t>(value & 0x7f));
		return 5;
	}
	
	if (value >> 42 == 0)
	{
		output.push_back(static_cast<int8_t>((value >> 35) | 0x80));
		output.push_back(static_cast<int8_t>((value >> 28) | 0x80));
		output.push_back(static_cast<int8_t>((value >> 21) | 0x80));
		output.push_back(static_cast<int8_t>((value >> 14) | 0x80));
		output.push_back(static_cast<int8_t>((value >> 7) | 0x80));
		output.push_back(static_cast<int8_t>(value & 0x7f));
		return 6;
	}
	
	if (value >> 49 == 0)
	{
		output.push_back(static_cast<int8_t>((value >> 42) | 0x80));
		output.push_back(static_cast<int8_t>((value >> 35) | 0x80));
		output.push_back(static_cast<int8_t>((value >> 28) | 0x80));
		output.push_back(static_cast<int8_t>((value >> 21) | 0x80));
		output.push_back(static_cast<int8_t>((value >> 14) | 0x80));
		output.push_back(static_cast<int8_t>((value >> 7) | 0x80));
		output.push_back(static_cast<int8_t>(value & 0x7f));
		return 7;
	}
	
	if (value >> 56 == 0)
	{
		output.push_back(static_cast<int8_t>((value >> 49) | 0x80));
		output.push_back(static_cast<int8_t>((value >> 42) | 0x80));
		output.push_back(static_cast<int8_t>((value >> 35) | 0x80));
		output.push_back(static_cast<int8_t>((value >> 28) | 0x80));
		output.push_back(static_cast<int8_t>((value >> 21) | 0x80));
		output.push_back(static_cast<int8_t>((value >> 14) | 0x80));
		output.push_back(static_cast<int8_t>((value >> 7) | 0x80));
		output.push_back(static_cast<int8_t>(value & 0x7f));
		return 8;
	}
	
	if (value >> 63 == 0)
	{
		output.push_back(static_cast<int8_t>((value >> 56) | 0x80));
		output.push_back(static_cast<int8_t>((value >> 49) | 0x80));
		output.push_back(static_cast<int8_t>((value >> 42) | 0x80));
		output.push_back(static_cast<int8_t>((value >> 35) | 0x80));
		output.push_back(static_cast<int8_t>((value >> 28) | 0x80));
		output.push_back(static_cast<int8_t>((value >> 21) | 0x80));
		output.push_back(static_cast<int8_t>((value >> 14) | 0x80));
		output.push_back(static_cast<int8_t>((value >> 7) | 0x80));
		output.push_back(static_cast<int8_t>(value & 0x7f));
		return 9;
	}
	
	// only a negative number gets here
	output.push_back(static_cast<int8_t>(1 | 0x80));
	output.push_back(static_cast<int8_t>((value >> 56) | 0x80));
	output.push_back(static_cast<int8_t>((value >> 49) | 0x80));
	output.push_back(static_cast<int8_t>((value >> 42) | 0x80));
	output.push_back(static_cast<int8_t>((value >> 35) | 0x80));
	output.push_back(static_cast<int8_t>((value >> 28) | 0x80));
	output.push_back(static_cast<int8_t>((value >> 21) | 0x80));
	output.push_back(static_cast<int8_t>((value >> 14) | 0x80));
	output.push_back(static_cast<int8_t>((value >> 7) | 0x80));
	output.push_back(static_cast<int8_t>(value & 0x7f));
	return 10;
}

int64_t readVarInt(std::vector<int8_t>& input)
{
	int position = 0;
	int8_t b = input[position];
	int64_t result = b & 0x7f;
	
	while ((b & 0x80) != 0)
	{
		++position;
		b = input[position];
		result = (result << 7) | (b & 0x7f);
	}
	
	return result;
}

}
