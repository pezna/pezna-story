#ifndef PSL_UTILS_STATICS_H
#define PSL_UTILS_STATICS_H

namespace PSL
{

constexpr int MAX_NAME_LENGTH = 127;
constexpr int MAX_NUM_NAMED_CONTAINERS = 1'000'000'000;
constexpr int MAX_NUM_VARIABLES = 1'000'000'000;

}

#endif // PSL_UTILS_STATICS_H
