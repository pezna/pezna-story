#ifndef PSL_LEXER_H
#define PSL_LEXER_H

#include <vector>
#include <array>
#include <sstream>
#include <string>
#include <memory>

#include "token.hpp"

namespace PSL
{
using std::vector;
using std::array;
using std::istream;
using std::ostringstream;
using std::shared_ptr;
using std::string;

class Lexer
{
	/** Contains all Tokens after lex() has run */
	vector<shared_ptr<Token>> _tokens;
	/** The filepath that the input stream is reading from */
	string _filepath;
	/** The source of the code */
	istream& _inputStream;
	/** Holds the text as it is read from `inputStream_` */
	vector<char> _buffer;
	/** Current index into the `_buffer` */
	int32_t _index;
	/** The current line being read */
	int32_t _line;
	/** The offset into the current line */
	int32_t _lineOffset;
	/** The current character in `_buffer` */
	char _current;
	/** Will hold text for each capture group (ex: to get IDs or keywords) */
	ostringstream _captureBuffer;
	/** The index into `_buffer` that the capture should start with */
	int32_t _captureStart;
	
	/**
	 * Load more of the file into memory
	 */
	void loadFileIfNeeded();
	/**
	 * Advance the current character (or load more into buffer)
	 */
	void advance();
	
	/**
	 * Look-ahead at the next character
	 */
	char lka() {return lka(1);}
	char lka(int advance);
	bool shouldIgnoreNewline();
	shared_ptr<Token>& peekToken();
	void pushToken(shared_ptr<Token>& token);
	void pushToken(shared_ptr<Token>&& token);
	shared_ptr<Token> popToken();
	/**
	 * Quick way to create tokens for operators and symbols
	 */
	shared_ptr<Token> createSymbolToken(TokenType type);
	
	/**
	 * Read an identifier that meets the allowed identifier requirements
	 */
	shared_ptr<Token> readId();
	
	/**
	 * Read the raw string literal, without removing the quotes and escape characters
	 */
	shared_ptr<Token> readString(char quote);
	
	/**
	 * Read the raw multi-line string literal, without removing the quotes and escape characters
	 */
	shared_ptr<Token> readMultilineString(char quote);
	
	/**
	 * Read the current number
	 */
	shared_ptr<Token> readNumber();
	
	bool consumeFraction();
	bool consumeChar(char ch);
	bool consumeDigit();
	bool consumeBinDigit();
	bool consumeOctDigit();
	bool consumeHexDigit();
	void startCapture();
	string endCapture();
	bool isWhitespace();
	bool isDigit();
	bool isHexDigit();
	bool isOctDigit();
	bool isBinDigit();
	bool isAllowedIdentifierStart();
	bool isAllowedIdentifierPart();
	void advanceUnicodePoint();
	void skipWhitespace();
	void skipComment();
	bool isEOF();
	bool isNewline();
	void error(const string& message);
	
	int32_t getTotalOffset() {return _index;}
	int64_t getBufferSize() {return (int64_t) _buffer.size();}
	
public:
	Lexer(const string& filepath, istream& inputStream)
	:
		_filepath(filepath),
		_inputStream{inputStream},
		_index{-1},
		_line{1},
		_lineOffset{-1},
		_current{0},
		_captureStart{-1}
	{
		_buffer.reserve(4096);
	}
	
	/**
	 * Convert all text into tokens
	 */
	vector<shared_ptr<Token>> lex();
};

}

#endif //PSL_LEXER_H
