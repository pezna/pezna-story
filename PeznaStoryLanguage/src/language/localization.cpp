#include "localization.hpp"

#include <iostream>
#include <limits>

namespace PSL
{

unordered_map<string, bool> Localization::_textToBool = {
		{"true", true},
		{"false", false}
};

// static
unordered_map<string, TokenType> Localization::_keywordToType = {
		{ "do", TokenType::Do },
		{ "in", TokenType::In },
		{ "if", TokenType::If },
		{ "is", TokenType::Is },
		{ "or", TokenType::Or },
		{ "end", TokenType::End },
		{ "for", TokenType::For },
		{ "not", TokenType::Not },
		{ "and", TokenType::And },
		{ "var", TokenType::Var },
		{ "else", TokenType::Else },
		{ "goto", TokenType::Goto },
		{ "exit", TokenType::Exit },
		{ "true", TokenType::BoolLiteral },
		{ "while", TokenType::While },
		{ "label", TokenType::Label },
		{ "event", TokenType::Event },
		{ "false", TokenType::BoolLiteral },
		{ "story", TokenType::Story },
		{ "choice", TokenType::Choice },
		{ "return", TokenType::Return },
		{ "section", TokenType::Section },
		{ "include", TokenType::Include },
		{ "continue", TokenType::Continue },
		{ "sequence", TokenType::Sequence },
		{ "character", TokenType::Character },
		{ "namespace", TokenType::Namespace },
		{ "final", TokenType::Final },
		{ "bool", TokenType::Bool },
		{ "int", TokenType::Int },
		{ "float", TokenType::Float },
		{ "string", TokenType::String },
		{ "list", TokenType::List },
		{ "dict", TokenType::Dict },
		{ "default", TokenType::Default },
};

// static
unordered_map<string, SequenceCreateOption> Localization::_sequenceCreationOptions = {
		{ "loop", SequenceCreateOption::Loop },
		{ "random", SequenceCreateOption::Random },
		{ "order", SequenceCreateOption::Order }
};

const string CANNOT_START_STATEMENT_KEY = "cannot_start_statement";
const string CANNOT_START_PRIMARY_KEY = "cannot_start_primary";
const string CANNOT_START_EXPRESSION_KEY = "cannot_start_expression";
const string CANNOT_START_NAMESPACE_INSIDE_NAMESPACE_KEY = "cannot_start_namespace_inside_namespace";
const string CANNOT_START_STATEMENT_SECTION_NAMESPACE_KEY = "cannot_start_statement_section_namespace";
const string CANNOT_START_SECTION_NAMESPACE_KEY = "cannot_start_section_namespace";
const string INVALID_STATEMENT_OUTSIDE_SECTION_KEY = "invalid_statement_outside_section_key";
const string EXPECTED_ONE_KEY = "expected_one";
const string EXPECTED_MULTIPLE_KEY = "expected_multiple";
const string EXPECTED_ASSIGNMENT_KEY = "expected_assignment";
const string FOR_LOOP_EXPECTED_ID_KEY = "for_loop_expected_id";
const string INVALID_VARIABLE_TYPE = "invalid_variable_type";
const string INVALID_SEQUENCE_OPTION_KEY = "invalid_sequence_option";
const string SCOPE_NOTATION_LEFT_SIDE = "scope_notation_left_side";
const string SELECTOR_NO_EXPRESSION_KEY = "selector_no_expression";
const string PARSER_EXCEPTION_DETAIL_KEY = "parser_exception_detail";
const string SECTION_ALREADY_EXISTS_KEY = "section_already_exists";
const string SEQUENCE_NAME_IN_USE_KEY = "sequence_name_in_use";
const string MAX_ID_LENGTH_KEY = "max_id_length";
const string MAX_NUM_NAMESPACES_SECTIONS_KEY = "max_num_namespaces_sections";
const string MAX_NUM_VARIABLES_KEY = "max_num_variables";
const string VARIABLE_CREATE_MUST_BE_NAME_KEY = "variable_create_must_be_id";
const string VARIABLE_ALREADY_EXISTS_KEY = "variable_already_exists";
const string CHOICE_MUST_BE_AFTER_LINE_OR_CHOICE_KEY = "choice_must_be_after_line_or_choice";
const string LABEL_ALREADY_EXISTS_IN_SECTION_KEY = "label_already_exists_in_section";
const string NO_LOOP_FOUND_TO_CONTINUE_KEY = "no_loop_found_to_continue";
const string NO_BLOCK_FOUND_TO_EXIT_KEY = "no_block_found_to_exit";
const string VARIABLE_NOT_FOUND = "variable_not_found";
const string NAMESPACE_NOT_FOUND = "namespace_not_found";
const string SECTION_NOT_FOUND = "section_not_found";
const string NAME_NOT_FOUND = "name_not_found";
const string EXPECTED_VALUE_TYPE = "expected_value_type";
const string EXPECTED_VARIABLE_OR_VALUE = "expected_variable_or_value";
const string TERNARY_RESULT_VALUE_TYPES = "ternary_result_value_types";
const string OPERATOR_MUST_ACCEPT = "operator_must_accept";
const string OPERATOR_MUST_ACCEPT_UNARY = "operator_must_accept_unary";
const string CALL_MUST_BE_SECTION = "call_must_be_section";
const string INVALID_NUMBER_OF_ARGUMENTS = "invalid_number_of_arguments";
const string INTEGER_OUT_OF_BOUNDS = "integer_out_of_bounds";
const string FLOAT_OUT_OF_BOUNDS = "float_out_of_bounds";

// static
unordered_map<string, string> Localization::_errorMessages = {
		{ CANNOT_START_STATEMENT_KEY, "Cannot start statement with %s" },
		{ CANNOT_START_PRIMARY_KEY, "Cannot start primary expression with token: %s" },
		{ CANNOT_START_EXPRESSION_KEY, "Cannot start expression with token: %s" },
		{ CANNOT_START_NAMESPACE_INSIDE_NAMESPACE_KEY, "Cannot start a namespace inside another namespace" },
		{ CANNOT_START_STATEMENT_SECTION_NAMESPACE_KEY, "Cannot start statement, section, or namespace" },
		{ CANNOT_START_SECTION_NAMESPACE_KEY, "Cannot start section or namespace" },
		{ EXPECTED_ONE_KEY, "Expected current token type to be %s, not %s" },
		{ EXPECTED_MULTIPLE_KEY, "Expected current token type to be one of %s, not %s" },
		{ EXPECTED_ASSIGNMENT_KEY, "Expected assignment operator, not %s" },
		{ FOR_LOOP_EXPECTED_ID_KEY, "For-loop needs an Name for variable assignment" },
		{ INVALID_VARIABLE_TYPE, "Invalid variable type: %s "},
		{ INVALID_SEQUENCE_OPTION_KEY, "Invalid sequence creation option: %s" },
		{ SCOPE_NOTATION_LEFT_SIDE, "Left side of scope notation must be %s, not %s" },
		{ INVALID_STATEMENT_OUTSIDE_SECTION_KEY, "Statements outside a section must be variable definition or include statements" },
		{ SELECTOR_NO_EXPRESSION_KEY, "Selector must contain an expression between [ and ]" },
		{ PARSER_EXCEPTION_DETAIL_KEY, "%s - at token: %s" },
		{ SECTION_ALREADY_EXISTS_KEY, "Section %s in file \"%s\" on line %d:%d already exists in file \"%s\" on line %d:%d" },
		{ SEQUENCE_NAME_IN_USE_KEY, "Sequence name \"%s\" is already being used by something else" },
		{ MAX_ID_LENGTH_KEY, "ID length is %d; maximum length is %d" },
		{ MAX_NUM_NAMESPACES_SECTIONS_KEY, "Number of namespaces and sections is %d; maximum number is %d" },
		{ MAX_NUM_VARIABLES_KEY, "Number of variables is %d; maximum number is %d" },
		{ VARIABLE_CREATE_MUST_BE_NAME_KEY, "Variable creation must only have NAME on left-hand-side" },
		{ VARIABLE_ALREADY_EXISTS_KEY, "Variable \"%s\" already exists" },
		{ CHOICE_MUST_BE_AFTER_LINE_OR_CHOICE_KEY, "Choice must be after a line or another choice" },
		{ LABEL_ALREADY_EXISTS_IN_SECTION_KEY, "Label \"%s\" already exists in section \"%s\"" },
		{ NO_LOOP_FOUND_TO_CONTINUE_KEY, "No compatible loop found to continue" },
		{ NO_BLOCK_FOUND_TO_EXIT_KEY, "No compatible block found to exit" },
		{ VARIABLE_NOT_FOUND, "Variable \"%s\" not found" },
		{ NAMESPACE_NOT_FOUND, "Namespace \"%s\" not found" },
		{ SECTION_NOT_FOUND, "Section \"%s\" not found" },
		{ NAME_NOT_FOUND, "No namespace, section, or variable named \"%s\" was found" },
		{ EXPECTED_VALUE_TYPE, "Expected value type \"%s\", not \"%s\"" },
		{ EXPECTED_VARIABLE_OR_VALUE, "Expected a variable or value type" },
		{ TERNARY_RESULT_VALUE_TYPES, "Ternary result value types must be the same on true and false sides, not \"%s\" and \"%s\"" },
		{ OPERATOR_MUST_ACCEPT, "Operator %s was given \"%s\" and \"%s\", but requires %s on the left-side, and %s on the right-side" },
		{ OPERATOR_MUST_ACCEPT_UNARY, "Operator %s was given \"%s\", but requires %s" },
		{ CALL_MUST_BE_SECTION, "Section call must be a section name" },
		{ INVALID_NUMBER_OF_ARGUMENTS, "Section can only take %d arguments, not %d" },
		{ INTEGER_OUT_OF_BOUNDS, "Integer must be between %d and %d, not %s" },
		{ FLOAT_OUT_OF_BOUNDS, "Float must be between %f and %f, not %s" },
};

// static
void Localization::setKeywordToType(unordered_map<string, TokenType> keywordToType)
{
	Localization::_keywordToType = keywordToType;
}

// static
void Localization::setSequenceCreationOptions(unordered_map<string, SequenceCreateOption> sequenceCreationOptions)
{
	Localization::_sequenceCreationOptions = sequenceCreationOptions;
}

// static
void Localization::setErrorMessages(unordered_map<string, string> errorMessages)
{
	Localization::_errorMessages = errorMessages;
}

// static
bool Localization::getBool(const string& text)
{
	auto it = Localization::_textToBool.find(text);
	if (it != Localization::_textToBool.end())
	{
		return it->second;
	}
	
	return false;
}

// static
TokenType Localization::getKeywordType(const string& text, const TokenType& defaultReturn)
{
	auto it = Localization::_keywordToType.find(text);
	if (it != Localization::_keywordToType.end())
	{
		return it->second;
	}
	
	return defaultReturn;
}

// static
SequenceCreateOption Localization::getSequenceCreateOption(const string& text)
{
	auto it = Localization::_sequenceCreationOptions.find(text);
	if (it != Localization::_sequenceCreationOptions.end())
	{
		return it->second;
	}
	
	return SequenceCreateOption::Invalid;
}

// static
string Localization::expectedOne(const string& expected, const string& nott)
{
	string& message = Localization::_errorMessages[EXPECTED_ONE_KEY];
	int size = message.size() + expected.length() + nott.length();
	char buf[size];
	std::snprintf(buf, size, message.c_str(), expected.c_str(), nott.c_str());
	return string(buf);
}

// static
string Localization::expectedMultiple(const string& expected, const string& nott)
{
	string& message = Localization::_errorMessages[EXPECTED_MULTIPLE_KEY];
	int size = message.size() + expected.length() + nott.length();
	char buf[size];
	std::snprintf(buf, size, message.c_str(), expected.c_str(), nott.c_str());
	return string(buf);
}

// static
string Localization::expectedAssignment(const string& nott)
{
	string& message = Localization::_errorMessages[EXPECTED_ASSIGNMENT_KEY];
	int size = message.size() + nott.length();
	char buf[size];
	std::snprintf(buf, size, message.c_str(), nott.c_str());
	return string(buf);
}

// static
string Localization::invalidStatementOutsideSection()
{
	return Localization::_errorMessages[INVALID_STATEMENT_OUTSIDE_SECTION_KEY];
}

// static
string Localization::parserExceptionDetail(const string& message, const string& token)
{
	string& detail = Localization::_errorMessages[PARSER_EXCEPTION_DETAIL_KEY];
	int size = detail.size() + message.size() + token.length();
	char buf[size];
	std::snprintf(buf, size, detail.c_str(), message.c_str(), token.c_str());
	return string(buf);
}

// static
string Localization::selectorNoExpression()
{
	return Localization::_errorMessages[SELECTOR_NO_EXPRESSION_KEY];
}

// static
string Localization::invalidSequenceOption(const string& option)
{
	string& message = Localization::_errorMessages[INVALID_SEQUENCE_OPTION_KEY];
	int size = message.size() + option.length();
	char buf[size];
	std::snprintf(buf, size, message.c_str(), option.c_str());
	return string(buf);
}

string Localization::cannotStartNamespaceInsideNamespace()
{
	return Localization::_errorMessages[CANNOT_START_NAMESPACE_INSIDE_NAMESPACE_KEY];
}

// static
string Localization::cannotStartStatementSectionNamespace()
{
	return Localization::_errorMessages[CANNOT_START_STATEMENT_SECTION_NAMESPACE_KEY];
}

// static
string Localization::cannotStartSectionNamespace()
{
	return Localization::_errorMessages[CANNOT_START_SECTION_NAMESPACE_KEY];
}

// static
string Localization::cannotStartStatement(const string& token)
{
	string& message = Localization::_errorMessages[CANNOT_START_STATEMENT_KEY];
	int size = message.size() + token.length();
	char buf[size];
	std::snprintf(buf, size, message.c_str(), token.c_str());
	return string(buf);
}

// static
string Localization::cannotStartPrimary(const string& tokenType)
{
	string& message = Localization::_errorMessages[CANNOT_START_PRIMARY_KEY];
	int size = message.size() + tokenType.length();
	char buf[size];
	std::snprintf(buf, size, message.c_str(), tokenType.c_str());
	return string(buf);
}

// static
string Localization::cannotStartExpression(const string& tokenType)
{
	string& message = Localization::_errorMessages[CANNOT_START_EXPRESSION_KEY];
	int size = message.size() + tokenType.length();
	char buf[size];
	std::snprintf(buf, size, message.c_str(), tokenType.c_str());
	return string(buf);
}

// static
string Localization::forLoopExpectedId()
{
	return Localization::_errorMessages[FOR_LOOP_EXPECTED_ID_KEY];
}

// static
string Localization::invalidVariableType(const string& tokenType)
{
	string& message = Localization::_errorMessages[INVALID_VARIABLE_TYPE];
	int size = message.size() + tokenType.length();
	char buf[size];
	std::snprintf(buf, size, message.c_str(), tokenType.c_str());
	return string(buf);
}

// static
string Localization::scopeNotationLeftSide(const string& expected, const string& nott)
{
	string& message = Localization::_errorMessages[SCOPE_NOTATION_LEFT_SIDE];
	int size = message.size() + expected.length() + nott.length();
	char buf[size];
	std::snprintf(buf, size, message.c_str(), expected.c_str(), nott.c_str());
	return string(buf);
}

// static
string Localization::sectionAlreadyExists(const string& sectionName, const string& newFilepath, int64_t newLine, int64_t newColumn,
			const string& existingFilepath, int64_t existingLine, int64_t existingColumn)
{
	string& message = Localization::_errorMessages[SECTION_ALREADY_EXISTS_KEY];
	int size = message.length() + sectionName.length() + newFilepath.length() + existingFilepath.length() + 30;
	char buf[size];
	std::snprintf(buf, size, message.c_str(), sectionName.c_str(), newFilepath.c_str(), newLine, newColumn, existingFilepath.c_str(), existingLine, existingColumn);
	return string(buf);
}

string Localization::sequenceNameAlreadyInUse(const string& name)
{
	string& message = Localization::_errorMessages[SEQUENCE_NAME_IN_USE_KEY];
	int size = message.size() + name.length();
	char buf[size];
	std::snprintf(buf, size, message.c_str(), name.c_str());
	return string(buf);
}

// static
string Localization::maxIdLength(int given, int limit)
{
	string& detail = Localization::_errorMessages[MAX_ID_LENGTH_KEY];
	int size = detail.size() + 20;
	char buf[size];
	std::snprintf(buf, size, detail.c_str(), given, limit);
	return string(buf);
}

// static
string Localization::maxNumNamespacesSections(int given, int limit)
{
	string& detail = Localization::_errorMessages[MAX_NUM_NAMESPACES_SECTIONS_KEY];
	int size = detail.size() + 20;
	char buf[size];
	std::snprintf(buf, size, detail.c_str(), given, limit);
	return string(buf);
}

// static
string Localization::maxNumVariables(int given, int limit)
{
	string& detail = Localization::_errorMessages[MAX_NUM_VARIABLES_KEY];
	int size = detail.size() + 20;
	char buf[size];
	std::snprintf(buf, size, detail.c_str(), given, limit);
	return string(buf);
}

// static
string Localization::variableCreateMustBeName()
{
	return Localization::_errorMessages[VARIABLE_CREATE_MUST_BE_NAME_KEY];
}

// static
string Localization::variableAlreadyExists(const string& name)
{
	string& message = Localization::_errorMessages[VARIABLE_ALREADY_EXISTS_KEY];
	int size = message.size() + name.length();
	char buf[size];
	std::snprintf(buf, size, message.c_str(), name.c_str());
	return string(buf);
}

// static
string Localization::choiceMustBeAfterLineOrChoice()
{
	return Localization::_errorMessages[CHOICE_MUST_BE_AFTER_LINE_OR_CHOICE_KEY];
}

// static
string Localization::labelAlreadyExistsInSection(const string& label, const string& section)
{
	string& message = Localization::_errorMessages[LABEL_ALREADY_EXISTS_IN_SECTION_KEY];
	int size = message.size() + label.length() + section.length();
	char buf[size];
	std::snprintf(buf, size, message.c_str(), label.c_str(), section.c_str());
	return string(buf);
}

// static
string Localization::noLoopFoundToContinue()
{
	return Localization::_errorMessages[NO_LOOP_FOUND_TO_CONTINUE_KEY];
}

// static
string Localization::noBlockFoundToExit()
{
	return Localization::_errorMessages[NO_BLOCK_FOUND_TO_EXIT_KEY];
}

// static
string Localization::variableNotFound(const string& name)
{
	string& message = Localization::_errorMessages[VARIABLE_NOT_FOUND];
	int size = message.size() + name.length();
	char buf[size];
	std::snprintf(buf, size, message.c_str(), name.c_str());
	return string(buf);
}

// static
string Localization::namespaceNotFound(const string& name)
{
	string& message = Localization::_errorMessages[NAMESPACE_NOT_FOUND];
	int size = message.size() + name.length();
	char buf[size];
	std::snprintf(buf, size, message.c_str(), name.c_str());
	return string(buf);
}

// static
string Localization::sectionNotFound(const string& name)
{
	string& message = Localization::_errorMessages[SECTION_NOT_FOUND];
	int size = message.size() + name.length();
	char buf[size];
	std::snprintf(buf, size, message.c_str(), name.c_str());
	return string(buf);
}

// static
string Localization::nameNotFound(const string& name)
{
	string& message = Localization::_errorMessages[NAME_NOT_FOUND];
	int size = message.size() + name.length();
	char buf[size];
	std::snprintf(buf, size, message.c_str(), name.c_str());
	return string(buf);
}

// static
string Localization::expectedValueType(const string& expected, const string& given)
{
	string& message = Localization::_errorMessages[EXPECTED_VALUE_TYPE];
	int size = message.size() + expected.length() + given.length();
	char buf[size];
	std::snprintf(buf, size, message.c_str(), expected.c_str(), given.c_str());
	return string(buf);
}

// static
string Localization::expectedVariableOrValue()
{
	return Localization::_errorMessages[EXPECTED_VARIABLE_OR_VALUE];
}

// static
string Localization::ternaryResultValueTypes(const string& trueType, const string& falseType)
{
	string& message = Localization::_errorMessages[TERNARY_RESULT_VALUE_TYPES];
	int size = message.size() + trueType.length() + falseType.length();
	char buf[size];
	std::snprintf(buf, size, message.c_str(), trueType.c_str(), falseType.c_str());
	return string(buf);
}

// static
string Localization::operatorMustAccept(const string& operatorName, const string& leftGiven, const string& rightGiven, const string& leftRequired, const string& rightRequired)
{
	string& message = Localization::_errorMessages[OPERATOR_MUST_ACCEPT];
	int size = message.size() + operatorName.length() + leftGiven.length() + rightGiven.length() + leftRequired.length() + rightRequired.length();
	char buf[size];
	std::snprintf(buf, size, message.c_str(), operatorName.c_str(), leftGiven.c_str(), rightGiven.c_str(), leftRequired.c_str(), rightRequired.c_str());
	return string(buf);
}

// static
string Localization::operatorMustAccept(const string& operatorName, const string& given, const string& required)
{
	string& message = Localization::_errorMessages[OPERATOR_MUST_ACCEPT_UNARY];
	int size = message.size() + operatorName.length() + given.length() + required.length();
	char buf[size];
	std::snprintf(buf, size, message.c_str(), operatorName.c_str(), given.c_str(), required.c_str());
	return string(buf);
}

// static
string Localization::callMustBeSection()
{
	return Localization::_errorMessages[CALL_MUST_BE_SECTION];
}

// static
string Localization::invalidNumberOfArguments(int numParameters, int numArguments)
{
	string& detail = Localization::_errorMessages[INVALID_NUMBER_OF_ARGUMENTS];
	int size = detail.size() + 20;
	char buf[size];
	std::snprintf(buf, size, detail.c_str(), numParameters, numArguments);
	return string(buf);
}

string Localization::integerOutOfBounds(const string& given)
{
	string& message = Localization::_errorMessages[INTEGER_OUT_OF_BOUNDS];
	int size = message.size() + given.length() + 40;
	char buf[size];
	std::snprintf(buf, size, message.c_str(), std::numeric_limits<int64_t>::min(), std::numeric_limits<int64_t>::max(), given.c_str());
	return string(buf);
}

string Localization::floatOutOfBounds(const string& given)
{
	string& message = Localization::_errorMessages[FLOAT_OUT_OF_BOUNDS];
	int size = message.size() + given.length() + 60;
	char buf[size];
	std::snprintf(buf, size, message.c_str(), std::numeric_limits<double>::min(), std::numeric_limits<double>::max(), given.c_str());
	return string(buf);
}

}
