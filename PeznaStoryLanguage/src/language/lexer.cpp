#include "lexer.hpp"

#include <utf8.h>

#include <iostream>
#include <ctype.h>
#include <stdexcept>
#include <utility>

#include "exceptions.hpp"
#include "utils/utils.hpp"

namespace PSL
{

/*********
 * Lexer *
 *********/

void Lexer::loadFileIfNeeded()
{
	static constexpr int readSize = 4096;
	if (_index == -1)
	{
		int amountRead = 0;
		do
		{
			char readBuffer[readSize + 1];
			// add more text to the buffer if possible
			_inputStream.read(readBuffer, readSize);
			amountRead = _inputStream.gcount();
			if (amountRead > 0)
			{
				_buffer.insert(_buffer.end(), readBuffer, readBuffer + amountRead);
			}
		}
		while (amountRead > 0);
		
		bool eofStream = _inputStream.eof();
		// Do not use eof_ in this condition, because failbit is set when getting to the
		// end of the file. `eof_` is used for the Lexer's `buffer_`
		if (!eofStream && _inputStream.fail())
		{
			string msg = "Error while attempting to read or close text stream. ";
			msg += _inputStream.bad() ? "badbit" : "";
			throw LexerReadException(_filepath, msg);
		}
	}
}

void Lexer::advance()
{
	if (!isEOF())
	{
		loadFileIfNeeded();

		if (_current == '\n')
		{
			++_line;
			//total offset into the text after this newline occurs
			_lineOffset = getTotalOffset();
		}

		if (_index < getBufferSize())
		{
			_index += 1;
			_current = _buffer[_index];
		}
		else
		{
			_index += 1;
			_current = '\0';
		}
	}
}

bool Lexer::shouldIgnoreNewline()
{
	if (_tokens.size() == 0)
	{
		return false;
	}

	shared_ptr<Token>& peeked = peekToken();
	if (peeked->isAssignment() || peeked->isBinaryOperator())
	{
		return true;
	}

	switch (peeked->type)
	{
		case TokenType::Arrow:
		case TokenType::IgnoreNewline:
		case TokenType::Newline:
		case TokenType::Dot:
		case TokenType::DoubleColon:
		case TokenType::Ternary:
			return true;
		default:
			return false;
	}
}

vector<shared_ptr<Token>> Lexer::lex()
{
	_tokens.clear();

	int openParen = 0;
	int openBrack = 0;
	int openBrace = 0;

	advance(); //load first character
	while (!isEOF())
	{
		int32_t currentOffset = getTotalOffset();
		int column = currentOffset - _lineOffset;
		if (isWhitespace())
		{
			if (isNewline())
			{
				if (openParen == 0 && openBrack == 0 && openBrace == 0 && !shouldIgnoreNewline())
				{
					pushToken(std::make_shared<Token>(TokenType::Newline, "\n", _line, column, currentOffset, currentOffset + 1));
				}
				else if (peekToken()->type == TokenType::IgnoreNewline)
				{
					popToken(); //remove the IgnoreNewline
				}
			}

			skipWhitespace();
			continue;
		}

		char la = lka();
		switch (_current)
		{
			case '=':
				if (la == '=')
				{
					pushToken(createSymbolToken(TokenType::Equals));
					advance();
					advance();
					continue;
				}

				pushToken(createSymbolToken(TokenType::Assign));
				advance();
				continue;
			case '!':
				if (la == '=')
				{
					pushToken(createSymbolToken(TokenType::NotEquals));
					advance();
					advance();
					continue;
				}

				pushToken(createSymbolToken(TokenType::Not));
				advance();
				continue;
			case '>':
				if (la == '>')
				{
					if (lka(2) == '=')
					{
						pushToken(createSymbolToken(TokenType::BitRightShiftAssign));
						advance();
						advance();
						advance();
						continue;
					}

					pushToken(createSymbolToken(TokenType::BitRightShift));
					advance();
					advance();
					continue;
				}
				else if (la == '=')
				{
					pushToken(createSymbolToken(TokenType::GreaterThanEquals));
					advance();
					advance();
					continue;
				}

				pushToken(createSymbolToken(TokenType::GreaterThan));
				advance();
				continue;
			case '<':
				if (la == '<')
				{
					if (lka(2) == '=')
					{
						pushToken(createSymbolToken(TokenType::BitLeftShiftAssign));
						advance();
						advance();
						advance();
						continue;
					}

					pushToken(createSymbolToken(TokenType::BitLeftShift));
					advance();
					advance();
					continue;
				}
				else if (la == '=')
				{
					pushToken(createSymbolToken(TokenType::LessThanEquals));
					advance();
					advance();
					continue;
				}

				pushToken(createSymbolToken(TokenType::LessThan));
				advance();
				continue;
			case '&':
				if (la == '&')
				{
					pushToken(createSymbolToken(TokenType::And));
					advance();
					advance();
					continue;
				}
				else if (la == '=')
				{
					pushToken(createSymbolToken(TokenType::BitAndAssign));
					advance();
					advance();
					continue;
				}

				pushToken(createSymbolToken(TokenType::BitAnd));
				advance();
				continue;
			case '^':
				if (la == '=')
				{
					pushToken(createSymbolToken(TokenType::BitXorAssign));
					advance();
					advance();
					continue;
				}

				pushToken(createSymbolToken(TokenType::BitXor));
				advance();
				continue;
			case '|':
				if (la == '|')
				{
					pushToken(createSymbolToken(TokenType::Or));
					advance();
					advance();
					continue;
				}
				else if (la == '=')
				{
					pushToken(createSymbolToken(TokenType::BitOrAssign));
					advance();
					advance();
					continue;
				}

				pushToken(createSymbolToken(TokenType::BitOr));
				advance();
				continue;
			case '~':
				pushToken(createSymbolToken(TokenType::BitNot));
				advance();
				continue;
			case '*':
				if (la == '*')
				{
					if (lka(2) == '=')
					{
						pushToken(createSymbolToken(TokenType::PowAssign));
						advance();
						advance();
						advance();
						continue;
					}

					pushToken(createSymbolToken(TokenType::Pow));
					advance();
					advance();
					continue;
				}
				else if (la == '=')
				{
					pushToken(createSymbolToken(TokenType::MulAssign));
					advance();
					advance();
					continue;
				}

				pushToken(createSymbolToken(TokenType::Mul));
				advance();
				continue;
			case '+':
				if (la == '=')
				{
					pushToken(createSymbolToken(TokenType::AddAssign));
					advance();
					advance();
					continue;
				}

				pushToken(createSymbolToken(TokenType::Add));
				advance();
				continue;
			case '-':
				if (la == '>')
				{
					pushToken(createSymbolToken(TokenType::Arrow));
					advance();
					advance();
					continue;
				}
				else if (la == '=')
				{
					pushToken(createSymbolToken(TokenType::SubAssign));
					advance();
					advance();
					continue;
				}

				pushToken(createSymbolToken(TokenType::Sub));
				advance();
				continue;
			case '/':
				if (la == '=')
				{
					pushToken(createSymbolToken(TokenType::DivAssign));
					advance();
					advance();
					continue;
				}

				pushToken(createSymbolToken(TokenType::Div));
				advance();
				continue;
			case '%':
				if (la == '=')
				{
					pushToken(createSymbolToken(TokenType::ModAssign));
					advance();
					advance();
					continue;
				}

				pushToken(createSymbolToken(TokenType::Mod));
				advance();
				continue;
			case '[':
				++openBrack;
				pushToken(createSymbolToken(TokenType::OpenBrack));
				advance();
				continue;
			case ']':
				--openBrack;
				pushToken(createSymbolToken(TokenType::CloseBrack));
				advance();
				continue;
			case '{':
				++openBrace;
				pushToken(createSymbolToken(TokenType::OpenBrace));
				advance();
				continue;
			case '}':
				--openBrace;
				pushToken(createSymbolToken(TokenType::CloseBrace));
				advance();
				continue;
			case '(':
				++openParen;
				pushToken(createSymbolToken(TokenType::OpenParen));
				advance();
				continue;
			case ')':
				--openParen;
				pushToken(createSymbolToken(TokenType::CloseParen));
				advance();
				continue;
			case ',':
				pushToken(createSymbolToken(TokenType::Comma));
				advance();
				continue;
			case ':':
				if (la == ':')
				{
					pushToken(createSymbolToken(TokenType::DoubleColon));
					advance();
					advance();
					continue;
				}
				pushToken(createSymbolToken(TokenType::Colon));
				advance();
				continue;
			case ';':
				pushToken(createSymbolToken(TokenType::SemiColon));
				advance();
				continue;
			case '.':
				pushToken(createSymbolToken(TokenType::Dot));
				advance();
				continue;
			case '?':
				pushToken(createSymbolToken(TokenType::Ternary));
				advance();
				continue;
			case '\\':
				if (openParen > 0 || openBrack > 0 || openBrace > 0)
				{
					advance();
					continue;
				}

				pushToken(createSymbolToken(TokenType::IgnoreNewline));
				advance();
				continue;
			case '#':
				skipComment();
				continue;
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				pushToken(readNumber());
				continue;
			case '"':
			case '\'':
				if (la == _current && lka(2) == _current)
				{
					// multi-line string
					pushToken(readMultilineString(_current));
					continue;
				}
				pushToken(readString(_current));
				continue;
			default:
				if (isAllowedIdentifierStart())
				{
					shared_ptr<Token> token = readId();

					if (_tokens.size() > 0)
					{
						const TokenType& peekType = peekToken()->type;

						if (token->type == TokenType::If && peekType == TokenType::Else)
						{
							shared_ptr<Token> elseToken = popToken(); // remove the 'else' already there
							pushToken(std::make_shared<Token>(TokenType::ElseIf, "else if", elseToken->line, elseToken->column, elseToken->startOffset, token->endOffset));
							continue;
						}
						else if (token->type == TokenType::Is && peekType == TokenType::Not)
						{
							// not is | ! is
							// remove the 'not' already there
							shared_ptr<Token> notToken = popToken();
							pushToken(std::make_shared<Token>(TokenType::IsNot, "not is", notToken->line, notToken->column, notToken->startOffset, token->endOffset));
							continue;
						}
						else if (token->type == TokenType::Not && peekType == TokenType::Is)
						{
							// is not
							// remove the 'is' already there
							shared_ptr<Token> isToken = popToken();
							pushToken(std::make_shared<Token>(TokenType::IsNot, "is not", isToken->line, isToken->column, isToken->startOffset, token->endOffset));
							continue;
						}
						else if (token->type == TokenType::In)
						{
							if (peekType == TokenType::Is)
							{
								// is in
								// remove the 'is' already there
								shared_ptr<Token> isToken = popToken();
								pushToken(std::make_shared<Token>(TokenType::In, "is in", isToken->line, isToken->column, isToken->startOffset, token->endOffset));
								continue;
							}
							else if (peekType == TokenType::IsNot)
							{
								// is not in
								// remove the 'is not' already there
								shared_ptr<Token> isNotToken = popToken();
								pushToken(std::make_shared<Token>(TokenType::NotIn, "is not in", isNotToken->line, isNotToken->column, isNotToken->startOffset, token->endOffset));
								continue;
							}
							else if (peekType == TokenType::Not)
							{
								// not int
								// remove the 'not' already there
								shared_ptr<Token> notToken = popToken();
								pushToken(std::make_shared<Token>(TokenType::NotIn, "not in", notToken->line, notToken->column, notToken->startOffset, token->endOffset));
								continue;
							}
						}
					}

					pushToken(std::move(token));
					continue;
				}
				else if (isAllowedIdentifierPart())
				{
					// failed to meet start requirements, but is an allowed identifier part
					error(string("Cannot start identifier with character: '").append(1, _current).append(1, '\''));
				}
				
				std::cout << "Print unallowed " << std::to_string((int)_current) << " line " << _line << " tokens size " << _tokens.size() << std::endl;
				if (_tokens.size())
				{
					std::cout << "peek " << *peekToken() << std::endl;
				}
				error(string("Unallowed character: '").append(1, _current).append(1, '\''));
			break;
		}
	}

	// don't check for openers to be fully closed because we won't be able to
	// adequately report the line and column of the first opener
	return _tokens;
}

char Lexer::lka(int advance)
{
	if (_index + advance < getBufferSize())
	{
		return _buffer[_index + advance];
	}
	
	return 0;
}

shared_ptr<Token>& Lexer::peekToken()
{
	return _tokens.back();
}

void Lexer::pushToken(shared_ptr<Token>& token)
{
	_tokens.push_back(token);
}

void Lexer::pushToken(shared_ptr<Token>&& token)
{
	_tokens.push_back(token);
}

shared_ptr<Token> Lexer::popToken()
{
	shared_ptr<Token> token = _tokens.back();
	_tokens.pop_back();
	return token;
}

shared_ptr<Token> Lexer::createSymbolToken(TokenType type)
{
	int currentOffset = getTotalOffset();
	int column = currentOffset - _lineOffset;
	string text = Token::getSymbolText(type);
	return std::make_shared<Token>(type, std::move(text), _line, column, currentOffset, currentOffset + text.length());
}

shared_ptr<Token> Lexer::readId()
{
	startCapture();
	int startOffset = getTotalOffset();
	int column = startOffset - _lineOffset;

	do
	{
		advanceUnicodePoint();
	}
	while (isAllowedIdentifierPart());

	string text = endCapture();
	TokenType type = Token::getKeywordType(text);

	return std::make_shared<Token>(type, std::move(text), _line, column, startOffset, startOffset + text.length());
}

shared_ptr<Token> Lexer::readString(char quote)
{
	startCapture();
	int startOffset = getTotalOffset();
	int column = startOffset - _lineOffset;
	int line = _line;

	advance(); //move past the first quote

	while (_current != quote)
	{
		if (_current == '\\')
		{
			advance();
		}

		if (isNewline())
		{
			error("Single-line string cannot contain raw newline. Escape it: \\n");
		}

		advance();
	}

	advance(); //move past the last quote

	string text = endCapture();
	return std::make_shared<Token>(TokenType::StringLiteral, std::move(text), line, column, startOffset, startOffset + text.length());
}

shared_ptr<Token> Lexer::readMultilineString(char quote)
{
	startCapture();
	int startOffset = getTotalOffset();
	int column = startOffset - _lineOffset;
	int line = _line;

	advance();
	advance();
	advance(); //move past the first 3 quotes

	while (_current != quote || (lka(1) != quote || lka(2) != quote))
	{
		if (_current == '\\')
		{
			advance();
		}

		advance();
	}
	advance();
	advance();
	advance(); //move past the last 3 quotes

	string text = endCapture();
	return std::make_shared<Token>(TokenType::StringLiteral, std::move(text), line, column, startOffset, startOffset + text.length());
}

shared_ptr<Token> Lexer::readNumber()
{
	startCapture();
	int startOffset = getTotalOffset();
	int column = startOffset - _lineOffset;

	bool special = false;
	bool decimal = false;
	if (_current == '0')
	{
		//check if it is a "special" number
		char la = lka();
		if (la == 'x')
		{
			//hex number
			special = true;
			advance(); //move past 0
			advance(); //move past 'x'

			while (consumeHexDigit()) {}
			//a special literal should only have non-identifier characters after it
			//a hex literal must not simply be '0x'
			if (isAllowedIdentifierPart() || (_index - _captureStart) == 2)
			{
				error(string("Invalid hexadecimal literal: '").append(1, _current).append(1, '\''));
			}
		}
		else if (la == 'o')
		{
			//oct number
			special = true;
			advance(); //move past 0
			advance(); //move past 'o'

			while (consumeOctDigit()) {}

			if (isAllowedIdentifierPart() || (_index - _captureStart) == 2)
			{
				error(string("Invalid octal literal: '").append(1, _current).append(1, '\''));
			}
		}
		else if (la == 'b')
		{
			//binary number
			special = true;
			advance(); //move past 0
			advance(); //move past 'b'

			while (consumeBinDigit()) {}

			if (isAllowedIdentifierPart() || (_index - _captureStart) == 2)
			{
				error(string("Invalid binary literal: '").append(1, _current).append(1, '\''));
			}
		}
	}

	while (consumeDigit()) {}

	if (!special)
	{
		decimal = consumeFraction();
	}

	string text = endCapture();
	return std::make_shared<Token>(decimal ? TokenType::FloatLiteral : TokenType::IntLiteral, std::move(text), _line, column, startOffset, startOffset + text.length());
}

bool Lexer::consumeFraction()
{
	if (!consumeChar('.'))
	{
		return false;
	}

	if (!isDigit())
	{
		error("Fraction must have digits after '.'");
	}

	while (consumeDigit()) {}

	return true;
}

bool Lexer::consumeChar(char ch)
{
	if (_current != ch)
	{
		return false;
	}
	advance();
	return true;
}

bool Lexer::consumeDigit()
{
	if (!isDigit())
	{
		return false;
	}
	advance();
	return true;
}

bool Lexer::consumeBinDigit()
{
	if (!isBinDigit())
	{
		return false;
	}
	advance();
	return true;
}

bool Lexer::consumeOctDigit()
{
	if (!isOctDigit())
	{
		return false;
	}
	advance();
	return true;
}

bool Lexer::consumeHexDigit()
{
	if (!isHexDigit())
	{
		return false;
	}
	advance();
	return true;
}

void Lexer::startCapture()
{
	// start at the current character
	_captureStart = _index;
}

string Lexer::endCapture()
{
	int numChars = _index - _captureStart;
	string captured;
	if (_captureBuffer.tellp() > 0)
	{
		_captureBuffer << string(&(_buffer.front()) + _captureStart, numChars);
		captured = _captureBuffer.str();
		_captureBuffer.str("");
		_captureBuffer.clear();
	}
	else
	{
		captured = string(&(_buffer.front()) + _captureStart, numChars);
	}

	return captured;
}

bool Lexer::isWhitespace()
{
	return isspace(_current);
}

bool Lexer::isDigit()
{
	return _current >= '0' && _current <= '9';
}

bool Lexer::isHexDigit()
{
	return isDigit() ||
			(_current >= 'a' && _current <= 'f') ||
			(_current >= 'A' && _current <= 'F');
}

bool Lexer::isOctDigit()
{
	return (_current >= '0' && _current <= '7');
}

bool Lexer::isBinDigit()
{
	return (_current == '0' || _current == '1');
}

bool Lexer::isAllowedIdentifierStart()
{
	try
	{
		uint32_t codePoint = utf8::peek_next(&_buffer[_index], &_buffer.back() + 1);
		return Utils::isIdentifierStart(codePoint);
	}
	catch (utf8::not_enough_room& ex)
	{
		return false;
	}
}

bool Lexer::isAllowedIdentifierPart()
{
	try
	{
		uint32_t codePoint = utf8::peek_next(&_buffer[_index], &_buffer.back() + 1);
		return Utils::isIdentifierPart(codePoint);
	}
	catch (utf8::not_enough_room& ex)
	{
		return false;
	}
}

void Lexer::advanceUnicodePoint()
{
	const char* start1 = &_buffer[_index];
	const char* start2 = &_buffer[_index];
	const char* end = &_buffer.back() + 1;
	utf8::next(start2, end);

	_index += (start2 - start1);
	_current = *start2;
}

void Lexer::skipWhitespace()
{
	while (isWhitespace())
	{
		advance();
	}
}

void Lexer::skipComment()
{
	while (!isEOF() && !isNewline())
	{
		advance();
	}
}

bool Lexer::isEOF()
{
	return _index >= (int64_t)getBufferSize();
}

bool Lexer::isNewline()
{
	return _current == '\r' || _current == '\n';
}

void Lexer::error(const string& message)
{
	int column = getTotalOffset() - _lineOffset;
	int offset = getTotalOffset();
	int currLineInBuffer = _index - column + 1; //index after newline
	int nextLineInBuffer = currLineInBuffer + 1;

	while (nextLineInBuffer < getBufferSize() && _buffer[nextLineInBuffer] != '\n')
	{
		++nextLineInBuffer;
	}
	
	string lineText(&_buffer[currLineInBuffer], nextLineInBuffer - currLineInBuffer);
	shared_ptr<Token> lastToken = (_tokens.size() > 0)? _tokens.back() : nullptr;
	throw LexerException(_filepath, message, lineText, _line, column, offset, lastToken);
}

}
