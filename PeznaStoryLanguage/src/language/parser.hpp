#ifndef PSL_PARSER_H
#define PSL_PARSER_H

#include <sstream>
#include <stdexcept>
#include <initializer_list>
#include <memory>

#include "localization.hpp"
#include "nodes/nodes.hpp"

namespace PSL
{
using std::ostringstream;
using std::shared_ptr;

class Parser
{
public:
	Parser(const string& filepath, vector<shared_ptr<Token>> tokens)
	:
		_filepath(filepath),
		_tokens(tokens),
		_current{nullptr},
		_index{0}
	{
		_tokens.push_back(std::make_shared<Token>(TokenType::Eof, "", 4294967295, 0, 4294967295, 4294967295));
	}
	
	shared_ptr<StoryNode> parse();
	
private:
	string _filepath;
	vector<shared_ptr<Token>> _tokens;
	shared_ptr<Token> _current;
	int64_t _index;
	
	Token* lka() {return lka(1);}
	Token* lka(int advance);
	
	shared_ptr<StoryNode> story();
	shared_ptr<NamespaceNode> namespaceBlock();
	shared_ptr<SectionNode> sectionBlock();
	vector<shared_ptr<ParameterNode>> parameters();
	shared_ptr<ParameterNode> parameter();
	vector<shared_ptr<StatementNode>> statementList(bool outsideSection = false);
	shared_ptr<StatementNode> statement();
	shared_ptr<AssignmentStatementNode> variableCreateStatement();
	shared_ptr<IfStatementNode> ifStatement();
	shared_ptr<IfPartNode> ifPart();
	shared_ptr<ElseIfPartNode> elseIfPart();
	shared_ptr<ElsePartNode> elsePart();
	shared_ptr<WhileStatementNode> whileStatement();
	shared_ptr<ForStatementNode> forStatement();
	shared_ptr<DoStatementNode> doStatement();
	shared_ptr<GotoStatementNode> gotoStatement();
	shared_ptr<LabelStatementNode> labelStatement();
	shared_ptr<ChoiceStatementNode> choiceStatement();
	shared_ptr<ExitStatementNode> exitStatement();
	shared_ptr<ContinueStatementNode> continueStatement();
	shared_ptr<ReturnStatementNode> returnStatement();
	shared_ptr<IncludeStatementNode> includeStatement();
	shared_ptr<EventStatementNode> eventStatement();
	
	vector<shared_ptr<ExpressionNode>> expressionList();
	shared_ptr<ExpressionNode> expression();
	shared_ptr<ExpressionNode> ternaryExpression();
	shared_ptr<ExpressionNode> orExpression();
	shared_ptr<ExpressionNode> orExpression(shared_ptr<ExpressionNode> left);
	shared_ptr<ExpressionNode> andExpression();
	shared_ptr<ExpressionNode> andExpression(shared_ptr<ExpressionNode> left);
	shared_ptr<ExpressionNode> notExpression();
	shared_ptr<ExpressionNode> inExpression();
	shared_ptr<ExpressionNode> inExpression(shared_ptr<ExpressionNode> left);
	shared_ptr<ExpressionNode> isExpression();
	shared_ptr<ExpressionNode> isExpression(shared_ptr<ExpressionNode> left);
	shared_ptr<ExpressionNode> comparisonExpression();
	shared_ptr<ExpressionNode> comparisonExpression(shared_ptr<ExpressionNode> left);
	shared_ptr<ExpressionNode> bitOrExpression();
	shared_ptr<ExpressionNode> bitOrExpression(shared_ptr<ExpressionNode> left);
	shared_ptr<ExpressionNode> bitXorExpression();
	shared_ptr<ExpressionNode> bitXorExpression(shared_ptr<ExpressionNode> left);
	shared_ptr<ExpressionNode> bitAndExpression();
	shared_ptr<ExpressionNode> bitAndExpression(shared_ptr<ExpressionNode> left);
	shared_ptr<ExpressionNode> shiftExpression();
	shared_ptr<ExpressionNode> shiftExpression(shared_ptr<ExpressionNode> left);
	shared_ptr<ExpressionNode> additiveExpression();
	shared_ptr<ExpressionNode> additiveExpression(shared_ptr<ExpressionNode> left);
	shared_ptr<ExpressionNode> multiplicativeExpression();
	shared_ptr<ExpressionNode> multiplicativeExpression(shared_ptr<ExpressionNode> left);
	shared_ptr<ExpressionNode> unaryExpression();
	shared_ptr<ExpressionNode> powerExpression();
	shared_ptr<ExpressionNode> powerExpression(shared_ptr<ExpressionNode> left);
	shared_ptr<ExpressionNode> scopeNotationExpression();
	shared_ptr<ExpressionNode> scopeNotationExpression(shared_ptr<ExpressionNode> left);
	shared_ptr<ExpressionNode> primaryExpression();
	shared_ptr<ExpressionNode> atomPrimary();
	shared_ptr<ExpressionNode> listPrimary();
	shared_ptr<ExpressionNode> dictPrimary();
	shared_ptr<DictionaryPairNode> dictPair();
	shared_ptr<ExpressionNode> characterPrimary();
	shared_ptr<CharacterPairNode> characterPair();
	shared_ptr<ExpressionNode> sequencePrimary();
	shared_ptr<SequenceItemNode> sequenceItem();
	shared_ptr<ExpressionNode> groupPrimary();
	
	bool consumeOpt(TokenType type);
	void consume(TokenType type);
	void advance();
	bool isEOF();
	void expect(TokenType expectedType);
	void expect(std::initializer_list<TokenType> list);
	void expected(TokenType expectedType);
	void expected(std::initializer_list<TokenType> list);
	void error(const string& message);
	void error(const string& message, Token* token);
};

}

#endif //PSL_PARSER_H
