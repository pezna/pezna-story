#ifndef PSL_LOCALIZATION_H
#define PSL_LOCALIZATION_H

#include <cstdint>
#include <string>
#include <unordered_map>

#include "token.hpp"

namespace PSL
{
using std::string;
using std::unordered_map;

enum class SequenceCreateOption
{
	Invalid,
	Loop,
	Random,
	Order
};

class Localization
{
public:
	Localization() {}
	
	static void setKeywordToType(unordered_map<string, bool> textToBool);
	static void setKeywordToType(unordered_map<string, TokenType> keywordToType);
	static void setSequenceCreationOptions(unordered_map<string, SequenceCreateOption> sequenceCreationOptions);
	static void setErrorMessages(unordered_map<string, string> errorMessages);
	
	static bool getBool(const string& text);
	static TokenType getKeywordType(const string& text, const TokenType& defaultReturn);
	static SequenceCreateOption getSequenceCreateOption(const string& text);
	
	// error messages
	static string expectedOne(const string& expected, const string& nott);
	static string expectedMultiple(const string& expected, const string& nott);
	static string expectedAssignment(const string& nott);
	static string invalidStatementOutsideSection();
	static string parserExceptionDetail(const string& message, const string& token);
	static string selectorNoExpression();
	static string invalidSequenceOption(const string& option);
	static string cannotStartNamespaceInsideNamespace();
	static string cannotStartStatementSectionNamespace();
	static string cannotStartSectionNamespace();
	static string cannotStartStatement(const string& token);
	static string cannotStartPrimary(const string& tokenType);
	static string cannotStartExpression(const string& tokenType);
	static string forLoopExpectedId();
	static string invalidVariableType(const string& tokenType);
	static string scopeNotationLeftSide(const string& expected, const string& nott);
	
	static string sectionAlreadyExists(const string& sectionName, const string& newFilepath, int64_t newLine, int64_t newColumn,
			const string& existingFilepath, int64_t existingLine, int64_t existingColumn);
	static string sequenceNameAlreadyInUse(const string& name);
	static string maxIdLength(int given, int limit);
	static string maxNumNamespacesSections(int given, int limit);
	static string maxNumVariables(int given, int limit);
	static string variableCreateMustBeName();
	static string variableAlreadyExists(const string& name);
	static string choiceMustBeAfterLineOrChoice();
	static string labelAlreadyExistsInSection(const string& label, const string& section);
	static string noLoopFoundToContinue();
	static string noBlockFoundToExit();
	static string variableNotFound(const string& name);
	static string namespaceNotFound(const string& name);
	static string sectionNotFound(const string& name);
	static string nameNotFound(const string& name);
	static string expectedValueType(const string& expected, const string& given);
	static string expectedVariableOrValue();
	static string ternaryResultValueTypes(const string& trueType, const string& falseType);
	static string operatorMustAccept(const string& operatorName, const string& leftGiven, const string& rightGiven, const string& leftRequired, const string& rightRequired);
	static string operatorMustAccept(const string& operatorName, const string& given, const string& required);
	static string callMustBeSection();
	static string invalidNumberOfArguments(int numParameters, int numArguments);
	static string integerOutOfBounds(const string& given);
	static string floatOutOfBounds(const string& given);
	
private:
	static unordered_map<string, bool> _textToBool;
	static unordered_map<string, TokenType> _keywordToType;
	static unordered_map<string, SequenceCreateOption> _sequenceCreationOptions;
	static unordered_map<string, string> _errorMessages;
};

}

#endif //PSL_LOCALIZATION_H
