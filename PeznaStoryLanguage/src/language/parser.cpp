#include "parser.hpp"

#include <iostream>

#include "exceptions.hpp"

namespace PSL
{

shared_ptr<StoryNode> Parser::parse()
{
	Node::resetUniqueId();
	advance();
	return story();
}

Token* Parser::lka(int advance)
{
	if (isEOF())
	{
		return _current.get(); //current is EOF
	}
	return _tokens[_index + (advance - 1)].get();
}

shared_ptr<StoryNode> Parser::story()
{
	shared_ptr<StoryNode> story = std::make_shared<StoryNode>();
	story->startLine = 1;
	story->startColumn = 1;
	story->endLine = 1;
	story->endColumn = 1;
	
	story->mainNamespace = std::make_shared<NamespaceNode>();
	story->mainNamespace->startLine = story->startLine;
	story->mainNamespace->startColumn = story->startColumn;
	story->mainNamespace->startLine = story->startLine;
	story->mainNamespace->startColumn = story->startColumn;
	story->mainNamespace->name = std::make_shared<Token>(TokenType::Name, "~namespace~", 1, 1, 1, 1);
	
	/*
	 * TokenType::If the first token in a story is something that cannot start a statement,
	 * this will be caught in an infinite loop without an indexBefore marker to
	 * check if the token has advanced at all
	 */
	int64_t indexBefore = -1;
	while (!isEOF())
	{
		if (indexBefore == _index)
		{
			error(Localization::cannotStartStatementSectionNamespace());
			break;
		}
		
		indexBefore = _index;
		
		consumeOpt(TokenType::Newline);
		if (_current->type == TokenType::Namespace)
		{
			story->mainNamespace->subNamespaces.push_back(namespaceBlock());
		}
		else if (_current->type == TokenType::Section)
		{
			story->mainNamespace->sections.push_back(sectionBlock());
		}
		else if (!isEOF())
		{
			vector<shared_ptr<StatementNode>> stmts = statementList(true);
			story->mainNamespace->statements.insert(story->mainNamespace->statements.end(), stmts.begin(), stmts.end());
		}
	}
	
	shared_ptr<Token>& lastToken = _tokens[_tokens.size() - 2];
	story->endLine = lastToken->line;
	story->endColumn = lastToken->column + lastToken->text.length();
	story->mainNamespace->endLine = story->endLine;
	story->mainNamespace->endColumn = story->endColumn;
	
	return story;
}

shared_ptr<NamespaceNode> Parser::namespaceBlock()
{
	shared_ptr<NamespaceNode> ns = std::make_shared<NamespaceNode>();
	ns->startLine = _current->line;
	ns->startColumn = _current->column;
	
	consume(TokenType::Namespace);
	expect(TokenType::Name);
	
	ns->name = _current; //set name of the block
	advance();
	
	consumeOpt(TokenType::Colon);
	consume(TokenType::Newline);
	
	/*
	 * TokenType::If the first token in a namespace is something that cannot start a statement,
	 * this will be caught in an infinite loop without an indexBefore marker to
	 * check if the token has advanced at all
	 */
	int64_t indexBefore = -1;
	while (!isEOF())
	{
		if (indexBefore == _index)
		{
			error(Localization::cannotStartStatementSectionNamespace());
			break;
		}
		
		indexBefore = _index;
		
		consumeOpt(TokenType::Newline);
		if (_current->type == TokenType::Namespace)
		{
			error(Localization::cannotStartNamespaceInsideNamespace());
			break;
		}
		else if (_current->type == TokenType::Section)
		{
			ns->sections.push_back(sectionBlock());
		}
		else if (!isEOF())
		{
			vector<shared_ptr<StatementNode>> stmts = statementList(true);
			ns->statements.insert(ns->statements.end(), stmts.begin(), stmts.end());
		}
		
		if (_current->type == TokenType::End)
		{
			// This only happens when an inner section/namespace ends.
			// The current type will be the end of this namespace,
			// and if we don't break here, the exception about not being
			// able to start a statement, section, or namespace will trigger
			break;
		}
	}
	
	consume(TokenType::End);
	ns->endLine = _current->line;
	ns->endColumn = _current->column + _current->text.length();
	consume(TokenType::Namespace);
	
	return ns;
}

shared_ptr<SectionNode> Parser::sectionBlock()
{
	shared_ptr<SectionNode> section = std::make_shared<SectionNode>();
	section->startLine = _current->line;
	section->startColumn = _current->column;
	
	consume(TokenType::Section);
	expect(TokenType::Name);
	
	section->name = _current; //set name of the block
	advance();
	
	// blocks don't need parentheses if there are no parameters
	if (_current->type == TokenType::OpenParen)
	{
		section->parameters = parameters();
	}
	
	if (_current->type == TokenType::Arrow)
	{
		advance();
		
		if (!_current->isVariableType())
		{
			error(Localization::invalidVariableType(_current->text));
		}
		
		section->returnType = std::make_shared<TokenType>(_current->type);
		advance();
	}
	
	consumeOpt(TokenType::Colon);
	consume(TokenType::Newline);
	
	// if the block does not immediately end, it probably has statements
	if (_current->type != TokenType::End)
	{
		section->statements = statementList();
	}
	
	consume(TokenType::End);
	section->endLine = _current->line;
	section->endColumn = _current->column + _current->text.length();
	consume(TokenType::Section);
	
	return section;
}

vector<shared_ptr<ParameterNode>> Parser::parameters()
{
	consume(TokenType::OpenParen);
	vector<shared_ptr<ParameterNode>> params;
	while (_current->canStartVariableCreation())
	{
		params.push_back(parameter());
		// ensure that either a comma or closing parenthesis follows
		expect({TokenType::Comma, TokenType::CloseParen});
		if (_current->type != TokenType::CloseParen)
		{
			consume(TokenType::Comma);
		}
	}
	
	consume(TokenType::CloseParen);
	return params;
}

shared_ptr<ParameterNode> Parser::parameter()
{
	shared_ptr<ParameterNode> param = std::make_shared<ParameterNode>();
	param->startLine = _current->line;
	param->startColumn = _current->column;
	
	std::initializer_list<TokenType> allowedTokens = {
		TokenType::Var, TokenType::Final, TokenType::Character, TokenType::Sequence, TokenType::Event,
		TokenType::Bool, TokenType::Int, TokenType::Float, TokenType::String, TokenType::List, TokenType::Dict
	};
	expect(allowedTokens);
	
	while (_current->type == TokenType::Var || _current->type == TokenType::Final)
	{
		param->final = param->final || _current->type == TokenType::Final;
		advance();
	}
	
	if (_current->isVariableType())
	{
		param->variableType = _current->type;
		advance();
	}
	
	expect(TokenType::Name);
	param->name = _current;
	param->endLine = _current->line;
	param->endColumn = _current->column + _current->text.length();
	advance(); // move past TokenType::Name
	
	return param;
}

vector<shared_ptr<StatementNode>> Parser::statementList(bool outsideSection)
{
	vector<shared_ptr<StatementNode>> stmts;
	
	do
	{
		consumeOpt(TokenType::Newline);
		if (!_current->canStartStatement())
		{
			break; //no statement could be started, so the list is finished
		}
		shared_ptr<Token> tokenAtStatementStart = _current;
		stmts.push_back(statement());
		
		if (outsideSection)
		{
			StatementType lastType = stmts.back()->statementType();
			if (lastType != StatementType::AssignmentStatement && lastType != StatementType::IncludeStatement)
			{
				error(Localization::invalidStatementOutsideSection(), tokenAtStatementStart.get());
				break;
			}
		}
	}
	while (_current->type == TokenType::Newline);
	
	return stmts;
}

shared_ptr<StatementNode> Parser::statement()
{
	switch (_current->type)
	{
		case TokenType::If:
			return ifStatement();
		case TokenType::While:
			return whileStatement();
		case TokenType::For:
			return forStatement();
		case TokenType::Do:
			return doStatement();
		case TokenType::Goto:
			return gotoStatement();
		case TokenType::Label:
			return labelStatement();
		case TokenType::Choice:
			error(Localization::choiceMustBeAfterLineOrChoice());
			return nullptr;
		case TokenType::Exit:
			return exitStatement();
		case TokenType::Continue:
			return continueStatement();
		case TokenType::Return:
			return returnStatement();
		case TokenType::Include:
			return includeStatement();
		case TokenType::Event:
			return eventStatement();
		case TokenType::Var:
		case TokenType::Final:
		case TokenType::Character:
		case TokenType::Sequence:
		case TokenType::Bool:
		case TokenType::Int:
		case TokenType::Float:
		case TokenType::String:
		case TokenType::List:
		case TokenType::Dict:
			return variableCreateStatement();
		//expr or assignment
		case TokenType::OpenParen:
		case TokenType::OpenBrace:
		case TokenType::OpenBrack:
		case TokenType::Name:
			shared_ptr<ExpressionNode> left = expression();
			// check if the current token is an assignment operator
			if (_current->isAssignment())
			{
				shared_ptr<Token> op = _current;
				advance();
				shared_ptr<ExpressionNode> right = expression();
				shared_ptr<AssignmentStatementNode> assign = std::make_shared<AssignmentStatementNode>();
				assign->startLine = left->startLine;
				assign->startColumn = left->startColumn;
				assign->endLine = right->endLine;
				assign->endColumn = right->endColumn;
				
				assign->left = std::move(left);
				assign->op = op;
				assign->right = std::move(right);
				return assign;
			}
			else if (_current->type == TokenType::Colon)
			{
				// line stmt
				advance();
				shared_ptr<ExpressionNode> text = expression();
				shared_ptr<LineStatementNode> line = std::make_shared<LineStatementNode>();
				line->startLine = left->startLine;
				line->startColumn = left->startColumn;
				line->endLine = text->endLine;
				line->endColumn = text->endColumn;
				
				line->character = std::move(left);
				line->text = std::move(text);
				
				
				while (_current->type == TokenType::Choice || (_current->type == TokenType::Newline && lka()->type == TokenType::Choice))
				{
					consumeOpt(TokenType::Newline);
					line->choices.push_back(choiceStatement());
				}
				
				return line;
			}
			
			return left;
	}
	
	error(Localization::cannotStartStatement(Token::getTypeName(_current->type)));
	return nullptr;
}

shared_ptr<AssignmentStatementNode> Parser::variableCreateStatement()
{
	shared_ptr<AssignmentStatementNode> assign = std::make_shared<AssignmentStatementNode>();
	assign->create = true;
	assign->startLine = _current->line;
	assign->startColumn = _current->column;
	
	auto param = parameter();
	assign->final = param->final;
	assign->variableType = param->variableType;
	auto name = std::make_shared<AtomPrimaryNode>();
	name->token = param->name;
	name->startLine = param->name->line;
	name->startColumn = param->name->column;
	name->endLine = param->name->line;
	name->endColumn = param->name->column + param->name->text.length();
	assign->left = name;
	
	if (!_current->isAssignment())
	{
		error(Localization::expectedAssignment(Token::getTypeName(_current->type)));
	}
	
	assign->op = _current;
	advance(); // move past operator
	
	assign->right = expression();
	assign->endLine = assign->right->endLine;
	assign->endColumn = assign->right->endColumn;
	
	return assign;
}

shared_ptr<IfStatementNode> Parser::ifStatement()
{
	shared_ptr<IfStatementNode> iff = std::make_shared<IfStatementNode>();
	
	iff->ifPart = ifPart();
	
	iff->startLine = iff->ifPart->startLine;
	iff->startColumn = iff->ifPart->startColumn;
	
	while (_current->type == TokenType::ElseIf)
	{
		iff->elseIfParts.push_back(elseIfPart());
	}
	
	if (_current->type == TokenType::Else)
	{
		iff->elsePart = elsePart();
	}
	
	consume(TokenType::End);
	iff->endLine = _current->line;
	iff->endColumn = _current->column + _current->text.length();
	consume(TokenType::If);
	
	return iff;
}

shared_ptr<IfPartNode> Parser::ifPart()
{
	shared_ptr<IfPartNode> ifPart = std::make_shared<IfPartNode>();
	ifPart->startLine = _current->line;
	ifPart->startColumn = _current->column;
	
	consume(TokenType::If);
	ifPart->condition = expression();
	// first set the endings to the colon, in case there are no stmts
	ifPart->endLine = _current->line;
	ifPart->endColumn = _current->column + _current->text.length();
	consumeOpt(TokenType::Colon);
	consume(TokenType::Newline);
	ifPart->statements = statementList();
	
	if (ifPart->statements.size() > 0)
	{
		ifPart->endLine = ifPart->statements.back()->endLine;
		ifPart->endColumn = ifPart->statements.back()->endColumn;
	}
	
	return ifPart;
}

shared_ptr<ElseIfPartNode> Parser::elseIfPart()
{
	shared_ptr<ElseIfPartNode> elseIfPart = std::make_shared<ElseIfPartNode>();
	elseIfPart->startLine = _current->line;
	elseIfPart->startColumn = _current->column;
	
	consume(TokenType::ElseIf);
	elseIfPart->condition = expression();
	// first set the endings to the colon, in case there are no stmts
	elseIfPart->endLine = _current->line;
	elseIfPart->endColumn = _current->column + _current->text.length();
	consumeOpt(TokenType::Colon);
	consume(TokenType::Newline);
	elseIfPart->statements = statementList();
	
	if (elseIfPart->statements.size() > 0)
	{
		elseIfPart->endLine = elseIfPart->statements.back()->endLine;
		elseIfPart->endColumn = elseIfPart->statements.back()->endColumn;
	}
	
	return elseIfPart;
}

shared_ptr<ElsePartNode> Parser::elsePart()
{
	shared_ptr<ElsePartNode> elsePart = std::make_shared<ElsePartNode>();
	elsePart->startLine = _current->line;
	elsePart->startColumn = _current->column;
	
	consume(TokenType::Else);
	// first set the endings to the colon, in case there are no stmts
	elsePart->endLine = _current->line;
	elsePart->endColumn = _current->column + _current->text.length();
	consumeOpt(TokenType::Colon);
	consume(TokenType::Newline);
	
	elsePart->statements = statementList();
	
	if (elsePart->statements.size() > 0)
	{
		elsePart->endLine = elsePart->statements.back()->endLine;
		elsePart->endColumn = elsePart->statements.back()->endColumn;
	}
	
	return elsePart;
}

shared_ptr<WhileStatementNode> Parser::whileStatement()
{
	shared_ptr<WhileStatementNode> whilee = std::make_shared<WhileStatementNode>();
	whilee->startLine = _current->line;
	whilee->startColumn = _current->column;
	
	consume(TokenType::While);
	whilee->condition = expression();
	consumeOpt(TokenType::Colon);
	consume(TokenType::Newline);
	whilee->statements = statementList();
	consume(TokenType::End);
	whilee->endLine = _current->line;
	whilee->endColumn = _current->column + _current->text.length();
	consume(TokenType::While);
	
	return whilee;
}

shared_ptr<ForStatementNode> Parser::forStatement()
{
	shared_ptr<ForStatementNode> forr = std::make_shared<ForStatementNode>();
	forr->startLine = _current->line;
	forr->startColumn = _current->column;
	
	consume(TokenType::For);
	expect(TokenType::Name);
	
	forr->variable = _current;
	advance();
	
	consume(TokenType::In);
	forr->expression = expression();
	consumeOpt(TokenType::Colon);
	consume(TokenType::Newline);
	forr->statements = statementList();
	consume(TokenType::End);
	forr->endLine = _current->line;
	forr->endColumn = _current->column + _current->text.length();
	consume(TokenType::For);
	
	return forr;
}

shared_ptr<DoStatementNode> Parser::doStatement()
{
	shared_ptr<DoStatementNode> doo = std::make_shared<DoStatementNode>();
	doo->startLine = _current->line;
	doo->startColumn = _current->column;
	consume(TokenType::Do);
	consumeOpt(TokenType::Colon);
	consume(TokenType::Newline);
	doo->statements = statementList();
	consume(TokenType::End);
	consume(TokenType::Do);
	consume(TokenType::If);
	
	doo->endCondition = expression();
	doo->endLine = doo->endCondition->endLine;
	doo->endColumn = doo->endCondition->endColumn;
	
	return doo;
}

shared_ptr<GotoStatementNode> Parser::gotoStatement()
{
	shared_ptr<GotoStatementNode> gotoo = std::make_shared<GotoStatementNode>();
	gotoo->startLine = _current->line;
	gotoo->startColumn = _current->column;
	consume(TokenType::Goto);
	expect(TokenType::Name);
	gotoo->token = _current;
	gotoo->endLine = _current->line;
	gotoo->endColumn = _current->column + _current->text.length();
	advance();
	
	return gotoo;
}

shared_ptr<LabelStatementNode> Parser::labelStatement()
{
	shared_ptr<LabelStatementNode> label = std::make_shared<LabelStatementNode>();
	label->startLine = _current->line;
	label->startColumn = _current->column;
	consume(TokenType::Label);
	label->label = _current;
	label->endLine = _current->line;
	label->endColumn = _current->column + _current->text.length();
	consume(TokenType::Name);
	
	return label;
}

shared_ptr<ChoiceStatementNode> Parser::choiceStatement()
{
	shared_ptr<ChoiceStatementNode> choice = std::make_shared<ChoiceStatementNode>();
	choice->startLine = _current->line;
	choice->startColumn = _current->column;
	consume(TokenType::Choice);
	choice->expressions = expressionList();
	consumeOpt(TokenType::Colon);
	consume(TokenType::Newline);
	choice->statements = statementList();
	consume(TokenType::End);
	choice->endLine = _current->line;
	choice->endColumn = _current->column + _current->text.length();
	consume(TokenType::Choice);
	
	return choice;
}

shared_ptr<ExitStatementNode> Parser::exitStatement()
{
	shared_ptr<ExitStatementNode> exit = std::make_shared<ExitStatementNode>();
	exit->startLine = _current->line;
	exit->startColumn = _current->column;
	exit->endLine = _current->line;
	exit->endColumn = _current->column + _current->text.length();
	
	consume(TokenType::Exit);
	// not optional
	expect({TokenType::Story, TokenType::Section, TokenType::Choice, TokenType::While, TokenType::For, TokenType::Do, TokenType::If});
	exit->level = _current;
	advance();
	
	exit->endLine = exit->level->line;
	exit->endColumn = exit->level->column + exit->level->text.length();
	
	return exit;
}

shared_ptr<ContinueStatementNode> Parser::continueStatement()
{
	shared_ptr<ContinueStatementNode> cont = std::make_shared<ContinueStatementNode>();
	cont->startLine = _current->line;
	cont->startColumn = _current->column;
	cont->endLine = _current->line;
	cont->endColumn = _current->column + _current->text.length();
	
	consume(TokenType::Continue);
	if (_current->type != TokenType::Newline)
	{
		switch (_current->type)
		{
			case TokenType::While:
			case TokenType::For:
			case TokenType::Do:
				cont->level = _current;
			break;
			default:
				expected({TokenType::Newline, TokenType::While, TokenType::For, TokenType::Do});
			break;
		}
		
		cont->endLine = _current->line;
		cont->endColumn = _current->column + _current->text.length();
		
		advance();
	}
	
	return cont;
}

shared_ptr<ReturnStatementNode> Parser::returnStatement()
{
	shared_ptr<ReturnStatementNode> ret = std::make_shared<ReturnStatementNode>();
	ret->startLine = _current->line;
	ret->startColumn = _current->column;
	ret->endLine = _current->line;
	ret->endColumn = _current->column + _current->text.length();
	
	consume(TokenType::Return);
	if (_current->type != TokenType::Newline)
	{
		ret->expression = expression();
		ret->endLine = ret->expression->endLine;
		ret->endColumn = ret->expression->endColumn;
	}
	
	return ret;
}

shared_ptr<IncludeStatementNode> Parser::includeStatement()
{
	shared_ptr<IncludeStatementNode> include = std::make_shared<IncludeStatementNode>();
	include->startLine = _current->line;
	include->startColumn = _current->column;
	
	consume(TokenType::Include);
	include->filename = _current;
	include->endLine = _current->line;
	include->endColumn = _current->column + _current->text.length();
	consume(TokenType::StringLiteral);
	
	return include;
}

shared_ptr<EventStatementNode> Parser::eventStatement()
{
	shared_ptr<EventStatementNode> trigger = std::make_shared<EventStatementNode>();
	trigger->startLine = _current->line;
	trigger->startColumn = _current->column;
	
	consume(TokenType::Event);
	trigger->expression = expression();
	
	trigger->endLine = trigger->expression->endLine;
	trigger->endColumn = trigger->expression->endColumn;
	
	return trigger;
}

vector<shared_ptr<ExpressionNode>> Parser::expressionList()
{
	vector<shared_ptr<ExpressionNode>> exprs;
	/*
	 * Using consumed TokenType::Comma ensures that an "expected TokenType::Comma not TokenType::CloseBrack"
	 * isn't thrown at the end of a list primary.
	 * It also ensures that expression lists stop when they didn't get a comma,
	 * even if it is at a token that can start an expression.
	 */
	bool consumedComma = true;
	while (_current->canStartExpression())
	{
		if (!consumedComma)
		{
			break;
		}
		exprs.push_back(expression());
		consumedComma = consumeOpt(TokenType::Comma);
	}
	
	return exprs;
}

shared_ptr<ExpressionNode> Parser::expression()
{
	shared_ptr<ExpressionNode> tern = ternaryExpression();
	
	if (_current->type == TokenType::Newline || _current->isAssignment())
	{
		return tern;
	}
	
	while (_current->isBinaryOperator())
	{
		switch (_current->type)
		{
			case TokenType::Equals:
			case TokenType::NotEquals:
			case TokenType::GreaterThanEquals:
			case TokenType::LessThanEquals:
			case TokenType::GreaterThan:
			case TokenType::LessThan:
				tern = comparisonExpression(tern);
			break;
			case TokenType::And:
				tern = andExpression(tern);
			break;
			case TokenType::Or:
				tern = orExpression(tern);
			break;
			case TokenType::In:
			case TokenType::NotIn:
				tern = inExpression(tern);
			break;
			case TokenType::Is:
			case TokenType::IsNot:
				tern = isExpression(tern);
			break;
			case TokenType::Pow:
			case TokenType::OpenParen:
			case TokenType::OpenBrack:
				tern = powerExpression(tern);
			break;
			case TokenType::Dot:
			case TokenType::DoubleColon:
				tern = scopeNotationExpression(tern);
			break;
			case TokenType::Add:
			case TokenType::Sub:
				tern = additiveExpression(tern);
			break;
			case TokenType::Mul:
			case TokenType::Div:
			case TokenType::Mod:
				tern = multiplicativeExpression(tern);
			break;
			case TokenType::BitAnd:
				tern = bitAndExpression(tern);
			break;
			case TokenType::BitOr:
				tern = bitOrExpression(tern);
			break;
			case TokenType::BitLeftShift:
			case TokenType::BitRightShift:
				tern = shiftExpression(tern);
			break;
			default:
			break;
		}
	}
	
	return tern;
}

shared_ptr<ExpressionNode> Parser::ternaryExpression()
{
	shared_ptr<ExpressionNode> left = orExpression();
	
	if (_current->type == TokenType::Ternary)
	{
		shared_ptr<TernaryExpressionNode> ternary = std::make_shared<TernaryExpressionNode>();
		ternary->condition = left;
		ternary->startLine = left->startLine;
		ternary->startColumn = left->startColumn;
		
		advance(); // move past TokenType::Ternary
		ternary->trueExpression = expression();
		ternary->endLine = ternary->trueExpression->endLine;
		ternary->endColumn = ternary->trueExpression->endColumn;
		
		consume(TokenType::Colon);
		
		ternary->falseExpression = expression();
		ternary->endLine = ternary->falseExpression->endLine;
		ternary->endColumn = ternary->falseExpression->endColumn;
		
		left = ternary;
	}
	
	return left;
}

shared_ptr<ExpressionNode> Parser::orExpression()
{
	shared_ptr<ExpressionNode> left = andExpression();
	return orExpression(left);
}

shared_ptr<ExpressionNode> Parser::orExpression(shared_ptr<ExpressionNode> left)
{
	if (_current->type == TokenType::Or)
	{
		shared_ptr<BinaryExpressionNode> binary = std::make_shared<BinaryExpressionNode>();
		binary->type = ExpressionType::OrExpression;
		binary->left = left;
		binary->startLine = left->startLine;
		binary->startColumn = left->startColumn;
		
		binary->op = _current;
		advance();
		binary->right = andExpression();
		
		binary->endLine = binary->right->endLine;
		binary->endColumn = binary->right->endColumn;
		
		left = binary;
	}
	
	return left;
}

shared_ptr<ExpressionNode> Parser::andExpression()
{
	shared_ptr<ExpressionNode> left = notExpression();
	return andExpression(left);
}

shared_ptr<ExpressionNode> Parser::andExpression(shared_ptr<ExpressionNode> left)
{
	if (_current->type == TokenType::And)
	{
		shared_ptr<BinaryExpressionNode> binary = std::make_shared<BinaryExpressionNode>();
		binary->type = ExpressionType::AndExpression;
		binary->left = left;
		binary->startLine = left->startLine;
		binary->startColumn = left->startColumn;
		
		binary->op = _current;
		advance();
		binary->right = notExpression();
		
		binary->endLine = binary->right->endLine;
		binary->endColumn = binary->right->endColumn;
		
		left = binary;
	}
	
	return left;
}

shared_ptr<ExpressionNode> Parser::notExpression()
{
	if (_current->type == TokenType::Not)
	{
		shared_ptr<UnaryExpressionNode> unary = std::make_shared<UnaryExpressionNode>();
		unary->op = _current;
		unary->startLine = unary->op->line;
		unary->startColumn = unary->op->column;
		advance();
		unary->expression = notExpression();
		
		unary->endLine = unary->expression->endLine;
		unary->endColumn = unary->expression->endColumn;
		
		return unary;
	}
	
	return inExpression();
}

shared_ptr<ExpressionNode> Parser::inExpression()
{
	shared_ptr<ExpressionNode> left = isExpression();
	return inExpression(left);
}

shared_ptr<ExpressionNode> Parser::inExpression(shared_ptr<ExpressionNode> left)
{
	if (_current->type == TokenType::In || _current->type == TokenType::NotIn)
	{
		shared_ptr<BinaryExpressionNode> binary = std::make_shared<BinaryExpressionNode>();
		binary->type = ExpressionType::InExpression;
		binary->left = left;
		binary->startLine = left->startLine;
		binary->startColumn = left->startColumn;
		
		binary->op = _current;
		advance();
		binary->right = isExpression();
		
		binary->endLine = binary->right->endLine;
		binary->endColumn = binary->right->endColumn;
		
		left = binary;
	}
	
	return left;
}

shared_ptr<ExpressionNode> Parser::isExpression()
{
	shared_ptr<ExpressionNode> left = comparisonExpression();
	return isExpression(left);
}

shared_ptr<ExpressionNode> Parser::isExpression(shared_ptr<ExpressionNode> left)
{
	if (_current->type == TokenType::Is || _current->type == TokenType::IsNot)
	{
		shared_ptr<BinaryExpressionNode> binary = std::make_shared<BinaryExpressionNode>();
		binary->type = ExpressionType::IsExpression;
		binary->left = left;
		binary->startLine = left->startLine;
		binary->startColumn = left->startColumn;
		
		binary->op = _current;
		advance();
		if (!_current->isVariableType())
		{
			error(Localization::invalidVariableType(_current->text));
		}
		binary->right = atomPrimary();
		
		binary->endLine = binary->right->endLine;
		binary->endColumn = binary->right->endColumn;
		
		left = binary;
	}
	
	return left;
}

shared_ptr<ExpressionNode> Parser::comparisonExpression()
{
	shared_ptr<ExpressionNode> left = bitOrExpression();
	return comparisonExpression(left);
}

shared_ptr<ExpressionNode> Parser::comparisonExpression(shared_ptr<ExpressionNode> left)
{
	if (_current->type == TokenType::LessThan || _current->type == TokenType::LessThanEquals ||
		_current->type == TokenType::GreaterThan || _current->type == TokenType::GreaterThanEquals ||
		_current->type == TokenType::Equals || _current->type == TokenType::NotEquals)
	{
		shared_ptr<BinaryExpressionNode> binary = std::make_shared<BinaryExpressionNode>();
		binary->type = ExpressionType::ComparisonExpression;
		binary->left = left;
		binary->startLine = left->startLine;
		binary->startColumn = left->startColumn;
		
		binary->op = _current;
		advance();
		binary->right = bitOrExpression();
		
		binary->endLine = binary->right->endLine;
		binary->endColumn = binary->right->endColumn;
		
		left = binary;
	}
	
	return left;
}

shared_ptr<ExpressionNode> Parser::bitOrExpression()
{
	shared_ptr<ExpressionNode> left = bitXorExpression();
	return bitOrExpression(left);
}

shared_ptr<ExpressionNode> Parser::bitOrExpression(shared_ptr<ExpressionNode> left)
{
	if (_current->type == TokenType::BitOr)
	{
		shared_ptr<BinaryExpressionNode> binary = std::make_shared<BinaryExpressionNode>();
		binary->type = ExpressionType::BitOrExpression;
		binary->left = left;
		binary->startLine = left->startLine;
		binary->startColumn = left->startColumn;
		
		binary->op = _current;
		advance();
		binary->right = bitXorExpression();
		
		binary->endLine = binary->right->endLine;
		binary->endColumn = binary->right->endColumn;
		
		left = binary;
	}
	
	return left;
}

shared_ptr<ExpressionNode> Parser::bitXorExpression()
{
	shared_ptr<ExpressionNode> left = bitAndExpression();
	return bitXorExpression(left);
}

shared_ptr<ExpressionNode> Parser::bitXorExpression(shared_ptr<ExpressionNode> left)
{
	if (_current->type == TokenType::BitXor)
	{
		shared_ptr<BinaryExpressionNode> binary = std::make_shared<BinaryExpressionNode>();
		binary->type = ExpressionType::BitXorExpression;
		binary->left = left;
		binary->startLine = left->startLine;
		binary->startColumn = left->startColumn;
		
		binary->op = _current;
		advance();
		binary->right = bitAndExpression();
		
		binary->endLine = binary->right->endLine;
		binary->endColumn = binary->right->endColumn;
		
		left = binary;
	}
	
	return left;
}

shared_ptr<ExpressionNode> Parser::bitAndExpression()
{
	shared_ptr<ExpressionNode> left = shiftExpression();
	return bitAndExpression(left);
}

shared_ptr<ExpressionNode> Parser::bitAndExpression(shared_ptr<ExpressionNode> left)
{
	if (_current->type == TokenType::BitAnd)
	{
		shared_ptr<BinaryExpressionNode> binary = std::make_shared<BinaryExpressionNode>();
		binary->type = ExpressionType::BitAndExpression;
		binary->left = left;
		binary->startLine = left->startLine;
		binary->startColumn = left->startColumn;
		
		binary->op = _current;
		advance();
		binary->right = shiftExpression();
		
		binary->endLine = binary->right->endLine;
		binary->endColumn = binary->right->endColumn;
		
		left = binary;
	}
	
	return left;
}

shared_ptr<ExpressionNode> Parser::shiftExpression()
{
	shared_ptr<ExpressionNode> left = additiveExpression();
	return shiftExpression(left);
}

shared_ptr<ExpressionNode> Parser::shiftExpression(shared_ptr<ExpressionNode> left)
{
	if (_current->type == TokenType::BitLeftShift || _current->type == TokenType::BitRightShift)
	{
		shared_ptr<BinaryExpressionNode> binary = std::make_shared<BinaryExpressionNode>();
		binary->type = ExpressionType::BitShiftExpression;
		binary->left = left;
		binary->startLine = left->startLine;
		binary->startColumn = left->startColumn;
		
		binary->op = _current;
		advance();
		binary->right = additiveExpression();
		
		binary->endLine = binary->right->endLine;
		binary->endColumn = binary->right->endColumn;
		
		left = binary;
	}
	
	return left;
}

shared_ptr<ExpressionNode> Parser::additiveExpression()
{
	shared_ptr<ExpressionNode> left = multiplicativeExpression();
	return additiveExpression(left);
}

shared_ptr<ExpressionNode> Parser::additiveExpression(shared_ptr<ExpressionNode> left)
{
	if (_current->type == TokenType::Add || _current->type == TokenType::Sub)
	{
		shared_ptr<BinaryExpressionNode> binary = std::make_shared<BinaryExpressionNode>();
		binary->type = ExpressionType::AdditiveExpression;
		binary->left = left;
		binary->startLine = left->startLine;
		binary->startColumn = left->startColumn;
		
		binary->op = _current;
		advance();
		binary->right = multiplicativeExpression();
		
		binary->endLine = binary->right->endLine;
		binary->endColumn = binary->right->endColumn;
		
		left = binary;
	}
	
	return left;
}

shared_ptr<ExpressionNode> Parser::multiplicativeExpression()
{
	shared_ptr<ExpressionNode> left = unaryExpression();
	return multiplicativeExpression(left);
}

shared_ptr<ExpressionNode> Parser::multiplicativeExpression(shared_ptr<ExpressionNode> left)
{
	if (_current->type == TokenType::Mul || _current->type == TokenType::Div || _current->type == TokenType::Mod)
	{
		shared_ptr<BinaryExpressionNode> binary = std::make_shared<BinaryExpressionNode>();
		binary->type = ExpressionType::MultiplicativeExpression;
		binary->left = left;
		binary->startLine = left->startLine;
		binary->startColumn = left->startColumn;
		
		binary->op = _current;
		advance();
		binary->right = unaryExpression();
		
		binary->endLine = binary->right->endLine;
		binary->endColumn = binary->right->endColumn;
		
		left = binary;
	}
	
	return left;
}

shared_ptr<ExpressionNode> Parser::unaryExpression()
{
	if (_current->type == TokenType::Sub || _current->type == TokenType::BitNot)
	{
		shared_ptr<UnaryExpressionNode> unary = std::make_shared<UnaryExpressionNode>();
		unary->op = _current;
		unary->startLine = unary->op->line;
		unary->startColumn = unary->op->column;
		
		advance();
		unary->expression = unaryExpression();
		
		unary->endLine = unary->expression->endLine;
		unary->endColumn = unary->expression->endColumn;
		
		return unary;
	}
	
	return powerExpression();
}

shared_ptr<ExpressionNode> Parser::powerExpression()
{
	shared_ptr<ExpressionNode> left = scopeNotationExpression();
	return powerExpression(left);
}

shared_ptr<ExpressionNode> Parser::powerExpression(shared_ptr<ExpressionNode> left)
{
	//check for trailers
	while (_current->type == TokenType::OpenParen || _current->type == TokenType::OpenBrack)
	{
		switch (_current->type)
		{
			case TokenType::OpenParen:
			{
				consume(TokenType::OpenParen);
				shared_ptr<CallExpressionNode> call = std::make_shared<CallExpressionNode>();
				call->expression = left;
				call->arguments = expressionList();
				
				call->startLine = left->startLine;
				call->startColumn = left->startColumn;
				call->endLine = _current->line;
				call->endColumn = _current->column + _current->text.length();
				consume(TokenType::CloseParen);
				
				left = call;
			}
			break;
			case TokenType::OpenBrack:
			{
				consume(TokenType::OpenBrack);
				shared_ptr<SelectorExpressionNode> selector = std::make_shared<SelectorExpressionNode>();
				selector->expression = left;
				
				selector->startLine = left->startLine;
				selector->startColumn = left->startColumn;
				
				if (_current->canStartExpression()) {
					selector->startIndex = expression();
				}
				
				selector->slice = consumeOpt(TokenType::Comma) || selector->slice;
				
				if (selector->slice && _current->canStartExpression()) {
					selector->endIndex = expression();
				}
				
				selector->slice = consumeOpt(TokenType::Comma) || selector->slice;
				
				if (selector->slice && _current->canStartExpression()) {
					selector->step = expression();
				}
				
				if (selector->startIndex == nullptr && !selector->slice) {
					error(Localization::selectorNoExpression());
				}
				
				selector->endLine = _current->line;
				selector->endColumn = _current->column + _current->text.length();
				consume(TokenType::CloseBrack);
				
				left = selector;
			}
			break;
		}
	}
	
	if (_current->type == TokenType::Pow)
	{
		shared_ptr<BinaryExpressionNode> binary = std::make_shared<BinaryExpressionNode>();
		binary->type = ExpressionType::PowerExpression;
		binary->left = left;
		binary->startLine = left->startLine;
		binary->startColumn = left->startColumn;
		
		binary->op = _current;
		advance();
		binary->right = unaryExpression();
		
		binary->endLine = binary->right->endLine;
		binary->endColumn = binary->right->endColumn;
		
		left = binary;
	}
	
	return left;
}

shared_ptr<ExpressionNode> Parser::scopeNotationExpression()
{
	shared_ptr<ExpressionNode> left = primaryExpression();
	return scopeNotationExpression(left);
}

shared_ptr<ExpressionNode> Parser::scopeNotationExpression(shared_ptr<ExpressionNode> left)
{
	if (_current->type == TokenType::DoubleColon)
	{
		if (left->expressionType() != ExpressionType::AtomPrimary)
		{
			error(Localization::scopeNotationLeftSide(Token::getTypeName(TokenType::Name), ExpressionNode::getTypeName(left->expressionType())));
		}
		else
		{
			auto atom = static_cast<AtomPrimaryNode*>(left.get());
			if (atom->token->type != TokenType::Name)
			{
				error(Localization::scopeNotationLeftSide(Token::getTypeName(TokenType::Name), Token::getTypeName(atom->token->type)));
			}
		}
	}
	
	if (_current->type != TokenType::Dot && _current->type != TokenType::DoubleColon)
	{
		return left;
	}
	
	shared_ptr<ScopeNotationExpressionNode> notation = std::make_shared<ScopeNotationExpressionNode>();
	notation->type = _current->type == TokenType::Dot
			? ExpressionType::DotExpression
			: ExpressionType::DoubleColonExpression;
	notation->left = left;
	notation->startLine = left->startLine;
	notation->startColumn = left->startColumn;
	
	notation->op = _current;
	advance();
	expect(TokenType::Name);
	notation->right = _current;
	advance();
	
	notation->endLine = _current->line;
	notation->endColumn = _current->column + _current->text.length();
	
	return notation;
}

shared_ptr<ExpressionNode> Parser::primaryExpression()
{
	switch (_current->type)
	{
		case TokenType::BoolLiteral:
		case TokenType::IntLiteral:
		case TokenType::FloatLiteral:
		case TokenType::StringLiteral:
		case TokenType::Name:
		case TokenType::Default:
			return atomPrimary();
		case TokenType::OpenBrack:
			return listPrimary();
		case TokenType::OpenBrace:
			return dictPrimary();
		case TokenType::Character:
			return characterPrimary();
		case TokenType::Sequence:
			return sequencePrimary();
		case TokenType::OpenParen:
			return groupPrimary();
	}
	
	ostringstream oss;
	oss << "Unknown primary token: " << Token::getTypeName(_current->type);
	error(oss.str());
	return nullptr;
}

shared_ptr<ExpressionNode> Parser::atomPrimary()
{
	shared_ptr<AtomPrimaryNode> atom = std::make_shared<AtomPrimaryNode>();
	atom->startLine = _current->line;
	atom->startColumn = _current->column;
	
	atom->token = _current;
	
	atom->endLine = _current->line;
	atom->endColumn = _current->column + _current->text.length();
	
	advance();
	return atom;
}

shared_ptr<ExpressionNode> Parser::listPrimary()
{
	shared_ptr<ListPrimaryNode> list = std::make_shared<ListPrimaryNode>();
	list->startLine = _current->line;
	list->startColumn = _current->column;
	consume(TokenType::OpenBrack);
	
	list->expressions = expressionList();
	
	list->endLine = _current->line;
	list->endColumn = _current->column + _current->text.length();
	consume(TokenType::CloseBrack);
	
	if (_current->type == TokenType::Default)
	{
		advance();
		list->defaultExpression = expression();
		list->endLine = list->defaultExpression->endLine;
		list->endColumn = list->defaultExpression->endColumn;
	}
	
	return list;
}

shared_ptr<ExpressionNode> Parser::dictPrimary()
{
	shared_ptr<DictionaryPrimaryNode> dict = std::make_shared<DictionaryPrimaryNode>();
	dict->startLine = _current->line;
	dict->startColumn = _current->column;
	consume(TokenType::OpenBrace);
	
	while (_current->canStartExpression())
	{
		dict->pairs.push_back(dictPair());
		consumeOpt(TokenType::Comma);
	}
	
	dict->endLine = _current->line;
	dict->endColumn = _current->column + _current->text.length();
	consume(TokenType::CloseBrace);
	
	if (_current->type == TokenType::Default)
	{
		advance();
		dict->defaultExpression = expression();
		dict->endLine = dict->defaultExpression->endLine;
		dict->endColumn = dict->defaultExpression->endColumn;
	}
	
	return dict;
}

shared_ptr<DictionaryPairNode> Parser::dictPair()
{
	shared_ptr<DictionaryPairNode> pair = std::make_shared<DictionaryPairNode>();
	pair->key = expression();
	consume(TokenType::Colon);
	pair->value = expression();
	
	pair->startLine = pair->key->startLine;
	pair->startColumn = pair->key->startColumn;
	pair->endLine = pair->value->endLine;
	pair->endColumn = pair->value->endColumn;
	
	return pair;
}

shared_ptr<ExpressionNode> Parser::characterPrimary()
{
	shared_ptr<CharacterPrimaryNode> chars = std::make_shared<CharacterPrimaryNode>();
	chars->startLine = _current->line;
	chars->startColumn = _current->column;
	consume(TokenType::Character);
	consume(TokenType::OpenBrace);
	while (_current->type == TokenType::Name)
	{
		chars->pairs.push_back(characterPair());
		consumeOpt(TokenType::Comma);
	}
	
	chars->endLine = _current->line;
	chars->endColumn = _current->column + _current->text.length();
	consume(TokenType::CloseBrace);
	
	return chars;
}

shared_ptr<CharacterPairNode> Parser::characterPair()
{
	shared_ptr<CharacterPairNode> pair = std::make_shared<CharacterPairNode>();
	pair->name = _current;
	consume(TokenType::Name);
	consume(TokenType::Assign);
	pair->value = expression();
	
	pair->startLine = pair->name->line;
	pair->startColumn = pair->name->column;
	pair->endLine = pair->value->endLine;
	pair->endColumn = pair->value->endColumn;
	
	return pair;
}

shared_ptr<ExpressionNode> Parser::sequencePrimary()
{
	shared_ptr<SequencePrimaryNode> seq = std::make_shared<SequencePrimaryNode>();
	seq->startLine = _current->line;
	seq->startColumn = _current->column;
	
	consume(TokenType::Sequence);
	
	if (_current->type == TokenType::Name)
	{
		seq->name = _current;
		advance();
	}
	
	if (_current->type == TokenType::LessThan)
	{
		advance();
		while (_current->type == TokenType::Name)
		{
			switch (Localization::getSequenceCreateOption(_current->text))
			{
				case SequenceCreateOption::Loop:
					seq->loop = true;
					break;
				case SequenceCreateOption::Random:
					seq->random = true;
					break;
				case SequenceCreateOption::Order:
					seq->order = true;
					break;
				default:
					error(Localization::invalidSequenceOption(_current->text));
			}
			
			if (!consumeOpt(TokenType::Comma))
			{
				advance();
			}
		}
		
		consume(TokenType::GreaterThan);
	}
	
	consume(TokenType::OpenBrack);
	
	while (_current->canStartExpression())
	{
		seq->items.push_back(sequenceItem());
		
		consumeOpt(TokenType::Comma);
	}
	
	seq->endLine = _current->line;
	seq->endColumn = _current->column + _current->text.length();
	consume(TokenType::CloseBrack);
	
	return seq;
}

shared_ptr<SequenceItemNode> Parser::sequenceItem()
{
	shared_ptr<SequenceItemNode> seqItem = std::make_shared<SequenceItemNode>();
	seqItem->expression = expression();
	
	seqItem->startLine = seqItem->expression->startLine;
	seqItem->startColumn = seqItem->expression->startColumn;
	seqItem->endLine = seqItem->expression->endLine;
	seqItem->endColumn = seqItem->expression->endColumn;
	
	if (_current->type == TokenType::Colon)
	{
		advance();
		seqItem->statement = statement();
		
		seqItem->endLine = seqItem->statement->endLine;
		seqItem->endColumn = seqItem->statement->endColumn;
	}
	
	return seqItem;
}

shared_ptr<ExpressionNode> Parser::groupPrimary()
{
	shared_ptr<GroupPrimaryNode> group = std::make_shared<GroupPrimaryNode>();
	group->startLine = _current->line;
	group->startColumn = _current->column;
	consume(TokenType::OpenParen);
	
	group->expression = expression();
	
	group->endLine = _current->line;
	group->endColumn = _current->column + _current->text.length();
	consume(TokenType::CloseParen);
	
	return group;
}

bool Parser::consumeOpt(TokenType type)
{
	if (_current->type == type)
	{
		advance();
		return true;
	}
	return false;
}

void Parser::consume(TokenType type)
{
	if (_current->type != type)
	{
		expected(type);
	}
	advance();
}

void Parser::advance()
{
	if (_index < _tokens.size())
	{
		_current = _tokens[_index++];
	}
	
	// no need to check for >= tokens_.size() and set current_ to EOF,
	// because the last token is always EOF
}

bool Parser::isEOF()
{
	return _current->type == TokenType::Eof;
}

void Parser::expect(TokenType expectedType)
{
	if (_current->type != expectedType)
	{
		expected(expectedType);
	}
}

void Parser::expect(std::initializer_list<TokenType> list)
{
	for (auto it = list.begin(); it != list.end(); ++it)
	{
		if (_current->type == *it)
		{
			return;
		}
	}
	
	expected(list);
}

void Parser::expected(TokenType expectedType)
{
	error(Localization::expectedOne(Token::getTypeName(expectedType), Token::getTypeName(_current->type)));
}

void Parser::expected(std::initializer_list<TokenType> list)
{
	string msg = "(";
	
	int64_t i = 0;
	for (auto it = list.begin(); it != list.end(); ++it)
	{
		msg += Token::getTypeName(*it);
		if (i++ < list.size()-1)
		{
			msg += ", ";
		}
	}
	
	msg += ")";
	
	error(Localization::expectedMultiple(msg, Token::getTypeName(_current->type)));
}

void Parser::error(const string& message)
{
	throw ParserException(_filepath, message, *_current);
}

void Parser::error(const string& message, Token* token)
{
	throw ParserException(_filepath, message, *token);
}

}
