#ifndef PSL_STORY_H
#define PSL_STORY_H

#include <vector>
#include <unordered_map>
#include <set>
#include <string>
#include <memory>

#include "execution_entry.hpp"
#include "named_container.hpp"
#include "../visitors/node_visitor.hpp"
#include "../visitors/node_register.hpp"

namespace PSL
{
using std::vector;
using std::unordered_map;
using std::set;
using std::string;
using std::shared_ptr;

class StoryManager
{
public:
	StoryManager(shared_ptr<StoryNode> startingStory);
	
	shared_ptr<NamedContainer>& getNamedContainer(NamespaceNode* ns);
	shared_ptr<NamedContainer>& getNamedContainer(SectionNode* section);
	shared_ptr<NamedContainer>& getNamedContainer(SequencePrimaryNode* sequence);
	int64_t addStringGetId(const string& str);
	int64_t addNameGetId(const string& str);
	void addNodeExecutionEntry(Node* node, shared_ptr<ExecutionEntry> entry);
	shared_ptr<ExecutionEntry> getNodeExecutionEntry(Node* node);
	shared_ptr<NamedContainer> createNamedContainer(NamespaceNode* ns, NamedContainer* parent);
	shared_ptr<NamedContainer> createNamedContainer(SectionNode* section, NamedContainer* parent);
	shared_ptr<NamedContainer> createNamedContainer(SequencePrimaryNode* sequence, const string& name, NamedContainer* parent);
	void generateNamedContainerIds(NamedContainer* container);
	
	unordered_map<string, int64_t>& strings() { return _strings; }
	unordered_map<string, int64_t>& names() { return _names; }
	shared_ptr<NamedContainer>& mainNamedContainer() { return _mainNamedContainer; }
	shared_ptr<StoryNode>& startingStory() { return _startingStory; }
	NodeRegister& nodeRegister() { return _nodeRegister; }
	
private:
	NodeRegister _nodeRegister;
	shared_ptr<StoryNode> _startingStory;
	shared_ptr<NamedContainer> _mainNamedContainer;
	unordered_map<Node*, shared_ptr<NamedContainer>> _namedContainers;
	unordered_map<string, shared_ptr<StoryNode>> _stories;
	vector<string> _storyIncludeOrder;
	unordered_map<int64_t, shared_ptr<ExecutionEntry>> _nodeToExecutionEntry;
	unordered_map<string, int64_t> _strings;
	unordered_map<string, int64_t> _names;
	set<int64_t> _existingNameIds;
	unordered_map<string, int64_t> _namedContainerNames;
	set<int64_t> _existingNameContainerIds;
	
	void loadIncludedFiles(shared_ptr<StoryNode>& story);
};

}

#endif //PSL_STORY_H
