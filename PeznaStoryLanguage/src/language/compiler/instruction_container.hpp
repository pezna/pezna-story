#ifndef PSL_INSTRUCTION_CONTAINER_H
#define PSL_INSTRUCTION_CONTAINER_H

#include <cstdint>
#include <functional>
#include <memory>
#include <vector>
#include <ostream>

namespace PSL
{
using std::function;
using std::shared_ptr;
using std::vector;
using std::initializer_list;

class InstructionData
{
	bool _finalized;
	int64_t _size;
	vector<int8_t> _data;
	function<vector<int8_t>(int64_t offset)> _lazyRetrieveData;
	
public:
	InstructionData(initializer_list<int8_t> data)
	:
		_finalized{true},
		_size{(int64_t)data.size()},
		_data(data)
	{}
	
	InstructionData(vector<int8_t> data)
	:
		_finalized{true},
		_size{(int64_t)data.size()},
		_data(data)
	{}

	InstructionData(function<vector<int8_t>(int64_t offset)> lazyRetrieveData, int64_t expectedSize)
	:
		_finalized{false},
		_size{expectedSize},
		_lazyRetrieveData(lazyRetrieveData)
	{}
	
	bool isFinalized() const;
	
	void finalize(int64_t offset);
	
	vector<int8_t>& data();
	
	int64_t size() const;
};

struct InstructionContainer
{
	InstructionContainer* nextSibling;
	InstructionContainer* previousSibling;
	vector<InstructionData> instructions;
	
	InstructionContainer()
	:
		nextSibling{nullptr},
		previousSibling{nullptr}
	{}
	
	/**
	 * The starting index of this container's data within its linked list
	 */
	int64_t index() const;
	
	/**
	 * The total length of the bytes contained within this container's instructions
	 */
	int64_t size() const;
	
	/**
	 * The index of this container within it's list
	 */
	int64_t listIndex() const;
	
	/**
	 * The total length of the list this container
	 */
	int64_t listSize() const;
	
	bool isFinalized() const;
	
	void finalize();
	
	InstructionContainer* lastSibling();
	
	InstructionContainer* firstSibling();
	
	void insertBefore(InstructionContainer* container);
	
	void insertAfter(InstructionContainer* container);
	
	void addInstruction(InstructionData&& instruction);
	
	void addInstruction(initializer_list<int8_t> bytes);
	
	void addInstruction(vector<int8_t> bytes);
	
	void addInstructions(InstructionContainer& instructionContainer);
	
	void output(std::ostream& os);
};

}

#endif // PSL_INSTRUCTION_CONTAINER_H
