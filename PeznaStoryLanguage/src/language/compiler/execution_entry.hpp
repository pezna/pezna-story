#ifndef PSL_EXECUTION_ENTRY_H
#define PSL_EXECUTION_ENTRY_H

#include <utility>

#include "instruction_container.hpp"
#include "named_container.hpp"
#include "value.hpp"
#include "variable.hpp"

namespace PSL
{

struct ExecutionEntry
{
	ExecutionEntry() {}
	virtual ~ExecutionEntry() {}
	
	virtual bool isValue() { return false; }
	virtual bool isVariable() { return false; }
	virtual bool isContainer() { return false; }
};

struct ValueExecutionEntry : public ExecutionEntry
{
	Value value;
	
	ValueExecutionEntry(Value value)
	:
		value(value)
	{}
	
	bool isValue() override { return true; }
};

struct ContainerExecutionEntry : public ExecutionEntry
{
	NamedContainer* namedContainer;
	
	ContainerExecutionEntry(NamedContainer* namedContainer)
	:
		namedContainer(namedContainer)
	{}
	
	bool isContainer() override { return true; }
};

struct VariableExecutionEntry : public ExecutionEntry
{
	Variable* variable;
	
	VariableExecutionEntry(Variable* variable)
	:
		variable(variable)
	{}
	
	bool isVariable() override { return true; }
};

}

#endif // PSL_EXECUTION_ENTRY_H