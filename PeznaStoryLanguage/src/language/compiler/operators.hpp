#ifndef PSL_OPERATOR_H
#define PSL_OPERATOR_H

#include <vector>
#include <string>
#include <exception>

#include "value.hpp"
#include "../token.hpp"

namespace PSL
{
using std::vector;

struct Operator
{
	virtual ~Operator() {}
	
	virtual vector<ValueType> leftRequiredTypes() = 0;
	virtual vector<ValueType> rightRequiredTypes() = 0;
	virtual bool canAccept(ValueType& left, ValueType& right) = 0;
	virtual ValueType determineResultType(ValueType& left, ValueType& right) = 0;
};

// ==
struct EqualsOperator : public Operator
{
	vector<ValueType> leftRequiredTypes() override { return { ValueType::ANY }; }
	vector<ValueType> rightRequiredTypes() override { return { ValueType::ANY }; }
	bool canAccept(ValueType& left, ValueType& right) override { return true; }
	ValueType determineResultType(ValueType& left, ValueType& right) override { return ValueType::BOOLEAN; }
};

// !=
struct NotEqualsOperator : public Operator
{
	vector<ValueType> leftRequiredTypes() override { return { ValueType::ANY }; }
	vector<ValueType> rightRequiredTypes() override { return { ValueType::ANY }; }
	bool canAccept(ValueType& left, ValueType& right) override { return true; }
	ValueType determineResultType(ValueType& left, ValueType& right) override { return ValueType::BOOLEAN; }
};

// >=
struct GreaterThanEqualsOperator : public Operator
{
	vector<ValueType> acceptableTypes{ ValueType::INTEGER, ValueType::FLOAT };
	vector<ValueType> leftRequiredTypes() override { return acceptableTypes; }
	vector<ValueType> rightRequiredTypes() override { return acceptableTypes; }
	
	bool canAccept(ValueType& left, ValueType& right) override
	{
		return left.canBeNumber() && right.canBeNumber();
	}
	
	ValueType determineResultType(ValueType& left, ValueType& right) override
	{
		return ValueType::BOOLEAN;
	}
};

// <=
struct LessThanEqualsOperator : public Operator
{
	vector<ValueType> acceptableTypes{ ValueType::INTEGER, ValueType::FLOAT };
	vector<ValueType> leftRequiredTypes() override { return acceptableTypes; }
	vector<ValueType> rightRequiredTypes() override { return acceptableTypes; }
	
	bool canAccept(ValueType& left, ValueType& right) override
	{
		return left.canBeNumber() && right.canBeNumber();
	}
	
	ValueType determineResultType(ValueType& left, ValueType& right) override
	{
		return ValueType::BOOLEAN;
	}
};

// >
struct GreaterThanOperator : public Operator
{
	vector<ValueType> acceptableTypes{ ValueType::INTEGER, ValueType::FLOAT };
	vector<ValueType> leftRequiredTypes() override { return acceptableTypes; }
	vector<ValueType> rightRequiredTypes() override { return acceptableTypes; }
	
	bool canAccept(ValueType& left, ValueType& right) override
	{
		return left.canBeNumber() && right.canBeNumber();
	}
	
	ValueType determineResultType(ValueType& left, ValueType& right) override
	{
		return ValueType::BOOLEAN;
	}
};

// <
struct LessThanOperator : public Operator
{
	vector<ValueType> acceptableTypes{ ValueType::INTEGER, ValueType::FLOAT };
	vector<ValueType> leftRequiredTypes() override { return acceptableTypes; }
	vector<ValueType> rightRequiredTypes() override { return acceptableTypes; }
	
	bool canAccept(ValueType& left, ValueType& right) override
	{
		return left.canBeNumber() && right.canBeNumber();
	}
	
	ValueType determineResultType(ValueType& left, ValueType& right) override
	{
		return ValueType::BOOLEAN;
	}
};

struct NotOperator : public Operator
{
	vector<ValueType> leftRequiredTypes() override { return { ValueType::ANY }; }
	vector<ValueType> rightRequiredTypes() override { return { ValueType::ANY }; }
	
	bool canAccept(ValueType& left, ValueType& right) override
	{
		return true;
	}
	
	ValueType determineResultType(ValueType& left, ValueType& right) override
	{
		return ValueType::BOOLEAN;
	}
};

struct AndOperator : public Operator
{
	vector<ValueType> leftRequiredTypes() override { return { ValueType::ANY }; }
	vector<ValueType> rightRequiredTypes() override { return { ValueType::ANY }; }
	
	bool canAccept(ValueType& left, ValueType& right) override
	{
		return true;
	}
	
	ValueType determineResultType(ValueType& left, ValueType& right) override
	{
		return ValueType::BOOLEAN;
	}
};

struct OrOperator : public Operator
{
	vector<ValueType> leftRequiredTypes() override { return { ValueType::ANY }; }
	vector<ValueType> rightRequiredTypes() override { return { ValueType::ANY }; }
	
	bool canAccept(ValueType& left, ValueType& right) override
	{
		return true;
	}
	
	ValueType determineResultType(ValueType& left, ValueType& right) override
	{
		return ValueType::BOOLEAN;
	}
};

struct InOperator : public Operator
{
	vector<ValueType> acceptableRightTypes = { ValueType::STRING, ValueType::LIST, ValueType::DICT, ValueType::CHARACTER };
	vector<ValueType> leftRequiredTypes() override { return { ValueType::ANY }; }
	vector<ValueType> rightRequiredTypes() override { return acceptableRightTypes; }
	
	bool canAccept(ValueType& left, ValueType& right) override
	{
		return right.canBeString() || right.canBeList() || right.canBeDict() || right.canBeCharacter();
	}
	
	ValueType determineResultType(ValueType& left, ValueType& right) override
	{
		return ValueType::BOOLEAN;
	}
};

struct NotInOperator : public Operator
{
	vector<ValueType> acceptableRightTypes = { ValueType::STRING, ValueType::LIST, ValueType::DICT, ValueType::CHARACTER };
	vector<ValueType> leftRequiredTypes() override { return { ValueType::ANY }; }
	vector<ValueType> rightRequiredTypes() override { return acceptableRightTypes; }
	
	bool canAccept(ValueType& left, ValueType& right) override
	{
		return right.canBeString() || right.canBeList() || right.canBeDict() || right.canBeCharacter();
	}
	
	ValueType determineResultType(ValueType& left, ValueType& right) override
	{
		return ValueType::BOOLEAN;
	}
};

struct IsOperator : public Operator
{
	vector<ValueType> leftRequiredTypes() override { return { ValueType::ANY }; }
	vector<ValueType> rightRequiredTypes() override { return { ValueType::ANY }; }
	
	bool canAccept(ValueType& left, ValueType& right) override
	{
		return true; // Is and IsNot are only for types and are handled by parser, so just have true here
	}
	
	ValueType determineResultType(ValueType& left, ValueType& right) override
	{
		return ValueType::BOOLEAN;
	}
};

struct IsNotOperator : public Operator
{
	vector<ValueType> leftRequiredTypes() override { return { ValueType::ANY }; }
	vector<ValueType> rightRequiredTypes() override { return { ValueType::ANY }; }
	
	bool canAccept(ValueType& left, ValueType& right) override
	{
		return true; // Is and IsNot are only for types and are handled by parser, so just have true here
	}
	
	ValueType determineResultType(ValueType& left, ValueType& right) override
	{
		return ValueType::BOOLEAN;
	}
};

struct PowOperator : public Operator
{
	vector<ValueType> acceptableTypes{ ValueType::INTEGER, ValueType::FLOAT };
	vector<ValueType> leftRequiredTypes() override { return acceptableTypes; }
	vector<ValueType> rightRequiredTypes() override { return acceptableTypes; }
	
	bool canAccept(ValueType& left, ValueType& right) override
	{
		return left.canBeNumber() && right.canBeNumber();
	}
	
	ValueType determineResultType(ValueType& left, ValueType& right) override
	{
		if (left.canBeFloat() || right.canBeFloat())
		{
			return ValueType::FLOAT;
		}
		
		return ValueType::INTEGER;
	}
};

struct AddOperator : public Operator
{
	vector<ValueType> acceptableTypes{ ValueType::STRING, ValueType::INTEGER, ValueType::FLOAT };
	vector<ValueType> leftRequiredTypes() override { return acceptableTypes; }
	vector<ValueType> rightRequiredTypes() override { return acceptableTypes; }
	
	bool canAccept(ValueType& left, ValueType& right) override
	{
		return (left.canBeNumber() || left.canBeString()) && (right.canBeNumber() || right.canBeString());
	}
	
	ValueType determineResultType(ValueType& left, ValueType& right) override
	{
		if (left.canBeString() || right.canBeString())
		{
			return ValueType::STRING;
		}
		else if (left.canBeFloat() || right.canBeFloat())
		{
			return ValueType::FLOAT;
		}
		
		return ValueType::INTEGER;
	}
};

struct SubOperator : public Operator
{
	vector<ValueType> acceptableTypes{ ValueType::INTEGER, ValueType::FLOAT };
	vector<ValueType> leftRequiredTypes() override { return acceptableTypes; }
	vector<ValueType> rightRequiredTypes() override { return acceptableTypes; }
	
	bool canAccept(ValueType& left, ValueType& right) override
	{
		return left.canBeNumber() && right.canBeNumber();
	}
	
	ValueType determineResultType(ValueType& left, ValueType& right) override
	{
		if (left.canBeFloat() || right.canBeFloat())
		{
			return ValueType::FLOAT;
		}
		
		return ValueType::INTEGER;
	}
};

struct MulOperator : public Operator
{
	vector<ValueType> acceptableTypes{ ValueType::INTEGER, ValueType::FLOAT };
	vector<ValueType> leftRequiredTypes() override { return acceptableTypes; }
	vector<ValueType> rightRequiredTypes() override { return acceptableTypes; }
	
	bool canAccept(ValueType& left, ValueType& right) override
	{
		return left.canBeNumber() && right.canBeNumber();
	}
	
	ValueType determineResultType(ValueType& left, ValueType& right) override
	{
		if (left.canBeFloat() || right.canBeFloat())
		{
			return ValueType::FLOAT;
		}
		
		return ValueType::INTEGER;
	}
};

struct DivOperator : public Operator
{
	vector<ValueType> acceptableTypes{ ValueType::INTEGER, ValueType::FLOAT };
	vector<ValueType> leftRequiredTypes() override { return acceptableTypes; }
	vector<ValueType> rightRequiredTypes() override { return acceptableTypes; }
	
	bool canAccept(ValueType& left, ValueType& right) override
	{
		return left.canBeNumber() && right.canBeNumber();
	}
	
	ValueType determineResultType(ValueType& left, ValueType& right) override
	{
		if (left.canBeFloat() || right.canBeFloat())
		{
			return ValueType::FLOAT;
		}
		
		return ValueType::INTEGER;
	}
};

struct ModOperator : public Operator
{
	vector<ValueType> acceptableTypes{ ValueType::INTEGER, ValueType::FLOAT };
	vector<ValueType> leftRequiredTypes() override { return acceptableTypes; }
	vector<ValueType> rightRequiredTypes() override { return acceptableTypes; }
	
	bool canAccept(ValueType& left, ValueType& right) override
	{
		return left.canBeNumber() && right.canBeNumber();
	}
	
	ValueType determineResultType(ValueType& left, ValueType& right) override
	{
		if (left.canBeFloat() || right.canBeFloat())
		{
			return ValueType::FLOAT;
		}
		
		return ValueType::INTEGER;
	}
};

struct BitAndOperator : public Operator
{
	vector<ValueType> acceptableTypes{ ValueType::INTEGER };
	vector<ValueType> leftRequiredTypes() override { return acceptableTypes; }
	vector<ValueType> rightRequiredTypes() override { return acceptableTypes; }
	
	bool canAccept(ValueType& left, ValueType& right) override
	{
		return left.canBeInteger() && right.canBeInteger();
	}
	
	ValueType determineResultType(ValueType& left, ValueType& right) override
	{
		return ValueType::INTEGER;
	}
};

struct BitXorOperator : public Operator
{
	vector<ValueType> acceptableTypes{ ValueType::INTEGER };
	vector<ValueType> leftRequiredTypes() override { return acceptableTypes; }
	vector<ValueType> rightRequiredTypes() override { return acceptableTypes; }
	
	bool canAccept(ValueType& left, ValueType& right) override
	{
		return left.canBeInteger() && right.canBeInteger();
	}
	
	ValueType determineResultType(ValueType& left, ValueType& right) override
	{
		return ValueType::INTEGER;
	}
};

struct BitOrOperator : public Operator
{
	vector<ValueType> acceptableTypes{ ValueType::INTEGER };
	vector<ValueType> leftRequiredTypes() override { return acceptableTypes; }
	vector<ValueType> rightRequiredTypes() override { return acceptableTypes; }
	
	bool canAccept(ValueType& left, ValueType& right) override
	{
		return left.canBeInteger() && right.canBeInteger();
	}
	
	ValueType determineResultType(ValueType& left, ValueType& right) override
	{
		return ValueType::INTEGER;
	}
};

struct BitNotOperator : public Operator
{
	vector<ValueType> acceptableTypes{ ValueType::INTEGER };
	vector<ValueType> leftRequiredTypes() override { return acceptableTypes; }
	vector<ValueType> rightRequiredTypes() override { return acceptableTypes; }
	
	bool canAccept(ValueType& left, ValueType& right) override
	{
		return left.canBeInteger() && right.canBeInteger();
	}
	
	ValueType determineResultType(ValueType& left, ValueType& right) override
	{
		return ValueType::INTEGER;
	}
};

struct BitLeftShiftOperator : public Operator
{
	vector<ValueType> acceptableTypes{ ValueType::INTEGER };
	vector<ValueType> leftRequiredTypes() override { return acceptableTypes; }
	vector<ValueType> rightRequiredTypes() override { return acceptableTypes; }
	
	bool canAccept(ValueType& left, ValueType& right) override
	{
		return left.canBeInteger() && right.canBeInteger();
	}
	
	ValueType determineResultType(ValueType& left, ValueType& right) override
	{
		return ValueType::INTEGER;
	}
};

struct BitRightShiftOperator : public Operator
{
	vector<ValueType> acceptableTypes{ ValueType::INTEGER };
	vector<ValueType> leftRequiredTypes() override { return acceptableTypes; }
	vector<ValueType> rightRequiredTypes() override { return acceptableTypes; }
	
	bool canAccept(ValueType& left, ValueType& right) override
	{
		return left.canBeInteger() && right.canBeInteger();
	}
	
	ValueType determineResultType(ValueType& left, ValueType& right) override
	{
		return ValueType::INTEGER;
	}
};

struct Operators
{
	static EqualsOperator Equals;
	static NotEqualsOperator NotEquals;
	static GreaterThanEqualsOperator GreaterThanEquals;
	static LessThanEqualsOperator LessThanEquals;
	static GreaterThanOperator GreaterThan;
	static LessThanOperator LessThan;
	static NotOperator Not;
	static AndOperator And;
	static OrOperator Or;
	static InOperator In;
	static NotInOperator NotIn;
	static IsOperator Is;
	static IsNotOperator IsNot;
	static PowOperator Pow;
	static AddOperator Add;
	static SubOperator Sub;
	static MulOperator Mul;
	static DivOperator Div;
	static ModOperator Mod;
	static BitAndOperator BitAnd;
	static BitXorOperator BitXor;
	static BitOrOperator BitOr;
	static BitNotOperator BitNot;
	static BitLeftShiftOperator BitLeftShift;
	static BitRightShiftOperator BitRightShift;
	
	static Operator& getFromTokenType(TokenType type)
	{
		switch (type)
		{
			case TokenType::Equals:
				return Operators::Equals;
			case TokenType::NotEquals:
				return Operators::NotEquals;
			case TokenType::GreaterThanEquals:
				return Operators::GreaterThanEquals;
			case TokenType::LessThanEquals:
				return Operators::LessThanEquals;
			case TokenType::GreaterThan:
				return Operators::GreaterThan;
			case TokenType::LessThan:
				return Operators::LessThan;
			case TokenType::Not:
				return Operators::Not;
			case TokenType::And:
				return Operators::And;
			case TokenType::Or:
				return Operators::Or;
			case TokenType::In:
				return Operators::In;
			case TokenType::NotIn:
				return Operators::NotIn;
			case TokenType::Is:
				return Operators::Is;
			case TokenType::IsNot:
				return Operators::IsNot;
			case TokenType::Pow:
				return Operators::Pow;
			case TokenType::Add:
				return Operators::Add;
			case TokenType::Sub:
				return Operators::Sub;
			case TokenType::Mul:
				return Operators::Mul;
			case TokenType::Div:
				return Operators::Div;
			case TokenType::Mod:
				return Operators::Mod;
			case TokenType::BitAnd:
				return Operators::BitAnd;
			case TokenType::BitXor:
				return Operators::BitXor;
			case TokenType::BitOr:
				return Operators::BitOr;
			case TokenType::BitNot:
				return Operators::BitNot;
			case TokenType::BitLeftShift:
				return Operators::BitLeftShift;
			case TokenType::BitRightShift:
				return Operators::BitRightShift;
		}
		
		throw std::runtime_error(std::string("No operator for token type ") + Token::getTypeName(type));
	}
};

}

#endif // PSL_OPERATOR_H