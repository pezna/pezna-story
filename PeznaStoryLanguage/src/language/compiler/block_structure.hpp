#ifndef PSL_BLOCK_STRUCTURE_H
#define PSL_BLOCK_STRUCTURE_H

#include <cstdint>
#include <utility>

namespace PSL
{

enum class BlockStructureType
{
	None, // default
	If,
	Do,
	While,
	For,
	Choice
};

struct BlockStructure
{
	BlockStructureType type;
	int64_t labelNameIndexBefore;
	int64_t labelNameIndexAfter;
	bool hasLabelBefore;
	bool hasLabelAfter;
	
	BlockStructure()
	:
		type{BlockStructureType::None},
		labelNameIndexBefore{0},
		labelNameIndexAfter{0}
	{
		hasLabelBefore = labelNameIndexBefore > 0;
		hasLabelAfter = labelNameIndexAfter > 0;
	}
	
	BlockStructure(BlockStructureType type, int64_t labelNameIndexBefore, int64_t labelNameIndexAfter)
	:
		type{type},
		labelNameIndexBefore{labelNameIndexBefore},
		labelNameIndexAfter{labelNameIndexAfter}
	{
		hasLabelBefore = labelNameIndexBefore > 0;
		hasLabelAfter = labelNameIndexAfter > 0;
	}
	
	BlockStructure(BlockStructure&& other)
	:
		type(std::move(other.type)),
		labelNameIndexBefore(std::move(other.labelNameIndexBefore)),
		labelNameIndexAfter(std::move(other.labelNameIndexAfter))
	{
		hasLabelBefore = labelNameIndexBefore > 0;
		hasLabelAfter = labelNameIndexAfter > 0;
	}
	
	BlockStructure& operator=(const BlockStructure& other)
	{
		type = other.type;
		labelNameIndexBefore = other.labelNameIndexBefore;
		labelNameIndexAfter = other.labelNameIndexAfter;
		hasLabelBefore = other.hasLabelBefore;
		hasLabelAfter = other.hasLabelAfter;
		
		return *this;
	}
	
	bool isLoop()
	{
		return type == BlockStructureType::Do || type == BlockStructureType::While || type == BlockStructureType::For;
	}
};

}

#endif // PSL_BLOCK_STRUCTURE_H
