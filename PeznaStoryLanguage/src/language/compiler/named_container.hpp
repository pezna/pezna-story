#ifndef PSL_NAMED_CONTAINER_H
#define PSL_NAMED_CONTAINER_H

#include <cstdint>
#include <vector>
#include <unordered_map>
#include <string>
#include <memory>

#include "instruction_container.hpp"
#include "value.hpp"
#include "variable.hpp"
#include "../nodes/node.hpp"
#include "../nodes/namespace_node.hpp"
#include "../nodes/section_node.hpp"

namespace PSL
{
using std::vector;
using std::unordered_map;
using std::string;
using std::shared_ptr;

class NamedContainer
{
public:
	NamedContainer(NamespaceNode* ns, NamedContainer* parent);
	NamedContainer(SectionNode* section, NamedContainer* parent);
	NamedContainer(SequencePrimaryNode* sequence, const string& name, NamedContainer* parent);
	
	/**
	 * The parent container of this container
	 */
	NamedContainer* parent;
	
	/**
	 * The children containers
	 */
	vector<shared_ptr<NamedContainer>> children;
	
	/**
	 * The AST nodes that constitute this container
	 * Two namespaces in different files with the same parentage and name
	 * will be contained in the same NamedContainer
	 */
	vector<Node*> nodes;
	
	/**
	 * Variables created in this container
	 */
	unordered_map<int64_t, Variable> variables;
	
	/**
	 * The types for this parameter
	 */
	vector<ValueType> parameterTypes;
	
	/**
	 * The labels that are within this named container,
	 * and the offset that should be jumped to when trying to reach them
	 */
	unordered_map<int64_t, int64_t> labels;
	
	/**
	 * The container that will contain the instructions for this named container
	 */
	InstructionContainer instructionContainer;
	
	/**
	 * The name ID for this namespace/section in the code
	 */
	int64_t nameId;
	
	/**
	 * The name for this namespace/section in the code
	 */
	string name;
	
	/**
	 * Fully qualified name with all parents
	 */
	string fullyQualifiedName;
	
	/**
	 * Effectively an index for the named container.
	 * Also the unique id that will be used for the function in the bytecode
	 * Section function will contain the statements within it.
	 * Namespace function will contain the namespace-level statements.
	 */
	int64_t id;
	
	/**
	 * The offset into the total bytecode that will be outputted (inclusive)
	 */
	int64_t bytecodeOffset;
	
	/**
	 * The length of all the bytecode that constitutes this container's statements
	 */
	int64_t bytecodeLength;
	
	/**
	 * Whether this is for a namespace
	 */
	bool isNamespace;
	
	/**
	 * Whether this is for a section
	 */
	bool isSection;
	
	/**
	 * Whether this is for a sequence
	 */
	bool isSequence;
	
	/**
	 * The return type of this container, if specified
	 */
	ValueType returnType;
	
	/**
	 * Add a variable that belongs to the local stack
	 */
	Variable* addVariable(int64_t nameId, AssignmentStatementNode* creation, Value value);
	
	/**
	 * Add a variable that belongs to the local stack
	 */
	Variable* addVariable(int64_t nameId, ParameterNode* parameter, Value value);
	
	/**
	 * Whether or not the variable with the name index belongs to this container
	 */
	bool hasVariable(int64_t nameId);
	
	/**
	 * Get a variable in this container
	 * @param name - the name of the variable to look for
	 * @param outVariable - the variable to be assigned to if it is found
	 * @return whether or not the variable was found
	 */
	Variable* getVariable(int64_t nameId);
	
	/**
	 * Find a variable in this container or in its parents
	 * @param name - the name of the variable to look for
	 * @param outVariable - the variable to be assigned to if it is found
	 * @return whether or not the variable was found
	 */
	Variable* findVariableInScope(int64_t nameId);
	
	/**
	 * Find a variable starting with this container's highest ancestor and working downwards
	 * @param name - the name of the variable to look for
	 * @param outVariable - the variable to be assigned to if it is found
	 * @return whether or not the variable was found
	 */
	Variable* findVariableGlobally(int64_t nameId);
	
	/**
	 * Find an ancestor of this NamedContainer by its name
	 * @param name - the name of the container to look for
	 */
	NamedContainer* findAncestorNamed(int64_t nameId);
	
	/**
	 * Find a sibling of this NamedContainer by its name
	 * @param name - the name of the container to look for
	 */
	NamedContainer* findSiblingNamed(int64_t nameId);
	
	/**
	 * Find an ancestor or sibling of this NamedContainer by its name
	 * @param name - the name of the container to look for
	 */
	NamedContainer* findAncestorOrSiblingNamed(int64_t nameId);
	
	/**
	 * Find a child of this NamedContainer by its name
	 * @param name - the name of the container to look for
	 */
	NamedContainer* findChildNamed(int64_t nameId);
	
	/**
	 * Find an ancestor, child, or sibling of this NamedContainer by its name
	 * @param name - the name of the container to look for
	 */
	NamedContainer* findVisibleContainerNamed(int64_t nameId);
	
	/**
	 * Count the number of named containers below this one.
	 */
	int64_t getNumDescendants();
	
	/**
	 * Check if this container has the given label
	 * @param label - the text of the label
	 */
	bool hasLabel(int64_t nameId);
	
	/**
	 * Get the location of the given label within this container
	 * @param label - the text of the label
	 */
	int64_t getLabel(int64_t nameId);
	
	/**
	 * Add the label and its location to this container
	 * @param label - the text of the label
	 * @param location - the offset into the code that the label occurs
	 */
	void addLabel(int64_t nameId, int64_t location);
};

}

#endif // PSL_NAMED_CONTAINER_H
