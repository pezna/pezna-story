#include "value.hpp"

#include <vector>

namespace PSL
{
constexpr int T_NONE = 0;
constexpr int T_BOOLEAN = 1 << 0;
constexpr int T_INTEGER = 1 << 1;
constexpr int T_FLOAT = 1 << 2;
constexpr int T_STRING = 1 << 3;
constexpr int T_LIST = 1 << 4;
constexpr int T_DICT = 1 << 5;
constexpr int T_CHARACTER = 1 << 6;
constexpr int T_ANY = 0xff;
	
ValueType ValueType::NONE = ValueType(T_NONE);
ValueType ValueType::BOOLEAN = ValueType(T_BOOLEAN);
ValueType ValueType::INTEGER = ValueType(T_INTEGER);
ValueType ValueType::FLOAT = ValueType(T_FLOAT);
ValueType ValueType::STRING = ValueType(T_STRING);
ValueType ValueType::LIST = ValueType(T_LIST);
ValueType ValueType::DICT = ValueType(T_DICT);
ValueType ValueType::CHARACTER = ValueType(T_CHARACTER);
ValueType ValueType::ANY = ValueType(T_ANY);

std::unordered_map<int, int> TYPE_CONVERSION_MAP = {
		{T_NONE, T_NONE},
		{T_BOOLEAN, T_BOOLEAN | T_INTEGER | T_FLOAT | T_STRING},
		{T_INTEGER, T_BOOLEAN | T_INTEGER | T_FLOAT | T_STRING},
		{T_FLOAT, T_BOOLEAN | T_INTEGER | T_FLOAT | T_STRING},
		{T_STRING, T_BOOLEAN | T_INTEGER | T_FLOAT | T_STRING},
		{T_LIST, T_BOOLEAN | T_STRING | T_LIST | T_DICT},
		{T_DICT, T_BOOLEAN | T_STRING | T_LIST | T_DICT},
		{T_CHARACTER, T_BOOLEAN | T_STRING | T_DICT | T_CHARACTER},
};

std::unordered_map<int, string> STRING_MAP = {
		{T_NONE, "none"},
		{T_BOOLEAN, "bool"},
		{T_INTEGER, "int"},
		{T_FLOAT, "float"},
		{T_STRING, "string"},
		{T_LIST, "list"},
		{T_DICT, "dict"},
		{T_CHARACTER, "character"},
		{T_ANY, "var"}
};

bool ValueType::operator==(const ValueType& other)
{
	return type == other.type;
}

bool ValueType::operator!=(const ValueType& other)
{
	return type != other.type;
}

bool ValueType::isHashable() const
{
	return isNumber() || isBoolean() || isString();
}

bool ValueType::isNumber() const
{
	return type == T_INTEGER || type == T_FLOAT;
}

bool ValueType::canBeNumber() const
{
	return (type & (T_INTEGER | T_FLOAT)) > 0;
}

bool ValueType::isBoolean() const
{
	return type == T_BOOLEAN;
}

bool ValueType::canBeBoolean() const
{
	return (type & T_BOOLEAN) > 0;
}

bool ValueType::isInteger() const
{
	return type == T_INTEGER;
}

bool ValueType::canBeInteger() const
{
	return (type & T_INTEGER) > 0;
}

bool ValueType::isFloat() const
{
	return type == T_FLOAT;
}

bool ValueType::canBeFloat() const
{
	return (type & T_FLOAT) > 0;
}

bool ValueType::isString() const
{
	return type == T_STRING;
}

bool ValueType::canBeString() const
{
	return (type & T_STRING) > 0;
}

bool ValueType::isList() const
{
	return type == T_LIST;
}

bool ValueType::canBeList() const
{
	return (type & T_LIST) > 0;
}

bool ValueType::isDict() const
{
	return type == T_DICT;
}

bool ValueType::canBeDict() const
{
	return (type & T_DICT) > 0;
}

bool ValueType::isCharacter() const
{
	return type == T_CHARACTER;
}

bool ValueType::canBeCharacter() const
{
	return (type & T_CHARACTER) > 0;
}

bool ValueType::isAny() const
{
	return type == T_ANY;
}

bool ValueType::canBeAny() const
{
	return true;
}

bool ValueType::canConvertTo(int otherType) const
{
	if (otherType == T_ANY || type == T_ANY)
	{
		return true;
	}
	
	return (TYPE_CONVERSION_MAP.at(type) & otherType) == otherType;
}

bool ValueType::canConvertTo(const ValueType& otherType) const
{
	return canConvertTo(otherType.type);
}

bool ValueType::isCompatible(const ValueType& otherType)
{
	return (type & otherType.type) > 0;
}

string ValueType::toString() const
{
	std::ostringstream oss;
	oss << "(";
	bool first = true;
	
	for (auto& entry : STRING_MAP)
	{
		if ((entry.first & type) > 0)
		{
			if (entry.first == T_ANY && type != T_ANY)
			{
				// don't print "var" unless it is fully "var"
				continue;
			}
			
			if (!first)
			{
				oss << " | ";
			}
			
			oss << entry.second;
			first = false;
		}
	}
	
	oss << ")";
	return oss.str();
}

ValueType ValueType::createFromTokenType(TokenType tokenType)
{
	switch (tokenType)
	{
		case TokenType::Character:
			return ValueType::CHARACTER;
		case TokenType::Bool:
			return ValueType::BOOLEAN;
		case TokenType::Int:
			return ValueType::INTEGER;
		case TokenType::Float:
			return ValueType::FLOAT;
		case TokenType::String:
			return ValueType::STRING;
		case TokenType::List:
			return ValueType::LIST;
		case TokenType::Dict:
			return ValueType::DICT;
		default:
			return ValueType::ANY;
	}
}

std::ostream& operator<<(std::ostream& os, const ValueType& value)
{
	return os << value.toString();
}

}
