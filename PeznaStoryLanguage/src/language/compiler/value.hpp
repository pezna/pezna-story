#ifndef PSL_VALUE_TYPE_H
#define PSL_VALUE_TYPE_H

#include <cstdint>
#include <string>
#include <sstream>
#include <unordered_map>
#include <vector>

#include "../token.hpp"

namespace PSL
{
using std::string;
using std::vector;

struct ValueType
{
	static ValueType NONE;
	static ValueType BOOLEAN;
	static ValueType INTEGER;
	static ValueType FLOAT;
	static ValueType STRING;
	static ValueType LIST;
	static ValueType DICT;
	static ValueType CHARACTER;
	static ValueType ANY;
	
	int type;
	
	ValueType()
	:
		type{0}
	{}
	
	ValueType(int type)
	:
		type{type}
	{}
	
	ValueType(const ValueType& other) = default;
	
	ValueType& operator=(const ValueType& other) = default;
	bool operator==(const ValueType& other);
	bool operator!=(const ValueType& other);
	
	bool isHashable() const;
	bool isNumber() const;
	bool canBeNumber() const;
	bool isBoolean() const;
	bool canBeBoolean() const;
	bool isInteger() const;
	bool canBeInteger() const;
	bool isFloat() const;
	bool canBeFloat() const;
	bool isString() const;
	bool canBeString() const;
	bool isList() const;
	bool canBeList() const;
	bool isDict() const;
	bool canBeDict() const;
	bool isCharacter() const;
	bool canBeCharacter() const;
	bool isAny() const;
	bool canBeAny() const;
	
	bool canConvertTo(int otherType) const;
	bool canConvertTo(const ValueType& otherType) const;
	bool isCompatible(const ValueType& otherType);
	string toString() const;
	
	static ValueType createFromTokenType(TokenType tokenType);
	
	friend std::ostream& operator<<(std::ostream& os, const ValueType& value);
};

struct Value
{
	int64_t nameId;
	ValueType type;
	vector<Value> children;
	
	Value()
	:
		nameId(0)
	{}
	
	Value(ValueType type)
	:
		nameId(0),
		type(type)
	{}
	
	Value(int64_t nameId, ValueType type)
	:
		nameId(nameId),
		type(type)
	{}
	
	Value(const Value& other) = default;
	Value(Value&& other) = default;
	Value& operator=(const Value& other) = default;
};

}

#endif //PSL_VALUE_TYPE_H
