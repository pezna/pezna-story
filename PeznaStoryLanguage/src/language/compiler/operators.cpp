#include "operators.hpp"

namespace PSL
{

EqualsOperator Operators::Equals;
NotEqualsOperator Operators::NotEquals;
GreaterThanEqualsOperator Operators::GreaterThanEquals;
LessThanEqualsOperator Operators::LessThanEquals;
GreaterThanOperator Operators::GreaterThan;
LessThanOperator Operators::LessThan;
NotOperator Operators::Not;
AndOperator Operators::And;
OrOperator Operators::Or;
InOperator Operators::In;
NotInOperator Operators::NotIn;
IsOperator Operators::Is;
IsNotOperator Operators::IsNot;
PowOperator Operators::Pow;
AddOperator Operators::Add;
SubOperator Operators::Sub;
MulOperator Operators::Mul;
DivOperator Operators::Div;
ModOperator Operators::Mod;
BitAndOperator Operators::BitAnd;
BitXorOperator Operators::BitXor;
BitOrOperator Operators::BitOr;
BitNotOperator Operators::BitNot;
BitLeftShiftOperator Operators::BitLeftShift;
BitRightShiftOperator Operators::BitRightShift;

}