#include "instruction_container.hpp"

namespace PSL
{

/*******************
 * InstructionData *
 *******************/
bool InstructionData::isFinalized() const
{
	return _finalized;
}

void InstructionData::finalize(int64_t offset)
{
	if (_finalized)
	{
		return;
	}
	
	_data = _lazyRetrieveData(offset);
	
	if (_data.size() == _size)
	{
		_finalized = true;
	}
}

vector<int8_t>& InstructionData::data()
{
	return _data;
}

int64_t InstructionData::size() const
{
	return _size;
}

/************************
 * InstructionContainer *
 ************************/

int64_t InstructionContainer::index() const
{
	if (previousSibling == nullptr)
	{
		return 0;
	}
	
	return previousSibling->index() + previousSibling->size();
}

int64_t InstructionContainer::size() const
{
	int64_t size = 0;
	for (const InstructionData& data : instructions)
	{
		size += data.size();
	}
	
	return size;
}

int64_t InstructionContainer::listIndex() const
{
	int64_t i = 0;
	
	const InstructionContainer* cont = this;
	while (cont->previousSibling != nullptr)
	{
		cont = cont->previousSibling;
		++i;
	}
	
	return i;
}

int64_t InstructionContainer::listSize() const
{
	int64_t i = listIndex();
	
	const InstructionContainer* cont = this;
	while (cont->nextSibling != nullptr)
	{
		cont = cont->nextSibling;
		++i;
	}
	
	return i + 1;
}

bool InstructionContainer::isFinalized() const
{
	for (const InstructionData& data : instructions)
	{
		if (!data.isFinalized()) {
			return false;
		}
	}
	
	return true;
}

void InstructionContainer::finalize()
{
	int64_t offset = 0;
	for (InstructionData& data : instructions)
	{
		data.finalize(offset);
		offset += data.size();
	}
}

void InstructionContainer::output(std::ostream& os)
{
	for (InstructionData& data : instructions)
	{
		vector<int8_t>& bytes = data.data();
		
		if (bytes.size() == 0)
		{
			continue;
		}
		
		os.write(reinterpret_cast<const char*>(&bytes[0]), bytes.size());
	}
}

InstructionContainer* InstructionContainer::firstSibling()
{
	if (previousSibling == nullptr)
	{
		return this;
	}
	
	InstructionContainer* cont = previousSibling;
	while (cont->previousSibling != nullptr)
	{
		cont = cont->previousSibling;
	}
	
	return cont;
}

InstructionContainer* InstructionContainer::lastSibling()
{
	if (nextSibling == nullptr)
	{
		return this;
	}
	
	InstructionContainer* cont = nextSibling;
	while (cont->nextSibling != nullptr)
	{
		cont = cont->nextSibling;
	}
	
	return cont;
}

void InstructionContainer::insertBefore(InstructionContainer* container)
{
	container->nextSibling = this;
	
	if (previousSibling == nullptr)
	{
		previousSibling = container;
	}
	else
	{
		previousSibling->nextSibling = container;
		container->previousSibling = previousSibling;
	}
}

void InstructionContainer::insertAfter(InstructionContainer* container)
{
	container->previousSibling = this;
	
	if (nextSibling == nullptr)
	{
		nextSibling = container;
	}
	else
	{
		nextSibling->previousSibling = container;
		container->nextSibling = nextSibling;
	}
}

void InstructionContainer::addInstruction(InstructionData&& instruction)
{
	instructions.push_back(std::move(instruction));
}

void InstructionContainer::addInstruction(initializer_list<int8_t> bytes)
{
	instructions.emplace_back(bytes);
}

void InstructionContainer::addInstruction(vector<int8_t> bytes)
{
	instructions.emplace_back(bytes);
}

void InstructionContainer::addInstructions(InstructionContainer& instructionContainer)
{
	instructions.insert(instructions.end(), instructionContainer.instructions.begin(), instructionContainer.instructions.end());
}

}
