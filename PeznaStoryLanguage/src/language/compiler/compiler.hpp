#ifndef PSL_COMPILER_H
#define PSL_COMPILER_H

#include <string>
#include <unordered_map>
#include <tuple>
#include <ostream>
#include <functional>

#include "block_structure.hpp"
#include "execution_entry.hpp"
#include "instruction_container.hpp"
#include "story_manager.hpp"
#include "value.hpp"
#include "../nodes/nodes.hpp"
#include "../visitors/node_visitor.hpp"
#include "../visitors/node_register.hpp"

namespace PSL
{
using std::string;
using std::tuple;
using std::unordered_map;

class Compiler : NodeVisitor<void>
{
private:
	StoryManager _storyManager;
	InstructionContainer _metadataContainer;
	InstructionContainer _namesMetadata;
	InstructionContainer _stringsMetadata;
	InstructionContainer _containersMetadata;
	vector<NamedContainer*> _currentNamedContainers;
	vector<InstructionContainer*> _currentInstructionContainers;
	vector<BlockStructure> _currentBlockStructures;

	void createAllNameMetadata();
	void createAllStringMetadata();
	void createMetadata(NamedContainer* namedContainer);
	void createVariableMetadataForContainer(NamedContainer* namedContainer, InstructionContainer& ic);
	void createLabelMetadataForContainer(NamedContainer* namedContainer, InstructionContainer& ic);
	int64_t addLabel(const string& label, Node* node);
	InstructionContainer* getCurrentInstructionContainer();
	NamedContainer* getCurrentNamedContainer();
	
	void visitNamedContainer(NamedContainer* namedContainer, NamedContainer* parent);
	void visitNode(Node* node) override;
	void visitStory(StoryNode* node) override;
	void visitNamespace(NamespaceNode* node) override;
	void visitSection(SectionNode* node) override;
	void visitChild(ChildNode* node) override;
	void visitParameter(ParameterNode* node) override;
	
	void visitStatement(StatementNode* node) override;
	void visitAssignmentStatement(AssignmentStatementNode* node) override;
	void visitChoiceStatement(ChoiceStatementNode* node) override;
	void visitContinueStatement(ContinueStatementNode* node) override;
	void visitDoStatement(DoStatementNode* node) override;
	void visitExitStatement(ExitStatementNode* node) override;
	void visitForStatement(ForStatementNode* node) override;
	void visitGotoStatement(GotoStatementNode* node) override;
	void visitIfStatement(IfStatementNode* node) override;
	void visitIfPart(IfPartNode* node) override;
	void visitElseIfPart(ElseIfPartNode* node) override;
	void visitElsePart(ElsePartNode* node) override;
	void visitIncludeStatement(IncludeStatementNode* node) override;
	void visitLabelStatement(LabelStatementNode* node) override;
	void visitLineStatement(LineStatementNode* node) override;
	void visitReturnStatement(ReturnStatementNode* node) override;
	void visitEventStatement(EventStatementNode* node) override;
	void visitWhileStatement(WhileStatementNode* node) override;
	
	void visitExpression(ExpressionNode* node) override;
	void visitTernaryExpression(TernaryExpressionNode* node) override;
	void visitBinaryExpression(BinaryExpressionNode* node) override;
	void visitUnaryExpression(UnaryExpressionNode* node) override;
	void visitScopeNotationExpression(ScopeNotationExpressionNode* node) override;
	void visitCallExpression(CallExpressionNode* node) override;
	void visitSelectorExpression(SelectorExpressionNode* node) override;
	void visitAtomPrimary(AtomPrimaryNode* node) override;
	void visitListPrimary(ListPrimaryNode* node) override;
	void visitDictionaryPrimary(DictionaryPrimaryNode* node) override;
	void visitDictionaryPair(DictionaryPairNode* node) override;
	void visitCharacterPrimary(CharacterPrimaryNode* node) override;
	void visitCharacterPair(CharacterPairNode* node) override;
	void visitSequencePrimary(SequencePrimaryNode* node) override;
	void finalizeSequencePrimary(SequencePrimaryNode* node);
	void visitSequenceItem(SequenceItemNode* item) override;
	void visitGroupPrimary(GroupPrimaryNode* node) override;
	
	void ensureSameValueTypes(Node* expectedNode, Node* givenNode, std::function<string(string, string)> errorFunc);
	ValueType getNodeValueType(Node* node);
	Value getNodeValue(Node* node);
	ValueType getOperatorReturnValue(Node* leftNode, Node* rightNode, Token* operatorToken);
	ValueType getOperatorReturnValue(Node* node, Token* operatorToken);
	
	void error(string message, Node* node);
	void error(string message, Node* node, Token* token);
	
public:
	Compiler(shared_ptr<StoryNode> startingStory);
	~Compiler() {}
	
	void compile();
	void output(std::ostream& os);
	
	InstructionContainer& firstMetadata();
};

}

#endif //PSL_COMPILER_H
