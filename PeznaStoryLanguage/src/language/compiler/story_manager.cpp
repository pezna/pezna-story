#include "story_manager.hpp"

#include <ghc/filesystem.hpp>

#include "../utils/crc32.hpp"
#include "../exceptions.hpp"
#include "../lexer.hpp"
#include "../parser.hpp"

namespace PSL
{

/*********
 * Story *
 *********/
StoryManager::StoryManager(shared_ptr<StoryNode> startingStory)
{
	_mainNamedContainer = nullptr;
	_stories[startingStory->filepath] = startingStory;
	_storyIncludeOrder.push_back(startingStory->filepath);
	_nodeRegister.addStory(startingStory);
	_startingStory = startingStory;
	
	loadIncludedFiles(startingStory);
	
	for (const string& storyPath : _storyIncludeOrder)
	{
		auto& story = _stories.at(storyPath);
		createNamedContainer(story->mainNamespace.get(), nullptr);
	}
}

void StoryManager::loadIncludedFiles(shared_ptr<StoryNode>& parentStory)
{
	string startFolder = ghc::filesystem::path(parentStory->filepath).parent_path().string() + (char)ghc::filesystem::path::preferred_separator;
	vector<Node*> includes = _nodeRegister.getDescendantsWithType(parentStory.get(), StatementType::IncludeStatement);
	
	for (auto node : includes)
	{
		IncludeStatementNode* include = static_cast<IncludeStatementNode*>(node);
		ghc::filesystem::path includePath = startFolder + include->filename->text.substr(1, include->filename->text.length() - 2);
		string includePathString = ghc::filesystem::absolute(includePath.lexically_normal()).string();
		include->filepath = includePathString;
		
		if (_stories.find(includePathString) != _stories.end())
		{
			// story has already been retrieved, skip it
			continue;
		}
		
		std::ifstream filestream(includePathString);
		Lexer lexer(includePathString, filestream);
		Parser parser(includePathString, lexer.lex());
		
		shared_ptr<StoryNode> story = parser.parse();
		story->filepath = includePathString;
		_nodeRegister.addStory(story);
		_stories[includePathString] = story;
		_storyIncludeOrder.push_back(includePathString);
		
		loadIncludedFiles(story);
	}
}

shared_ptr<NamedContainer> StoryManager::createNamedContainer(NamespaceNode* ns, NamedContainer* parent)
{
	shared_ptr<NamedContainer> container = nullptr;
	if (parent == nullptr)
	{
		if (_mainNamedContainer == nullptr)
		{
			_mainNamedContainer = std::make_shared<NamedContainer>(ns, nullptr);
		}
		else
		{
			_mainNamedContainer->nodes.push_back(ns);
		}
		
		_namedContainers[ns] = _mainNamedContainer;
		container = _mainNamedContainer;
	}
	else
	{
		for (shared_ptr<NamedContainer> cont : parent->children)
		{
			if (cont->name == ns->name->text && cont->isNamespace)
			{
				container = cont;
				break;
			}
		}
		
		if (container == nullptr)
		{
			container = std::make_shared<NamedContainer>(ns, parent);
			parent->children.push_back(container);
		}
		else
		{
			container->nodes.push_back(ns);
		}
		
		_namedContainers[ns] = container;
	}
	
	generateNamedContainerIds(container.get());
	
	// create children named container
	for (shared_ptr<NamespaceNode>& subNs : ns->subNamespaces)
	{
		createNamedContainer(subNs.get(), container.get());
	}
	
	for (shared_ptr<SectionNode>& section : ns->sections)
	{
		createNamedContainer(section.get(), container.get());
	}
	
	return container;
}

shared_ptr<NamedContainer> StoryManager::createNamedContainer(SectionNode* section, NamedContainer* parent)
{
	for (shared_ptr<NamedContainer> cont : parent->children)
	{
		if (cont->name == section->name->text && cont->isSection)
		{
			SectionNode* existingSection = static_cast<SectionNode*>(cont->nodes[0]);
			StoryNode* newStory = static_cast<StoryNode*>(_nodeRegister.getAncestorsWithType(section, NodeType::Story)[0]);
			StoryNode* existingStory = static_cast<StoryNode*>(_nodeRegister.getAncestorsWithType(existingSection, NodeType::Story)[0]);
			
			throw SectionAlreadyExistsException(Localization::sectionAlreadyExists(
					section->name->text, newStory->filepath, section->startLine, section->startColumn,
					existingStory->filepath, existingSection->startLine, existingSection->startColumn
				),
				*newStory, *section, *existingStory, *existingSection
			);
		}
	}
	
	shared_ptr<NamedContainer> container = std::make_shared<NamedContainer>(section, parent);
	generateNamedContainerIds(container.get());
	parent->children.push_back(container);
	_namedContainers[section] = container;
	
	return container;
}

shared_ptr<NamedContainer> StoryManager::createNamedContainer(SequencePrimaryNode* sequence, const string& name, NamedContainer* parent)
{
	if (parent->findChildNamed(addNameGetId(name)) != nullptr)
	{
		auto stories = _nodeRegister.getAncestorsWith(sequence, [](Node* other) {
			return other->nodeType() == NodeType::Story;
		});
		auto story = static_cast<StoryNode*>(stories[0]);
		
		throw CompilationException(story->filepath, Localization::sequenceNameAlreadyInUse(name), *sequence);
	}
	
	shared_ptr<NamedContainer> container = std::make_shared<NamedContainer>(sequence, name, parent);
	generateNamedContainerIds(container.get());
	parent->children.push_back(container);
	_namedContainers[sequence] = container;
	
	return container;
}

void StoryManager::generateNamedContainerIds(NamedContainer* container)
{
	int64_t id = -1;
	const string& name = container->fullyQualifiedName;
	auto search = _namedContainerNames.find(name);
	if (search != _namedContainerNames.end())
	{
		id = search->second;
	}
	else
	{
		id = crc(name.begin(), name.end()) & 0xffff;
		while (_existingNameContainerIds.find(id) != _existingNameContainerIds.end())
		{
			++id;
		}
		
		_namedContainerNames.insert(search, std::make_pair(name, id));
		_existingNameContainerIds.insert(id);
	}
	
	container->id = id;
	container->nameId = addNameGetId(container->name);
}

shared_ptr<NamedContainer>& StoryManager::getNamedContainer(NamespaceNode* ns)
{
	return _namedContainers[ns];
}

shared_ptr<NamedContainer>& StoryManager::getNamedContainer(SectionNode* section)
{
	return _namedContainers[section];
}

shared_ptr<NamedContainer>& StoryManager::getNamedContainer(SequencePrimaryNode* sequence)
{
	return _namedContainers[sequence];
}

int64_t StoryManager::addStringGetId(const string& str)
{
	auto search = _strings.find(str);
	if (search == _strings.end())
	{
		int64_t id = _strings.size();
		_strings.insert(search, std::make_pair(str, id));
		return id;
	}
	
	return search->second;
}

int64_t StoryManager::addNameGetId(const string& str)
{
	auto search = _names.find(str);
	if (search == _names.end())
	{
		int64_t id = crc(str.begin(), str.end()) & 0xffff;
		while (_existingNameIds.find(id) != _existingNameIds.end())
		{
			++id;
		}
		_names.insert(search, std::make_pair(str, id));
		_existingNameIds.insert(id);
		return id;
	}
	
	return search->second;
}

void StoryManager::addNodeExecutionEntry(Node* node, shared_ptr<ExecutionEntry> entry)
{
	_nodeToExecutionEntry[node->uniqueId] = entry;
}

shared_ptr<ExecutionEntry> StoryManager::getNodeExecutionEntry(Node* node)
{
	auto search = _nodeToExecutionEntry.find(node->uniqueId);
	return search == _nodeToExecutionEntry.end() ? nullptr : search->second;
}

}
