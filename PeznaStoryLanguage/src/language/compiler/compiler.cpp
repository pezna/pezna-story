#include "compiler.hpp"

#include <psvm/instruction.hpp>

#include <fstream>

#include "instruction_container.hpp"
#include "value.hpp"
#include "operators.hpp"
#include "../exceptions.hpp"
#include "../localization.hpp"
#include "../utils/byte_writer_utils.hpp"
#include "../utils/statics.hpp"
#include "../utils/string_utils.hpp"

namespace PSL
{
char NOOP_OP = PSVM::StoryOpcode::NOOP_OP;
char EQUALS_OP = PSVM::StoryOpcode::EQUALS_OP;
char NOT_EQUALS_OP = PSVM::StoryOpcode::NOT_EQUALS_OP;
char GREATER_THAN_EQUALS_OP = PSVM::StoryOpcode::GREATER_THAN_EQUALS_OP;
char LESS_THAN_EQUALS_OP = PSVM::StoryOpcode::LESS_THAN_EQUALS_OP;
char GREATER_THAN_OP = PSVM::StoryOpcode::GREATER_THAN_OP;
char LESS_THAN_OP = PSVM::StoryOpcode::LESS_THAN_OP;
char NOT_OP = PSVM::StoryOpcode::NOT_OP;
char IN_OP = PSVM::StoryOpcode::IN_OP;
char NOT_IN_OP = PSVM::StoryOpcode::NOT_IN_OP;
char IS_OP = PSVM::StoryOpcode::IS_OP;
char IS_NOT_OP = PSVM::StoryOpcode::IS_NOT_OP;
char OR_OP = PSVM::StoryOpcode::OR_OP;
char POWER_OP = PSVM::StoryOpcode::POWER_OP;
char ADDITION_OP = PSVM::StoryOpcode::ADDITION_OP;
char SUBTRACT_OP = PSVM::StoryOpcode::SUBTRACT_OP;
char NEGATE_OP = PSVM::StoryOpcode::NEGATE_OP;
char MULTIPLY_OP = PSVM::StoryOpcode::MULTIPLY_OP;
char DIVIDE_OP = PSVM::StoryOpcode::DIVIDE_OP;
char MODULUS_OP = PSVM::StoryOpcode::MODULUS_OP;
char BIT_AND_OP = PSVM::StoryOpcode::BIT_AND_OP;
char BIT_XOR_OP = PSVM::StoryOpcode::BIT_XOR_OP;
char BIT_OR_OP = PSVM::StoryOpcode::BIT_OR_OP;
char BIT_NOT_OP = PSVM::StoryOpcode::BIT_NOT_OP;
char BIT_LEFT_SHIFT_OP = PSVM::StoryOpcode::BIT_LEFT_SHIFT_OP;
char BIT_RIGHT_SHIFT_OP = PSVM::StoryOpcode::BIT_RIGHT_SHIFT_OP;
char INT_CONST_OP = PSVM::StoryOpcode::INT_CONST_OP;
char FLOAT_CONST_OP = PSVM::StoryOpcode::FLOAT_CONST_OP;
char BOOL_CONST_OP = PSVM::StoryOpcode::BOOL_CONST_OP;
char STRING_CONST_OP = PSVM::StoryOpcode::STRING_CONST_OP;
char BUILD_LIST_OP = PSVM::StoryOpcode::BUILD_LIST_OP;
char BUILD_LIST_WITH_DEFAULT_OP = PSVM::StoryOpcode::BUILD_LIST_WITH_DEFAULT_OP;
char BUILD_DICTIONARY_OP = PSVM::StoryOpcode::BUILD_DICTIONARY_OP;
char BUILD_DICTIONARY_WITH_DEFAULT_OP = PSVM::StoryOpcode::BUILD_DICTIONARY_WITH_DEFAULT_OP;
char BUILD_CHARACTER_OP = PSVM::StoryOpcode::BUILD_CHARACTER_OP;
char GET_SEQUENCE_INDEX_OP = PSVM::StoryOpcode::GET_SEQUENCE_INDEX_OP;
char DEFAULT_CONST_OP = PSVM::StoryOpcode::DEFAULT_CONST_OP;
char LINE_OP = PSVM::StoryOpcode::LINE_OP;
char CHOICES_OP = PSVM::StoryOpcode::CHOICES_OP;
char GOTO_OP = PSVM::StoryOpcode::GOTO_OP;
char JUMP_FORWARD_OP = PSVM::StoryOpcode::JUMP_FORWARD_OP;
char POP_JUMP_IF_TRUE_OP = PSVM::StoryOpcode::POP_JUMP_IF_TRUE_OP;
char POP_JUMP_IF_FALSE_OP = PSVM::StoryOpcode::POP_JUMP_IF_FALSE_OP;
char JUMP_IF_TRUE_OR_POP_OP = PSVM::StoryOpcode::JUMP_IF_TRUE_OR_POP_OP;
char JUMP_IF_FALSE_OR_POP_OP = PSVM::StoryOpcode::JUMP_IF_FALSE_OR_POP_OP;
char POP_OP = PSVM::StoryOpcode::POP_OP;
char DUPLICATE_OP = PSVM::StoryOpcode::DUPLICATE_OP;
char CREATE_VARIABLE_OP = PSVM::StoryOpcode::CREATE_VARIABLE_OP;
char LOAD_VARIABLE_OP = PSVM::StoryOpcode::LOAD_VARIABLE_OP;
char STORE_OP = PSVM::StoryOpcode::STORE_OP;
char GET_ATTRIBUTE_OP = PSVM::StoryOpcode::GET_ATTRIBUTE_OP;
char CALL_OP = PSVM::StoryOpcode::CALL_OP;
char SLICE_OP = PSVM::StoryOpcode::SLICE_OP;
char SUBSCRIPT_OP = PSVM::StoryOpcode::SUBSCRIPT_OP;
char FOR_ITERABLE_OP = PSVM::StoryOpcode::FOR_ITERABLE_OP;
char EVENT_OP = PSVM::StoryOpcode::EVENT_OP;
char RETURN_OP = PSVM::StoryOpcode::RETURN_OP;
char RETURN_VALUE_OP = PSVM::StoryOpcode::RETURN_VALUE_OP;
char TERMINATE_OP = PSVM::StoryOpcode::TERMINATE_OP;
char BEGIN_METADATA_OP = PSVM::StoryOpcode::BEGIN_METADATA_OP;
char END_METADATA_OP = PSVM::StoryOpcode::END_METADATA_OP;
char BEGIN_NAMES_OP = PSVM::StoryOpcode::BEGIN_NAMES_OP;
char END_NAMES_OP = PSVM::StoryOpcode::END_NAMES_OP;
char BEGIN_STRINGS_OP = PSVM::StoryOpcode::BEGIN_STRINGS_OP;
char END_STRINGS_OP = PSVM::StoryOpcode::END_STRINGS_OP;
char BEGIN_CONTAINERS_OP = PSVM::StoryOpcode::BEGIN_CONTAINERS_OP;
char END_CONTAINERS_OP = PSVM::StoryOpcode::END_CONTAINERS_OP;
char BEGIN_VARIABLES_OP = PSVM::StoryOpcode::BEGIN_VARIABLES_OP;
char END_VARIABLES_OP = PSVM::StoryOpcode::END_VARIABLES_OP;
char BEGIN_LABELS_OP = PSVM::StoryOpcode::BEGIN_LABELS_OP;
char END_LABELS_OP = PSVM::StoryOpcode::END_LABELS_OP;
char BEGIN_BLOCK_OP = PSVM::StoryOpcode::BEGIN_BLOCK_OP;
char END_BLOCK_OP = PSVM::StoryOpcode::END_BLOCK_OP;

Compiler::Compiler(shared_ptr<StoryNode> startingStory)
:
	_storyManager(StoryManager(startingStory))
{}

void Compiler::compile()
{
	NamedContainer* mainNamedContainer = _storyManager.mainNamedContainer().get();
	// compile the code into its respective containers before handling metadata
	visitNamedContainer(mainNamedContainer, nullptr);

	_namesMetadata.addInstruction({ BEGIN_NAMES_OP });
	_stringsMetadata.addInstruction({ BEGIN_STRINGS_OP });
	_containersMetadata.addInstruction({ BEGIN_CONTAINERS_OP });
	
	int64_t numNamedContainers = mainNamedContainer->getNumDescendants() + 1;
	vector<int8_t> vecNumContainers;
	Utils::writeVarInt(numNamedContainers, vecNumContainers);
	_containersMetadata.addInstruction(vecNumContainers);
	
	createMetadata(mainNamedContainer);
	createAllNameMetadata();
	createAllStringMetadata();

	_namesMetadata.addInstruction({ END_NAMES_OP });
	_stringsMetadata.addInstruction({ END_STRINGS_OP });
	_containersMetadata.addInstruction({ END_CONTAINERS_OP });
	
	_metadataContainer.addInstruction({ BEGIN_METADATA_OP });
	_metadataContainer.addInstructions(_namesMetadata);
	_metadataContainer.addInstructions(_stringsMetadata);
	_metadataContainer.addInstructions(_containersMetadata);
	_metadataContainer.addInstruction({ END_METADATA_OP });
}

InstructionContainer& Compiler::firstMetadata()
{
	return _metadataContainer;
}

void Compiler::output(std::ostream& os)
{
	InstructionContainer* cont = firstMetadata().firstSibling();
	do
	{
		cont->finalize();
		cont->output(os);

		cont = cont->nextSibling;
	}
	while (cont != nullptr);
}

void Compiler::createMetadata(NamedContainer* namedContainer)
{
	if (namedContainer->name.length() > MAX_NAME_LENGTH)
	{
		error(
			Localization::maxIdLength(namedContainer->name.length(), MAX_NAME_LENGTH),
			namedContainer->nodes.front()
		);
	}

	int64_t nameIndex = _storyManager.addNameGetId(namedContainer->name);

	vector<int8_t> bytes;
	Utils::writeVarInt(namedContainer->id, bytes);
	Utils::writeVarInt(nameIndex, bytes);
	Utils::writeVarInt(namedContainer->parent != nullptr ? namedContainer->parent->id : 0, bytes);
	Utils::writeVarInt(namedContainer->isNamespace, bytes);
	Utils::writeVarInt((int64_t)namedContainer->parameterTypes.size(), bytes);
	Utils::writeVarInt(namedContainer->bytecodeOffset, bytes);
	Utils::writeVarInt(namedContainer->bytecodeLength, bytes);

	_containersMetadata.addInstruction(InstructionData(bytes));
	
	_containersMetadata.addInstruction({ BEGIN_VARIABLES_OP });
	createVariableMetadataForContainer(namedContainer, _containersMetadata);
	_containersMetadata.addInstruction({ END_VARIABLES_OP });
	_containersMetadata.addInstruction({ BEGIN_LABELS_OP });
	createLabelMetadataForContainer(namedContainer, _containersMetadata);
	_containersMetadata.addInstruction({ END_LABELS_OP });
	
	for (shared_ptr<NamedContainer>& child : namedContainer->children)
	{
		createMetadata(child.get());
	}
}

void Compiler::createAllNameMetadata()
{
	vector<int8_t> bytes;
	// write number of names to bytecode
	Utils::writeVarInt(_storyManager.names().size(), bytes);
	
	for (auto& pair : _storyManager.names())
	{
		const string& text = pair.first;
		// write name index
		Utils::writeVarInt(pair.second, bytes);
		// write name length
		Utils::writeVarInt(text.length(), bytes);
		// write all name text characters
		for (char c : text)
		{
			bytes.push_back(c);
		}
	}
	
	_namesMetadata.addInstruction(InstructionData(bytes));
}

/**
 * This must be called after the rest of the program has compiled
 */
void Compiler::createAllStringMetadata()
{
	vector<int8_t> bytes;
	Utils::writeVarInt(_storyManager.strings().size(), bytes);
	
	for (auto& pair : _storyManager.strings())
	{
		// write string index
		Utils::writeVarInt(pair.second, bytes);
		// write string length
		Utils::writeVarInt(pair.first.length(), bytes);
		// write all string characters
		for (char c : pair.first)
		{
			bytes.push_back(c);
		}
	}
	
	_stringsMetadata.addInstruction(InstructionData(bytes));
}

void Compiler::createLabelMetadataForContainer(NamedContainer* namedContainer, InstructionContainer& ic)
{
	vector<int8_t> bytes;
	Utils::writeVarInt(namedContainer->labels.size(), bytes);

	for (auto& pair : namedContainer->labels)
	{
		Utils::writeVarInt(pair.first, bytes); // name index
		Utils::writeVarInt(pair.second, bytes); // offset
	}

	ic.addInstruction(InstructionData(bytes));
}

void Compiler::createVariableMetadataForContainer(NamedContainer* namedContainer, InstructionContainer& ic)
{
	vector<int8_t> bytes;
	int64_t totalVariables = 0;
	
	auto compileVariable = [this, &bytes, &totalVariables]
		(const string& name, bool isFinal, bool isParameter)
		{
			int64_t nameIndex = this->_storyManager.addNameGetId(name);
			// add variable to metadata bytes
			Utils::writeVarInt(nameIndex, bytes);
			bytes.push_back(isFinal);
			bytes.push_back(isParameter);
			
			++totalVariables;
		};
	
	if (namedContainer->isSection)
	{
		// set parameters as variables
		// there is only ever one section in a named container's nodes
		SectionNode* section = static_cast<SectionNode*>(namedContainer->nodes[0]);
		for (shared_ptr<ParameterNode>& param : section->parameters)
		{
			compileVariable(param->name->text, param->final, true);
		}
	}

	for (Node* node : namedContainer->nodes)
	{
		NodeRegister& nr = _storyManager.nodeRegister();
		vector<Node*> assignments = nr.getChildrenWithType(node, StatementType::AssignmentStatement);
		for (Node* n : assignments)
		{
			AssignmentStatementNode* assign = static_cast<AssignmentStatementNode*>(n);
			if (!assign->create)
			{
				continue;
			}

			AtomPrimaryNode* left = static_cast<AtomPrimaryNode*>(assign->left.get());
			compileVariable(left->token->text, assign->final, false);
		}
	}
	
	vector<int8_t> vecTotalVars;
	Utils::writeVarInt(totalVariables, vecTotalVars);
	
	ic.addInstruction(InstructionData(vecTotalVars));
	ic.addInstruction(InstructionData(bytes));
}

int64_t Compiler::addLabel(const string& label, Node* node)
{
	NamedContainer* nc = getCurrentNamedContainer();
	InstructionContainer* ic = getCurrentInstructionContainer();

	int64_t nameIndex = _storyManager.addNameGetId(label);
	if (nc->hasLabel(nameIndex))
	{
		error(Localization::labelAlreadyExistsInSection(label, nc->name), node);
	}

	// create an empty InstructionData that will be later run to set
	// the offset for the label, so that goto statements work
	ic->addInstruction(InstructionData([nc, nameIndex](int64_t offset)
			{
				nc->addLabel(nameIndex, offset);
				return vector<int8_t>();
			}, 0));

	nc->addLabel(nameIndex, 0);
	return nameIndex;
}

InstructionContainer* Compiler::getCurrentInstructionContainer()
{
	return _currentInstructionContainers.back();
}

NamedContainer* Compiler::getCurrentNamedContainer()
{
	return _currentNamedContainers.back();
}

void Compiler::visitNode(Node* node)
{

}

void Compiler::visitStory(StoryNode* node)
{

}

void Compiler::visitNamedContainer(NamedContainer* namedContainer, NamedContainer* parent)
{
	// add the named container's instruction container to the linked list of instruction containers
	if (parent == nullptr)
	{
		_metadataContainer.lastSibling()->insertAfter(&namedContainer->instructionContainer);
	}
	else
	{
		parent->instructionContainer.lastSibling()->insertAfter(&namedContainer->instructionContainer);
	}

	namedContainer->bytecodeOffset = namedContainer->instructionContainer.index();
	vector<int8_t> bytes;
	bytes.push_back(BEGIN_BLOCK_OP);
	Utils::writeVarInt(namedContainer->id, bytes);
	namedContainer->instructionContainer.addInstruction(InstructionData(bytes));

	for (Node* node : namedContainer->nodes)
	{
		if (namedContainer->isNamespace)
		{
			visitNamespace(static_cast<NamespaceNode*>(node));
		}
		else if (namedContainer->isSection)
		{
			visitSection(static_cast<SectionNode*>(node));
		}
		else if (namedContainer->isSequence)
		{
			finalizeSequencePrimary(static_cast<SequencePrimaryNode*>(node));
		}
	}

	namedContainer->instructionContainer.addInstruction({ END_BLOCK_OP });
	namedContainer->bytecodeLength = namedContainer->instructionContainer.size();
	// finalize so that labels can set their offset
	namedContainer->instructionContainer.finalize();

	// compile all its children
	for (shared_ptr<NamedContainer>& child : namedContainer->children)
	{
		visitNamedContainer(child.get(), namedContainer);
	}
}

void Compiler::visitNamespace(NamespaceNode* node)
{
	shared_ptr<NamedContainer>& nc = _storyManager.getNamedContainer(node);
	_currentNamedContainers.push_back(nc.get());
	_currentInstructionContainers.push_back(&nc->instructionContainer);

	for (shared_ptr<StatementNode>& stmt : node->statements)
	{
		// only assignments are allowed in a namespace's bytecode
		if (stmt->statementType() != StatementType::AssignmentStatement)
		{
			continue;
		}

		visitStatement(stmt.get());
	}

	_currentInstructionContainers.pop_back();
	_currentNamedContainers.pop_back();
}

void Compiler::visitSection(SectionNode* node)
{
	shared_ptr<NamedContainer>& nc = _storyManager.getNamedContainer(node);
	_currentNamedContainers.push_back(nc.get());
	_currentInstructionContainers.push_back(&nc->instructionContainer);
	
	for (shared_ptr<ParameterNode>& param : node->parameters)
	{
		visitParameter(param.get());
	}
	
	for (shared_ptr<StatementNode>& stmt : node->statements)
	{
		if (stmt->statementType() != StatementType::IncludeStatement)
		{
			visitStatement(stmt.get());
		}
	}

	_currentInstructionContainers.pop_back();
	_currentNamedContainers.pop_back();
}

void Compiler::visitChild(ChildNode* node)
{
	throw std::runtime_error("Compiler visitChild() should never be called. Every node that contains a child should call the respective child function directly.");
}

void Compiler::visitParameter(ParameterNode* node)
{
	NamedContainer* nc = getCurrentNamedContainer();
	InstructionContainer* ic = getCurrentInstructionContainer();
	
	ValueType valueType = ValueType::createFromTokenType(node->variableType);
	auto nameId = _storyManager.addNameGetId(node->name->text);
	nc->addVariable(nameId, node, valueType);
	Variable* var = nc->getVariable(nameId);
	
	vector<int8_t> bytes{ LOAD_VARIABLE_OP };
	Utils::writeVarInt(nc->id, bytes);
	Utils::writeVarInt(var->nameId, bytes);
}

void Compiler::visitStatement(StatementNode* node)
{
	switch (node->statementType())
	{
		case StatementType::AssignmentStatement:
			visitAssignmentStatement(static_cast<AssignmentStatementNode*>(node));
			break;
		case StatementType::ChoiceStatement:
			visitChoiceStatement(static_cast<ChoiceStatementNode*>(node));
			break;
		case StatementType::ContinueStatement:
			visitContinueStatement(static_cast<ContinueStatementNode*>(node));
			break;
		case StatementType::EventStatement:
			visitEventStatement(static_cast<EventStatementNode*>(node));
			break;
		case StatementType::ExitStatement:
			visitExitStatement(static_cast<ExitStatementNode*>(node));
			break;
		case StatementType::ExpressionStatement:
			visitExpression(static_cast<ExpressionNode*>(node));
			break;
		case StatementType::ForStatement:
			visitForStatement(static_cast<ForStatementNode*>(node));
			break;
		case StatementType::GotoStatement:
			visitGotoStatement(static_cast<GotoStatementNode*>(node));
			break;
		case StatementType::IfStatement:
			visitIfStatement(static_cast<IfStatementNode*>(node));
			break;
		case StatementType::IncludeStatement:
			break;
		case StatementType::LabelStatement:
			visitLabelStatement(static_cast<LabelStatementNode*>(node));
			break;
		case StatementType::LineStatement:
			visitLineStatement(static_cast<LineStatementNode*>(node));
			break;
		case StatementType::DoStatement:
			visitDoStatement(static_cast<DoStatementNode*>(node));
			break;
		case StatementType::ReturnStatement:
			visitReturnStatement(static_cast<ReturnStatementNode*>(node));
			break;
		case StatementType::WhileStatement:
			visitWhileStatement(static_cast<WhileStatementNode*>(node));
			break;
	}
}

void Compiler::visitAssignmentStatement(AssignmentStatementNode* node)
{
	InstructionContainer* ic = getCurrentInstructionContainer();
	
	if (node->create)
	{
		AtomPrimaryNode* atom = dynamic_cast<AtomPrimaryNode*>(node->left.get());
		if (atom == nullptr)
		{
			error(Localization::variableCreateMustBeName(), node->left.get());
		}
		
		vector<int8_t> bytes;
		bytes.push_back(CREATE_VARIABLE_OP); // create should also load to stack
		int64_t nameId = _storyManager.addNameGetId(atom->token->text);
		Utils::writeVarInt(nameId, bytes);
		ic->addInstruction(bytes);
		
		// add variable to container
		auto nc = getCurrentNamedContainer();
		ValueType valueType(ValueType::createFromTokenType(node->variableType));
		if (nc->getVariable(nameId) != nullptr)
		{
			error(Localization::variableAlreadyExists(atom->token->text), atom);
		}
		Variable* variable = nc->addVariable(nameId, node, Value(valueType));
		_storyManager.addNodeExecutionEntry(atom, std::make_shared<VariableExecutionEntry>(variable));
	}
	else
	{
		// left expression should result in a variable address being pushed to the stack
		visitExpression(node->left.get());
	}

	if (node->op->type != TokenType::Assign)
	{
		// anything not an immediate assign needs the left expression to be duplicated
		ic->addInstruction({ DUPLICATE_OP });
	}

	visitExpression(node->right.get());
	
	ensureSameValueTypes(node->left.get(), node->right.get(), &Localization::expectedValueType);
	
	switch (node->op->type)
	{
		case TokenType::PowAssign:
			ic->addInstruction({ POWER_OP });
			break;
		case TokenType::AddAssign:
			ic->addInstruction({ ADDITION_OP });
			break;
		case TokenType::SubAssign:
			ic->addInstruction({ SUBTRACT_OP });
			break;
		case TokenType::MulAssign:
			ic->addInstruction({ MULTIPLY_OP });
			break;
		case TokenType::DivAssign:
			ic->addInstruction({ DIVIDE_OP });
			break;
		case TokenType::ModAssign:
			ic->addInstruction({ MODULUS_OP });
			break;
		case TokenType::BitAndAssign:
			ic->addInstruction({ BIT_AND_OP });
			break;
		case TokenType::BitXorAssign:
			ic->addInstruction({ BIT_XOR_OP });
			break;
		case TokenType::BitOrAssign:
			ic->addInstruction({ BIT_OR_OP });
			break;
		case TokenType::BitLeftShiftAssign:
			ic->addInstruction({ BIT_LEFT_SHIFT_OP });
			break;
		case TokenType::BitRightShiftAssign:
			ic->addInstruction({ BIT_RIGHT_SHIFT_OP });
			break;
	}

	ic->addInstruction({ STORE_OP });
}

/**
 * This will only compile the statements inside the choice, not the expressions for the choice.
 * Those are compiled in visitLineStatement
 */
void Compiler::visitChoiceStatement(ChoiceStatementNode* node)
{
	string labelAfter = "0la-a" + std::to_string(node->uniqueId);
	// add the label text to names and the generated index will be the same one
	// used when addLabel() is called below
	int64_t labelNameIndexAfter = _storyManager.addNameGetId(labelAfter);

	_currentBlockStructures.push_back(BlockStructure(BlockStructureType::Choice, 0, labelNameIndexAfter));

	for (shared_ptr<StatementNode>& stmt : node->statements)
	{
		visitStatement(stmt.get());
	}

	addLabel(labelAfter, node);
	_currentBlockStructures.pop_back();
}

void Compiler::visitContinueStatement(ContinueStatementNode* node)
{
	InstructionContainer* ic = getCurrentInstructionContainer();
	BlockStructure blockStructure;
	bool foundLoop = false;

	if (node->level == nullptr)
	{
		for (int i = _currentBlockStructures.size() - 1; i >= 0; --i)
		{
			blockStructure = _currentBlockStructures[i];
			if (blockStructure.isLoop())
			{
				foundLoop = true;
				break;
			}
		}
	}
	else
	{
		for (int i = _currentBlockStructures.size() - 1; i >= 0; --i)
		{
			blockStructure = _currentBlockStructures[i];
			if (node->level->type == TokenType::Do && blockStructure.type == BlockStructureType::Do)
			{
				foundLoop = true;
				break;
			}
			else if (node->level->type == TokenType::While && blockStructure.type == BlockStructureType::While)
			{
				foundLoop = true;
				break;
			}
			else if (node->level->type == TokenType::For && blockStructure.type == BlockStructureType::For)
			{
				foundLoop = true;
				break;
			}
		}
	}

	if (!foundLoop)
	{
		error(Localization::noLoopFoundToContinue(), node);
	}

	if (!blockStructure.hasLabelBefore)
	{
		throw std::runtime_error("Block structure should have a label before to continue. This should never happen.");
	}

	vector<int8_t> bytes;
	bytes.push_back(GOTO_OP);
	Utils::writeVarInt(blockStructure.labelNameIndexBefore, bytes);

	ic->addInstruction(InstructionData(bytes));
}

void Compiler::visitDoStatement(DoStatementNode* node)
{
	InstructionContainer* ic = getCurrentInstructionContainer();
	string labelBefore = "0la-b" + std::to_string(node->uniqueId);
	string labelAfter = "0la-a" + std::to_string(node->uniqueId);
	// add the label text to names and the generated index will be the same one
	// used when addLabel() is called below
	int64_t labelNameIndexBefore = _storyManager.addNameGetId(labelBefore);
	int64_t labelNameIndexAfter = _storyManager.addNameGetId(labelAfter);

	_currentBlockStructures.push_back(BlockStructure(BlockStructureType::Do, labelNameIndexBefore, labelNameIndexAfter));
	addLabel(labelBefore, node);

	for (shared_ptr<StatementNode>& stmt : node->statements)
	{
		visitStatement(stmt.get());
	}

	visitExpression(node->endCondition.get());

	vector<int8_t> gotoBytes;
	gotoBytes.push_back(GOTO_OP);
	Utils::writeVarInt(labelNameIndexBefore, gotoBytes);

	vector<int8_t> bytes;
	bytes.push_back(POP_JUMP_IF_TRUE_OP);
	Utils::writeVarInt(gotoBytes.size(), bytes);
	bytes.insert(bytes.end(), gotoBytes.begin(), gotoBytes.end());

	ic->addInstruction(InstructionData(bytes));

	addLabel(labelAfter, node);
	_currentBlockStructures.pop_back();
}

void Compiler::visitExitStatement(ExitStatementNode* node)
{
	InstructionContainer* ic = getCurrentInstructionContainer();
	BlockStructure blockStructure;
	bool foundBlock = false;

	for (int i = _currentBlockStructures.size() - 1; i >= 0; --i)
	{
		blockStructure = _currentBlockStructures[i];

		if (node->level->type == TokenType::Story)
		{
			ic->addInstruction({ TERMINATE_OP });
			return;
		}
		else if (node->level->type == TokenType::Section)
		{
			ic->addInstruction({ RETURN_OP });
			return;
		}
		else if (node->level->type == TokenType::Choice && blockStructure.type == BlockStructureType::Choice)
		{
			foundBlock = true;
			break;
		}
		else if (node->level->type == TokenType::If && blockStructure.type == BlockStructureType::If)
		{
			foundBlock = true;
			break;
		}
		else if (node->level->type == TokenType::Do && blockStructure.type == BlockStructureType::Do)
		{
			foundBlock = true;
			break;
		}
		else if (node->level->type == TokenType::While && blockStructure.type == BlockStructureType::While)
		{
			foundBlock = true;
			break;
		}
		else if (node->level->type == TokenType::For && blockStructure.type == BlockStructureType::For)
		{
			foundBlock = true;
			break;
		}
	}

	if (!foundBlock)
	{
		error(Localization::noBlockFoundToExit(), node);
	}

	if (!blockStructure.hasLabelAfter)
	{
		throw std::runtime_error("Block structure should have a label after to exit. This should never happen.");
	}

	vector<int8_t> bytes;
	bytes.push_back(GOTO_OP);
	Utils::writeVarInt(blockStructure.labelNameIndexAfter, bytes);

	ic->addInstruction(InstructionData(bytes));
}

void Compiler::visitForStatement(ForStatementNode* node)
{
	InstructionContainer* ic = getCurrentInstructionContainer();

	string labelBefore = "0la-b" + std::to_string(node->uniqueId);
	string labelAfter = "0la-a" + std::to_string(node->uniqueId);
	// add the label text to names and the generated index will be the same one
	// used when addLabel() is called below
	int64_t labelNameIndexBefore = _storyManager.addNameGetId(labelBefore);
	int64_t labelNameIndexAfter = _storyManager.addNameGetId(labelAfter);

	_currentBlockStructures.push_back(BlockStructure(BlockStructureType::For, labelNameIndexBefore, labelNameIndexAfter));

	visitExpression(node->expression.get()); // execute initial expression and place the value on stack

	addLabel(labelBefore, node);

	vector<int8_t> bytes;
	int64_t varNameIndex = _storyManager.addNameGetId(node->variable->text);
	// ensure second from stack is turned into an iterator, and set the variable on the top of the stack to the next value
	bytes.push_back(FOR_ITERABLE_OP);
	Utils::writeVarInt(varNameIndex, bytes); // variable that will be set is placed on stack
	Utils::writeVarInt(labelNameIndexAfter, bytes); // the label that will be jumped to if the iterator is finished

	ic->addInstruction(InstructionData(bytes));
	bytes.clear();

	for (shared_ptr<StatementNode>& stmt : node->statements)
	{
		visitStatement(stmt.get());
	}

	bytes.push_back(GOTO_OP);
	Utils::writeVarInt(labelNameIndexBefore, bytes);

	addLabel(labelAfter, node);
	_currentBlockStructures.pop_back();
}

void Compiler::visitGotoStatement(GotoStatementNode* node)
{
	InstructionContainer* ic = getCurrentInstructionContainer();

	auto nameId = _storyManager.addNameGetId(node->token->text);
	
	vector<int8_t> bytes{ GOTO_OP };
	Utils::writeVarInt(nameId, bytes);
	ic->addInstruction(bytes);
}

void Compiler::visitIfStatement(IfStatementNode* node)
{
	string labelAfter = "0la-a" + std::to_string(node->uniqueId);
	// add the label text to names and the generated index will be the same one
	// used when addLabel() is called below
	int64_t labelNameIndexAfter = _storyManager.addNameGetId(labelAfter);

	_currentBlockStructures.push_back(BlockStructure(BlockStructureType::If, 0, labelNameIndexAfter));

	visitIfPart(node->ifPart.get());

	for (shared_ptr<ElseIfPartNode>& elseIf : node->elseIfParts)
	{
		visitElseIfPart(elseIf.get());
	}

	if (node->elsePart != nullptr)
	{
		visitElsePart(node->elsePart.get());
	}

	addLabel(labelAfter, node);
	_currentBlockStructures.pop_back();
}

void Compiler::visitIfPart(IfPartNode* node)
{
	InstructionContainer* ic = getCurrentInstructionContainer();
	BlockStructure& blockStructure = _currentBlockStructures.back();

	if (blockStructure.type != BlockStructureType::If)
	{
		throw std::runtime_error("Expected block structure type to be If. This should never happen.");
	}

	visitExpression(node->condition.get());

	InstructionContainer subContainer;
	_currentInstructionContainers.push_back(&subContainer);
	for (shared_ptr<StatementNode>& stmt : node->statements)
	{
		visitStatement(stmt.get());
	}
	_currentInstructionContainers.pop_back();

	vector<int8_t> bytes;
	bytes.push_back(POP_JUMP_IF_FALSE_OP);
	Utils::writeVarInt(subContainer.size(), bytes);

	ic->addInstruction(InstructionData(bytes));
	ic->addInstructions(subContainer);
}

void Compiler::visitElseIfPart(ElseIfPartNode* node)
{
	InstructionContainer* ic = getCurrentInstructionContainer();
	BlockStructure& blockStructure = _currentBlockStructures.back();

	if (blockStructure.type != BlockStructureType::If)
	{
		throw std::runtime_error("Expected block structure type to be If. This should never happen.");
	}

	visitExpression(node->condition.get());

	InstructionContainer subContainer;
	_currentInstructionContainers.push_back(&subContainer);
	for (shared_ptr<StatementNode>& stmt : node->statements)
	{
		visitStatement(stmt.get());
	}
	_currentInstructionContainers.pop_back();

	vector<int8_t> gotoBytes;
	gotoBytes.push_back(GOTO_OP);
	Utils::writeVarInt(blockStructure.labelNameIndexAfter, gotoBytes);

	vector<int8_t> bytes;
	bytes.push_back(POP_JUMP_IF_FALSE_OP);
	Utils::writeVarInt(subContainer.size() + gotoBytes.size(), bytes);

	ic->addInstruction(InstructionData(bytes));
	ic->addInstructions(subContainer);
	ic->addInstruction(InstructionData(gotoBytes));
}

void Compiler::visitElsePart(ElsePartNode* node)
{
	for (shared_ptr<StatementNode>& stmt : node->statements)
	{
		visitStatement(stmt.get());
	}
}

void Compiler::visitIncludeStatement(IncludeStatementNode* node)
{

}

void Compiler::visitLabelStatement(LabelStatementNode* node)
{
	addLabel(node->label->text, node);
}

void Compiler::visitLineStatement(LineStatementNode* node)
{
	InstructionContainer* ic = getCurrentInstructionContainer();

	visitExpression(node->character.get());
	visitExpression(node->text.get());

	// first get the number of expressions that will be displayed with the line
	int numChoiceExpressions = 0;
	for (shared_ptr<ChoiceStatementNode>& choice : node->choices)
	{
		for (shared_ptr<ExpressionNode>& expr : choice->expressions)
		{
			visitExpression(expr.get());
			++numChoiceExpressions;
		}
	}

	vector<int8_t> bytes;
	if (numChoiceExpressions > 0)
	{
		bytes.push_back(CHOICES_OP);
		Utils::writeVarInt(numChoiceExpressions, bytes);
	}
	bytes.push_back(LINE_OP);
	ic->addInstruction(InstructionData(bytes));
	bytes.clear();

	// if choices exist, after a choice is chosen,
	// the index of that chosen choice will now be on the top of the stack

	int previousNumChoiceExpressions = 0;
	// then put the individual choice statements after the line
	for (shared_ptr<ChoiceStatementNode>& choice : node->choices)
	{
		bytes.insert(bytes.end(), choice->expressions.size(), DUPLICATE_OP);

		for (int64_t i = 0; i < choice->expressions.size(); ++i)
		{
			bytes.push_back(INT_CONST_OP);
			Utils::writeVarInt(previousNumChoiceExpressions++, bytes);
			bytes.push_back(EQUALS_OP);
		}

		bytes.insert(bytes.end(), choice->expressions.size() - 1, OR_OP);

		InstructionContainer choiceContainer;
		_currentInstructionContainers.push_back(&choiceContainer);
		visitChoiceStatement(choice.get());
		_currentInstructionContainers.pop_back();

		bytes.push_back(POP_JUMP_IF_FALSE_OP);
		Utils::writeVarInt(choiceContainer.size(), bytes);
		ic->addInstruction(InstructionData(bytes));
		bytes.clear();

		ic->addInstructions(choiceContainer);
	}

	if (previousNumChoiceExpressions > 0)
	{
		// the choice option index will be duplicated to perform the above operations,
		// so pop the original from the stack
		ic->addInstruction({ POP_OP });
	}
}

void Compiler::visitReturnStatement(ReturnStatementNode* node)
{
	InstructionContainer* ic = getCurrentInstructionContainer();

	if (node->expression != nullptr)
	{
		visitExpression(node->expression.get());

		ic->addInstruction({ RETURN_VALUE_OP });
		return;
	}

	ic->addInstruction({ RETURN_OP });
}

void Compiler::visitEventStatement(EventStatementNode* node)
{
	InstructionContainer* ic = getCurrentInstructionContainer();

	visitExpression(node->expression.get());
	
	ic->addInstruction({ EVENT_OP });
}

void Compiler::visitWhileStatement(WhileStatementNode* node)
{
	InstructionContainer* ic = getCurrentInstructionContainer();

	string labelBefore = "0la-b" + std::to_string(node->uniqueId);
	string labelAfter = "0la-a" + std::to_string(node->uniqueId);
	int64_t labelNameIndexBefore = _storyManager.addNameGetId(labelBefore);
	int64_t labelNameIndexAfter = _storyManager.addNameGetId(labelAfter);

	_currentBlockStructures.push_back(BlockStructure(BlockStructureType::While, labelNameIndexBefore, labelNameIndexAfter));

	addLabel(labelBefore, node);

	visitExpression(node->condition.get());

	InstructionContainer subContainer;
	_currentInstructionContainers.push_back(&subContainer);

	for (shared_ptr<StatementNode>& stmt : node->statements)
	{
		visitStatement(stmt.get());
	}

	_currentInstructionContainers.pop_back();

	vector<int8_t> bytes;
	bytes.push_back(POP_JUMP_IF_FALSE_OP);
	Utils::writeVarInt(subContainer.size(), bytes);

	ic->addInstruction(InstructionData(bytes));
	ic->addInstructions(subContainer);

	addLabel(labelAfter, node);
	_currentBlockStructures.pop_back();
}


void Compiler::visitExpression(ExpressionNode* node)
{
	switch (node->expressionType())
	{
		case ExpressionType::TernaryExpression:
			return visitTernaryExpression(static_cast<TernaryExpressionNode*>(node));
		case ExpressionType::OrExpression:
		case ExpressionType::AndExpression:
		case ExpressionType::InExpression:
		case ExpressionType::IsExpression:
		case ExpressionType::ComparisonExpression:
		case ExpressionType::BitOrExpression:
		case ExpressionType::BitXorExpression:
		case ExpressionType::BitAndExpression:
		case ExpressionType::BitShiftExpression:
		case ExpressionType::AdditiveExpression:
		case ExpressionType::MultiplicativeExpression:
		case ExpressionType::PowerExpression:
			return visitBinaryExpression(static_cast<BinaryExpressionNode*>(node));
		case ExpressionType::UnaryExpression:
			return visitUnaryExpression(static_cast<UnaryExpressionNode*>(node));
		case ExpressionType::DotExpression:
		case ExpressionType::DoubleColonExpression:
			return visitScopeNotationExpression(static_cast<ScopeNotationExpressionNode*>(node));
		case ExpressionType::CallExpression:
			return visitCallExpression(static_cast<CallExpressionNode*>(node));
		case ExpressionType::SelectorExpression:
			return visitSelectorExpression(static_cast<SelectorExpressionNode*>(node));
		case ExpressionType::GroupPrimary:
			return visitGroupPrimary(static_cast<GroupPrimaryNode*>(node));
		case ExpressionType::AtomPrimary:
			return visitAtomPrimary(static_cast<AtomPrimaryNode*>(node));
		case ExpressionType::ListPrimary:
			return visitListPrimary(static_cast<ListPrimaryNode*>(node));
		case ExpressionType::DictionaryPrimary:
			return visitDictionaryPrimary(static_cast<DictionaryPrimaryNode*>(node));
		case ExpressionType::CharacterPrimary:
			return visitCharacterPrimary(static_cast<CharacterPrimaryNode*>(node));
		case ExpressionType::SequencePrimary:
			return visitSequencePrimary(static_cast<SequencePrimaryNode*>(node));
	}
}

void Compiler::visitTernaryExpression(TernaryExpressionNode* node)
{
	InstructionContainer* ic = getCurrentInstructionContainer();
	vector<int8_t> bytes;

	visitExpression(node->condition.get());
	bytes.push_back(POP_JUMP_IF_FALSE_OP);

	// create a container that the true-expression will write itself into
	InstructionContainer trueContainer;
	_currentInstructionContainers.push_back(&trueContainer);
	visitExpression(node->trueExpression.get());
	_currentInstructionContainers.pop_back();

	// write the number of bytes occupied by the true-expression,
	// in order to jump over them if the condition is false
	Utils::writeVarInt(trueContainer.size(), bytes);

	// add "PopJumpIfFalse delta" to instruction container so that the
	// instruction data of trueContainer can be added after it
	ic->addInstruction(InstructionData(bytes));
	bytes.clear();

	// add the trueContainer's instructions to the original instruction container
	ic->addInstructions(trueContainer);

	bytes.push_back(JUMP_FORWARD_OP);

	// repeat for false-expression
	InstructionContainer falseContainer;
	_currentInstructionContainers.push_back(&falseContainer);
	visitExpression(node->falseExpression.get());
	_currentInstructionContainers.pop_back();

	Utils::writeVarInt(falseContainer.size(), bytes);
	ic->addInstruction(InstructionData(bytes));
	bytes.clear();

	ic->addInstructions(falseContainer);
	
	ensureSameValueTypes(node->trueExpression.get(), node->falseExpression.get(), &Localization::ternaryResultValueTypes);
}

void Compiler::visitBinaryExpression(BinaryExpressionNode* node)
{
	InstructionContainer* ic = getCurrentInstructionContainer();
	vector<int8_t> bytes;

	visitExpression(node->left.get());

	InstructionContainer rightContainer;
	_currentInstructionContainers.push_back(&rightContainer);
	visitExpression(node->right.get());
	_currentInstructionContainers.pop_back();
	
	ValueType opValueType = getOperatorReturnValue(node->left.get(), node->right.get(), node->op.get());
	_storyManager.addNodeExecutionEntry(node, std::make_shared<ValueExecutionEntry>(Value(opValueType)));
	
	if (node->op->type == TokenType::Or || node->op->type == TokenType::And)
	{
		bytes.push_back(node->op->type == TokenType::Or ? JUMP_IF_TRUE_OR_POP_OP : JUMP_IF_FALSE_OR_POP_OP);
		Utils::writeVarInt(rightContainer.size(), bytes);
		ic->addInstruction(InstructionData(bytes));
		bytes.clear();
	}

	switch (node->op->type)
	{
		case TokenType::Equals:
			bytes.push_back(EQUALS_OP);
			break;
		case TokenType::NotEquals:
			bytes.push_back(NOT_EQUALS_OP);
			break;
		case TokenType::GreaterThanEquals:
			bytes.push_back(GREATER_THAN_EQUALS_OP);
			break;
		case TokenType::LessThanEquals:
			bytes.push_back(LESS_THAN_EQUALS_OP);
			break;
		case TokenType::GreaterThan:
			bytes.push_back(GREATER_THAN_OP);
			break;
		case TokenType::LessThan:
			bytes.push_back(LESS_THAN_OP);
			break;
		case TokenType::And:
		case TokenType::Or:
			break;
		case TokenType::In:
			bytes.push_back(IN_OP);
			break;
		case TokenType::NotIn:
			bytes.push_back(NOT_IN_OP);
			break;
		case TokenType::Is:
			bytes.push_back(IS_OP);
			// For Is and IsNot, the result of visiting the right-side expression will be nothing because AtomPrimary does not output for variable types
			// So detect the value type and output it here
			Utils::writeVarInt(ValueType::createFromTokenType(static_cast<AtomPrimaryNode*>(node->right.get())->token->type).type, bytes);
			break;
		case TokenType::IsNot:
			bytes.push_back(IS_NOT_OP);
			// For Is and IsNot, the result of visiting the right-side expression will be nothing because AtomPrimary does not output for variable types
			// So detect the value type and output it here
			Utils::writeVarInt(ValueType::createFromTokenType(static_cast<AtomPrimaryNode*>(node->right.get())->token->type).type, bytes);
			break;
		case TokenType::Pow:
			bytes.push_back(POWER_OP);
			break;
		case TokenType::Add:
			bytes.push_back(ADDITION_OP);
			break;
		case TokenType::Sub:
			bytes.push_back(SUBTRACT_OP);
			break;
		case TokenType::Mul:
			bytes.push_back(MULTIPLY_OP);
			break;
		case TokenType::Div:
			bytes.push_back(DIVIDE_OP);
			break;
		case TokenType::Mod:
			bytes.push_back(MODULUS_OP);
			break;
		case TokenType::BitAnd:
			bytes.push_back(BIT_AND_OP);
			break;
		case TokenType::BitXor:
			bytes.push_back(BIT_XOR_OP);
			break;
		case TokenType::BitOr:
			bytes.push_back(BIT_OR_OP);
			break;
		case TokenType::BitLeftShift:
			bytes.push_back(BIT_LEFT_SHIFT_OP);
			break;
		case TokenType::BitRightShift:
			bytes.push_back(BIT_RIGHT_SHIFT_OP);
			break;
		default:
			error("binary operator not compiled", node, node->op.get());
	}

	ic->addInstructions(rightContainer);

	if (bytes.size() > 0)
	{
		ic->addInstruction(InstructionData(bytes));
	}
}

void Compiler::visitUnaryExpression(UnaryExpressionNode* node)
{
	InstructionContainer* ic = getCurrentInstructionContainer();

	visitExpression(node->expression.get());
	
	ValueType opValueType = getOperatorReturnValue(node->expression.get(), node->op.get());
	_storyManager.addNodeExecutionEntry(node, std::make_shared<ValueExecutionEntry>(Value(opValueType)));
	
	switch (node->op->type)
	{
		case TokenType::Not:
			ic->addInstruction({ NOT_OP });
			break;
		case TokenType::BitNot:
			ic->addInstruction({ BIT_NOT_OP });
			break;
		case TokenType::Sub:
			ic->addInstruction({ NEGATE_OP });
			break;
		default:
			error("unary operator not compiled", node, node->op.get());
	}
}

void Compiler::visitScopeNotationExpression(ScopeNotationExpressionNode* node)
{
	InstructionContainer* ic = getCurrentInstructionContainer();
	NamedContainer* nc = getCurrentNamedContainer();
	
	if (node->op->type == TokenType::DoubleColon)
	{
		auto leftAtom = static_cast<AtomPrimaryNode*>(node->left.get());
		auto& leftText = leftAtom->token->text;
		auto leftNameId = _storyManager.addNameGetId(leftText);
		auto& rightText = node->right->text;
		auto rightNameId = _storyManager.addNameGetId(rightText);
		
		NamedContainer* leftContainer = nc->findVisibleContainerNamed(leftNameId);
		if (leftContainer == nullptr)
		{
			error(Localization::namespaceNotFound(leftText), leftAtom);
		}
		_storyManager.addNodeExecutionEntry(leftAtom, std::make_shared<ContainerExecutionEntry>(leftContainer));
		
		NamedContainer* rightContainer = leftContainer->findChildNamed(leftNameId);
		if (rightContainer != nullptr)
		{
			_storyManager.addNodeExecutionEntry(node, std::make_shared<ContainerExecutionEntry>(rightContainer));
		}
		else
		{
			Variable* variable = leftContainer->getVariable(rightNameId);
			if (variable == nullptr)
			{
				error(Localization::nameNotFound(rightText), node);
			}
			
			_storyManager.addNodeExecutionEntry(node, std::make_shared<VariableExecutionEntry>(variable));
		}
	}
	else
	{
		visitExpression(node->left.get());
		auto leftEntry = _storyManager.getNodeExecutionEntry(node->left.get());
		Value leftValue;
		if (leftEntry->isValue())
		{
			leftValue = static_cast<ValueExecutionEntry*>(leftEntry.get())->value;
		}
		else if (leftEntry->isVariable())
		{
			leftValue = static_cast<VariableExecutionEntry*>(leftEntry.get())->variable->getValue();
		}
		else
		{
			error(Localization::expectedVariableOrValue(), node->left.get());
		}
		
		auto rightNameId = _storyManager.addNameGetId(node->right->text);
		Value rightValue(rightNameId, ValueType::ANY);
		for (auto& child : leftValue.children)
		{
			if (child.nameId == rightNameId)
			{
				rightValue.type = child.type;
				break;
			}
		}
		_storyManager.addNodeExecutionEntry(node, std::make_shared<ValueExecutionEntry>(rightValue));
		
		vector<int8_t> bytes;
		bytes.push_back(GET_ATTRIBUTE_OP);
		Utils::writeVarInt(rightNameId, bytes);
		ic->addInstruction(bytes);
	}
}

void Compiler::visitCallExpression(CallExpressionNode* node)
{
	InstructionContainer* ic = getCurrentInstructionContainer();
	NamedContainer* nc = getCurrentNamedContainer();
	NamedContainer* callContainer = nullptr;
	
	if (node->expression->expressionType() == ExpressionType::AtomPrimary)
	{
		auto atom = static_cast<AtomPrimaryNode*>(node->expression.get());
		auto nameId = _storyManager.addNameGetId(atom->token->text);
		callContainer = nc->findVisibleContainerNamed(nameId);
		if (callContainer != nullptr)
		{
			_storyManager.addNodeExecutionEntry(node->expression.get(), std::make_shared<ContainerExecutionEntry>(callContainer));
		}
		else
		{
			error(Localization::sectionNotFound(atom->token->text), atom);
		}
	}
	else
	{
		visitExpression(node->expression.get());
		auto entry = _storyManager.getNodeExecutionEntry(node->expression.get());
		if (entry == nullptr || !entry->isContainer())
		{
			error(Localization::callMustBeSection(), node->expression.get());
		}
		callContainer = static_cast<ContainerExecutionEntry*>(entry.get())->namedContainer;
	}
	
	auto numArguments = node->arguments.size();
	auto numParameters = callContainer->parameterTypes.size();
	if (numParameters != numArguments)
	{
		error(Localization::invalidNumberOfArguments(numParameters, numArguments), node);
	}
	
	int i = 0;
	for (shared_ptr<ExpressionNode>& param : node->arguments)
	{
		visitExpression(param.get());
		auto argValueType = getNodeValueType(param.get());
		auto& paramValueType = callContainer->parameterTypes[i];
		if (!argValueType.isCompatible(paramValueType))
		{
			error(Localization::expectedValueType(paramValueType.toString(), argValueType.toString()), node);
		}
		++i;
	}
	
	vector<int8_t> bytes;
	bytes.push_back(CALL_OP);
	Utils::writeVarInt(callContainer->id, bytes);
	Utils::writeVarInt(numArguments, bytes);
	
	ic->addInstruction(bytes);
	_storyManager.addNodeExecutionEntry(node, std::make_shared<ValueExecutionEntry>(Value(callContainer->returnType)));
}

void Compiler::visitSelectorExpression(SelectorExpressionNode* node)
{
	InstructionContainer* ic = getCurrentInstructionContainer();

	visitExpression(node->expression.get());

	if (node->slice)
	{
		int8_t arg = 0;
		if (node->startIndex != nullptr)
		{
			visitExpression(node->startIndex.get());
			arg |= 0b100;
		}

		if (node->endIndex != nullptr)
		{
			visitExpression(node->endIndex.get());
			arg |= 0b10;
		}

		if (node->step != nullptr)
		{
			visitExpression(node->step.get());
			arg |= 1;
		}

		ic->addInstruction({ SLICE_OP, arg });
	}
	else
	{
		visitExpression(node->startIndex.get());
		ic->addInstruction({ SUBSCRIPT_OP });
	}
	
	_storyManager.addNodeExecutionEntry(node, std::make_shared<ValueExecutionEntry>(Value(ValueType::ANY)));
}

void Compiler::visitAtomPrimary(AtomPrimaryNode* node)
{
	InstructionContainer* ic = getCurrentInstructionContainer();

	const string& text = node->token->text;
	vector<int8_t> bytes;

	if (node->token->type == TokenType::BoolLiteral)
	{
		bytes.push_back(BOOL_CONST_OP);
		bytes.push_back(Localization::getBool(text) ? 1 : 0);
		
		_storyManager.addNodeExecutionEntry(node, std::make_shared<ValueExecutionEntry>(Value(ValueType::BOOLEAN)));
	}
	else if (node->token->type == TokenType::IntLiteral)
	{
		bytes.push_back(INT_CONST_OP);
		int64_t num = 0;
		try
		{
			if (text.find("0b") == 0)
			{
				num = std::stoll(text.data() + 2, nullptr, 2);
			}
			else if (text.find("0o") == 0)
			{
				num = std::stoll(text.data() + 2, nullptr, 8);
			}
			else if (text.find("0x") == 0)
			{
				num = std::stoll(text.data() + 2, nullptr, 16);
			}
			else
			{
				num = std::stoll(text);
			}
		}
		catch (std::exception ex)
		{
			error(Localization::integerOutOfBounds(text), nullptr);
		}
		
		Utils::writeVarInt(num, bytes);
		
		_storyManager.addNodeExecutionEntry(node, std::make_shared<ValueExecutionEntry>(Value(ValueType::INTEGER)));
	}
	else if (node->token->type == TokenType::FloatLiteral)
	{
		bytes.push_back(FLOAT_CONST_OP);
		auto decimalIndex = text.rfind('.');
		string whole = text.substr(0, decimalIndex);
		string decimal = text.substr(decimalIndex + 1);
		int64_t wholeNum = 0;
		int64_t decimalNum = 0;
		try
		{
			wholeNum = std::stoll(whole);
			decimalNum = std::stoll(decimal);
		}
		catch (std::exception ex)
		{
			error(Localization::floatOutOfBounds(text), nullptr);
		}
		
		Utils::writeVarInt(wholeNum, bytes);
		Utils::writeVarInt(decimalNum, bytes);
		_storyManager.addNodeExecutionEntry(node, std::make_shared<ValueExecutionEntry>(Value(ValueType::FLOAT)));
	}
	else if (node->token->type == TokenType::StringLiteral)
	{
		char first = text[0];
		int64_t start = 0; // inclusive
		int64_t end = text.length(); // exclusive

		while (start < text.length() && text[start] == first)
		{
			++start;
		}

		while (end > 0 && text[end - 1] == first)
		{
			--end;
		}

		auto stringId = _storyManager.addStringGetId(text.substr(start, end - start));

		bytes.push_back(STRING_CONST_OP);
		Utils::writeVarInt(stringId, bytes);
		
		_storyManager.addNodeExecutionEntry(node, std::make_shared<ValueExecutionEntry>(Value(ValueType::STRING)));
	}
	else if (node->token->type == TokenType::Name)
	{
		// If name gets here, it must be a variable
		int64_t nameId = _storyManager.addNameGetId(node->token->text);
		
		auto nc = getCurrentNamedContainer();
		Variable* variable = nc->findVariableInScope(nameId);
		if (variable == nullptr)
		{
			error(Localization::variableNotFound(node->token->text), node);
		}
		
		_storyManager.addNodeExecutionEntry(node, std::make_shared<VariableExecutionEntry>(variable));
		
		bytes.push_back(LOAD_VARIABLE_OP);
		Utils::writeVarInt(variable->containerId, bytes);
		Utils::writeVarInt(variable->nameId, bytes);
	}
	else if (node->token->type == TokenType::Default)
	{
		bytes.push_back(DEFAULT_CONST_OP);
		_storyManager.addNodeExecutionEntry(node, std::make_shared<ValueExecutionEntry>(Value(ValueType::ANY)));
	}

	ic->addInstruction(InstructionData(std::move(bytes)));
}

void Compiler::visitListPrimary(ListPrimaryNode* node)
{
	InstructionContainer* ic = getCurrentInstructionContainer();

	for (shared_ptr<ExpressionNode>& expr : node->expressions)
	{
		visitExpression(expr.get());
	}

	vector<int8_t> bytes;
	if (node->defaultExpression == nullptr)
	{
		bytes.push_back(BUILD_LIST_OP);
	}
	else
	{
		visitExpression(node->defaultExpression.get());
		bytes.push_back(BUILD_LIST_WITH_DEFAULT_OP);
	}
	
	Utils::writeVarInt(node->expressions.size(), bytes);
	
	ic->addInstruction(InstructionData(bytes));
	
	_storyManager.addNodeExecutionEntry(node, std::make_shared<ValueExecutionEntry>(Value(ValueType::LIST)));
}

void Compiler::visitDictionaryPrimary(DictionaryPrimaryNode* node)
{
	InstructionContainer* ic = getCurrentInstructionContainer();

	for (shared_ptr<DictionaryPairNode>& pair : node->pairs)
	{
		visitDictionaryPair(pair.get());
	}

	vector<int8_t> bytes;
	if (node->defaultExpression == nullptr)
	{
		bytes.push_back(BUILD_DICTIONARY_OP);
	}
	else
	{
		visitExpression(node->defaultExpression.get());
		bytes.push_back(BUILD_DICTIONARY_WITH_DEFAULT_OP);
	}
	
	Utils::writeVarInt(node->pairs.size(), bytes);
	ic->addInstruction(InstructionData(bytes));
	
	_storyManager.addNodeExecutionEntry(node, std::make_shared<ValueExecutionEntry>(Value(ValueType::DICT)));
}

void Compiler::visitDictionaryPair(DictionaryPairNode* node)
{
	visitExpression(node->key.get());
	visitExpression(node->value.get());
}

void Compiler::visitCharacterPrimary(CharacterPrimaryNode* node)
{
	InstructionContainer* ic = getCurrentInstructionContainer();
	// create value for character
	Value characterValue(ValueType::CHARACTER);
	characterValue.children.reserve(node->pairs.size());
	
	// the character name ids will be outputted sequentially after BUILD_CHARACTER_OP
	vector<int64_t> propertyNameIds;
	// bytecode for character creation
	vector<int8_t> bytes;

	for (shared_ptr<CharacterPairNode>& pair : node->pairs)
	{
		int64_t nameId = _storyManager.addNameGetId(pair->name->text);
		propertyNameIds.push_back(nameId);

		visitCharacterPair(pair.get());
		
		auto entry = _storyManager.getNodeExecutionEntry(pair.get());
		if (entry->isValue())
		{
			characterValue.children.emplace_back(nameId, static_cast<ValueExecutionEntry*>(entry.get())->value.type);
		}
		else if (entry->isVariable())
		{
			characterValue.children.emplace_back(nameId, static_cast<VariableExecutionEntry*>(entry.get())->variable->getValue().type);
		}
	}
	
	bytes.push_back(BUILD_CHARACTER_OP);
	Utils::writeVarInt(node->pairs.size(), bytes);
	for (auto id : propertyNameIds)
	{
		Utils::writeVarInt(id, bytes);
	}
	
	ic->addInstruction(InstructionData(bytes));
	
	// add valuetype to node execution
	_storyManager.addNodeExecutionEntry(node, std::make_shared<ValueExecutionEntry>(characterValue));
}

void Compiler::visitCharacterPair(CharacterPairNode* node)
{
	visitExpression(node->value.get());
	auto entry = _storyManager.getNodeExecutionEntry(node->value.get());
	_storyManager.addNodeExecutionEntry(node, entry);
}

void Compiler::visitSequencePrimary(SequencePrimaryNode* node)
{
	NamedContainer* parentContainer = getCurrentNamedContainer();
	InstructionContainer* ic = getCurrentInstructionContainer();
	
	bool createFunctionCall = node->name == nullptr;
	if (node->name != nullptr)
	{
		auto parent = _storyManager.nodeRegister().getParent(node);
		auto parentNodeType = parent->nodeType();
		if (parentNodeType == NodeType::Statement)
		{
			auto stmt = static_cast<StatementNode*>(parent);
			auto stmtType = stmt->statementType();
			createFunctionCall = (stmtType == StatementType::AssignmentStatement || stmtType == StatementType::ExpressionStatement);
		}
		else if (parentNodeType == NodeType::Child)
		{
			auto child = static_cast<ChildNode*>(parent);
			auto childType = child->childType();
			createFunctionCall = (childType == ChildType::CharacterPair || childType == ChildType::DictionaryPair || childType == ChildType::ParameterPart || childType == ChildType::SequenceItem);
		}
	}
	
	string sequenceName = node->name != nullptr
		? node->name->text
		: "0seq-" + std::to_string(parentContainer->children.size());
	// Sequence named containers (and therfore blocks) have the section (or namespace) parent container
	// as its parent even though sections normally cannot have child sections.
	// This allows it to have access to parent variables in the virtual machine.
	shared_ptr<NamedContainer> sequenceContainer = _storyManager.createNamedContainer(node, sequenceName, parentContainer);
	
	// the sequence's container will be handled in visitNamedContainer(),
	// which will then call finalizeSequencePrimary() to compile the sequence's contents
	
	if (createFunctionCall)
	{
		vector<int8_t> bytes{ CALL_OP };
		Utils::writeVarInt(sequenceContainer->id, bytes);
		Utils::writeVarInt(0, bytes);
		ic->addInstruction(bytes);
	}
	else
	{
		ic->addInstruction({ NOOP_OP });
	}
	
	_storyManager.addNodeExecutionEntry(node, std::make_shared<ValueExecutionEntry>(Value(ValueType::ANY)));
}

void Compiler::finalizeSequencePrimary(SequencePrimaryNode* node)
{
	shared_ptr<NamedContainer>& nc = _storyManager.getNamedContainer(node);
	_currentNamedContainers.push_back(nc.get());
	_currentInstructionContainers.push_back(&nc->instructionContainer);
	
	// this is the instruction container of the sequence named container
	InstructionContainer* ic = getCurrentInstructionContainer();
	vector<int8_t> bytes{ GET_SEQUENCE_INDEX_OP, node->loop, node->random, node->order };
	Utils::writeVarInt(node->items.size(), bytes);
	ic->addInstruction(bytes);
	bytes.clear();
	
	for (int64_t i = 0; i < node->items.size(); ++i)
	{
		bytes.push_back(DUPLICATE_OP);
		bytes.push_back(INT_CONST_OP);
		Utils::writeVarInt(i, bytes);
		bytes.push_back(EQUALS_OP);
		ic->addInstruction(bytes);
		bytes.clear();
		
		visitSequenceItem(node->items[i].get());
	}
	
	_currentInstructionContainers.pop_back();
	_currentNamedContainers.pop_back();
}

void Compiler::visitSequenceItem(SequenceItemNode* item)
{
	// this is the instruction container of the sequence named container
	InstructionContainer* ic = getCurrentInstructionContainer();
	InstructionContainer subContainer;
	_currentInstructionContainers.push_back(&subContainer);

	if (item->statement != nullptr)
	{
		visitStatement(item->statement.get());
	}

	visitExpression(item->expression.get());
	subContainer.addInstruction({ RETURN_VALUE_OP });

	_currentInstructionContainers.pop_back();

	vector<int8_t> bytes;
	bytes.push_back(POP_JUMP_IF_FALSE_OP);
	Utils::writeVarInt(subContainer.size(), bytes);
	ic->addInstruction(InstructionData(bytes));
	ic->addInstructions(subContainer);
}

void Compiler::visitGroupPrimary(GroupPrimaryNode* node)
{
	visitExpression(node->expression.get());
}

void Compiler::ensureSameValueTypes(Node* expectedNode, Node* givenNode, std::function<string(string, string)> errorFunc)
{
	auto expected = _storyManager.getNodeExecutionEntry(expectedNode);
	auto given = _storyManager.getNodeExecutionEntry(givenNode);
	if (expected == nullptr)
	{
		throw std::runtime_error("[heehee 1 null] - this should never happen");
	}
	
	if (given == nullptr)
	{
		throw std::runtime_error("[heehee 2 null] - this should never happen");
	}
	
	Value expectedValue;
	Value givenValue;
	
	if (expected->isValue())
	{
		expectedValue = static_cast<ValueExecutionEntry*>(expected.get())->value;
	}
	else if (expected->isVariable())
	{
		expectedValue = static_cast<VariableExecutionEntry*>(expected.get())->variable->assignments.back().second;
	}
	else
	{
		error(Localization::expectedVariableOrValue(), expectedNode);
	}
	
	if (given->isValue())
	{
		givenValue = static_cast<ValueExecutionEntry*>(given.get())->value;
	}
	else if (given->isVariable())
	{
		givenValue = static_cast<VariableExecutionEntry*>(given.get())->variable->assignments.back().second;
	}
	else
	{
		error(Localization::expectedVariableOrValue(), givenNode);
	}
	
	if (expectedValue.type == ValueType::ANY || givenValue.type == ValueType::ANY)
	{
		return;
	}
	
	if (!expectedValue.type.isCompatible(givenValue.type))
	{
		error(errorFunc(expectedValue.type.toString(), givenValue.type.toString()), givenNode);
	}
}

ValueType Compiler::getNodeValueType(Node* node)
{
	auto execution = _storyManager.getNodeExecutionEntry(node);
	if (execution == nullptr)
	{
		throw std::runtime_error("[lol 1 null] - this should never happen");
	}
	
	Value value;
	
	if (execution->isValue())
	{
		value = static_cast<ValueExecutionEntry*>(execution.get())->value;
	}
	else if (execution->isVariable())
	{
		value = static_cast<VariableExecutionEntry*>(execution.get())->variable->assignments.back().second;
	}
	else
	{
		error(Localization::expectedVariableOrValue(), node);
	}
	
	return value.type;
}

Value Compiler::getNodeValue(Node* node)
{
	auto execution = _storyManager.getNodeExecutionEntry(node);
	if (execution == nullptr)
	{
		throw std::runtime_error("[lol 2 null] - this should never happen");
	}
	
	if (execution->isValue())
	{
		return static_cast<ValueExecutionEntry*>(execution.get())->value;
	}
	else if (execution->isVariable())
	{
		return static_cast<VariableExecutionEntry*>(execution.get())->variable->assignments.back().second;
	}
	
	error(Localization::expectedVariableOrValue(), node);
	return Value();
}

ValueType Compiler::getOperatorReturnValue(Node* leftNode, Node* rightNode, Token* operatorToken)
{
	ValueType leftType = getNodeValueType(leftNode);
	ValueType rightType = getNodeValueType(rightNode);
	Operator& op = Operators::getFromTokenType(operatorToken->type);
	if (!op.canAccept(leftType, rightType))
	{
		std::ostringstream leftOss;
		leftOss << op.leftRequiredTypes();
		
		std::ostringstream rightOss;
		rightOss << op.rightRequiredTypes();
		
		error(Localization::operatorMustAccept(operatorToken->text, leftType.toString(), rightType.toString(), leftOss.str(), rightOss.str()), leftNode, operatorToken);
	}
	
	return op.determineResultType(leftType, rightType);
}

ValueType Compiler::getOperatorReturnValue(Node* node, Token* operatorToken)
{
	ValueType type = getNodeValueType(node);
	Operator& op = Operators::getFromTokenType(operatorToken->type);
	if (!op.canAccept(type, type))
	{
		std::ostringstream oss;
		oss << op.leftRequiredTypes();
		
		error(Localization::operatorMustAccept(operatorToken->text, type.toString(), oss.str()), node, operatorToken);
	}
	
	return op.determineResultType(type, type);
}

void Compiler::error(string message, Node* node)
{
	auto stories = _storyManager.nodeRegister().getAncestorsWith(node, [](Node* other) {
		return other->nodeType() == NodeType::Story;
	});
	auto story = static_cast<StoryNode*>(stories[0]);
	
	throw CompilationException(story->filepath, message, *node);
}

void Compiler::error(string message, Node* node, Token* token)
{
	auto stories = _storyManager.nodeRegister().getAncestorsWith(node, [](Node* other) {
		return other->nodeType() == NodeType::Story;
	});
	auto story = static_cast<StoryNode*>(stories[0]);
	
	throw CompilationException(story->filepath, message, *token);
}

}
