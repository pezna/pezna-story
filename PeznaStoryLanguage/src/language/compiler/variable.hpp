#ifndef PSL_COMPILER_VARIABLE_H
#define PSL_COMPILER_VARIABLE_H

#include <cstdint>
#include <utility>

#include "value.hpp"
#include "../nodes/nodes.hpp"

namespace PSL
{
using std::pair;
using std::vector;

struct Variable
{
	/** The id of the container this is declared in */
	int64_t containerId;
	
	/** The index of the variable's name */
	int64_t nameId;
	
	/** Whether or not this variable is a parameter */
	bool isParameter;
	
	/**
	 * The statements where this variable is assigned.
	 * The first is the definition
	 */
	vector<pair<Node*, Value>> assignments;
	
	Variable()
	:
		nameId{0},
		containerId{-1},
		isParameter{false}
	{}
	
	Variable(Variable&& other) = default;
	
	Variable(const Variable& other) = default;
	
	Variable(int64_t nameId, int64_t containerId, pair<Node*, Value> creationStatement, bool isParameter)
	:
		nameId{nameId},
		containerId{containerId},
		isParameter{isParameter},
		assignments({creationStatement})
	{}
	
	virtual ~Variable() {}
	
	Variable& operator=(const Variable& other) = default;
	
	Value& getValue()
	{
		return assignments.front().second;
	}
};

}

#endif // PSL_COMPILER_VARIABLE_H
