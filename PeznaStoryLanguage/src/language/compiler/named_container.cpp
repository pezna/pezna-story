#include "named_container.hpp"

#include "../exceptions.hpp"
#include "../localization.hpp"
#include "../utils/statics.hpp"

namespace PSL
{

/******************
 * NamedContainer *
 ******************/

NamedContainer::NamedContainer(NamespaceNode* ns, NamedContainer* parent)
{
	this->id = 0; // should be set in StoryManager
	this->nameId = 0; // should be set in StoryManager
	this->parent = parent;
	this->nodes.push_back(ns);
	this->name = ns->name->text;
	auto parentName = parent ? (parent->fullyQualifiedName + "::") : "";
	this->fullyQualifiedName = parentName +  this->name;
	this->isNamespace = true;
	this->isSection = false;
	this->isSequence = false;
	this->bytecodeOffset = 0;
	this->bytecodeLength = 0;
}

NamedContainer::NamedContainer(SectionNode* section, NamedContainer* parent)
{
	this->id = 0; // should be set in StoryManager
	this->nameId = 0; // should be set in StoryManager
	this->parent = parent;
	this->nodes.push_back(section);
	this->name = section->name->text;
	auto parentName = parent ? (parent->fullyQualifiedName + "::") : "";
	this->fullyQualifiedName = parentName +  this->name;
	this->isNamespace = false;
	this->isSection = true;
	this->isSequence = false;
	this->bytecodeOffset = 0;
	this->bytecodeLength = 0;
	
	for (auto& param : section->parameters)
	{
		ValueType valueType = ValueType::createFromTokenType(param->variableType);
		this->parameterTypes.push_back(valueType);
	}
}

NamedContainer::NamedContainer(SequencePrimaryNode* sequence, const string& name, NamedContainer* parent)
{
	this->id = 0; // should be set in StoryManager
	this->nameId = 0; // should be set in StoryManager
	this->parent = parent;
	this->nodes.push_back(sequence);
	this->name = name;
	auto parentName = parent ? (parent->fullyQualifiedName + "#") : "";
	this->fullyQualifiedName = parentName +  this->name;
	this->isNamespace = false;
	this->isSection = false;
	this->isSequence = true;
	this->bytecodeOffset = 0;
	this->bytecodeLength = 0;
}

Variable* NamedContainer::addVariable(int64_t nameId, AssignmentStatementNode* creation, Value value)
{
	variables.emplace(nameId, Variable(nameId, this->id, std::make_pair(creation, value), false));
	return getVariable(nameId);
}

Variable* NamedContainer::addVariable(int64_t nameId, ParameterNode* parameter, Value value)
{
	variables.emplace(nameId, Variable(nameId, this->id, std::make_pair(parameter, value), true));
	return getVariable(nameId);
}

bool NamedContainer::hasVariable(int64_t nameId)
{
	auto it = variables.find(nameId);
	return it != variables.end();
}

Variable* NamedContainer::getVariable(int64_t nameId)
{
	auto it = variables.find(nameId);
	return it != variables.end() ? &it->second : nullptr;
}

Variable* NamedContainer::findVariableInScope(int64_t nameId)
{
	auto it = variables.find(nameId);
	if (it != variables.end())
	{
		return &it->second;
	}
	
	if (parent == nullptr)
	{
		return nullptr;
	}
	
	return parent->findVariableInScope(nameId);
}

Variable* NamedContainer::findVariableGlobally(int64_t nameId)
{
	Variable* var = nullptr;
	if (parent != nullptr)
	{
		var = parent->findVariableGlobally(nameId);
	}

	if (var == nullptr)
	{
		var = getVariable(nameId);
	}

	return var;
}

NamedContainer* NamedContainer::findAncestorNamed(int64_t nameId)
{
	if (parent == nullptr)
	{
		return nullptr;
	}
	
	if (parent->nameId == nameId)
	{
		return parent;
	}

	return parent->findAncestorNamed(nameId);
}

NamedContainer* NamedContainer::findSiblingNamed(int64_t nameId)
{
	if (parent != nullptr)
	{
		for (auto& sibling : parent->children)
		{
			if (sibling->nameId == nameId)
			{
				return sibling.get();
			}
		}
	}
	
	return nullptr;
}

NamedContainer* NamedContainer::findAncestorOrSiblingNamed(int64_t nameId)
{
	auto nc = findAncestorNamed(nameId);
	if (nc == nullptr)
	{
		nc = findSiblingNamed(nameId);
	}
	
	return nc;
}

NamedContainer* NamedContainer::findChildNamed(int64_t nameId)
{
	for (auto& child : children)
	{
		if (child->nameId == nameId)
		{
			return child.get();
		}
	}
	
	return nullptr;
}

NamedContainer* NamedContainer::findVisibleContainerNamed(int64_t nameId)
{
	auto* nc = findAncestorOrSiblingNamed(nameId);
	if (nc == nullptr)
	{
		return findChildNamed(nameId);
	}
	
	return nc;
}

int64_t NamedContainer::getNumDescendants()
{
	int64_t count = children.size();
	
	for (shared_ptr<NamedContainer>& child : children)
	{
		count += child->getNumDescendants();
	}
	
	return count;
}

bool NamedContainer::hasLabel(int64_t nameId)
{
	return labels.find(nameId) != labels.end();
}

int64_t NamedContainer::getLabel(int64_t nameId)
{
	auto it = labels.find(nameId);
	if (it == labels.end())
	{
		return 0;
	}

	return it->second;
}

void NamedContainer::addLabel(int64_t nameId, int64_t location)
{
	labels[nameId] = location;
}

}
