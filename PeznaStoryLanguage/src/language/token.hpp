#ifndef PSL_PARSER_TOKEN_H
#define PSL_PARSER_TOKEN_H

#include <string>
#include <cstdint>
#include <iostream>
#include <vector>
#include <utility>

namespace PSL
{
using std::string;
using std::ostream;
using std::vector;

enum class TokenType
{
	Story, Namespace, Section, Choice, If, Else, ElseIf, While, For, Do,
	Goto, Exit, Label, Event, Include, Continue, Return, End, Default,
	Var, Final, Character, Sequence, Bool, Int, Float, String, List, Dict,
	
	Equals, NotEquals, GreaterThanEquals, LessThanEquals, GreaterThan, LessThan,
	Not, And, Or, In, NotIn, Is, IsNot,
	
	Pow, Add, Sub, Mul, Div, Mod, BitAnd, BitXor, BitOr, BitNot, BitLeftShift, BitRightShift,
	
	Assign, PowAssign, AddAssign, SubAssign, MulAssign, DivAssign, ModAssign, BitAndAssign,
	BitXorAssign, BitOrAssign, BitLeftShiftAssign, BitRightShiftAssign,
	
	OpenBrack, CloseBrack, OpenParen, CloseParen, OpenBrace, CloseBrace,
	Comma, Colon, DoubleColon, SemiColon, Dot, Ternary, Arrow,
	
	BoolLiteral, IntLiteral, FloatLiteral, StringLiteral, Name,
	
	Comment, Whitespace, IgnoreNewline, Newline, Eof
};

struct Token
{
	const TokenType type;
	const string text;
	const int64_t line, column;
	/** Offset in the entire text (inclusive, exclusive) */
	const int64_t startOffset, endOffset;
	
	Token()
	:
		type(TokenType::Eof),
		line(0),
		column(0),
		startOffset(0),
		endOffset(0)
	{}
	
	Token(Token& other) = default;
	
	Token(Token&& other) = default;
	
	Token(TokenType type, string&& text, int64_t line, int64_t column, int64_t startOffset, int64_t endOffset)
	:
		type(type),
		text(std::move(text)),
		line(line),
		column(column),
		startOffset(startOffset),
		endOffset(endOffset)
	{}
	
	static string getTypeName(TokenType type);
	static string getSymbolText(TokenType type);
	static TokenType getKeywordType(const string& text);
	bool isBinaryOperator();
	bool isAssignment();
	bool canStartExpression();
	bool canStartStatement();
	bool canStartVariableCreation();
	bool isVariableType();
	
	friend ostream& operator<<(ostream& os, const Token& token);
};

}

#endif //PSL_PARSER_TOKEN_H
