#include "node_register.hpp"

namespace PSL
{

void NodeRegister::addStory(shared_ptr<StoryNode> node)
{
	visitStory(node.get());
}

unordered_map<Token*, Node*>& NodeRegister::tokensWithParent()
{
	return _tokenToParent;
}

void NodeRegister::visitNode(Node* node)
{
	switch (node->nodeType())
	{
		case NodeType::Story:
			return visitStory(static_cast<StoryNode*>(node));
		case NodeType::Namespace:
			return visitNamespace(static_cast<NamespaceNode*>(node));
		case NodeType::Section:
			return visitSection(static_cast<SectionNode*>(node));
		case NodeType::Statement:
			return visitStatement(static_cast<StatementNode*>(node));
		case NodeType::Child:
			return visitChild(static_cast<ChildNode*>(node));
	}
}

void NodeRegister::visitStory(StoryNode* node)
{
	// story should have no parent
	_childToParent[node->mainNamespace.get()] = node;
	visitNode(node->mainNamespace.get());
	
	_parentToChildren[node] = { node->mainNamespace.get() };
}

void NodeRegister::visitNamespace(NamespaceNode* node)
{
	std::vector<Node*> children;
	
	for (shared_ptr<StatementNode>& stmt : node->statements)
	{
		_childToParent[stmt.get()] = node;
		children.push_back(stmt.get());
		visitNode(stmt.get());
	}
	
	for (shared_ptr<NamespaceNode>& subNs : node->subNamespaces)
	{
		_childToParent[subNs.get()] = node;
		children.push_back(subNs.get());
		visitNode(subNs.get());
	}
	
	for (shared_ptr<SectionNode>& section : node->sections)
	{
		_childToParent[section.get()] = node;
		children.push_back(section.get());
		visitNode(section.get());
	}
	
	_parentToChildren[node] = children;
	
	_tokenToParent[node->name.get()] = node;
}

void NodeRegister::visitSection(SectionNode* node)
{
	std::vector<Node*> children;
	
	for (shared_ptr<StatementNode>& stmt : node->statements)
	{
		_childToParent[stmt.get()] = node;
		children.push_back(stmt.get());
		visitNode(stmt.get());
	}
	
	_parentToChildren[node] = children;
	
	for (auto& param : node->parameters)
	{
		_childToParent[param.get()] = node;
		children.push_back(param.get());
		visitNode(param.get());
	}
	
	_tokenToParent[node->name.get()] = node;
}

void NodeRegister::visitStatement(StatementNode* node)
{
	switch (node->statementType())
	{
		case StatementType::AssignmentStatement:
			return visitAssignmentStatement(static_cast<AssignmentStatementNode*>(node));
		case StatementType::ChoiceStatement:
			return visitChoiceStatement(static_cast<ChoiceStatementNode*>(node));
		case StatementType::ContinueStatement:
			return visitContinueStatement(static_cast<ContinueStatementNode*>(node));
		case StatementType::EventStatement:
			return visitEventStatement(static_cast<EventStatementNode*>(node));
		case StatementType::ExitStatement:
			return visitExitStatement(static_cast<ExitStatementNode*>(node));
		case StatementType::ExpressionStatement:
			return visitExpression(static_cast<ExpressionNode*>(node));
		case StatementType::ForStatement:
			return visitForStatement(static_cast<ForStatementNode*>(node));
		case StatementType::GotoStatement:
			return visitGotoStatement(static_cast<GotoStatementNode*>(node));
		case StatementType::IfStatement:
			return visitIfStatement(static_cast<IfStatementNode*>(node));
		case StatementType::IncludeStatement:
			return visitIncludeStatement(static_cast<IncludeStatementNode*>(node));
		case StatementType::LabelStatement:
			return visitLabelStatement(static_cast<LabelStatementNode*>(node));
		case StatementType::LineStatement:
			return visitLineStatement(static_cast<LineStatementNode*>(node));
		case StatementType::DoStatement:
			return visitDoStatement(static_cast<DoStatementNode*>(node));
		case StatementType::ReturnStatement:
			return visitReturnStatement(static_cast<ReturnStatementNode*>(node));
		case StatementType::WhileStatement:
			return visitWhileStatement(static_cast<WhileStatementNode*>(node));
	}
}

void NodeRegister::visitChild(ChildNode* node)
{
	switch (node->childType())
	{
		case ChildType::CharacterPair:
			return visitCharacterPair(static_cast<CharacterPairNode*>(node));
		case ChildType::DictionaryPair:
			return visitDictionaryPair(static_cast<DictionaryPairNode*>(node));
		case ChildType::SequenceItem:
			return visitSequenceItem(static_cast<SequenceItemNode*>(node));
		case ChildType::IfPart:
			return visitIfPart(static_cast<IfPartNode*>(node));
		case ChildType::ElseIfPart:
			return visitElseIfPart(static_cast<ElseIfPartNode*>(node));
		case ChildType::ElsePart:
			return visitElsePart(static_cast<ElsePartNode*>(node));
		case ChildType::ParameterPart:
			return visitParameter(static_cast<ParameterNode*>(node));
	}
}

void NodeRegister::visitParameter(ParameterNode* node)
{
	_tokenToParent[node->name.get()] = node;
}

void NodeRegister::visitAssignmentStatement(AssignmentStatementNode* node)
{
	_childToParent[node->left.get()] = node;
	_childToParent[node->right.get()] = node;
	
	visitNode(node->left.get());
	visitNode(node->right.get());
	
	_parentToChildren[node] = { node->left.get(), node->right.get() };
}

void NodeRegister::visitChoiceStatement(ChoiceStatementNode* node)
{
	std::vector<Node*> children;
	
	for (shared_ptr<StatementNode>& stmt : node->statements)
	{
		_childToParent[stmt.get()] = node;
		children.push_back(stmt.get());
		visitNode(stmt.get());
	}
	
	for (shared_ptr<ExpressionNode>& expr : node->expressions)
	{
		_childToParent[expr.get()] = node;
		children.push_back(expr.get());
		visitNode(expr.get());
	}
	
	_parentToChildren[node] = children;
}

void NodeRegister::visitContinueStatement(ContinueStatementNode* node)
{
	if (node->level != nullptr)
	{
		_tokenToParent[node->level.get()] = node;
	}
}

void NodeRegister::visitExitStatement(ExitStatementNode* node)
{
	if (node->level != nullptr)
	{
		_tokenToParent[node->level.get()] = node;
	}
}

void NodeRegister::visitForStatement(ForStatementNode* node)
{
	std::vector<Node*> children;
	
	_tokenToParent[node->variable.get()] = node;
	
	_childToParent[node->expression.get()] = node;
	children.push_back(node->expression.get());
	visitNode(node->expression.get());
	
	for (shared_ptr<StatementNode>& stmt : node->statements)
	{
		_childToParent[stmt.get()] = node;
		children.push_back(stmt.get());
		visitNode(stmt.get());
	}
	
	_parentToChildren[node] = children;
}

void NodeRegister::visitGotoStatement(GotoStatementNode* node)
{
	_tokenToParent[node->token.get()] = node;
}

void NodeRegister::visitIfStatement(IfStatementNode* node)
{
	std::vector<Node*> children;
	
	_childToParent[node->ifPart.get()] = node;
	children.push_back(node->ifPart.get());
	visitNode(node->ifPart.get());
	
	for (shared_ptr<ElseIfPartNode>& part : node->elseIfParts)
	{
		_childToParent[part.get()] = node;
		children.push_back(part.get());
		visitNode(part.get());
	}
	
	if (node->elsePart != nullptr)
	{
		_childToParent[node->elsePart.get()] = node;
		children.push_back(node->elsePart.get());
		visitNode(node->elsePart.get());
	}
	
	_parentToChildren[node] = children;
}

void NodeRegister::visitIfPart(IfPartNode* node)
{
	std::vector<Node*> children;
	
	_childToParent[node->condition.get()] = node;
	children.push_back(node->condition.get());
	visitNode(node->condition.get());
	
	for (shared_ptr<StatementNode>& stmt : node->statements)
	{
		_childToParent[stmt.get()] = node;
		children.push_back(stmt.get());
		visitNode(stmt.get());
	}
	
	_parentToChildren[node] = children;
}

void NodeRegister::visitElseIfPart(ElseIfPartNode* node)
{
	std::vector<Node*> children;
	
	_childToParent[node->condition.get()] = node;
	children.push_back(node->condition.get());
	visitNode(node->condition.get());
	
	for (shared_ptr<StatementNode>& stmt : node->statements)
	{
		_childToParent[stmt.get()] = node;
		children.push_back(stmt.get());
		visitNode(stmt.get());
	}
	
	_parentToChildren[node] = children;
}

void NodeRegister::visitElsePart(ElsePartNode* node)
{
	std::vector<Node*> children;
	
	for (shared_ptr<StatementNode>& stmt : node->statements)
	{
		_childToParent[stmt.get()] = node;
		children.push_back(stmt.get());
		visitNode(stmt.get());
	}
	
	_parentToChildren[node] = children;
}

void NodeRegister::visitIncludeStatement(IncludeStatementNode* node)
{
	_tokenToParent[node->filename.get()] = node;
}

void NodeRegister::visitLabelStatement(LabelStatementNode* node)
{
	_tokenToParent[node->label.get()] = node;
}

void NodeRegister::visitLineStatement(LineStatementNode* node)
{
	_childToParent[node->character.get()] = node;
	_childToParent[node->text.get()] = node;
	
	visitNode(node->character.get());
	visitNode(node->text.get());
	
	_parentToChildren[node] = { node->character.get(), node->text.get() };
}

void NodeRegister::visitDoStatement(DoStatementNode* node)
{
	std::vector<Node*> children;
	
	for (shared_ptr<StatementNode>& stmt : node->statements)
	{
		_childToParent[stmt.get()] = node;
		children.push_back(stmt.get());
		visitNode(stmt.get());
	}

	_childToParent[node->endCondition.get()] = node;
	children.push_back(node->endCondition.get());
	visitNode(node->endCondition.get());
	
	_parentToChildren[node] = children;
}

void NodeRegister::visitReturnStatement(ReturnStatementNode* node)
{
	if (node->expression != nullptr)
	{
		_childToParent[node->expression.get()] = node;
		visitNode(node->expression.get());
		_parentToChildren[node] = { node->expression.get() };
	}
}

void NodeRegister::visitEventStatement(EventStatementNode* node)
{
	_childToParent[node->expression.get()] = node;
	visitNode(node->expression.get());
	_parentToChildren[node] = { node->expression.get() };
}

void NodeRegister::visitWhileStatement(WhileStatementNode* node)
{
	std::vector<Node*> children;
	
	_childToParent[node->condition.get()] = node;
	children.push_back(node->condition.get());
	visitNode(node->condition.get());
	
	for (shared_ptr<StatementNode>& stmt : node->statements)
	{
		_childToParent[stmt.get()] = node;
		children.push_back(stmt.get());
		visitNode(stmt.get());
	}
	
	_parentToChildren[node] = children;
}

void NodeRegister::visitExpression(ExpressionNode* node)
{
	switch (node->expressionType())
	{
		case ExpressionType::TernaryExpression:
			return visitTernaryExpression(static_cast<TernaryExpressionNode*>(node));
		case ExpressionType::OrExpression:
		case ExpressionType::AndExpression:
		case ExpressionType::InExpression:
		case ExpressionType::IsExpression:
		case ExpressionType::ComparisonExpression:
		case ExpressionType::BitOrExpression:
		case ExpressionType::BitXorExpression:
		case ExpressionType::BitAndExpression:
		case ExpressionType::BitShiftExpression:
		case ExpressionType::AdditiveExpression:
		case ExpressionType::MultiplicativeExpression:
		case ExpressionType::PowerExpression:
			return visitBinaryExpression(static_cast<BinaryExpressionNode*>(node));
		case ExpressionType::UnaryExpression:
			return visitUnaryExpression(static_cast<UnaryExpressionNode*>(node));
		case ExpressionType::AtomPrimary:
			return visitAtomPrimary(static_cast<AtomPrimaryNode*>(node));
		case ExpressionType::ListPrimary:
			return visitListPrimary(static_cast<ListPrimaryNode*>(node));
		case ExpressionType::DictionaryPrimary:
			return visitDictionaryPrimary(static_cast<DictionaryPrimaryNode*>(node));
		case ExpressionType::CharacterPrimary:
			return visitCharacterPrimary(static_cast<CharacterPrimaryNode*>(node));
		case ExpressionType::DotExpression:
		case ExpressionType::DoubleColonExpression:
			return visitScopeNotationExpression(static_cast<ScopeNotationExpressionNode*>(node));
		case ExpressionType::CallExpression:
			return visitCallExpression(static_cast<CallExpressionNode*>(node));
		case ExpressionType::SelectorExpression:
			return visitSelectorExpression(static_cast<SelectorExpressionNode*>(node));
		case ExpressionType::GroupPrimary:
			return visitGroupPrimary(static_cast<GroupPrimaryNode*>(node));
		case ExpressionType::SequencePrimary:
			return visitSequencePrimary(static_cast<SequencePrimaryNode*>(node));
	}
}

void NodeRegister::visitTernaryExpression(TernaryExpressionNode* node)
{
	_childToParent[node->condition.get()] = node;
	_childToParent[node->trueExpression.get()] = node;
	_childToParent[node->falseExpression.get()] = node;
	
	visitNode(node->condition.get());
	visitNode(node->trueExpression.get());
	visitNode(node->falseExpression.get());
	
	_parentToChildren[node] = { node->condition.get(), node->trueExpression.get(), node->falseExpression.get()};
}

void NodeRegister::visitBinaryExpression(BinaryExpressionNode* node)
{
	_childToParent[node->left.get()] = node;
	_childToParent[node->right.get()] = node;
	
	visitNode(node->left.get());
	visitNode(node->right.get());
	
	_tokenToParent[node->op.get()] = node;
	
	_parentToChildren[node] = { node->left.get(), node->right.get() };
}

void NodeRegister::visitUnaryExpression(UnaryExpressionNode* node)
{
	_childToParent[node->expression.get()] = node;
	visitNode(node->expression.get());
	_parentToChildren[node] = { node->expression.get() };
	
	_tokenToParent[node->op.get()] = node;
}

void NodeRegister::visitScopeNotationExpression(ScopeNotationExpressionNode* node)
{
	_childToParent[node->left.get()] = node;
	_tokenToParent[node->right.get()] = node;
	
	visitNode(node->left.get());
	
	_parentToChildren[node] = { node->left.get() };
}

void NodeRegister::visitCallExpression(CallExpressionNode* node)
{
	std::vector<Node*> children;
	
	_childToParent[node->expression.get()] = node;
	children.push_back(node->expression.get());
	visitNode(node->expression.get());
	
	for (shared_ptr<ExpressionNode>& expr : node->arguments)
	{
		_childToParent[expr.get()] = node;
		children.push_back(expr.get());
		visitNode(expr.get());
	}
	
	_parentToChildren[node] = children;
}

void NodeRegister::visitSelectorExpression(SelectorExpressionNode* node)
{
	std::vector<Node*> children;
	
	_childToParent[node->expression.get()] = node;
	children.push_back(node->expression.get());
	visitNode(node->expression.get());
	
	if (node->startIndex != nullptr)
	{
		_childToParent[node->startIndex.get()] = node;
		children.push_back(node->startIndex.get());
		visitNode(node->startIndex.get());
	}
	
	if (node->endIndex != nullptr)
	{
		_childToParent[node->endIndex.get()] = node;
		children.push_back(node->endIndex.get());
		visitNode(node->endIndex.get());
	}
	
	if (node->step != nullptr)
	{
		_childToParent[node->step.get()] = node;
		children.push_back(node->step.get());
		visitNode(node->step.get());
	}
	
	_parentToChildren[node] = children;
}

void NodeRegister::visitAtomPrimary(AtomPrimaryNode* node)
{
	_tokenToParent[node->token.get()] = node;
}

void NodeRegister::visitListPrimary(ListPrimaryNode* node)
{
	std::vector<Node*> children;
	
	for (shared_ptr<ExpressionNode>& expr : node->expressions)
	{
		_childToParent[expr.get()] = node;
		children.push_back(expr.get());
		visitNode(expr.get());
	}
	
	_parentToChildren[node] = children;
}

void NodeRegister::visitDictionaryPrimary(DictionaryPrimaryNode* node)
{
	std::vector<Node*> children;
	
	for (shared_ptr<DictionaryPairNode>& pair : node->pairs)
	{
		_childToParent[pair.get()] = node;
		children.push_back(pair.get());
		visitNode(pair.get());
	}
	
	_parentToChildren[node] = children;
}

void NodeRegister::visitDictionaryPair(DictionaryPairNode* node)
{
	_childToParent[node->key.get()] = node;
	_childToParent[node->value.get()] = node;
	
	visitNode(node->key.get());
	visitNode(node->value.get());
	
	_parentToChildren[node] = { node->key.get(), node->value.get() };
}

void NodeRegister::visitCharacterPrimary(CharacterPrimaryNode* node)
{
	std::vector<Node*> children;
	
	for (shared_ptr<CharacterPairNode>& pair : node->pairs)
	{
		_childToParent[pair.get()] = node;
		children.push_back(pair.get());
		visitNode(pair.get());
	}
	
	_parentToChildren[node] = children;
}

void NodeRegister::visitCharacterPair(CharacterPairNode* node)
{
	_childToParent[node->value.get()] = node;
	visitNode(node->value.get());
	
	_parentToChildren[node] = { node->value.get() };
	_tokenToParent[node->name.get()] = node;
}

void NodeRegister::visitSequencePrimary(SequencePrimaryNode* node)
{
	std::vector<Node*> children;
	
	for (shared_ptr<SequenceItemNode>& item : node->items)
	{
		_childToParent[item.get()] = node;
		children.push_back(item.get());
		visitNode(item.get());
	}
	
	_parentToChildren[node] = children;
}

void NodeRegister::visitSequenceItem(SequenceItemNode* node)
{
	std::vector<Node*> children;
	
	_childToParent[node->expression.get()] = node;
	children.push_back(node->expression.get());
	visitNode(node->expression.get());
	
	if (node->statement != nullptr)
	{
		_childToParent[node->statement.get()] = node;
		children.push_back(node->statement.get());
		visitNode(node->statement.get());
	}
	
	_parentToChildren[node] = children;
}

void NodeRegister::visitGroupPrimary(GroupPrimaryNode* node)
{
	_childToParent[node->expression.get()] = node;
	_parentToChildren[node] = { node->expression.get() };
	visitNode(node->expression.get());
}

Node* NodeRegister::getParent(Node* child)
{
	auto it = _childToParent.find(child);
	return it != _childToParent.end() ? it->second : nullptr;
}

vector<Node*> NodeRegister::getChildren(Node* parent)
{
	auto it = _parentToChildren.find(parent);
	return it != _parentToChildren.end() ? it->second : vector<Node*>();
}

vector<Node*> NodeRegister::getChildrenWith(Node* parent, std::function<bool(Node*)> condition)
{
	vector<Node*> result;
	auto it = _parentToChildren.find(parent);
	if (it != _parentToChildren.end())
	{
		for (Node* n : it->second)
		{
			if (condition(n))
			{
				result.push_back(n);
			}
		}
	}
	
	return result;
}

vector<Node*> NodeRegister::getChildrenWithType(Node* parent, NodeType nodeType)
{
	return getChildrenWith(parent, [nodeType](Node* node) {
		return node->nodeType() == nodeType;
	});
}

vector<Node*> NodeRegister::getChildrenWithType(Node* parent, ChildType childType)
{
	return getChildrenWith(parent, [childType](Node* node) {
		return node->nodeType() == NodeType::Child
				&& static_cast<ChildNode*>(node)->childType() == childType;
	});
}

vector<Node*> NodeRegister::getChildrenWithType(Node* parent, StatementType statementType)
{
	return getChildrenWith(parent, [statementType](Node* node) {
		return node->nodeType() == NodeType::Statement
				&& static_cast<StatementNode*>(node)->statementType() == statementType;
	});
}

vector<Node*> NodeRegister::getChildrenWithType(Node* parent, ExpressionType expressionType)
{
	return getChildrenWith(parent, [expressionType](Node* node) {
		return node->nodeType() == NodeType::Statement
				&& static_cast<StatementNode*>(node)->statementType() == StatementType::ExpressionStatement
				&& static_cast<ExpressionNode*>(node)->expressionType() == expressionType;
	});
}

vector<Node*> NodeRegister::getDescendantsWith(Node* parent, std::function<bool(Node*)> condition)
{
	vector<Node*> result;
	
	auto it = _parentToChildren.find(parent);
	if (it != _parentToChildren.end())
	{
		for (Node* n : it->second)
		{
			if (condition(n))
			{
				result.push_back(n);
			}
			
			vector<Node*> ret = getDescendantsWith(n, condition);
			result.insert(result.end(), ret.begin(), ret.end());
		}
	}
	
	return result;
}

vector<Node*> NodeRegister::getDescendantsWithType(Node* parent, NodeType nodeType)
{
	return getDescendantsWith(parent, [nodeType](Node* node) {
		return node->nodeType() == nodeType;
	});
}

vector<Node*> NodeRegister::getDescendantsWithType(Node* parent, ChildType childType)
{
	return getDescendantsWith(parent, [childType](Node* node) {
		return node->nodeType() == NodeType::Child
				&& static_cast<ChildNode*>(node)->childType() == childType;
	});
}

vector<Node*> NodeRegister::getDescendantsWithType(Node* parent, StatementType statementType)
{
	return getDescendantsWith(parent, [statementType](Node* node) {
		return node->nodeType() == NodeType::Statement
				&& static_cast<StatementNode*>(node)->statementType() == statementType;
	});
}

vector<Node*> NodeRegister::getDescendantsWithType(Node* parent, ExpressionType expressionType)
{
	return getDescendantsWith(parent, [expressionType](Node* node) {
		return node->nodeType() == NodeType::Statement
				&& static_cast<StatementNode*>(node)->statementType() == StatementType::ExpressionStatement
				&& static_cast<ExpressionNode*>(node)->expressionType() == expressionType;
	});
}

vector<Node*> NodeRegister::getAncestorsWith(Node* parent, std::function<bool(Node*)> condition)
{
	vector<Node*> result;
	
	auto it = _childToParent.find(parent);
	if (it != _childToParent.end())
	{
		if (condition(it->second))
		{
			result.push_back(it->second);
		}
		
		vector<Node*> ret = getAncestorsWith(it->second, condition);
		result.insert(result.end(), ret.begin(), ret.end());
	}
	
	return result;
}

vector<Node*> NodeRegister::getAncestorsWithType(Node* parent, NodeType nodeType)
{
	return getAncestorsWith(parent, [nodeType](Node* node) {
		return node->nodeType() == nodeType;
	});
}

vector<Node*> NodeRegister::getAncestorsWithType(Node* parent, ChildType childType)
{
	return getAncestorsWith(parent, [childType](Node* node) {
		return node->nodeType() == NodeType::Child
				&& static_cast<ChildNode*>(node)->childType() == childType;
	});
}

vector<Node*> NodeRegister::getAncestorsWithType(Node* parent, StatementType statementType)
{
	return getAncestorsWith(parent, [statementType](Node* node) {
		return node->nodeType() == NodeType::Statement
				&& static_cast<StatementNode*>(node)->statementType() == statementType;
	});
}

vector<Node*> NodeRegister::getAncestorsWithType(Node* parent, ExpressionType expressionType)
{
	return getAncestorsWith(parent, [expressionType](Node* node) {
		return node->nodeType() == NodeType::Statement
				&& static_cast<StatementNode*>(node)->statementType() == StatementType::ExpressionStatement
				&& static_cast<ExpressionNode*>(node)->expressionType() == expressionType;
	});
}

vector<Token*> NodeRegister::getTokensWith(function<bool(Token*, Node*)> condition)
{
	vector<Token*> result;
	
	for (auto& pair : _tokenToParent)
	{
		if (condition(pair.first, pair.second))
		{
			result.push_back(pair.first);
		}
	}
	
	return result;
}

}
