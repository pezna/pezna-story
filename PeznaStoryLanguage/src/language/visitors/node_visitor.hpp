#ifndef PSL_NODE_VISITOR_H
#define PSL_NODE_VISITOR_H

#include "../nodes/nodes.hpp"

namespace PSL
{

template <class T>
class NodeVisitor
{
public:
	virtual ~NodeVisitor() {}
	
	virtual T visitNode(Node* node) = 0;
	virtual T visitStory(StoryNode* node) = 0;
	virtual T visitNamespace(NamespaceNode* node) = 0;
	virtual T visitSection(SectionNode* node) = 0;
	virtual T visitChild(ChildNode* node) = 0;
	virtual T visitParameter(ParameterNode* node) = 0;
	
	virtual T visitStatement(StatementNode* node) = 0;
	virtual T visitAssignmentStatement(AssignmentStatementNode* node) = 0;
	virtual T visitChoiceStatement(ChoiceStatementNode* node) = 0;
	virtual T visitContinueStatement(ContinueStatementNode* node) = 0;
	virtual T visitExitStatement(ExitStatementNode* node) = 0;
	virtual T visitForStatement(ForStatementNode* node) = 0;
	virtual T visitGotoStatement(GotoStatementNode* node) = 0;
	virtual T visitIfStatement(IfStatementNode* node) = 0;
	virtual T visitIfPart(IfPartNode* node) = 0;
	virtual T visitElseIfPart(ElseIfPartNode* node) = 0;
	virtual T visitElsePart(ElsePartNode* node) = 0;
	virtual T visitIncludeStatement(IncludeStatementNode* node) = 0;
	virtual T visitLabelStatement(LabelStatementNode* node) = 0;
	virtual T visitLineStatement(LineStatementNode* node) = 0;
	virtual T visitDoStatement(DoStatementNode* node) = 0;
	virtual T visitReturnStatement(ReturnStatementNode* node) = 0;
	virtual T visitEventStatement(EventStatementNode* node) = 0;
	virtual T visitWhileStatement(WhileStatementNode* node) = 0;
	
	virtual T visitExpression(ExpressionNode* node) = 0;
	virtual T visitTernaryExpression(TernaryExpressionNode* node) = 0;
	virtual T visitBinaryExpression(BinaryExpressionNode* node) = 0;
	
	// intentionally not required to implement
	virtual T visitOrExpression(BinaryExpressionNode* node) {}
	virtual T visitAndExpression(BinaryExpressionNode* node) {}
	virtual T visitInExpression(BinaryExpressionNode* node) {}
	virtual T visitIsExpression(BinaryExpressionNode* node) {}
	virtual T visitComparisonExpression(BinaryExpressionNode* node) {}
	virtual T visitBitOrExpression(BinaryExpressionNode* node) {}
	virtual T visitBitXorExpression(BinaryExpressionNode* node) {}
	virtual T visitBitAndExpression(BinaryExpressionNode* node) {}
	virtual T visitBitShiftExpression(BinaryExpressionNode* node) {}
	virtual T visitAdditiveExpression(BinaryExpressionNode* node) {}
	virtual T visitMultiplicativeExpression(BinaryExpressionNode* node) {}
	virtual T visitPowerExpression(BinaryExpressionNode* node) {}
	
	virtual T visitUnaryExpression(UnaryExpressionNode* node) = 0;
	virtual T visitScopeNotationExpression(ScopeNotationExpressionNode* node) = 0;
	virtual T visitCallExpression(CallExpressionNode* node) = 0;
	virtual T visitSelectorExpression(SelectorExpressionNode* node) = 0;
	virtual T visitAtomPrimary(AtomPrimaryNode* node) = 0;
	virtual T visitListPrimary(ListPrimaryNode* node) = 0;
	virtual T visitDictionaryPrimary(DictionaryPrimaryNode* node) = 0;
	virtual T visitDictionaryPair(DictionaryPairNode* node) = 0;
	virtual T visitCharacterPrimary(CharacterPrimaryNode* node) = 0;
	virtual T visitCharacterPair(CharacterPairNode* node) = 0;
	virtual T visitSequencePrimary(SequencePrimaryNode* node) = 0;
	virtual T visitSequenceItem(SequenceItemNode* item) = 0;
	virtual T visitGroupPrimary(GroupPrimaryNode* node) = 0;
};


}

#endif //PSL_NODE_VISITOR_H
