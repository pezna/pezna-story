#include <stdexcept>
#include <type_traits>
#include "node_printer.hpp"

namespace PSL
{

void NodePrinter::indent()
{
	++_indentLevel;
}

void NodePrinter::dedent()
{
	--_indentLevel;
	if (_indentLevel < 0)
	{
		throw std::runtime_error("Dedented too much");
	}
}

void NodePrinter::prePrint()
{
	if (_indentLevel > 0)
	{
		for (int i = 1; i < _indentLevel; ++i)
		{
			_os << _indentText;
		}
		_os << _branchText;
	}
}

void NodePrinter::visitNode(Node* node)
{
	prePrint();
	switch (node->nodeType())
	{
		case NodeType::Story:
		{
			StoryNode* story = static_cast<StoryNode*>(node);
			visitStory(story);
			indent();
			visitNode(story->mainNamespace.get());
			dedent();
		}
		break;
		case NodeType::Namespace:
		{
			NamespaceNode* ns = static_cast<NamespaceNode*>(node);
			visitNamespace(ns);
			indent();
			for (shared_ptr<StatementNode>& stmt : ns->statements)
			{
				visitNode(stmt.get());
			}
			
			for (shared_ptr<SectionNode>& s : ns->sections)
			{
				visitNode(s.get());
			}
			
			for (shared_ptr<NamespaceNode>& n : ns->subNamespaces)
			{
				visitNode(n.get());
			}
			dedent();
		}
		break;
		case NodeType::Section:
		{
			SectionNode* section = static_cast<SectionNode*>(node);
			visitSection(section);
			indent();
			for (shared_ptr<StatementNode>& s : section->statements)
			{
				visitNode(s.get());
			}
			dedent();
		}
		break;
		case NodeType::Statement:
			visitStatement(static_cast<StatementNode*>(node));
		break;
		case NodeType::Child:
			visitChild(static_cast<ChildNode*>(node));
		break;
	}
}

void NodePrinter::visitStory(StoryNode* node)
{
	_os << "(StoryNode)\n";
}

void NodePrinter::visitNamespace(NamespaceNode* node)
{
	_os << "(NamespaceNode: " << node->name->text << ")\n";
}

void NodePrinter::visitSection(SectionNode* node)
{
	_os << "(SectionNode: " << node->name->text << " [";
	for (int64_t i = 0; i < node->parameters.size(); ++i)
	{
		visitParameter(node->parameters[i].get());
		
		if (i < node->parameters.size()-1)
		{
			_os << ", ";
		}
	}
	
	_os << "]";
	if (node->returnType != nullptr)
	{
		_os << " returns " << Token::getTypeName(*node->returnType);
	}
	_os << ")\n";
}

void NodePrinter::visitChild(ChildNode* node)
{
	switch (node->childType())
	{
		case ChildType::CharacterPair:
			return visitCharacterPair(static_cast<CharacterPairNode*>(node));
		case ChildType::DictionaryPair:
			return visitDictionaryPair(static_cast<DictionaryPairNode*>(node));
		case ChildType::SequenceItem:
			return visitSequenceItem(static_cast<SequenceItemNode*>(node));
		case ChildType::IfPart:
			return visitIfPart(static_cast<IfPartNode*>(node));
		case ChildType::ElseIfPart:
			return visitElseIfPart(static_cast<ElseIfPartNode*>(node));
		case ChildType::ElsePart:
			return visitElsePart(static_cast<ElsePartNode*>(node));
		case ChildType::ParameterPart:
			return visitParameter(static_cast<ParameterNode*>(node));
	}
}

void NodePrinter::visitParameter(ParameterNode* node)
{
	if (node->final)
	{
		_os << "final ";
	}
	
	_os << Token::getTypeName(node->variableType) << " " << node->name->text;
}

void NodePrinter::visitStatement(StatementNode* node)
{
	switch (node->statementType())
	{
		case StatementType::AssignmentStatement:
			visitAssignmentStatement(static_cast<AssignmentStatementNode*>(node));
		break;
		case StatementType::ChoiceStatement:
			visitChoiceStatement(static_cast<ChoiceStatementNode*>(node));
		break;
		case StatementType::ContinueStatement:
			visitContinueStatement(static_cast<ContinueStatementNode*>(node));
		break;
		case StatementType::EventStatement:
			visitEventStatement(static_cast<EventStatementNode*>(node));
		break;
		case StatementType::ExitStatement:
			visitExitStatement(static_cast<ExitStatementNode*>(node));
		break;
		case StatementType::ExpressionStatement:
			visitExpression(static_cast<ExpressionNode*>(node));
			_os << "\n";
		break;
		case StatementType::ForStatement:
			visitForStatement(static_cast<ForStatementNode*>(node));
		break;
		case StatementType::GotoStatement:
			visitGotoStatement(static_cast<GotoStatementNode*>(node));
		break;
		case StatementType::IfStatement:
			visitIfStatement(static_cast<IfStatementNode*>(node));
		break;
		case StatementType::IncludeStatement:
			visitIncludeStatement(static_cast<IncludeStatementNode*>(node));
		break;
		case StatementType::LabelStatement:
			visitLabelStatement(static_cast<LabelStatementNode*>(node));
		break;
		case StatementType::LineStatement:
			visitLineStatement(static_cast<LineStatementNode*>(node));
		break;
		case StatementType::DoStatement:
			visitDoStatement(static_cast<DoStatementNode*>(node));
		break;
		case StatementType::ReturnStatement:
			visitReturnStatement(static_cast<ReturnStatementNode*>(node));
		break;
		case StatementType::WhileStatement:
			visitWhileStatement(static_cast<WhileStatementNode*>(node));
		break;
	}
}

void NodePrinter::visitAssignmentStatement(AssignmentStatementNode* node)
{
	_os << "(AssignmentStmt create=" << node->create << ", final=" << node->final << ", type=" << Token::getTypeName(node->variableType) << ": ";
	visitExpression(node->left.get());
	_os << ' ' << node->op->text << ' ';
	visitExpression(node->right.get());
	_os << ")\n";
}

void NodePrinter::visitChoiceStatement(ChoiceStatementNode* node)
{
	_os << "(ChoiceStmt: [";
	for (int64_t i = 0; i < node->expressions.size(); ++i)
	{
		visitExpression(node->expressions[i].get());
		if (i < node->expressions.size()-1)
		{
			_os << ", ";
		}
	}
	_os << "])\n";
	
	indent();
	for (shared_ptr<StatementNode>& s : node->statements)
	{
		visitNode(s.get());
	}
	dedent();
}

void NodePrinter::visitContinueStatement(ContinueStatementNode* node)
{
	_os << "(ContinueStmt: " << node->level->text << ")\n";
}

void NodePrinter::visitEventStatement(EventStatementNode* node)
{
	_os << "(EventStmt: ";
	visitExpression(node->expression.get());
	_os << ")\n";
}

void NodePrinter::visitExitStatement(ExitStatementNode* node)
{
	_os << "(ExitStmt: " << node->level->text << ")\n";
}

void NodePrinter::visitForStatement(ForStatementNode* node)
{
	_os << "(ForStmt: " << node->variable->text << " in ";
	visitExpression(node->expression.get());
	_os << ")\n";
	
	indent();
	for (shared_ptr<StatementNode>& s : node->statements)
	{
		visitNode(s.get());
	}
	dedent();
}

void NodePrinter::visitGotoStatement(GotoStatementNode* node)
{
	_os << "(GotoStmt: " << node->token->text << ")\n";
}

void NodePrinter::visitIfStatement(IfStatementNode* node)
{
	_os << "(IfStmt)\n";
	indent();
	
	visitNode(node->ifPart.get());
	
	for (shared_ptr<ElseIfPartNode>& elseIfStat : node->elseIfParts)
	{
		visitNode(elseIfStat.get());
	}
	
	if (node->elsePart != nullptr)
	{
		visitNode(node->elsePart.get());
	}
	
	dedent();
}

void NodePrinter::visitIfPart(IfPartNode* node)
{
	_os << "(IfStat: ";
	visitExpression(node->condition.get());
	_os << ")\n";
	indent();
	for (shared_ptr<StatementNode>& s : node->statements)
	{
		visitNode(s.get());
	}
	dedent();
}

void NodePrinter::visitElseIfPart(ElseIfPartNode* node)
{
	_os << "(ElseIfStat: ";
	visitExpression(node->condition.get());
	_os << ")\n";
	indent();
	for (shared_ptr<StatementNode>& s : node->statements)
	{
		visitNode(s.get());
	}
	dedent();
}

void NodePrinter::visitElsePart(ElsePartNode* node)
{
	_os << "(ElseStat)\n";
	indent();
	for (shared_ptr<StatementNode>& s : node->statements)
	{
		visitNode(s.get());
	}
	dedent();
}

void NodePrinter::visitIncludeStatement(IncludeStatementNode* node)
{
	_os << "(IncludeStmt: " << node->filename->text << ")\n";
}

void NodePrinter::visitLabelStatement(LabelStatementNode* node)
{
	_os << "(LabelStmt: " << node->label->text << ")\n";
}

void NodePrinter::visitLineStatement(LineStatementNode* node)
{
	_os << "(LineStmt)\n";
	indent();
	visitNode(node->character.get());
	visitNode(node->text.get());
	dedent();
}

void NodePrinter::visitDoStatement(DoStatementNode* node)
{
	_os << "(LoopStmt";
	if (node->endCondition != nullptr)
	{
		_os << "; end if:";
		visitExpression(node->endCondition.get());
		_os << ")\n";
	}
	else
	{
		_os << ")\n";
	}
	
	indent();
	for (shared_ptr<StatementNode>& s : node->statements)
	{
		visitNode(s.get());
	}
	dedent();
}

void NodePrinter::visitReturnStatement(ReturnStatementNode* node)
{
	_os << "(ReturnStmt: ";
	visitExpression(node->expression.get());
	_os << ")\n";
}

void NodePrinter::visitWhileStatement(WhileStatementNode* node)
{
	_os << "(WhileStmt: ";
	visitExpression(node->condition.get());
	_os << ")\n";
	indent();
	for (shared_ptr<StatementNode>& s : node->statements)
	{
		visitNode(s.get());
	}
	dedent();
}

void NodePrinter::visitExpression(ExpressionNode* node)
{
	switch (node->expressionType())
	{
		case ExpressionType::TernaryExpression:
			visitTernaryExpression(static_cast<TernaryExpressionNode*>(node));
		break;
		case ExpressionType::OrExpression:
		case ExpressionType::AndExpression:
		case ExpressionType::InExpression:
		case ExpressionType::IsExpression:
		case ExpressionType::ComparisonExpression:
		case ExpressionType::BitOrExpression:
		case ExpressionType::BitXorExpression:
		case ExpressionType::BitAndExpression:
		case ExpressionType::BitShiftExpression:
		case ExpressionType::AdditiveExpression:
		case ExpressionType::MultiplicativeExpression:
		case ExpressionType::PowerExpression:
			visitBinaryExpression(static_cast<BinaryExpressionNode*>(node));
		break;
		case ExpressionType::UnaryExpression:
			visitUnaryExpression(static_cast<UnaryExpressionNode*>(node));
		break;
		case ExpressionType::AtomPrimary:
			visitAtomPrimary(static_cast<AtomPrimaryNode*>(node));
		break;
		case ExpressionType::ListPrimary:
			visitListPrimary(static_cast<ListPrimaryNode*>(node));
		break;
		case ExpressionType::DictionaryPrimary:
			visitDictionaryPrimary(static_cast<DictionaryPrimaryNode*>(node));
		break;
		case ExpressionType::CharacterPrimary:
			visitCharacterPrimary(static_cast<CharacterPrimaryNode*>(node));
		break;
		case ExpressionType::DotExpression:
		case ExpressionType::DoubleColonExpression:
			visitScopeNotationExpression(static_cast<ScopeNotationExpressionNode*>(node));
		break;
		case ExpressionType::CallExpression:
			visitCallExpression(static_cast<CallExpressionNode*>(node));
		break;
		case ExpressionType::SelectorExpression:
			visitSelectorExpression(static_cast<SelectorExpressionNode*>(node));
		break;
		case ExpressionType::GroupPrimary:
			visitGroupPrimary(static_cast<GroupPrimaryNode*>(node));
		break;
		case ExpressionType::SequencePrimary:
			visitSequencePrimary(static_cast<SequencePrimaryNode*>(node));
		break;
	}
}

void NodePrinter::visitTernaryExpression(TernaryExpressionNode* node)
{
	_os << "(Ternary: ";
	visitExpression(node->condition.get());
	_os << "? ";
	visitExpression(node->trueExpression.get());
	_os << " : ";
	visitExpression(node->falseExpression.get());
	_os << ")";
}

void NodePrinter::visitBinaryExpression(BinaryExpressionNode* node)
{
	_os << "(Binary|" << ExpressionNode::getTypeName(node->type) << ": ";
	visitExpression(node->left.get());
	_os << ' ' << node->op->text << ' ';
	visitExpression(node->right.get());
	_os << ")";
}

void NodePrinter::visitUnaryExpression(UnaryExpressionNode* node)
{
	_os << "(Unary: " << node->op->text << ' ';
	visitExpression(node->expression.get());
	_os << ")";
}

void NodePrinter::visitScopeNotationExpression(ScopeNotationExpressionNode* node)
{
	_os << "(ScopeNotation: ";
	visitExpression(node->left.get());
	_os << node->op->text << node->right->text << ")";
}

void NodePrinter::visitCallExpression(CallExpressionNode* node)
{
	_os << "(Call: ";
	visitExpression(node->expression.get());
	_os << " [";
	for (int64_t i = 0; i < node->arguments.size(); ++i)
	{
		visitExpression(node->arguments[i].get());
		if (i < node->arguments.size()-1)
		{
			_os << ", ";
		}
	}
	_os << "])";
}

void NodePrinter::visitSelectorExpression(SelectorExpressionNode* node)
{
	_os << "(Selector: ";
	visitExpression(node->expression.get());
	_os << '[';
	if (node->slice)
	{
		if (node->startIndex != nullptr)
		{
			visitExpression(node->startIndex.get());
		}
		_os << ",";
		if (node->endIndex != nullptr)
		{
			visitExpression(node->endIndex.get());
		}
		_os << ",";
		if (node->step != nullptr)
		{
			visitExpression(node->step.get());
		}
	}
	else
	{
		visitExpression(node->startIndex.get());
	}
	_os << "])";
}

void NodePrinter::visitAtomPrimary(AtomPrimaryNode* node)
{
	_os << "(Atom|" << Token::getTypeName(node->token->type) << ":" << node->token->text << ")";
}

void NodePrinter::visitListPrimary(ListPrimaryNode* node)
{
	_os << "(List: [";
	for (int64_t i = 0; i < node->expressions.size(); ++i)
	{
		visitExpression(node->expressions[i].get());
		if (i < node->expressions.size()-1)
		{
			_os << ", ";
		}
	}
	_os << "])";
}

void NodePrinter::visitDictionaryPrimary(DictionaryPrimaryNode* node)
{
	_os << "(Dict: [";
	for (int64_t i = 0; i < node->pairs.size(); ++i)
	{
		visitDictionaryPair(node->pairs[i].get());
		if (i < node->pairs.size()-1)
		{
			_os << ", ";
		}
	}
	_os << "])";
}

void NodePrinter::visitDictionaryPair(DictionaryPairNode* node)
{
	_os << "(DictPair: ";
	visitExpression(node->key.get());
	_os << '=';
	visitExpression(node->value.get());
	_os << ")";
}

void NodePrinter::visitCharacterPrimary(CharacterPrimaryNode* node)
{
	_os << "(Character: [";
	for (int64_t i = 0; i < node->pairs.size(); ++i)
	{
		visitCharacterPair(node->pairs[i].get());
		if (i < node->pairs.size()-1)
		{
			_os << ", ";
		}
	}
	_os << "])";
}

void NodePrinter::visitCharacterPair(CharacterPairNode* node)
{
	_os << "(CharacterPair: " << node->name->text << '=';
	visitExpression(node->value.get());
	_os << ")";
}

void NodePrinter::visitSequencePrimary(SequencePrimaryNode* node)
{
	_os << "(Sequence: loop=" << node->loop << ", random=" << node->random << ", order=" << node->order << " [";
	for (int64_t i = 0; i < node->items.size(); ++i)
	{
		visitSequenceItem(node->items[i].get());
		if (i < node->items.size()-1)
		{
			_os << ", ";
		}
	}
	_os << "])";
}

void NodePrinter::visitSequenceItem(SequenceItemNode* node)
{
	_os << "(SeqItem: ";
	visitExpression(node->expression.get());
	if (node->statement != nullptr)
	{
		_os << ": ";
		visitStatement(node->statement.get());
	}
	_os << ")";
}

void NodePrinter::visitGroupPrimary(GroupPrimaryNode* node)
{
	_os << "(Group: ";
	visitExpression(node->expression.get());
	_os << ")";
}

}
