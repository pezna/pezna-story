#ifndef PSL_NODE_PRINTER_H
#define PSL_NODE_PRINTER_H

#include <string>
#include <iostream>

#include "node_visitor.hpp"
#include "../nodes/nodes.hpp"

namespace PSL
{
using std::string;

class NodePrinter : public NodeVisitor<void>
{
private:
	ostream& _os;
	int _indentLevel;
	string _indentText;
	string _branchText;
	
	void indent();
	void dedent();
	void prePrint();
	
	void visitStory(StoryNode* node) override;
	void visitNamespace(NamespaceNode* node) override;
	void visitSection(SectionNode* node) override;
	void visitChild(ChildNode* node) override;
	void visitParameter(ParameterNode* node) override;
	
	void visitStatement(StatementNode* node) override;
	void visitAssignmentStatement(AssignmentStatementNode* node) override;
	void visitChoiceStatement(ChoiceStatementNode* node) override;
	void visitContinueStatement(ContinueStatementNode* node) override;
	void visitExitStatement(ExitStatementNode* node) override;
	void visitForStatement(ForStatementNode* node) override;
	void visitGotoStatement(GotoStatementNode* node) override;
	void visitIfStatement(IfStatementNode* node) override;
	void visitIfPart(IfPartNode* node) override;
	void visitElseIfPart(ElseIfPartNode* node) override;
	void visitElsePart(ElsePartNode* node) override;
	void visitIncludeStatement(IncludeStatementNode* node) override;
	void visitLabelStatement(LabelStatementNode* node) override;
	void visitLineStatement(LineStatementNode* node) override;
	void visitDoStatement(DoStatementNode* node) override;
	void visitReturnStatement(ReturnStatementNode* node) override;
	void visitEventStatement(EventStatementNode* node) override;
	void visitWhileStatement(WhileStatementNode* node) override;
	
	void visitExpression(ExpressionNode* node) override;
	void visitTernaryExpression(TernaryExpressionNode* node) override;
	void visitBinaryExpression(BinaryExpressionNode* node) override;
	void visitUnaryExpression(UnaryExpressionNode* node) override;
	void visitScopeNotationExpression(ScopeNotationExpressionNode* node) override;
	void visitSelectorExpression(SelectorExpressionNode* node) override;
	void visitCallExpression(CallExpressionNode* node) override;
	void visitAtomPrimary(AtomPrimaryNode* node) override;
	void visitListPrimary(ListPrimaryNode* node) override;
	void visitDictionaryPrimary(DictionaryPrimaryNode* node) override;
	void visitDictionaryPair(DictionaryPairNode* node) override;
	void visitCharacterPrimary(CharacterPrimaryNode* node) override;
	void visitCharacterPair(CharacterPairNode* node) override;
	void visitSequencePrimary(SequencePrimaryNode* node) override;
	void visitSequenceItem(SequenceItemNode* item) override;
	void visitGroupPrimary(GroupPrimaryNode* node) override;
	
public:
	NodePrinter(ostream& os)
	:
		_os(os),
		_indentLevel(0),
		_indentText("    "),
		_branchText(" ---")
	{}
	
	~NodePrinter() {}
	
	void visitNode(Node* node) override;
};

}

#endif //PSL_NODE_PRINTER_H
