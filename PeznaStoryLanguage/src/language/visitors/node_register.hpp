#ifndef PSL_NODE_REGISTER_H
#define PSL_NODE_REGISTER_H

#include <functional>
#include <unordered_map>
#include <unordered_set>

#include "../token.hpp"
#include "node_visitor.hpp"

namespace PSL
{
using std::function;
using std::unordered_map;
using std::unordered_set;

class NodeRegister : private NodeVisitor<void>
{
private:
	void visitNode(Node* node) override;
	void visitStory(StoryNode* node) override;
	void visitNamespace(NamespaceNode* node) override;
	void visitSection(SectionNode* node) override;
	void visitStatement(StatementNode* node) override;
	void visitChild(ChildNode* node) override;
	void visitParameter(ParameterNode* node) override;
	
	void visitAssignmentStatement(AssignmentStatementNode* node) override;
	void visitChoiceStatement(ChoiceStatementNode* node) override;
	void visitContinueStatement(ContinueStatementNode* node) override;
	void visitExitStatement(ExitStatementNode* node) override;
	void visitForStatement(ForStatementNode* node) override;
	void visitGotoStatement(GotoStatementNode* node) override;
	void visitIfStatement(IfStatementNode* node) override;
	void visitIfPart(IfPartNode* node) override;
	void visitElseIfPart(ElseIfPartNode* node) override;
	void visitElsePart(ElsePartNode* node) override;
	void visitIncludeStatement(IncludeStatementNode* node) override;
	void visitLabelStatement(LabelStatementNode* node) override;
	void visitLineStatement(LineStatementNode* node) override;
	void visitDoStatement(DoStatementNode* node) override;
	void visitReturnStatement(ReturnStatementNode* node) override;
	void visitEventStatement(EventStatementNode* node) override;
	void visitWhileStatement(WhileStatementNode* node) override;
	
	void visitExpression(ExpressionNode* node) override;
	void visitTernaryExpression(TernaryExpressionNode* node) override;
	void visitBinaryExpression(BinaryExpressionNode* node) override;
	void visitUnaryExpression(UnaryExpressionNode* node) override;
	void visitScopeNotationExpression(ScopeNotationExpressionNode* node) override;
	void visitCallExpression(CallExpressionNode* node) override;
	void visitSelectorExpression(SelectorExpressionNode* node) override;
	void visitAtomPrimary(AtomPrimaryNode* node) override;
	void visitListPrimary(ListPrimaryNode* node) override;
	void visitDictionaryPrimary(DictionaryPrimaryNode* node) override;
	void visitDictionaryPair(DictionaryPairNode* node) override;
	void visitCharacterPrimary(CharacterPrimaryNode* node) override;
	void visitCharacterPair(CharacterPairNode* node) override;
	void visitSequencePrimary(SequencePrimaryNode* node) override;
	void visitSequenceItem(SequenceItemNode* node) override;
	void visitGroupPrimary(GroupPrimaryNode* node) override;
	
	unordered_map<Node*, Node*> _childToParent;
	unordered_map<Token*, Node*> _tokenToParent;
	unordered_map<Node*, vector<Node*>> _parentToChildren;
	
public:
	void addStory(shared_ptr<StoryNode> node);
	unordered_map<Token*, Node*>& tokensWithParent();
	
	Node* getParent(Node* child);
	vector<Node*> getChildren(Node* parent);
	
	vector<Node*> getChildrenWith(Node* parent, function<bool(Node*)> condition);
	vector<Node*> getChildrenWithType(Node* parent, NodeType nodeType);
	vector<Node*> getChildrenWithType(Node* parent, ChildType childType);
	vector<Node*> getChildrenWithType(Node* parent, StatementType statementType);
	vector<Node*> getChildrenWithType(Node* parent, ExpressionType expressionType);
	
	vector<Node*> getDescendantsWith(Node* parent, function<bool(Node*)> condition);
	vector<Node*> getDescendantsWithType(Node* parent, NodeType nodeType);
	vector<Node*> getDescendantsWithType(Node* parent, ChildType childType);
	vector<Node*> getDescendantsWithType(Node* parent, StatementType statementType);
	vector<Node*> getDescendantsWithType(Node* parent, ExpressionType expressionType);
	
	vector<Node*> getAncestorsWith(Node* child, function<bool(Node*)> condition);
	vector<Node*> getAncestorsWithType(Node* child, NodeType nodeType);
	vector<Node*> getAncestorsWithType(Node* child, ChildType childType);
	vector<Node*> getAncestorsWithType(Node* child, StatementType statementType);
	vector<Node*> getAncestorsWithType(Node* child, ExpressionType expressionType);
	
	vector<Token*> getTokensWith(function<bool(Token*, Node*)> condition);
};

}

#endif //PSL_NODE_REGISTER_H
