#ifndef PSL_NAMESPACE_NODE_H
#define PSL_NAMESPACE_NODE_H

#include "node.hpp"
#include "section_node.hpp"

namespace PSL
{

struct NamespaceNode : Node
{
	shared_ptr<Token> name;
	vector<shared_ptr<NamespaceNode>> subNamespaces;
	vector<shared_ptr<SectionNode>> sections;
	vector<shared_ptr<StatementNode>> statements;
	
	NodeType nodeType() const override {return NodeType::Namespace;}
};

}

#endif //PSL_NAMESPACE_NODE_H
