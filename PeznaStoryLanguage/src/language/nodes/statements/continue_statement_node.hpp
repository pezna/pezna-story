#ifndef PSL_CONTINUE_STMT_NODE_H
#define PSL_CONTINUE_STMT_NODE_H

#include "../expressions/expression_node.hpp"

namespace PSL
{

struct ContinueStatementNode: StatementNode
{
	shared_ptr<Token> level; // While, For, Do
	
	ContinueStatementNode()
	:
		level(nullptr)
	{}
	
	StatementType statementType() const override {return StatementType::ContinueStatement;}
};

}

#endif //PSL_CONTINUE_STMT_NODE_H
