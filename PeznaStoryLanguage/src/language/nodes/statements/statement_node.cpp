#include "../statements/statement_node.hpp"

#include <sstream>

namespace PSL
{

string StatementNode::getTypeName(StatementType type)
{
	switch (type)
	{
		case StatementType::AssignmentStatement: return "AssignmentStatement";
		case StatementType::ChoiceStatement: return "ChoiceStatement";
		case StatementType::ContinueStatement: return "ContinueStatement";
		case StatementType::EventStatement: return "EventStatement";
		case StatementType::ExitStatement: return "ExitStatement";
		case StatementType::ExpressionStatement: return "ExpressionStatement";
		case StatementType::ForStatement: return "ForStatement";
		case StatementType::GotoStatement: return "GotoStatement";
		case StatementType::IfStatement: return "IfStatement";
		case StatementType::IncludeStatement: return "IncludeStatement";
		case StatementType::LabelStatement: return "LabelStatement";
		case StatementType::LineStatement: return "LineStatement";
		case StatementType::DoStatement: return "DoStatement";
		case StatementType::ReturnStatement: return "ReturnStatement";
		case StatementType::WhileStatement: return "WhileStatement";
	}
	
	std::ostringstream oss;
	oss << "Requesting name for non-existent statement type: "
		<< static_cast<std::underlying_type<StatementType>::type>(type);
	throw std::runtime_error(oss.str());
}

}
