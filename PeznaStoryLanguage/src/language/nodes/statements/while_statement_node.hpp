#ifndef PSL_WHILE_STMT_NODE_H
#define PSL_WHILE_STMT_NODE_H

#include "../expressions/expression_node.hpp"

namespace PSL
{

struct WhileStatementNode: StatementNode
{
	shared_ptr<ExpressionNode> condition;
	vector<shared_ptr<StatementNode>> statements;
	
	StatementType statementType() const override {return StatementType::WhileStatement;}
};

}

#endif //PSL_WHILE_STMT_NODE_H
