#ifndef PSL_ASSIGNMENT_STMT_NODE_H
#define PSL_ASSIGNMENT_STMT_NODE_H

#include "../expressions/expression_node.hpp"

namespace PSL
{

struct AssignmentStatementNode: StatementNode
{
	bool create;
	bool final;
	shared_ptr<ExpressionNode> left;
	shared_ptr<Token> op;
	shared_ptr<ExpressionNode> right;
	TokenType variableType;
	
	AssignmentStatementNode()
	:
		create{false},
		final{false},
		left(nullptr),
		op(nullptr),
		right(nullptr),
		variableType(TokenType::Var)
	{}
	
	StatementType statementType() const override {return StatementType::AssignmentStatement;}
};

}

#endif //PSL_ASSIGNMENT_STMT_NODE_H
