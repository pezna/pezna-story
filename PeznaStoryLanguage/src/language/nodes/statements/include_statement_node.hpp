#ifndef PSL_INCLUDE_STMT_NODE_H
#define PSL_INCLUDE_STMT_NODE_H

#include "../expressions/expression_node.hpp"

namespace PSL
{

struct IncludeStatementNode: StatementNode
{
	// The filepath must be absolute and lexically normal
	string filepath;
	shared_ptr<Token> filename;
	
	StatementType statementType() const override {return StatementType::IncludeStatement;}
};

}

#endif //PSL_INCLUDE_STMT_NODE_H
