#ifndef PSL_EXIT_STMT_NODE_H
#define PSL_EXIT_STMT_NODE_H

#include "../expressions/expression_node.hpp"

namespace PSL
{

struct ExitStatementNode: StatementNode
{
	shared_ptr<Token> level; // Story | Section | Choice | While | For | Do | If
	
	ExitStatementNode()
	:
		level(nullptr)
	{}
	
	StatementType statementType() const override {return StatementType::ExitStatement;}
};

}

#endif //PSL_EXIT_STMT_NODE_H
