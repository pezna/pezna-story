#ifndef PSL_CHOICE_STMT_NODE_H
#define PSL_CHOICE_STMT_NODE_H

#include "../expressions/expression_node.hpp"

namespace PSL
{

struct ChoiceStatementNode: StatementNode
{
	vector<shared_ptr<ExpressionNode>> expressions;
	vector<shared_ptr<StatementNode>> statements;
	
	StatementType statementType() const override {return StatementType::ChoiceStatement;}
};

}

#endif //PSL_CHOICE_STMT_NODE_H
