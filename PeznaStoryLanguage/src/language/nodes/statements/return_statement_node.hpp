#ifndef PSL_RETURN_STMT_NODE_H
#define PSL_RETURN_STMT_NODE_H

#include "../expressions/expression_node.hpp"

namespace PSL
{

struct ReturnStatementNode: StatementNode
{
	shared_ptr<ExpressionNode> expression;
	
	ReturnStatementNode()
	:
		expression(nullptr)
	{}
	
	StatementType statementType() const override {return StatementType::ReturnStatement;}
};

}

#endif //PSL_RETURN_STMT_NODE_H
