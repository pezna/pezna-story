#ifndef PSL_FOR_STMT_NODE_H
#define PSL_FOR_STMT_NODE_H

#include "../expressions/expression_node.hpp"

namespace PSL
{

struct ForStatementNode: StatementNode
{
	shared_ptr<Token> variable; // Name
	shared_ptr<ExpressionNode> expression;
	vector<shared_ptr<StatementNode>> statements;
	
	StatementType statementType() const override {return StatementType::ForStatement;}
};

}

#endif //PSL_FOR_STMT_NODE_H
