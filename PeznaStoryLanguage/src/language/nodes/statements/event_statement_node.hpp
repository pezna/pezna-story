#ifndef PSL_EVENT_STMT_NODE_H
#define PSL_EVENT_STMT_NODE_H

#include "../expressions/expression_node.hpp"

namespace PSL
{

struct EventStatementNode : StatementNode
{
	shared_ptr<ExpressionNode> expression;
	
	EventStatementNode()
	:
		expression(nullptr)
	{}
	
	StatementType statementType() const override {return StatementType::EventStatement;}
};

}

#endif //PSL_EVENT_STMT_NODE_H
