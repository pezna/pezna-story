#ifndef PSL_STMT_NODE_H
#define PSL_STMT_NODE_H

#include "../node.hpp"

namespace PSL
{

enum class StatementType
{
	Invalid,
	AssignmentStatement,
	ChoiceStatement,
	ContinueStatement,
	EventStatement,
	ExitStatement,
	ExpressionStatement,
	ForStatement,
	GotoStatement,
	IfStatement,
	IncludeStatement,
	LabelStatement,
	LineStatement,
	DoStatement,
	ReturnStatement,
	WhileStatement
};

struct StatementNode: Node
{
	static string getTypeName(StatementType type);
	
	virtual ~StatementNode() {}
	virtual StatementType statementType() const {return StatementType::Invalid;}
	
	NodeType nodeType() const override {return NodeType::Statement;}
};

}

#endif //PSL_STMT_NODE_H
