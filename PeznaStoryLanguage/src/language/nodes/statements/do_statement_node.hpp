#ifndef PSL_LOOP_STMT_NODE_H
#define PSL_LOOP_STMT_NODE_H

#include "../expressions/expression_node.hpp"

namespace PSL
{

struct DoStatementNode: StatementNode
{
	shared_ptr<ExpressionNode> endCondition;
	vector<shared_ptr<StatementNode>> statements;
	
	StatementType statementType() const override {return StatementType::DoStatement;}
};

}

#endif //PSL_LOOP_STMT_NODE_H
