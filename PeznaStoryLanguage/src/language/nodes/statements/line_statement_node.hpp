#ifndef PSL_LINE_STMT_NODE_H
#define PSL_LINE_STMT_NODE_H

#include "../expressions/expression_node.hpp"
#include "choice_statement_node.hpp"

namespace PSL
{

struct LineStatementNode: StatementNode
{
	shared_ptr<ExpressionNode> character;
	shared_ptr<ExpressionNode> text;
	vector<shared_ptr<ChoiceStatementNode>> choices;
	
	StatementType statementType() const override {return StatementType::LineStatement;}
};

}

#endif //PSL_LINE_STMT_NODE_H
