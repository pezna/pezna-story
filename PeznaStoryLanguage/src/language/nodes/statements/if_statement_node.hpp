#ifndef PSL_IF_STMT_NODE_H
#define PSL_IF_STMT_NODE_H

#include "../child_node.hpp"
#include "../expressions/expression_node.hpp"

namespace PSL
{

struct IfPartNode: ChildNode
{
	shared_ptr<ExpressionNode> condition;
	vector<shared_ptr<StatementNode>> statements;
	
	ChildType childType() const override {return ChildType::IfPart;}
};

struct ElseIfPartNode: ChildNode
{
	shared_ptr<ExpressionNode> condition;
	vector<shared_ptr<StatementNode>> statements;
	
	ChildType childType() const override {return ChildType::ElseIfPart;}
};

struct ElsePartNode: ChildNode
{
	vector<shared_ptr<StatementNode>> statements;
	
	ChildType childType() const override {return ChildType::ElsePart;}
};

struct IfStatementNode: StatementNode
{
	shared_ptr<IfPartNode> ifPart;
	vector<shared_ptr<ElseIfPartNode>> elseIfParts;
	shared_ptr<ElsePartNode> elsePart;
	
	IfStatementNode()
	:
		elsePart(nullptr)
	{}
	
	StatementType statementType() const override {return StatementType::IfStatement;}
};

}

#endif //PSL_IF_STMT_NODE_H
