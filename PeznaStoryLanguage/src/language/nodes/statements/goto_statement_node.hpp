#ifndef PSL_GOTO_STMT_NODE_H
#define PSL_GOTO_STMT_NODE_H

#include "../expressions/expression_node.hpp"

namespace PSL
{

struct GotoStatementNode: StatementNode
{
	shared_ptr<Token> token;
	
	GotoStatementNode()
	:
		token(nullptr)
	{}
	
	StatementType statementType() const override {return StatementType::GotoStatement;}
};

}

#endif //PSL_GOTO_STMT_NODE_H
