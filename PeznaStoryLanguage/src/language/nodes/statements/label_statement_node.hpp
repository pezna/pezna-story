#ifndef PSL_LABEL_STMT_NODE_H
#define PSL_LABEL_STMT_NODE_H

#include "../expressions/expression_node.hpp"

namespace PSL
{

struct LabelStatementNode: StatementNode
{
	shared_ptr<Token> label;
	
	StatementType statementType() const override {return StatementType::LabelStatement;}
};

}

#endif //PSL_LABEL_STMT_NODE_H
