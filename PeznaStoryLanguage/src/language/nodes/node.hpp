#ifndef PSL_NODE_H
#define PSL_NODE_H

#include <string>
#include <vector>
#include <memory>

#include "../token.hpp"

namespace PSL
{
using std::string;
using std::vector;
using std::shared_ptr;
using std::unique_ptr;

enum class NodeType
{
	Invalid, Story, Namespace, Section, Statement, Child
};

struct Node
{
private:
	static int64_t _currentUniqueId;

public:
	static string getTypeName(NodeType type);
	static void resetUniqueId();
	
	Node()
	{
		startLine = 0;
		startColumn = 0;
		endLine = 0;
		endColumn = 0;
		uniqueId = _currentUniqueId++;
	}
	
	Node(const Node& other) = default;
	
	Node(Node&& other) = default;
	
	Node& operator=(const Node& other)
	{
		startLine = other.startLine;
		startColumn = other.startColumn;
		endLine = other.endLine;
		endColumn = other.endColumn;
		uniqueId = other.uniqueId;
		
		return *this;
	}
	
	virtual ~Node() {}
	
	int64_t startLine, startColumn, endLine, endColumn;
	int64_t uniqueId;
	
	virtual NodeType nodeType() const {return NodeType::Invalid;};
};

}

#endif //PSL_NODE_H
