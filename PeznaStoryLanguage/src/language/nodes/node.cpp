#include <sstream>
#include "node.hpp"

namespace PSL
{

int64_t Node::_currentUniqueId = 0;

void Node::resetUniqueId()
{
	_currentUniqueId = 0;
}

string Node::getTypeName(NodeType type)
{
	switch (type)
	{
		case NodeType::Story: return "Story";
		case NodeType::Namespace: return "Namespace";
		case NodeType::Section: return "Block";
		case NodeType::Statement: return "Statement";
		case NodeType::Child: return "Child";
	}
	
	std::ostringstream oss;
	oss << "Requesting name for non-existent node type: "
		<< static_cast<std::underlying_type<NodeType>::type>(type);
	throw std::runtime_error(oss.str());
}

}
