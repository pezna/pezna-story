#ifndef PSL_CHILD_NODE_H
#define PSL_CHILD_NODE_H

#include "node.hpp"

namespace PSL
{

enum class ChildType
{
	Invalid,
	CharacterPair,
	DictionaryPair,
	SequenceItem,
	IfPart,
	ElseIfPart,
	ElsePart,
	ParameterPart,
};

struct ChildNode: Node
{
	virtual ~ChildNode() {}
	static string getTypeName(ChildType type);
	
	virtual ChildType childType() const {return ChildType::Invalid;};
	
	NodeType nodeType() const override {return NodeType::Child;}
};

}

#endif //PSL_CHILD_NODE_H
