#ifndef PSL_SECTION_NODE_H
#define PSL_SECTION_NODE_H

#include "node.hpp"
#include "parameter_node.hpp"
#include "statements/statement_node.hpp"

namespace PSL
{

struct SectionNode : Node
{
	shared_ptr<Token> name;
	vector<shared_ptr<ParameterNode>> parameters;
	vector<shared_ptr<StatementNode>> statements;
	shared_ptr<TokenType> returnType;
	
	SectionNode()
	:
		name(nullptr),
		returnType(nullptr)
	{}
	
	NodeType nodeType() const override {return NodeType::Section;}
};

}

#endif // PSL_SECTION_NODE_H
