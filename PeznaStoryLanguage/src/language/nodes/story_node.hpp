#ifndef PSL_STORY_NODE_H
#define PSL_STORY_NODE_H

#include "node.hpp"
#include "namespace_node.hpp"

namespace PSL
{

struct StoryNode : Node
{
	// The filepath must be absolute and lexically normal
	string filepath;
	shared_ptr<NamespaceNode> mainNamespace;
	
	NodeType nodeType() const override {return NodeType::Story;}
};

}

#endif //PSL_STORY_NODE_H
