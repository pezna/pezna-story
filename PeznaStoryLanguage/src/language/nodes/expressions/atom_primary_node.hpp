#ifndef PSL_ATOM_PRIMARY_NODE_H
#define PSL_ATOM_PRIMARY_NODE_H

#include "../expressions/primary_expression_node.hpp"

namespace PSL
{

struct AtomPrimaryNode : PrimaryExpressionNode
{
	shared_ptr<Token> token;
	
	AtomPrimaryNode()
	:
		token{nullptr}
	{}
	
	ExpressionType expressionType() const override {return ExpressionType::AtomPrimary;}
};

}

#endif //PSL_ATOM_PRIMARY_NODE_H
