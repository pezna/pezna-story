#ifndef PSL_PRIMARY_EXPR_NODE_H
#define PSL_PRIMARY_EXPR_NODE_H

#include "../expressions/expression_node.hpp"

namespace PSL
{

struct PrimaryExpressionNode: ExpressionNode
{
	virtual ~PrimaryExpressionNode() {}
};

}

#endif //PSL_PRIMARY_EXPR_NODE_H
