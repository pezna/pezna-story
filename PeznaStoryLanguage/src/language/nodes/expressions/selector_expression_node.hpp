#ifndef PSL_SELECTOR_EXPR_NODE_H
#define PSL_SELECTOR_EXPR_NODE_H

#include "../expressions/expression_node.hpp"

namespace PSL
{

struct SelectorExpressionNode: ExpressionNode
{
	shared_ptr<ExpressionNode> expression;
	shared_ptr<ExpressionNode> startIndex;
	shared_ptr<ExpressionNode> endIndex;
	shared_ptr<ExpressionNode> step;
	bool slice;
	
	SelectorExpressionNode()
	:
		startIndex{nullptr},
		endIndex{nullptr},
		step{nullptr},
		slice{false}
	{}
	
	ExpressionType expressionType() const override {return ExpressionType::SelectorExpression;}
};

}

#endif //PSL_SELECTOR_EXPR_NODE_H
