#ifndef PSL_EXPR_NODE_H
#define PSL_EXPR_NODE_H

#include "../statements/statement_node.hpp"

namespace PSL
{

enum class ExpressionType
{
	Invalid,
	TernaryExpression,
	OrExpression,
	AndExpression,
	InExpression,
	IsExpression,
	ComparisonExpression,
	BitOrExpression,
	BitXorExpression,
	BitAndExpression,
	BitShiftExpression,
	AdditiveExpression,
	MultiplicativeExpression,
	UnaryExpression,
	PowerExpression,
	DotExpression,
	DoubleColonExpression,
	CallExpression,
	SelectorExpression,
	GroupPrimary,
	AtomPrimary,
	ListPrimary,
	DictionaryPrimary,
	CharacterPrimary,
	SequencePrimary,
};

struct ExpressionNode: StatementNode
{
	static string getTypeName(ExpressionType type);
	
	virtual ~ExpressionNode() {}
	virtual ExpressionType expressionType() const {return ExpressionType::Invalid;}
	
	StatementType statementType() const override {return StatementType::ExpressionStatement;}
};

}

#endif //PSL_EXPR_NODE_H
