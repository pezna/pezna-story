#ifndef PSL_UNARY_EXPR_NODE_H
#define PSL_UNARY_EXPR_NODE_H

#include "../expressions/expression_node.hpp"

namespace PSL
{

struct UnaryExpressionNode: ExpressionNode
{
	shared_ptr<Token> op; //Not, BitNot, Sub
	shared_ptr<ExpressionNode> expression;
	
	ExpressionType expressionType() const override {return ExpressionType::UnaryExpression;}
};

}

#endif //PSL_UNARY_EXPR_NODE_H
