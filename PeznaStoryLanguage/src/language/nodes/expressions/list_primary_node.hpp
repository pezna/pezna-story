#ifndef PSL_LIST_PRIMARY_NODE_H
#define PSL_LIST_PRIMARY_NODE_H

#include "../expressions/primary_expression_node.hpp"

namespace PSL
{

struct ListPrimaryNode : PrimaryExpressionNode
{
	vector<shared_ptr<ExpressionNode>> expressions;
	shared_ptr<ExpressionNode> defaultExpression;
	
	ListPrimaryNode()
	:
		defaultExpression{nullptr}
	{}
	
	ExpressionType expressionType() const override {return ExpressionType::ListPrimary;}
};

}

#endif //PSL_LIST_PRIMARY_NODE_H
