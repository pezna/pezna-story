#ifndef PSL_CALL_EXPR_NODE_H
#define PSL_CALL_EXPR_NODE_H

#include "../expressions/expression_node.hpp"

namespace PSL
{

struct CallExpressionNode: ExpressionNode
{
	shared_ptr<ExpressionNode> expression;
	vector<shared_ptr<ExpressionNode>> arguments;
	
	ExpressionType expressionType() const override {return ExpressionType::CallExpression;}
};

}

#endif //PSL_CALL_PRIMARY_NODE_H
