#include "../expressions/expression_node.hpp"

#include <sstream>

namespace PSL
{

string ExpressionNode::getTypeName(ExpressionType type)
{
	switch (type)
	{
		case ExpressionType::TernaryExpression: return "TernaryExpression";
		case ExpressionType::OrExpression: return "OrExpression";
		case ExpressionType::AndExpression: return "AndExpression";
		case ExpressionType::InExpression: return "InExpression";
		case ExpressionType::IsExpression: return "IsExpression";
		case ExpressionType::ComparisonExpression: return "ComparisonExpression";
		case ExpressionType::BitOrExpression: return "BitOrExpression";
		case ExpressionType::BitXorExpression: return "BitXorExpression";
		case ExpressionType::BitAndExpression: return "BitAndExpression";
		case ExpressionType::BitShiftExpression: return "BitShiftExpression";
		case ExpressionType::AdditiveExpression: return "AdditiveExpression";
		case ExpressionType::MultiplicativeExpression: return "MultiplicativeExpression";
		case ExpressionType::UnaryExpression: return "UnaryExpression";
		case ExpressionType::PowerExpression: return "PowerExpression";
		case ExpressionType::AtomPrimary: return "AtomPrimary";
		case ExpressionType::ListPrimary: return "ListPrimary";
		case ExpressionType::DictionaryPrimary: return "DictPrimary";
		case ExpressionType::CharacterPrimary: return "CharacterPrimary";
		case ExpressionType::SequencePrimary: return "SequencePrimary";
		case ExpressionType::DotExpression: return "DotPrimary";
		case ExpressionType::DoubleColonExpression: return "DoubleColonPrimary";
		case ExpressionType::CallExpression: return "CallPrimary";
		case ExpressionType::SelectorExpression: return "SelectorPrimary";
		case ExpressionType::GroupPrimary: return "GroupPrimary";
	}
	
	std::ostringstream oss;
	oss << "Requesting name for non-existent expr type: "
		<< static_cast<std::underlying_type<ExpressionType>::type>(type);
	throw std::runtime_error(oss.str());
}

}
