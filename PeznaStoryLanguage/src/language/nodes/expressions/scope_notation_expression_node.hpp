#ifndef PSL_SCOPE_NOTATION_EXPR_NODE_H
#define PSL_SCOPE_NOTATION_EXPR_NODE_H

#include "../expressions/expression_node.hpp"

namespace PSL
{

/**
 * Dot or DoubleColon
 */
struct ScopeNotationExpressionNode: ExpressionNode
{
	ExpressionType type;
	shared_ptr<ExpressionNode> left;
	shared_ptr<Token> right;
	shared_ptr<Token> op;
	
	ExpressionType expressionType() const override {return type;}
};

}

#endif //PSL_SCOPE_NOTATION_EXPR_NODE_H
