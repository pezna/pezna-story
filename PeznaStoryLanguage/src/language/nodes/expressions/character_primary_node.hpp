#ifndef PSL_CHARACTER_PRIMARY_NODE_H
#define PSL_CHARACTER_PRIMARY_NODE_H

#include "../child_node.hpp"
#include "../expressions/primary_expression_node.hpp"

namespace PSL
{

struct CharacterPairNode: ChildNode
{
	shared_ptr<Token> name;
	shared_ptr<ExpressionNode> value;
	
	ChildType childType() const override {return ChildType::CharacterPair;}
};

struct CharacterPrimaryNode: PrimaryExpressionNode
{
	vector<shared_ptr<CharacterPairNode>> pairs;
	
	ExpressionType expressionType() const override {return ExpressionType::CharacterPrimary;}
};

}

#endif //PSL_CHARACTER_PRIMARY_NODE_H
