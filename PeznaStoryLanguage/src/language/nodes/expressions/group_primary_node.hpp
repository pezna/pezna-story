#ifndef PSL_GROUP_EXPR_NODE_H
#define PSL_GROUP_EXPR_NODE_H

#include "../expressions/primary_expression_node.hpp"

namespace PSL
{

struct GroupPrimaryNode: PrimaryExpressionNode
{
	shared_ptr<ExpressionNode> expression;
	
	ExpressionType expressionType() const override {return ExpressionType::GroupPrimary;}
};

}

#endif //PSL_GROUP_EXPR_NODE_H
