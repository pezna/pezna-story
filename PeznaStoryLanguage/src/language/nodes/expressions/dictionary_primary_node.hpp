#ifndef PSL_DICT_PRIMARY_NODE_H
#define PSL_DICT_PRIMARY_NODE_H

#include "../child_node.hpp"
#include "../expressions/primary_expression_node.hpp"

namespace PSL
{

struct DictionaryPairNode : ChildNode
{
	shared_ptr<ExpressionNode> key;
	shared_ptr<ExpressionNode> value;
	
	ChildType childType() const override {return ChildType::DictionaryPair;}
};

struct DictionaryPrimaryNode : PrimaryExpressionNode
{
	vector<shared_ptr<DictionaryPairNode>> pairs;
	shared_ptr<ExpressionNode> defaultExpression;
	
	DictionaryPrimaryNode()
	:
		defaultExpression{nullptr}
	{}
	
	ExpressionType expressionType() const override {return ExpressionType::DictionaryPrimary;}
};

}

#endif //PSL_DICT_PRIMARY_NODE_H
