#ifndef PSL_BINARY_EXPR_NODE_H
#define PSL_BINARY_EXPR_NODE_H

#include "../expressions/expression_node.hpp"

namespace PSL
{

struct BinaryExpressionNode: ExpressionNode
{
	ExpressionType type;
	shared_ptr<ExpressionNode> left;
	shared_ptr<ExpressionNode> right;
	shared_ptr<Token> op;
	
	ExpressionType expressionType() const override {return type;}
};

}

#endif //PSL_BINARY_EXPR_NODE_H
