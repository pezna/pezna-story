#ifndef PSL_SEQUENCE_PRIMARY_NODE_H
#define PSL_SEQUENCE_PRIMARY_NODE_H

#include "../child_node.hpp"
#include "../expressions/expression_node.hpp"

namespace PSL
{

struct SequenceItemNode : ChildNode
{
	shared_ptr<ExpressionNode> expression;
	shared_ptr<StatementNode> statement;
	
	SequenceItemNode()
	:
		statement{nullptr}
	{}
	
	ChildType childType() const override {return ChildType::SequenceItem;}
};

struct SequencePrimaryNode : PrimaryExpressionNode
{
	bool loop;
	bool random;
	bool order;
	shared_ptr<Token> name;
	vector<shared_ptr<SequenceItemNode>> items;
	
	SequencePrimaryNode()
	:
		loop{false},
		random{false},
		order{false},
		name{nullptr}
	{}
	
	ExpressionType expressionType() const override {return ExpressionType::SequencePrimary;}
};

}

#endif //PSL_SEQUENCE_PRIMARY_NODE_H
