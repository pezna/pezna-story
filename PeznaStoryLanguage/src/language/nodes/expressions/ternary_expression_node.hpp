#ifndef PSL_TERNARY_EXPR_NODE_H
#define PSL_TERNARY_EXPR_NODE_H

#include "../expressions/expression_node.hpp"

namespace PSL
{

struct TernaryExpressionNode: ExpressionNode
{
	shared_ptr<ExpressionNode> condition;
	shared_ptr<ExpressionNode> trueExpression;
	shared_ptr<ExpressionNode> falseExpression;
	
	ExpressionType expressionType() const override {return ExpressionType::TernaryExpression;}
};

}

#endif //PSL_TERNARY_EXPR_NODE_H
