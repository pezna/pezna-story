#ifndef PSL_PARAMETER_NODE_H
#define PSL_PARAMETER_NODE_H

#include "child_node.hpp"

namespace PSL
{

struct ParameterNode : ChildNode
{
	bool final;
	TokenType variableType;
	shared_ptr<Token> name;
	
	ParameterNode()
	:
		final(false),
		variableType(TokenType::Var),
		name(nullptr)
	{}
	
	ChildType childType() const override {return ChildType::ParameterPart;}
};

}

#endif // PSL_PARAMETER_NODE_H
