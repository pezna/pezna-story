#include "child_node.hpp"

#include <sstream>

namespace PSL
{

string ChildNode::getTypeName(ChildType type)
{
	switch (type)
	{
		case ChildType::CharacterPair: return "CharacterPair";
		case ChildType::DictionaryPair: return "DictPair";
		case ChildType::SequenceItem: return "SequenceItem";
		case ChildType::IfPart: return "If";
		case ChildType::ElseIfPart: return "ElseIf";
		case ChildType::ElsePart: return "Else";
	}
	
	std::ostringstream oss;
	oss << "Requesting name for non-existent child node type: "
		<< static_cast<std::underlying_type<ChildType>::type>(type);
	throw std::runtime_error(oss.str());
}

}
