#ifndef PSL_EXCEPTIONS_H
#define PSL_EXCEPTIONS_H

#include <string>
#include <sstream>
#include <stdexcept>
#include <memory>

#include "localization.hpp"
#include "nodes/nodes.hpp"
#include "token.hpp"

namespace PSL
{
using std::string;
using std::shared_ptr;

struct LexerException: std::runtime_error
{
	/** File that the lexer was reading */
	const string filepath;
	/** Error message */
	const string message;
	/** Source line text */
	const string lineText;
	/** Line number (1-indexed) */
	const int64_t line;
	/** Column into the line that the errors occurs */
	const int64_t column;
	/** Total offset into the file that the error occurs */
	const int64_t offset;
	/** Last token inserted into the Lexer's tokens*/
	shared_ptr<Token> lastToken;
	/** Message to be returned by what() */
	string displayMessage;
	
	LexerException(const string& filepath, const string& message, const string& lineText, const int64_t line, int64_t column, const int64_t offset, shared_ptr<Token> lastToken)
	:
		runtime_error(message),
		filepath(filepath),
		message(message),
		lineText(lineText),
		line{line},
		column{column},
		offset{offset},
		lastToken{lastToken}
	{
		std::ostringstream oss;
		oss << "line " << line << ':' << column << " - " << message << '\n' << lineText << '\n';
		int lineIndex = 0;
		while (--column > 0)
		{
			if (std::isspace(lineText[lineIndex]))
			{
				oss << lineText[lineIndex++];
			}
			else
			{
				oss << ' ';
			}
		}
		oss << '^';
		displayMessage = oss.str();
	}
	
	const char* what() const noexcept override
	{
		return displayMessage.c_str();
	}
};

struct LexerReadException: std::istream::failure
{
	/** File that the lexer was reading */
	const string filepath;
	
	LexerReadException(const string& filepath, const string& message)
	:
		failure(message),
		filepath(filepath)
	{}
};

struct ParserException: std::runtime_error
{
	/** File that the lexer was reading */
	const string filepath;
	/** Error message */
	string message;
	/** Token that this error occured closest to */
	Token token;
	/** Message to be returned by what() */
	string displayMessage;
	
	ParserException(const string& filepath, const string& message, Token token)
	:
		runtime_error(message),
		filepath(filepath),
		message(message),
		token(token)
	{
		std::ostringstream oss;
		oss << token;
		displayMessage = Localization::parserExceptionDetail(message, oss.str());
	}
	
	const char* what() const noexcept override
	{
		return displayMessage.c_str();
	}
};

struct CompilerException : std::runtime_error
{
	/** File that the lexer was reading */
	const string filepath;
	
	CompilerException(const string& filepath)
	:
		runtime_error(""),
		filepath(filepath)
	{}
	
	virtual ~CompilerException() {}
};

struct SectionAlreadyExistsException : CompilerException
{
	string message;
	StoryNode newSectionStory;
	SectionNode newSection;
	StoryNode existingSectionStory;
	SectionNode existingSection;
	
	SectionAlreadyExistsException(const string& message, StoryNode newSectionStory, SectionNode newSection, StoryNode existingSectionStory, SectionNode existingSection)
	:
		CompilerException(existingSectionStory.filepath),
		message(message),
		newSectionStory(newSectionStory),
		newSection(newSection),
		existingSectionStory(existingSectionStory),
		existingSection(existingSection)
	{}
	
	const char* what() const noexcept override
	{
		return message.c_str();
	}
};

struct CompilationException : CompilerException
{
	string message;
	string displayMessage;
	Node node;
	Token token;
	bool isForNode;
	
	CompilationException(const string& filepath, const string& message, Node node)
	:
		CompilerException(filepath),
		message(message),
		node(node),
		isForNode{true}
	{
		long long line = node.startLine;
		long long column = node.startColumn;
		
		std::ostringstream oss;
		oss << "line " << line << ':' << column << " - " << message << '\n';
		displayMessage = oss.str();
	}
	
	CompilationException(const string& filepath, const string& message, Token token)
	:
		CompilerException(filepath),
		message(message),
		token(token),
		isForNode{false}
	{
		long long line = token.line;
		long long column = token.column;
		
		std::ostringstream oss;
		oss << "line " << line << ':' << column << " - " << message << '\n';
		displayMessage = oss.str();
	}
	
	const char* what() const noexcept override
	{
		return displayMessage.c_str();
	}
};

}

#endif // PSL_EXCEPTIONS_H
